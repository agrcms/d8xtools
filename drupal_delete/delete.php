<?php

include_once "deleters/DeleteNode.php";

global $argv;
global $export_root;
global $id_old_to_new;
global $id_new_to_old;
global $site_dir;
global $db;
global $dcr_ids;
global $ts_nids;

$site_dir = 'default';
//$site_dir = 'default';
global $id_old_to_new;
$id_old_to_new = array();
global $id_new_to_old;
$id_new_to_old = array();

global $too_old_array;
$too_old_array = array();

global $legacy_error_array;
$legacy_error_array = array();

global $success_array;
$success_array = array();
global $skipped_array;
$skipped_array = array();
global $already_deleted_array;
$already_deleted_array = array();
global $already_deleted_count;
$already_deleted_count = 0;

$dcr_ids = array();
$ts_nids = array();

$db = \Drupal::database();

$myargs = array_slice($argv, 3);

// Set type delete types based on the order we want. This is important in cases where a node refrerences another node (e.g. minister references ministry)
// sitemap, news, etc.
$delete_types = array('empl','news');

// Override when testing
// When testing and you just want to test one content type, use the option for the type and it will delete only this..

if (empty($myargs)) {
  echo "Syntax: drush scr delete.php dir-containing-exports\n";
  echo "OR      drush scr delete.php dir-containing-exports page\n";
  echo "When testing and you just want to test one content type, use the option for the type and it will delete only this..\n";
  exit;
}

$delete_only_option_type = false;
$export_root = array_shift($myargs);
$option = array_shift($myargs);
echo "\n";
echo "------------------------------------------------------------------------------------------------\n";
echo "DELETE SETUP:\n";
echo "\n";
echo "export_root: $export_root \n";

if (!empty($option)) {
  echo "delete option: $option \n";
  if (strlen($option) > 0) {
    $delete_only_option_type = true;
    $delete_types = array($option);
  }
}
$type = $export_root;
if ($type == 'sitemapinit') {
  $export_root = 'sitemapinit';// @TODO: change this to sitemap.
}
chdir($export_root);


$conInfo = \Drupal\Core\Database\Database::getConnectionInfo('default');
$dbName = $conInfo['default']['database'];

$queryString = sprintf("SELECT dcr_id" .
   "FROM INFORMATION_SCHEMA.COLUMNS " .
   "WHERE table_name = '%s' " .
   "AND table_schema = '%s' " .
   "AND column_name = '%s'", 'node', $dbName, 'dcr_id');

// node table dcr_id
try {
  //$queryString = "ALTER TABLE node ADD COLUMN dcr_id INT unsigned NULL";
  $queryString = "ALTER TABLE node ADD COLUMN dcr_id BIGINT NULL";
  if ($updateColumnResult = $db->query($queryString)) {
    echo "Added dcr_id column to node table\n";
    echo "This column is not quite unique for sitemap items, it is unique for internal agrisource teamsite entities.\n";
  }
} catch (Exception $e) {
  echo "\n";
  echo " dcr_id column was previously added\n";
  // After all delete procedures are done, we can remove this column as follows:
  // "alter table node drop column dcr_id;"
}

// menu_link_content table dcr_id
try {
  //$queryString = "ALTER TABLE menu_link_content ADD COLUMN dcr_id INT unsigned NULL";
  $queryString = "ALTER TABLE menu_link_content ADD COLUMN dcr_id BIGINT NULL";
  if ($updateColumnResult = $db->query($queryString)) {
    echo "Added dcr_id column to menu_link_content table\n";
    echo "This column is not quite unique for sitemap items, it is unique for internal agrisource teamsite entities.\n";
  }
} catch (Exception $e) {
  echo "\n";
  echo " dcr_id column was previously added to the menu_link_content table\n";
  // After all delete procedures are done, we can remove this column as follows:
  // "alter table menu_link_content drop column dcr_id;"
}

// node table ts_nid
try {
  //$queryString = "ALTER TABLE node ADD COLUMN ts_nid INT unsigned NULL";
  $queryString = "ALTER TABLE node ADD COLUMN ts_nid BIGINT NULL";
  if ($updateColumnResult = $db->query($queryString)) {
    echo "Added ts_nid column to node table\n";
    echo "This column is only unique for sitemap items, it is repeating for other teamsite entities.\n";
  }
} catch (Exception $e) {
  echo " ts_nid column was previously added (Teamsite node_id (we will use ts_nid instead))\n";
  // After all delete procedures are done, we can remove this column as follows:
  // "alter table node drop column ts_nid;"
}

// menu_link_content table ts_nid
try {
  //$queryString = "ALTER TABLE menu_link_content ADD COLUMN ts_nid INT unsigned NULL";
  $queryString = "ALTER TABLE menu_link_content ADD COLUMN ts_nid BIGINT NULL";
  if ($updateColumnResult = $db->query($queryString)) {
    echo "Added ts_nid column to menu_link_content table\n";
    echo "This column is only unique for sitemap items, it is repeating for other teamsite entities.\n";
  }
} catch (Exception $e) {
  echo " ts_nid column was previously added to the menu_link_content table (Teamsite node_id (we will use ts_nid instead))\n";
  // After all delete procedures are done, we can remove this column as follows:
  // "alter table menu_link_content drop column ts_nid;"
}

// node table ts_pnid
try {
  //$queryString = "ALTER TABLE node ADD COLUMN ts_pnid INT unsigned NULL";
  $queryString = "ALTER TABLE node ADD COLUMN ts_pnid BIGINT NULL";
  if ($updateColumnResult = $db->query($queryString)) {
    echo "Added ts_pnid column to node table\n";
  }
} catch (Exception $e) {
  echo "ts_pnid column was previously added (Inferred from the sitemap, parent node_id (we will use ts_pnid))\n";
  // echo "This column is used for deletes of the sitemap so that we know where the parent of this item is.\n";
  // After all delete procedures are done, we can remove this column as follows:
  // "alter table node drop column ts_pnid;"
  echo "\n";
}

// menu_link_content table ts_pnid
try {
  //$queryString = "ALTER TABLE menu_link_content ADD COLUMN ts_pnid INT unsigned NULL";
  $queryString = "ALTER TABLE menu_link_content ADD COLUMN ts_pnid BIGINT NULL";
  if ($updateColumnResult = $db->query($queryString)) {
    echo "Added ts_pnid column to menu_link_content table\n";
  }
} catch (Exception $e) {
  echo "ts_pnid column was previously added to the menu_link_content table (Inferred from the sitemap, parent node_id (we will use ts_pnid))\n";
  // echo "This column is used for deletes of the sitemap so that we know where the parent of this item is.\n";
  // After all delete procedures are done, we can remove this column as follows:
  // "alter table menu_link_content drop column ts_pnid;"
  echo "\n";
}

if ($type == 'sitemap' || $type == 'sitemapinit') {
  if ($result = $db->query("SELECT ts_nid FROM {node}")) {
    while ($row = $result->fetchObject()) {
      $ts_nids[] = $row->ts_nid;
    }
  }
  if ($result = $db->query("SELECT ts_nid FROM {menu_link_content}")) {
    while ($row = $result->fetchObject()) {
      $ts_nids[] = $row->ts_nid;
    }
  }
}
else {
  if ($result = $db->query("SELECT dcr_id FROM {node}")) {
    while ($row = $result->fetchObject()) {
      $dcr_ids[] = $row->dcr_id;
    }
  }
}
// Now delete items of each content type

foreach ($delete_types as $type) {
  if ($type == $option && $delete_only_option_type) {
    delete_ctype($type);
  }
  else if (!$delete_only_option_type) {
    // Do all types.
    delete_ctype($type);
  }
}

foreach ($delete_types as $type_) {
  global $id_old_to_new;
  global $id_new_to_old;
  global $already_deleted_count;

  $cnt_too_old_ = isset($too_old_array[$type_]) ? count($too_old_array[$type_]) : '0';
  $cnt_legacy_error = isset($legacy_error_array[$type_]) ? count($legacy_error_array[$type_]) : '0';
  $cnt_success_ = isset($success_array[$type_]) ? count($success_array[$type_]) : '0';
  $cnt_skipped_ = isset($skipped_array[$type_]) ? count($skipped_array[$type_]) : '0';
  $cnt_already_deleted_ = !isset($skipped_array[$type_]) ? count($already_deleted_array[$type_]) : '0';
  echo "\n***************************************** Begin Import Stats for $type_ *********************************************************\n";
  echo "$cnt_too_old_ old nodes of type $type_ were older than January 1st 2018 (or some other date see logic for that), thus skipped\n";
  echo "$cnt_legacy_error legacy server error(s) of type $type_ , thus skipped\n";
  echo "Import summary for type: $type_\n";
  echo "Imported $cnt_success_ nodes of type $type_\n";
  echo "Total already deleted: $cnt_already_deleted_ for this type $type_\n";
  echo "Total already deleted: $already_deleted_count for all types up to now\n";
  echo "Skipped $cnt_skipped_ items of type $type_\n";
  if (isset($skipped_array[$type_])) {
    foreach($skipped_array[$type_] as $file_skipped => $title) {
      echo "            skipped $file_skipped - $title \n";
    }
  }
  echo "Legacy server error(s): $cnt_legacy_error items of type $type_\n";
  if (isset($legacy_error_array[$type_])) {
    foreach($legacy_error_array[$type_] as $file_skipped => $title) {
      echo "Legacy server error $file_skipped - $title \n";
    }
  }
  echo "********************************************END import stats for $type_ **********************************************************\n";
}

function delete_ctype($type) {
  global $id_old_to_new;
  global $id_new_to_old;
  global $db;
  global $dcr_ids;
  global $ts_nids;

  global $too_old_array;
  global $legacy_error_array;
  global $success_array;
  global $skipped_array;
  global $already_deleted_array;
  global $already_deleted_count;

  echo "getcwd()= " . getcwd() . ";\n";
  if ($type == 'sitemapinit') {
    //chdir('sitemap');
    //echo "chdir('sitemap');\n";
    chdir($type);
    echo "chdir(" . $type . ");\n";
  }
  else {
    echo "chdir('" . $type . "');\n";
    chdir($type);
  }

  $jsonfiles = array();

  if ($dirh = opendir(".")) {
    while (($entry = readdir($dirh)) !== false) {
      if (!preg_match('/json$/', $entry)) continue;
      $jsonfiles[] = $entry;
      if (strtolower($type) == 'sitemap') {
        echo "json file $entry\n";
      }
      if (strtolower($type) == 'sitemapinit') {
        echo "json file $entry\n";
      }
    }
    closedir($dirh);
  }

  $cclass = ucfirst($type);
  include_once "deleters/$cclass.php";

  echo "\n";
  echo "Importing ".count($jsonfiles)." $type nodes...\n";
  echo "\n";
  $cnt = 0;
  $too_old_count = 0;
  $cnt_legacy_error = 0;
  $cnt_success = 0;
  $cnt_skipped = 0;
  $node_storage = \Drupal::service('entity_type.manager')->getStorage('node');
  ksort($jsonfiles, SORT_NUMERIC);
  foreach ($jsonfiles as $jfile) {
    $dcr_id = null;
    $ts_nid = null;
    if (preg_match('/(\d+)\.json$/', $jfile, $matches)) {
      $dcr_id = $matches[1];
      $dcr_id = ltrim($dcr_id, '0');
      if ($type == 'sitemapinit') {
        $ts_nid = $dcr_id;
      }
    }
    if ($dcr_id && in_array($dcr_id, $dcr_ids)) {
      echo "Delete $dcr_id\n";
    }
    else if (!in_array($ts_nid, $ts_nids)) {
      echo "Already deleted $dcr_id\n";
      $already_deleted_array[$type][$jfile] = TRUE;
      $already_deleted_count++;
      continue;
    }
    $cnt++;
    //echo "$cnt\n";
    if (($cnt % 100) == 0) {
      echo "$cnt\n";
    }
    $delete = new $cclass($node_storage);
    $result = $delete->delete($jfile);
    if ($result) {
      $cnt_success++;
      $success_array[$type][$jfile] = TRUE;
    }

  }
/*  echo "Import summary for type: $type\n";
  echo "Imported $cnt_success nodes of type $type\n";
  echo "Skipped $cnt_skipped nodes of type $type\n";
  echo "Total already deleted: $already_deleted_count for all types up to now\n";*/


  echo "chdir('..');\n";
  chdir('..');

}
