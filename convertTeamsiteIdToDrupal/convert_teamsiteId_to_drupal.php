<?php

global $argv;

$myargs = array_slice($argv, 3);
use Drush\Drush;
use Drupal\node\Entity\Node;
include "db_operations.php";
global $import_override;
$import_override = TRUE;


if (empty($myargs)) {
  echo "Syntax: drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL\n";
  echo "OR      drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php HARD_LAUNCH\n";
  echo "The FULL_MEAL_DEAL flag processes all available nodes.\n";
  echo "The HARD_LAUNCH flag currently hard coded for doing nodes newer than Oct-11-2020. Also skips blocks which we have already done by this point.\n";
  exit;
}

$import_phase = array_shift($myargs);
echo "Import phase chosen=$import_phase\n";
//sleep(2);
if ($import_phase != 'FULL_MEAL_DEAL' && $import_phase != 'HARD_LAUNCH') {
  echo "Syntax: drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL\n";
  echo "OR      drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php HARD_LAUNCH\n";
  echo "The FULL_MEAL_DEAL flag processes all available nodes.\n";
  echo "The HARD_LAUNCH flag currently hard coded for doing nodes newer than Oct-11-2020. Also skips blocks which we have already done by this point.\n";
  exit;
}

$nodeId;


$db = new DBOperations();
$countNode = 0;

$count = 0;

//process menu_link URLs in Footer
$storage = \Drupal::entityManager()->getStorage('menu_link_content');
$menu_links = $storage->loadByProperties(['menu_name' => "Footer"]);
processMenuLink($menu_links);

$menu_links_fr = $storage->loadByProperties(['menu_name' => "footer-fr"]);

processMenuLink($menu_links_fr);

//finished process footer


//$uuids = ['8c43040c-9e83-4f13-b774-f4771b2a5a8b', '49baecda-63c8-4e16-811b-be2ff38cea33'];

$uuids = ['2ae71642-ca4f-4e8a-9f43-588ae3e110f9',
          '30e4c971-edb5-48f2-9f12-86589e46b849',
          '41c877fc-4f51-422b-821c-1f23452edb93',
          '49baecda-63c8-4e16-811b-be2ff38cea33',
          '78aca85d-3afc-4257-902b-144ae0d874e1',
          '878c1db8-a3da-452d-8479-8a1c1f740d6e',
          '8c43040c-9e83-4f13-b774-f4771b2a5a8b',
          '9cc4d184-ead7-4997-9da1-fc10b12ab2af',
          'a499786a-3b20-4157-89a6-2d3a0a0edf06',
          'b1130f0a-f3ae-4dab-b765-4ffc5467d499',
          'b290c5dc-03ed-4f0b-bcf3-fa489258543c',
          'b382751d-0591-4714-8dc8-506d8d8aa343',
          'be4d3824-c613-4aef-8992-42ba10131caf'];

//$uuids = ['49baecda-63c8-4e16-811b-be2ff38cea33']; // This one doesn't seem to work.
foreach ($uuids as $uuid) {
  $block_content = \Drupal::service('entity.repository')->loadEntityByUuid('block_content', $uuid);
  if ($block_content) {
    $body = $block_content->get('body')->value;
    $body = updateInternalReference($body);
    // $body = html_entity_decode($body);
    Drush::output()->writeln($body);
    $block_content->body->value = $body;
    $block_content->setSyncing(TRUE);
    if ($import_phase == 'FULL_MEAL_DEAL') {
      $block_content->save();
    }
    if ($block_content->hasTranslation('fr')) {
      $block_content_fr = $block_content->getTranslation('fr');
      $body_fr = $block_content_fr->get('body')->value;
      $body_fr = updateInternalReference($body_fr);
      // $body_fr = html_entity_decode($body_fr);
      $block_content_fr->body->value = $body_fr;
      $block_content_fr->setSyncing(TRUE);
      if ($import_phase == 'FULL_MEAL_DEAL') {
        $block_content_fr->save();
      }
    }
  }
  $block_content = NULL;
  Drush::output()->writeln('Process block body field for uuid=' . $uuid);
}
// Done with blocks, now start NODES.


if ($import_phase == 'FULL_MEAL_DEAL') {
  $result = $db->getAllNodeIdFromNodeTable();
}
elseif ($import_phase == 'HARD_LAUNCH') {
  $result = $db->getHardlaunchNewsEmplFromTables();// HARD LAUNCH.
}

// Reporting.
global $referenceSuccessArray;
global $referenceFailureArray;
$referenceSuccessArray = [];
$referenceFailureArray = [];
global $dcrReferenceFailureArray;
$dcrReferenceFailureArray = [];

foreach ($result as $row) {
  global $nodeId;
  $nodeId = $row->nid;
  $referenceSuccessArray[$nodeId];
  $referenceFailureArray[$nodeId];
  //echo $nodeId . "\n";

  if(empty($nodeId))
    continue;
  //echo $nodeId . "\n";
  $node = Node::load($nodeId);

  $nodeValueEn = $node->body->value;
  $node->body->value = updateInternalReference($nodeValueEn);

  $changed = $node->get('field_modified')->getValue();
  $changed = reset($changed);
  $changed = current($changed);
  $changed = (isset($changed) && !empty($changed)) ? strtotime($changed) : time();
  $node->changed = $changed;
  $node->setSyncing(TRUE);
  $node->save();

  //update French content
  if ($node->hasTranslation("fr")) {
    $trnode = $node->getTranslation('fr');
    $nodeValueFr = $trnode->body->value;
    $trnode->body->value = updateInternalReference($nodeValueFr);

    $trnode->changed = $changed;
    $trnode->setSyncing(TRUE);
    $trnode->save();
  }
  $count++;
}

$result = $db->getDistinctNodeIdFromNodeBody();


foreach ($result as $row) {

    echo $row->entity_id . "\n" ;
    $nodeBodys = $db->getNodeBodyByEntityId($row->entity_id);
    foreach ($nodeBodys as $nodeBody) {
        $page = $nodeBody->body_value;
       // echo "\n count = " . $count++;
        $nodeBody->body_value = updateInternalReference($page);
        // echo $nodeBody->body_value;
        $db->updateNodeBody($nodeBody);


    }

    //

}

// Rapport des échouées.
foreach ($dcrReferenceFailureArray as $matchedId => $failure_details) {

   $surPages = [];
   $surDcrids = [];
   foreach ($failure_details as $faildetail) {
     $nid = $faildetail['nodeId'];
     $surPages[] = $nid;
     if (is_numeric($nid)) {
       $dbops = new DBOperations();
       $surDcrids[] = $dbops->getDcridByNid($nid);
     }
     else {
       $surDcrids[] = $nid;
     }
   }
   $surPages=array_unique($surPages);
   $surDcrids=array_unique($surDcrids);
   if (in_array($count, $surPages)) {
    $nombreEchoue = count($surPages)-1;
   }
   else {
    $nombreEchoue = count($surPages);
   }
   if (in_array($count, $surDcrids)) {
    $nombreEchoueDcr = count($surDcrids)-1;
   }
   else {
    $nombreEchoueDcr = count($surDcrids);
   }

   $surPages = implode(',', $surPages);
   $surDcrids = implode(',', $surDcrids);
   $surPages = str_replace((','.$count) , '' , $surPages);
   $surDcrids = str_replace((','.$count) , '' , $surDcrids);
   echo "$matchedId failed $nombreEchoue times on drupal nodeIds: $surPages \n";
   echo "$matchedId failed $nombreEchoueDcr times on teamsite dcrIds: $surDcrids \n";
}
echo "\n count = " . $count . "\n";

// $page = file_get_contents('/app/d8tools/convertTeamsiteIdToDrupal/testdata.txt');
// $page = updateInternalReference($page);
// echo $page . "\n";
//echo page;
function updateInternalReference($page){

    $matches = array();

    preg_match_all('<a href=\"(\/?e?f?r?a?n?g?\/?\?id=\s{0,1}[\d]{13})">', $page, $matches);
    $page = findAndReplace($matches, $page);

    preg_match_all('<a href=\"(\/{0,1}display-afficher.do\?id=\s{0,1}[\d]{13})">', $page, $matches);
    $page = findAndReplace($matches, $page);

    preg_match_all('<a href=\"(\S+\?id=\s{0,1}[\d]{13})">', $page, $matches);
    $page = findAndReplace($matches, $page);

//    preg_match_all('<a href=\"(\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);
    preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(\/?display-afficher.do|\/?|https?:\/\/.{0,6}?|)(agr.gc.ca\S+\?id=\s{0,1}[\d]{13}\S?.*?|dairyinfo.gc.ca\S+\?id=\s{0,1}[\d]{13}\S?.*?|infolait.gc.ca\S+\?id=\s{0,1}[\d]{13}\S?.*?|\S+\?id=\s{0,1}[\d]{13}\S?.*?)("? ?class=".*?"?|ref=".*?"|"? ?title=".*?"?|)">', $page, $matches);
    $page = findAndReplace($matches, $page);
//    preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(\/?display-afficher.do|\/?|https?:\/\/.{0,6}?|)(agr.gc.ca\S+\?id=\s{0,1}[\d]{13}|dairyinfo.gc.ca\S+\?id=\s{0,1}[\d]{13}|infolait.gc.ca\S+\?id=\s{0,1}[\d]{13}|\S+\?id=\s{0,1}[\d]{13})("? ?class=".*?"?|ref=".*?"|"? ?class=".*?"?|)">', $page, $matches);
//    preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(https?:\/\/.{0,6}?|)(agr.gc.ca\S+|dairyinfo.gc.ca\S+|infolait.gc.ca\S+|\S+)(\/eng\/.*?|\/fra.*?|)(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d)("? ?class=".*?"?|ref=".*?"|"? ?class=".*?"?|)">', $page, $matches);
//    $page = findAndReplace($matches, $page);


//    preg_match_all('<a href=\"(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);

//    preg_match_all('<a href=\"(https?:\/\/.*?)(agr.gc.ca|dairyinfo.gc.ca|infolait.gc.ca)(\S*?)(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);

//    preg_match_all('<a (class=\".*?\" |title=\".*?\" |)href=\"(https?:\/\/.*?)(agr.gc.ca|dairyinfo.gc.ca|infolait.gc.ca)(\S*?)(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);

  //preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(https?:\/\/\S{0,300}|w*\..*?[^ ]|)(\/eng\/.*?[^ ]|\/fra.*?[^ ]|)(\?id=\S+.\d?)">', $page, $matches);
//    preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(https?:\/\/\S{0,300}|w*\..*?[^ ]|)(\/eng\/.*?|\/fra.*?|)(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);

//    preg_match_all('/<a (class=".*?" |)href=\"?(http:\/\/\S{0,200}|w*\..*?|)(.*?"|\/eng\/.*?|\/fra.*?|)(\?id=\S+.\d?)">/i', $page, $matches);

    return $page;
}


function findAndReplace($matches, $page) {
    global $nodeId;
    $tempArray=$matches;
    foreach ($matches[0] as $cle => $val) {
        // echo "matched: " . $val[0] . "\n";
        // echo "part 1: " . $val[1] . "\n";
        echo $val . "\n" ;

        //skip if the link to AgriDoc
        if (stripos($val, "AgriDoc.do?") !== false) {
          echo "Agridoc ignor: " . $val . "\n" ;
          continue;
        }


        // $position = strpos($val, "?id=", 0);
        // //
        // $matchedId =  substr($val,$position+4, -1);
        // $position = strpos($matchedId, "&amp;lang", 0);
        // // echo $matchedId . "\n" ;
        // // echo $position . "\n" ;
        // $matchedAnchor = '';
        // $positionAnchor = strpos($matchedId, "#", 0);
        // if($positionAnchor>0)
        //   $matchedAnchor = substr($matchedId, $positionAnchor);
        // if($position>0)
        //   $matchedId = substr($matchedId, 0, $position);

        //if find match of pattern like "id=1580246566002", extract matchedId
        if(preg_match('/id=([0-9]+)/', $val, $matchedIDs)){

            // $matchedString = $matchedIDs[0][0];
            // $position = strpos($val, "?id=", 0);
          $matchedId = $matchedIDs[1];
          if (strlen($matchedId) !== 13) {
            echo "Unexpected matchedId = $matchedId \n";
            continue;
          }
          echo $matchedId . "\n" ;

          //get anchor value;
          $matchedAnchor = '';
          if(preg_match('/#(.*?)\"/', $val, $match)){
            $matchedAnchor = "#" . $match[1];
          }

          $node = findDrupalNodeIdByTeamsiteId($matchedId);
          echo 'node id=' . $node[0]->nid . "\n";

          $attributes = strtolower($tempArray[1][$cle]);
          $attributes = trim($attributes);
          if ($matchedId=='1584732749543') {
            echo 'ATTRIBUTES $attributes=';
            echo "\n";
            echo $attributes;
            echo "\n";
            echo "\n";
            echo "$val";
            echo "\n";
            echo print_r($tempArray, TRUE) . "\n";
            echo "sleep(1)\n";
            //sleep(1);
          }
          if (!empty($attributes) && !empty($node) && stripos($attributes, 'class') === 0) {
            // anchor has a class attribute.
            echo "val = " . $val ."\n";
            echo                      "a " . trim($attributes) . " data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\"" . "\n";
            $page = str_replace($val, "a " . trim($attributes) . " data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\"", $page);
            echo "\n";
            echo "\n";
            echo print_r(trim($attributes), TRUE);
            global $referenceSuccessArray;
            if (!isset($referenceSuccessArray[$nodeId])) {
              $referenceSuccessArray[$nodeId][] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            else {
              $successCount = count($referenceSuccessArray[$nodeId]);
              $successCount++;
              $referenceSuccessArray[$nodeId][$successCount-1] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            echo "\n";
          }
          if (!empty($node)) {
            echo "val = " . $val ."\n";
            echo                      "a data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\"" . "\n";
            $page = str_replace($val, "a data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\"", $page);
            global $referenceSuccessArray;
            if (!isset($referenceSuccessArray[$nodeId])) {
               $referenceSuccessArray[$nodeId][] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            else {
              $successCount = count($referenceSuccessArray[$nodeId]);
              $successCount++;
              $referenceSuccessArray[$nodeId][$successCount-1] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
          } else {
            //global $nodeId;
            echo " node_id = " . $nodeId;
            echo " dcr_id not existing in node table = " . $matchedId ."\n";
            global $referenceFailureArray;
            if (!isset($referenceFailureArray[$nodeId])) {
              $referenceFailureArray[$nodeId][0] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            else {
              $failureCount = count($referenceFailureArray[$nodeId]);
              $failureCount++;
              $referenceFailureArray[$nodeId][$failureCount-1] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            global $dcrReferenceFailureArray;
            $dcrFailCnt;
            $dcrReferenceFailureArray[$matchedId];
            if (!isset($dcrReferenceFailureArray[$matchedId])) {
              $dcrReferenceFailureArray[$matchedId][0] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'nodeId'    => $nodeId
              ];
            }
            else {
              $failureCount = count($dcrReferenceFailureArray[$matchedId]);
              $failureCount++;
              $dcrReferenceFailureArray[$matchedId][$failureCount-1] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'nodeId'    => $nodeId
              ];
            }
          }
        }
    }
    return $page;
}


/*
 * search for Drupal id by using Teamsite Id after content is imported into Drupal
 */
function findDrupalNodeIdByTeamsiteId($param) {
    $db = new DBOperations();
    $node = $db->getNodeBydcrId($param);
    //TODO
    return $node;
}

function processMenuLink($menu_links){
  if (!empty($menu_links)) {
    foreach ($menu_links as $menu_link) {
      // $link = [];
      // //$link['type'] = 'menu_link';
      // $link['mlid'] = $menu_link->id->value;
      // //$link['plid'] = $menu_link->parent->value ?? '0';
      // $link['menu_name'] = $menu_link->menu_name->value;
      // $link['link_title'] = $menu_link->title->value;
      $link = $menu_link->link->uri;
      //$link['options'] = $menu_link->link->options;
      //$link['weight'] = $menu_link->weight->value;
      // print_r($link);
      //https://www.agr.gc.ca/eng/news-from-agriculture-and-agri-food-canada/?id=1360882468517
      //<a href="/eng/news-from-agriculture-and-agri-food-canada/events/?id=1281614162882">Events</a> //fonctionne pas
      //<a href="?id=1603280592682">Minister Bibeau commemorates the 75<sup>th</sup> anniversary of   the founding of the Food and Agriculture Organization</a> //fonctionne bien
      //  https://regex101.com/r/5Fxpss/1/


      if(startsWith($link ,"https://www.agr.gc.ca") || startsWith($link ,"https://www.dairyinfo.gc.ca")) {
        $position = strpos($link, "?id=", 0);
        //
        $matchedId =  substr($link,$position+4);
        $position = strpos($matchedId, "&amp;lang", 0);
        // echo $matchedId . "\n" ;
        // echo $position . "\n" ;
        if($position>0)
          $matchedId = substr($matchedId, 0, $position);
        $res = preg_replace("/[^0-9]/", "", $matchedId );

        echo $link . "\n" ;
        echo $matchedId . "\n" ;
        echo $res . "\n";

        $node = findDrupalNodeIdByTeamsiteId($matchedId);
        print_r($node);
        if (!empty($node)) {
          $menu_link->link->uri = 'entity:node/' . $node[0]->nid;
          $menu_link->save();
        }

        //handle translation
        // if (!$menu_link->hasTranslation('fr') && !empty($titleFr)) {
        //   $tr_menu_link = $menu_link->getTranslation('fr');
        //   $trlink = $tr_menu_link->link->uri;
        //   if(startsWith($trlink ,"https://www.agr.gc.ca")) {
        //     $position = strpos($trlink, "?id=", 0);
        //     //
        //     $matchedId =  substr($trlink,$position+4);
        //     $position = strpos($matchedId, "&amp;lang", 0);
        //     // echo $matchedId . "\n" ;
        //     // echo $position . "\n" ;
        //     if($position>0)
        //       $matchedId = substr($matchedId, 0, $position);
        //     $res = preg_replace("/[^0-9]/", "", $matchedId );
        //
        //     echo $link . "\n" ;
        //     echo $matchedId . "\n" ;
        //     echo $res . "\n";
        //
        //     $node = findDrupalNodeIdByTeamsiteId($matchedId);
        //     print_r($node);
        //     if (!empty($node)) {
        //       $tr_menu_link->link->uri = 'entity:node/' . $node[0]->nid;
        //       $tr_menu_link->save();
        //     }
        //   }
        // }

      }
    }
  }
}


function startsWith ($string, $startString)
{
  $len = strlen($startString);
  return (substr($string, 0, $len) === $startString);
}

?>
