<?php

class DBOperations {
 private $connection;


// $connection = \Drupal::database();
// $opt = $connection->getConnectionOptions();
// $dbname = $opt['database'];

  public function __construct() {
    $this->$connection = \Drupal::database();
  }

  public function getHardlaunchNewsEmplFromTables() {
    // 1602288000 = oct 11 2020  // This function is specific to our hard launch
    $query = $this->$connection->query("select n.nid, n.type, fm.field_modified_value, fm.langcode from {node} as n inner join {node__field_modified} fm on n.nid = fm.entity_id where (n.type = 'empl' or n.type = 'news') and UNIX_TIMESTAMP(CAST(fm.field_modified_value AS DATE)) > 1602288000;");
    $result = $query->fetchAll();
    return $result;
  }


public function getAllNodeIdFromNodeTable()
{
//     $connection = \Drupal::database();
    $query = $this->$connection->query("SELECT nid FROM {node};");
    $result = $query->fetchAll();
    return $result;

}

public function getDistinctNodeIdFromNodeBody()
{
//     $connection = \Drupal::database();
    $query = $this->$connection->query("SELECT distinct(entity_id) FROM {node__body}");
    $result = $query->fetchAll();
    return $result;

}

public function getNodeBodyByEntityId($entityId)
{

    $query = $this->$connection->query(sprintf('SELECT * FROM {node__body} where  entity_id = %d', $entityId));
    $result = $query->fetchAll();
    return $result;

}

public function getDcridByNid($nid)
{

    $query = $this->$connection->query(sprintf('SELECT dcr_id FROM {node} where nid = %d', $nid));
    $result = $query->fetchAll();
    if (!empty($result))
      return $result[0]->dcr_id;

}

public function getNodeBydcrId($dcrId)
{

    $query = $this->$connection->query(sprintf('SELECT nid, uuid FROM {node} where dcr_id = %d', $dcrId));
    $result = $query->fetchAll();
    return $result;

}

public function getNodeIdBydcrId($dcrId)
{
    //echo sprintf('SELECT nid, uuid FROM {node} where dcr_id = %d', $dcrId) . "\n";
    $query = $this->$connection->query(sprintf('SELECT nid, uuid FROM {node} where dcr_id = %d', $dcrId));
    $result = $query->fetchAll();
    if (!empty($result))
      return $result[0]->nid;
    // return $result;

}

public function updateNodeBody($nodeBody)
{
  //$escaped_body_value = mysqli_real_escape_string( $this->$connection , $nodeBody->body_value );
  //echo $escaped_body_value;
/*  $fields = array(
  'body_value' => '%s'
);
$where = "entity_id = %d AND deleted = %d AND deleted = %d AND langcode='%s')";
db_update('node__body', $fields, $where, $nodeBody->body_value, $nodeBody->entity_id, $nodeBody->deleted,$nodeBody->delta,$nodeBody->langcode)
->execute();
*/
$num_updated = $this->$connection->update('node__body')
  ->fields([
    'body_value' => $nodeBody->body_value

  ])
  ->condition('entity_id', intval($nodeBody->entity_id), "=")
  ->condition('deleted', intval($nodeBody->deleted), "=")
  ->condition('delta', intval($nodeBody->delta) , "=")
  ->condition('langcode', $nodeBody->langcode)
  ->execute();
  // echo "num updated " . $num_updated . "\n";

    /*
   $query = db_update('node__body')
      ->expression('body_value', addslashes($nodeBody->body_value))
      ->condition('entity_id', intval($nodeBody->entity_id), "=")
      ->condition('deleted', intval($nodeBody->deleted), "=")
      ->condition('delta', intval($nodeBody->delta) , "=")
      ->condition('langcode', $nodeBody->langcode)
      ->execute();


      sqlsrv_query(
      $connection,
      'UPDATE some_table SET some_field = ? WHERE other_field = ?',
      array($_REQUEST['some_field'], $_REQUEST['id'])
  );
    //echo $escaped_body . "/n";
    $sql = sprintf("UPDATE {node__body} SET body_value='%s' WHERE entity_id=%d and deleted=%d and delta=%d and langcode='%s'",
      addslashes($nodeBody->body_value),
      $nodeBody->entity_id,
      $nodeBody->deleted,
      $nodeBody->delta,
      $nodeBody->langcode
    );
    echo $sql;

    $query = $this->$connection->query($sql);
*/
    //$result = $query->fetchAll();
    //return $result;

}
}
?>
