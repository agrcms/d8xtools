<?php
include ('vendor/simplehtmldom/simple_html_dom.php');


$myargs = array_slice($argv, 1);

$export_root = array_shift($myargs);
if (empty($export_root)) {
  echo "Please provide a path parameter to the cms content export folder in order to run this script: example /path/to/d8tools/export , in side this folder should be json files containging the dcr_id and body en and html body fr fields.\n";
  echo "\n";
  echo "example: php exportContentURL.php /path/to/d8tools/export";
  echo "\n";
  echo "it is also recommended to run this script from the d8tools/convertTeamsiteIdToDrupal folder so cd /path/to/d8tools/convertTeamsiteIdToDrupal then run the above example command.";
  echo "\n";
  echo "cd /path/to/d8tools/convertTeamsiteIdToDrupal;\n";
  echo "php exportContentURL.php /path/to/d8tools/export";
  echo "\n";
  echo "\n(assuming export contains folders containing json files with body en html and body fr html.";
  echo "\n";
  exit;
}
echo $export_root . "\n";
$count = 0;
$fileCount = 0;
$jsonfileCount = 0;
$assetExportProcessingFolder = 'export';

//Check if the asset export processing folder already exists.
if (!is_dir($assetExportProcessingFolder)) {
  echo getcwd() . 'export folder does not exist.' . "\n";
  exit;
}
$managing_branch = "PAB";
foreach (readFileSubDir($export_root) as $entry) {
//if ($dirh = opendir($export_root)) {
  //  while (($entry = readdir($dirh)) !== false) {
        if (! preg_match('/json$/', $entry))
            continue;
        // echo "$entry\n";
        $count ++;
        $jfile = $entry;
        // $jsonfiles[] = $entry;
        // foreach ($jsonfiles as $jfile) {
        // chdir($export_root);
        // echo $jfile . "\n";
        //echo $export_root . $jfile . "\n";
        //$data = json_decode(file_get_contents($export_root . $jfile));
        echo $jfile .  "\n";
        $data = json_decode(file_get_contents($jfile));
        $managing_branch = $data->managing_branch;
        // echo $data->body->en;
        $key = "content_" . $data->dcr_id;
        $obj = new class{};
        $obj->dcr_id = $data->dcr_id;
        // $obj->urlEn[] = array();
        // $obj->urlFr[] = array();
        if (! empty($data->body) && ! empty($data->body->en)) {
          $page = $data->body->en;
          $re = '/<!--(.*?)-->/m';
          $subst = '';
          $page_no_comments = preg_replace($re, $subst, $page);
          $page = $page_no_comments;
          $data->body->en = $page;

          $matches = array();
// preg_match_all('<a href=\"(display-afficher.do\?id=\S+.\d?)">', $page, $matches);
          // pregmatch ci-bas représente ce qu'on avait avant inclure dairyinfo
          // preg_match_all('/(\"http[s]{0,1}:\/\/?w*.agr.gc.ca)([^"]*?)(\.mp4|\.wmv|\.doc|\.ppt|\.pdf|\.pptx|\.docx|\.xls|\.xlsx|\.zip|\.json|\.xml|\.html)\"/i', $page, $matches);
          //preg_match_all('/(\"http[s]{0,1}:\/\/?w*.agr.gc.ca|\"http[s]{0,1}:\/\/?w*.dairyinfo.gc.ca)([^"]*?)(\.mp4|\.wmv|\.doc|\.ppt|\.pdf|\.pptx|\.docx|\.xls|\.xlsx|\.zip|\.json|\.xml|\.html)\"/i', $page, $matches);
          preg_match_all('/(\"http[s]{0,1}:\/\/?w*[4|5]{0,1}[.]{0,1}[.]{0,1}agr.gc.ca|\"http[s]{0,1}:\/\/?w*[5]{0,1}[.]{0,1}dairyinfo.gc.ca|\"http[s]{0,1}:\/\/?w*[5]{0,1}[.]{0,1}multimedia.agr.gc.ca|\"http[s]{0,1}:\/\/?w*[5]{0,1}[.]{0,1}infolait.gc.ca|\"http[s]{0,1}:\/\/?w*[5]{0,1}[.]{0,1}canadabrandgocaccess.agr.gc.ca|\"\/{1})([\S^"]*?)(\.mp3|\.mp4|\.wmv|\.docx|\.pptx|\.pdf|\.ppt|\.doc|\.xlsx|\.xls|\.zip|\.json|\.xml)\"/i', $page, $matches);
          // preg_match_all('/\"(.*?)(www.agr.gc.ca)(.*?)(\.json)(.*?)\"/i', $page, $matches);
          //preg_match_all('/\"(.*?)(www.agr.gc.ca)(.*?)(\.DOC)(.*?)\"/', $page, $matches);
          if (! empty($matches[0])) {
            foreach ($matches[0] as $val) {
              $val = substr($val, 1);
              $val = rtrim($val, "\"");
              $obj->urlEn[] = $val;
              // echo $val . "\n";
              $fileCount ++;
            }
          }

          //pattern /resources/prev/doc/ccb_dgcc/agrisource_policies_v8_EN.json
          preg_match_all('/\/resource(.*?)(\.json|\.html)/i', $page, $matches);
          if (! empty($matches[0])) {
            foreach ($matches[0] as $val) {
               // $val = substr($val, 1);
               $val = rtrim($val, "\"");
              $obj->urlEn[] = $val;
              // echo $val . "\n";
              $fileCount ++;
            }
          }

        }
        if (! empty($data->body) && ! empty($data->body->fr)) {
          $page = $data->body->fr;
          $re = '/<!--(.*?)-->/m';
          $subst = '';
          $page_no_comments = preg_replace($re, $subst, $page);
          $page = $page_no_comments;
          $data->body->fr = $page;

          $matches = array();
// preg_match_all('<a href=\"(display-afficher.do\?id=\S+.\d?)">', $page, $matches);
          // pregmatch ci-bas représente ce qu'on avait avant inclure dairyinfo
          //preg_match_all('/(\"http[s]{0,1}:\/\/www.agr.gc.ca)([^"]*?)(\.mp4|\.wmv|\.doc|\.ppt|\.pdf|\.pptx|\.docx|\.xls|\.xlsx|\.zip|\.json|\.xml)\"/i', $page, $matches);
          preg_match_all('/(\"http[s]{0,1}:\/\/?w*[4|5]{0,1}[.]{0,1}[.]{0,1}agr.gc.ca|\"http[s]{0,1}:\/\/?w*[5]{0,1}[.]{0,1}dairyinfo.gc.ca|\"http[s]{0,1}:\/\/?w*[5]{0,1}[.]{0,1}multimedia.agr.gc.ca|\"http[s]{0,1}:\/\/?w*[5]{0,1}[.]{0,1}infolait.gc.ca|\"http[s]{0,1}:\/\/?w*[5]{0,1}[.]{0,1}canadabrandgocaccess.agr.gc.ca|\"\/{1})([\S^"]*?)(\.mp3|\.mp4|\.wmv|\.docx|\.pptx|\.pdf|\.ppt|\.doc|\.xlsx|\.xls|\.zip|\.json|\.xml)\"/i', $page, $matches);
          // preg_match_all('/\"(.*?)(www.agr.gc.ca)(.*?)(\.json)(.*?)\"/i', $page, $matches);
          //preg_match_all('/\"(.*?)(www.agr.gc.ca)(.*?)(\.DOC)(.*?)\"/', $page, $matches);
          if (! empty($matches[0])) {
            foreach ($matches[0] as $val) {
              $val = substr($val, 1);
              $val = rtrim($val, "\"");
              $obj->urlFr[] = $val;
              // echo $val . "\n";
              $fileCount ++;
            }
          }
          //pattern /resources/prev/doc/ccb_dgcc/agrisource_policies_v8_EN.json
          preg_match_all('/\/resource(.*?)(\.json|\.html)/i', $page, $matches);
          if (! empty($matches[0])) {
            foreach ($matches[0] as $val) {
              // $val = substr($val, 1);
              $val = rtrim($val, "\"");
              $obj->urlFr[] = $val;
              // echo $val . "\n";
              $fileCount ++;
            }
          }
        }



        if (! empty($data->body) && ! empty($data->body->en)) {

            $html = str_get_html($data->body->en); // body
            if (! empty($html->find('a'))) {
              foreach ($html->find('a') as $element) {
                if (!empty($element->href)) {
                  $url_tmp = $element->href;
                  if (stripos($url_tmp, '.jpg') > 2 ||
                    stripos($url_tmp, '.jpeg') > 2 ||
                    stripos($url_tmp, '.tif') > 2 ||
                    stripos($url_tmp, '.png') > 2 ||
                    stripos($url_tmp, '.gif') > 2)
                  {
                    $obj->urlEn[] = $url_tmp;
                    $obj->urlEn = array_filter($obj->urlEn, 'strlen');
                    $obj->urlEn = array_unique($obj->urlEn); // Remove dupes.
                    $obj->urlEn = array_values($obj->urlEn); // Redoes the keys since removing dupes can make keys like 0,3,4 instead of 0,1,
                    $fileCount ++;
                  }
                }
              }
            }
            if (! empty($html->find('img'))) {
                foreach ($html->find('img') as $element) {
                    // echo $element->src . "\n";
                    if (! empty($element->src)) {
                      $obj->urlEn[] = $element->src;
                      $fileCount ++;
                    }
                    // if (! empty($element->href)) {
                    //   $obj->urlEn[] = $element->href;
                    //   $fileCount ++;
                    // }
                }
            }
            foreach($html->find('style') as $style) {
              $styles = $style->innertext;
              preg_match_all('/url\(\s*([\'"]?)(.*?\.png|.*?\.jpg|.*?\.tif|.*?\.gif|.*?\.jpeg)\1\s*\)/im', $styles, $matches);
              if (! empty($matches[0])) {
                foreach ($matches as $match) {
                  $val = $matches[2];
                  $val = rtrim($val, "\"");
                  $obj->urlEn[] = $val;
                  $fileCount ++;
                  echo "||||||||||||||||||||||||||||||||||||image En found||||||||||||||||||||||||||||||||||||||||||||||" . "\n";
                }
              }
            }
            if (! empty($html->find('video[poster]'))) {
              $videos = $html->find('video[poster]');
              // print_r($videos);
              foreach ($videos as $video) {
                echo $video . "\n";

                  if ($video->hasAttribute('poster') ) {
                    $url = $video->getAttribute('poster');
                    echo $url . "\n";
                    if(startsWith($url,"http://agr.gc.ca") ||
                       startsWith($url,"http://www.agr.gc.ca") ||
                       startsWith($url,"http://www5.agr.gc.ca") ||
                       startsWith($url,"http://www.infolait.gc.ca") ||
                       startsWith($url,"http://infolait.gc.ca") ||
                       startsWith($url,"http://www.dairyinfo.gc.ca") ||
                       startsWith($url,"http://dairyinfo.gc.ca") ||
                       startsWith($url,"http://multimedia.agr.gc.ca") ||
                       startsWith($url,"http://www.multimedia.agr.gc.ca") ||
                       startsWith($url,"http://www5.multimedia.agr.gc.ca") ||
                       startsWith($url,"http://www.canadabrandgocaccess.agr.gc.ca") ||
                       startsWith($url,"http://canadabrandgocaccess.agr.gc.ca") ||
                       startsWith($url,"https://agr.gc.ca") ||
                       startsWith($url,"https://www.agr.gc.ca") ||
                       startsWith($url,"https://www5.agr.gc.ca") ||
                       startsWith($url,"https://www.infolait.gc.ca") ||
                       startsWith($url,"https://www.dairyinfo.gc.ca") ||
                       startsWith($url,"https://infolait.gc.ca") ||
                       startsWith($url,"https://dairyinfo.gc.ca") ||
                       startsWith($url,"https://multimedia.agr.gc.ca") ||
                       startsWith($url,"https://www.multimedia.agr.gc.ca") ||
                       startsWith($url,"https://www5.multimedia.agr.gc.ca") ||
                       startsWith($url,"https://www.canadabrandgocaccess.agr.gc.ca") ||
                       startsWith($url,"https://canadabrandgocaccess.agr.gc.ca")) {
                      $obj->urlEn[] = $url;
                      $fileCount ++;
                    }
                  }
              }
            }
        }
        if (! empty($data->body) && ! empty($data->body->fr)) {

            $html = str_get_html($data->body->fr); // body
            if (! empty($html->find('a'))) {
              foreach ($html->find('a') as $element) {
                if (!empty($element->href)) {
                  $url_tmp = $element->href;
                  if (stripos($url_tmp, '.jpg') > 2 ||
                    stripos($url_tmp, '.jpeg') > 2 ||
                    stripos($url_tmp, '.tif') > 2 ||
                    stripos($url_tmp, '.png') > 2 ||
                    stripos($url_tmp, '.gif') > 2)
                  {
                    $obj->urlFr[] = $url_tmp;
                    $obj->urlFr = array_filter($obj->urlFr, 'strlen');
                    $obj->urlFr = array_unique($obj->urlFr); // Remove dupes.
                    $obj->urlFr = array_values($obj->urlFr); // Redoes the keys since removing dupes can make keys like 0,3,4 instead of 0,1,
                    $fileCount ++;
                  }
                }
              }
            }
            if (! empty($html->find('img'))) {
                foreach ($html->find('img') as $element) {
                  // echo $element->src . "\n";
                  if (! empty($element->src)) {
                    $obj->urlFr[] = $element->src;
                    $fileCount ++;
                  }
                }
            }
            foreach($html->find('style') as $style) {
              $styles = $style->innertext;
              preg_match_all('/url\(\s*([\'"]?)(.*?\.png|.*?\.jpg|.*?\.tif|.*?\.gif|.*?\.jpeg)\1\s*\)/im', $styles, $matches);
              if (! empty($matches[0])) {
                foreach ($matches as $match) {
                  $val = $matches[2];
                  $val = rtrim($val, "\"");
                  $obj->urlFr[] = $val;
                  $fileCount ++;
                  echo "||||||||||||||||||||||||||||||||||||image Fr found||||||||||||||||||||||||||||||||||||||||||||||" . "\n";
                }
              }
            }
            if (! empty($html->find('video[poster]'))) {
              $videos = $html->find('video[poster]');
              foreach ($videos as $video) {

                  if ($video->hasAttribute('poster') ) {
                    $url = $video->getAttribute('poster');
                    echo $url . "\n";
                    if(startsWith($url,"http://agr.gc.ca") ||
                      startsWith($url,"http://www.agr.gc.ca") ||
                      startsWith($url,"http://www5.agr.gc.ca") ||
                      startsWith($url,"http://www.infolait.gc.ca") ||
                      startsWith($url,"http://infolait.gc.ca") ||
                      startsWith($url,"http://www.dairyinfo.gc.ca") ||
                      startsWith($url,"http://dairyinfo.gc.ca") ||
                      startsWith($url,"http://multimedia.agr.gc.ca") ||
                      startsWith($url,"http://www.multimedia.agr.gc.ca") ||
                      startsWith($url,"http://www5.multimedia.agr.gc.ca") ||
                      startsWith($url,"http://www.canadabrandgocaccess.agr.gc.ca") ||
                      startsWith($url,"http://canadabrandgocaccess.agr.gc.ca") ||
                      startsWith($url,"https://agr.gc.ca") ||
                      startsWith($url,"https://www.agr.gc.ca") ||
                      startsWith($url,"https://www5.agr.gc.ca") ||
                      startsWith($url,"https://www.infolait.gc.ca") ||
                      startsWith($url,"https://www.dairyinfo.gc.ca") ||
                      startsWith($url,"https://infolait.gc.ca") ||
                      startsWith($url,"https://dairyinfo.gc.ca") ||
                      startsWith($url,"https://multimedia.agr.gc.ca") ||
                      startsWith($url,"https://www.multimedia.agr.gc.ca") ||
                      startsWith($url,"https://www5.multimedia.agr.gc.ca") ||
                      startsWith($url,"https://www.canadabrandgocaccess.agr.gc.ca") ||
                      startsWith($url,"https://canadabrandgocaccess.agr.gc.ca")) {
                     $obj->urlFr[] = $url;
                     $fileCount ++;
                    }
                  }
              }
            }
        }

        echo json_encode($obj) . "\n";
        if(! empty($obj->urlEn) && sizeof($obj->urlEn)>0)
          if ($fp = fopen("export/" . $key.'.json', 'w')) {
            $obj->managing_branch = $managing_branch;
            if (!empty($obj->urlEn)) {
              $obj->urlEn = array_filter($obj->urlEn, 'strlen');
              $obj->urlEn = array_unique($obj->urlEn); // Remove dupes.
              $obj->urlEn = array_values($obj->urlEn); // Redoes the keys since removing dupes can make keys like 0,3,4 instead of 0,1,
            }
            if (!empty($obj->urlFr)) {
              $obj->urlFr = array_filter($obj->urlFr, 'strlen');
              $obj->urlFr = array_unique($obj->urlFr); // Remove dupes.
              $obj->urlFr = array_values($obj->urlFr); // Redoes the keys since removing dupes can make keys like 0,3,4 instead of 0,1,2
            }
            fwrite($fp, json_encode($obj, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));
            fclose($fp);
            $jsonfileCount++;
          }

    // }
}

function startsWith ($string, $startString)
{
  $len = strlen($startString);
  return (substr($string, 0, $len) === $startString);
}

function readFileSubDir($scanDir) {

	$handle = opendir($scanDir);

	while (($fileItem = readdir($handle)) !== false) {
		// skip '.' and '..'
		if (($fileItem == '.') || ($fileItem == '..')) continue;
		$fileItem = rtrim($scanDir,'/') . '/' . $fileItem;

		// if dir found call again recursively
		if (is_dir($fileItem)) {
			foreach (readFileSubDir($fileItem) as $childFileItem) {
				yield $childFileItem;
			}

		} else {
			yield $fileItem;
		}
	}

	closedir($handle);
}

echo "Total Content links = " . $fileCount . "\n";
echo "Total content json file created  = " . $jsonfileCount . "\n";
echo "Total json file scaned = " . $count . "\n";

?>
