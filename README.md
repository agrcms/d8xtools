Several scripts for various levels/steps of export/import processing.


Initial export from teamsite starts with a table of values exported via sql to an excel spreadsheet

```
    select
        "METADATA_DCR_ID",
        "METADATA_DCR_LANG_CODE",
        "DC_TITLE_NAME",
/*       "DC_DESC",
        "AAFC_KEYWORD_NAME", */
        "DCTERMS_MODIFIED_DATE",
        "DCTERMS_ISSUED_DATE",
--        "DCR_NAME",
        "DCR_TYPE_NAME",
        "NODE_ID",
        "DCR_LAYOUT_NAME",
        "CREATED_DATE",
        "LAST_UPDATE_DATE" 
    from
        "AMAP"."AMAP_METADATA_DCR"
    where
                METADATA_DCR_LANG_CODE='eng' OR METADATA_DCR_LANG_CODE='fra'
```

From Here, export the excel to csv tab seperated with " identifiers the header row is adjusted removing most of the prefixes (as follows):

"dcr_id"        "lang"  "title" "modified"      "issued"        "type_name"     "node_id"       "layout_name" "created"        "updated"

Then another script is run to convert these csv rows into json files:

```
d8xtools/csv_to_json_export.php  path/to/d8xtools/Intranet_Database_dumpEN_FR_tab_adjusted.csv \t
php csv_to_json_export.php teamsite_data/Internet_General_Metadata-Take3.csv \t
```

the above script writes to the new_export folder

now that those are converted to json records, we run the updatejsons script on it as follows:


how to run the `update_json.php` script (must be inside the AAFC network)

```
cd /some/folder
git clone https://gitlab.com/agrcms/d8xtools.git
cd d8xtools
php update_json.php /some/folder/new_export teamsite
teamsite content json entities are written into json files in the new_export folder
```

Now for the sitemap

The source sitemap is in xml

I have created a script to convert it to json: 

```
cd internetx/xml2json
php teamsite_sitemap_xml2json.php > test99.json
```

IMPORTANT FIX corrige test99.json:

 a) vim search and replace wierd "0": "" pattern 

`:%s/,$\n\s*"0": ""//g`


and then from json to flatten it into json entities with this script:

`php internetx/process_sitemap.php`

sitemap json entities will be written into the `export` folder

`php update_json.php /some/folder/export sitemap`

sitemap json entities will be updated into the export folder with meta data and body content.


Troubleshooting: locate files with less than 800 bytes, have the update_json.php script scripts re-run the export (page reader)

```
mkdir run_json_update_again;
find new_export -type f -size -800c -exec echo cp\ {}\ run_json_update_again/. \;
```

#IMPORTING

```
cd docroot
drush scr ../d8xtools/drupal_export_import/import.php /var/www/clients/client3/web65/web/d8xtools/export_sitemap_02-10 sitemapinit
#make sure a folder or symlink called sitemap exists in /path/to/export that contains the json files, filename.json is dcrid.json ex: 123456789012.json
drush --root=/var/www/clients/client3/web65/web/docroot /path/to/export sitemap
#make sure a folder or symlink called page exists in /path/to/export that contains the json files, filename.json is dcrid.json ex: 123456789012.json
drush --root=/var/www/clients/client3/web65/web/docroot /path/to/export page
```

Drupal Bug:

When you run any import or update script, you receive the error:

```
**Drupal\Componet\Plugin\Exception\PlugninNOtFoundException: The "EntityHasField" plugin does not exist.
Valid plugin IDs for Drupal\Core\ValidationConstraintManger are: Callback...
In SqlContentEntiyStorage.php line 846:
The "EntityHasField" plugin does not exist. **
```

Workaround:

Go to `/en/admin/content`, add any fake node. After that, run the script agian.

 Image processing and link processing IMPORT STEPS:

```
cd /var/www/path/to/web/d8xtools/convertTeamsiteIdToDrupal/;
php exportContentURL.php /var/www/path/to/web/d8xtools/convertTeamsiteIdToDrupal/export;
php retrieveFileContentFromAgriSource.php export; mv exportcontent ../../docroot/html/sites/default/files/legacy
cd /var/www/path/to/web/docroot;

drush scr ../d8xtools/convertTeamsiteIdToDrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL;
drush scr ../d8xtools/convertTeamsiteIdToDrupal/updateContentURL.php /d8/d8xtools/convertTeamsiteIdToDrupal/export/
```
