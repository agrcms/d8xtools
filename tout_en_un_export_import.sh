#!/bin/bash
drupalsite=$1
d8xtools=`pwd`

catch() {
  echo "Error $1 occurred on $2";
  if [ $2 == 73 ] || [ $2 == 25 ]; then
    d8xtools=$3;
    docroot=$4;
    cd $docroot;
    echo "$2 Vas-y fait l'étape manuel. Error Workaround. Manually: Go to site, create a node, delete that node";
    echo 'chrono 60 sec';
    i=60;
    for (( i=60; i>0; i--)); do
      sleep 1 &
      printf "  $i \r"
      wait
    done
    cd $docroot;
    echo -n "Vas-y fait l'étape manuel. Error Workaround. Manually: Go to site, create a node, delete that node ensuite pese 1 [ENTER]: "
    read -n 1 un_et_pret
    echo "drush scr delete_all_news_empl.php";
          drush scr delete_all_news_empl.php;
      if [ "$un_et_pret" == "1" ]; then
      drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/internet_export_02-28 sitemapinit | tee ${docroot}/etape4_import_sitemapinit.log
      else
        echo "exit;"
        exit;
      fi
    else
      echo "erreur sur $2";
    sleep 2;
  fi
}
trap 'catch $? $LINENO $d8xtools $docroot' ERR

if [[ "$d8xtools" == *d8xtools ]]; then
  echo "d8xtools start export scripts\n";
elif [[ "$d8xtools" == *docroot ]]; then
  cd ../d8xtools;
  d8xtools=`pwd`
else
  echo "oops, not docroot , nor d8xtools";
  exit;
fi

cd ../docroot;
docroot=`pwd`;
cd $d8xtools;rm -rf ${d8xtools}/internet_export_02-28/;

php csv_to_json_export.php teamsite_data/Internet_General_Metadata-Take3.csv 2>&1 | tee ${docroot}/etape1_csv_to_json_export.log
echo "REVIEW ABOVE, NEXT STEP NOW";
sleep 5;
php update_json.php ${d8xtools}/internet_export_02-28/sitemapinit sitemap 2>&1 | tee ${docroot}/etape2_update_json_sitemap.log
echo "REVIEW ABOVE, NEXT STEP NOW";
sleep 5;
cd $d8xtools/internet_export_02-28/page;
#ln -s ../summ-somm/1226675563796.json 1226675563796.json
#ln -s ../summ-somm/1427987808646.json 1427987808646.json
cd $d8xtools;
php update_json.php ${d8xtools}/internet_export_02-28/page teamsite 2>&1 | tee ${docroot}/etape3_update_json_page.log
echo "REVIEW ABOVE, NEXT STEP NOW";
sleep 3;

#git checkout internet_export_02-28/sitemapinit/1354137773675.json
cd $docroot;
drush sql-drop -y;
drush sqlc < /tmp/aafc_clean_avant_import.sql;
drush pmu purge -y;drush pmu warmer -y;drush cr; drush cim -y; drush cr; drush cim -y;drush pmu content_moderation_notifications -y;
drush pmu safedelete;
drush scr delete_all_news_empl.php;
# | tee ${docroot}/etape4_import_sitemapinit.log
drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/internet_export_02-28 sitemapinit
echo "import sitemapinit done, continue";
sleep 120;
drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/internet_export_02-28 page 2>&1 | tee ${docroot}/etape5_import_page.log
echo "REVIEW ABOVE, NEXT STEP NOW";
sleep 300;

drush scr ../d8xtools/convertTeamsiteIdToDrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL page 2>&1 | tee ${docroot}/etape6_convert_teamsiteId_to_drupal_full_meal_deal.log
echo "REVIEW ABOVE, NEXT STEP NOW";
sleep 5;

cd ${d8xtools};

rm -rf convertTeamsiteIdToDrupal/export;
rm -rf convertTeamsiteIdToDrupal/exportcontent;

cp -pr internet_export_02-28/gene-gene ${d8xtools}/convertTeamsiteIdToDrupal/export;
cp -pr internet_export_02-28/sitemapinit ${d8xtools}/convertTeamsiteIdToDrupal/export;

cd ${d8xtools}/convertTeamsiteIdToDrupal;

php exportContentURL.php ${d8xtools}/convertTeamsiteIdToDrupal/export 2>&1 | tee ${docroot}/etape7_exportContentURL.log
rm -rf ${d8xtools}/convertTeamsiteIdToDrupal/export/1*.json;#plus besoin.
php retrieveFileContentFromAgrInternet.php export 2>&1 | tee ${docroot}/etape8_retrieveFileContentFromAgrInternet.log
#script ci-dessus va créer exportcontent avec les docx doc pdf png jpg etc dedans.
rm -rf ${docroot}/html/sites/default/files/legacy
mv ${d8xtools}/convertTeamsiteIdToDrupal/exportcontent ${docroot}/html/sites/default/files/legacy
cd ${docroot}/html/sites/default/files/legacy/pack/img
wget http://multimedia.agr.gc.ca/pack/img/aafc_homepage_banner_nov18.jpg
# Update internet_export_02-28/sitemapinit/1354137773675.json file
# background-image: url(http://multimedia.agr.gc.ca/pack/img/aafc_homepage_banner_nov18.jpg -> background-image: url(/sites/default/files/legacy/pack/img/aafc_homepage_banner_nov18.jpg

cd ${docroot};
drush scr ../d8xtools/convertTeamsiteIdToDrupal/updateContentURL.php ${d8xtools}/convertTeamsiteIdToDrupal/export/ 2>&1 | tee ${docroot}/etape9_updateContentURL.log
rm -rf ${d8xtools}/convertTeamsiteIdToDrupal/export/sites;

cd ${docroot};
drush scr ../d8xtools/drupal_export_import/importers/taxonomy/verifyAndAddMissingTermTranslations.php 2>&1 | tee ${docroot}/etape10_verifyAndAddMissingTermTranslations.log
echo "--------------------\n"
echo "\n"
drush scr ../d8xtools/drupal_export_import/importers/taxonomy/verifyAndAddMissingTermTranslations.php 2>&1 | tee ${docroot}/etape11_verifyAndAddMissingTermTranslations.log

echo "--------------------\n"
echo ""
echo "pushd ${docroot}/html/sites/default/files/legacy;";
      pushd ${docroot}/html/sites/default/files/legacy;
echo "curl https://wet-boew.github.io/themes-dist/GCWeb/img/520x200.png > special_title520x200.png";
      curl https://wet-boew.github.io/themes-dist/GCWeb/img/520x200.png > special_title520x200.png
echo "popd;"
      popd;
echo "\n"
echo "drush scr ../d8xtools/convertTeamsiteIdToDrupal/special_title_import_file.php 2>&1 | tee ${docroot}/etape12_special_title.log";
      drush scr ../d8xtools/convertTeamsiteIdToDrupal/special_title_import_file.php 2>&1 | tee ${docroot}/etape12_special_title.log
cd ${d8xtools}/convertTeamsiteIdToDrupal;
rm -rf export/sitemapinit;

