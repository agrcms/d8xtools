{
    "dcr_id": "1198259929463",
    "title": {
        "en": "Willow Redgall Sawfly",
        "fr": "Tenthr\u00e8de responsable de la galle rouge du saule"
    },
    "modified": "2020-01-24",
    "issued": "2009-05-04",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1354137773743",
    "dc_date_created": "2009-05-04",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/12/pfra_org_redgall_1198259929463_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/12/pfra_org_redgall_1198259929463_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Willow Redgall Sawfly",
        "fr": "Tenthr\u00e8de responsable de la galle rouge du saule"
    },
    "breadcrumb": {
        "en": "Willow Redgall Sawfly",
        "fr": "Tenthr\u00e8de responsable de la galle rouge du saule"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2009-05-04",
            "fr": "2009-05-04"
        },
        "modified": {
            "en": "2020-01-24",
            "fr": "2020-01-24"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Willow Redgall Sawfly",
            "fr": "Tenthr\u00e8de responsable de la galle rouge du saule"
        },
        "subject": {
            "en": "insecticides;insects;forestry;agroforestry;bugs",
            "fr": "insecticides;insectes;foresterie;agroforesterie"
        },
        "dcterms:subject": {
            "en": "pests;biology;entomology",
            "fr": "pesticide;organisme nuisible;entomologie"
        },
        "description": {
            "en": "Control: Chemical control of the willow redgall sawfly is rarely required due to parasites and predators that attack the sawfly and as the insect causes minimal permanent damage to the host.",
            "fr": null
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Photo credit;yellow sticky traps;color yellow;severe outbreak;minimal permanent damage",
            "fr": null
        },
        "audience": {
            "en": "gcaudience",
            "fr": "entreprises"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Willow Redgall Sawfly",
        "fr": "Tenthr\u00e8de responsable de la galle rouge du saule"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <p><i>Pontania proxima</i></p>\n \n<h2>Host</h2>\n<p>Willow</p>\n \n <h2>Appearance and Life Cycle</h2>\n<div class=\"row\">\n <div class=\"col-md-4 pull-right\">\n <div class=\"mrgn-bttm-md\">\n<img alt=\"Description of this image follows\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/pfra_redgall1.jpg\"> \n<figcaption><i>Galls formed on the leaf of a willow tree.<br>\nPhoto credit: Petr Kapitola, State Phytosanitary Administration, Bugwood.org</i></figcaption>\n \n</div>\n </div>\n\t<div class=\"mrgn-lft-md mrgn-rght-md\"> \n<p>The willow redgall sawfly has two generations a year, with the first generation adults emerging in late May or early June. The adult sawfly measures approximately 4 <abbr title=\"millimetres\">mm</abbr> in length and has a black body with yellow legs. Each adult can produce 2 to 35 eggs. The eggs are deposited singly or in a row on either side of the mid-rib of the developing leaves. At each location an egg is deposited a gall begins to form. By the time the larvae hatch the gall is fully developed. The larvae feed and develop within the galls for approximately five weeks. The full grown larvae are a yellowish-green color with a shiny, black head and measure approximately 7 <abbr title=\"millimetres\">mm</abbr> in length. When full grown, the larvae drop to the ground and form cocoons in the topsoil. The larvae pupate and emerge as adults approximately two weeks later in late July or early August. By mid-September the second generation larvae are full grown and drop to the ground where they overwinter in cocoons.</p>\n</div></div>\n  \n \n<h2>Damage</h2>\n\n<div class=\"row\">\n <div class=\"col-md-4 pull-right\">\n <div class=\"mrgn-bttm-md\">\n<img alt=\"Description of this image follows\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/pfra_redgall2.jpg\">\n<figcaption><i>Galls formed on a willow leaf.<br>\nPhoto credit: J.K. Lindsey, commanster.eu</i></figcaption>\n \n</div>\n </div>\n\t<div class=\"mrgn-lft-md mrgn-rght-md\"> \n<p>The adult sawfly forms the galls on the leaves by injecting a fluid into the leaf along with the egg. The galls measure approximately 8 <abbr title=\"millimetres\">mm</abbr> long, 6 <abbr title=\"millimetres\">mm</abbr> wide by 6 <abbr title=\"millimetres\">mm</abbr> deep. The galls appear red on the upper surface and are yellowish-green on the underside. Although the willow redgall sawfly causes minimal permanent damage, the galls are extremely unsightly especially on ornamental plantings. During a severe outbreak the galls cause a reduction in annual growth.</p>\n </div></div>\n \n<h2>Control</h2>\n\n<p>Chemical control of the willow redgall sawfly is rarely required due to parasites and predators that attack the sawfly and as the insect causes minimal permanent damage to the host. If a severe outbreak is anticipated, control may be achieved by applying carbaryl during the adult flight period. The flight period of the sawfly can be monitored using yellow sticky traps as the adults are attracted to the color yellow.</p>\n \n\r\n    ",
        "fr": "\r\n    <p><i>Pontania proxima</i></p>\n \n<h2>H\u00f4te</h2>\n<p>Saule</p>\n\n \n<h2>Aspect et cycle de vie</h2>\n\n<div class=\"row\">\n <div class=\"col-md-4 pull-right\">\n <div class=\"mrgn-bttm-md\">\n<img alt=\"La description de cette image suit\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/pfra_redgall1.jpg\">\n<figcaption><i>Galles form\u00e9es sur une feuille de saule.<br>\nPhoto : Petr Kapitola, <span lang=\"en\" xml:lang=\"en\">State Phytosanitary Administration</span>, Bugwood.org</i></figcaption>\n</div>\n\n</div>\n\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>La tenthr\u00e8de responsable de la galle rouge du saule produit deux g\u00e9n\u00e9rations par an. L'adulte mesure environ 4\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr>. Elle a le corps noir et les pattes jaunes. Chaque femelle adulte peut pondre de 20 \u00e0 35 oeufs. Les oeufs sont d\u00e9pos\u00e9s s\u00e9par\u00e9ment ou en rang\u00e9e d'un c\u00f4t\u00e9 ou de l'autre de la nervure centrale des feuilles en d\u00e9veloppement. \u00c0 chaque endroit o\u00f9 un oeuf est d\u00e9pos\u00e9, une galle se forme. Les galles atteignent leur taille maximale au moment o\u00f9 les larves \u00e9closent. Ces derni\u00e8res se nourrissent et se d\u00e9veloppent \u00e0 l'int\u00e9rieur des galles pendant environ cinq semaines. Mesurant 7\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr> \u00e0 maturit\u00e9, elles sont de couleur vert jaun\u00e2tre, avec la t\u00eate d'un noir luisant. \u00c0 ce point, elles se laissent tomber au sol et se nymphosent dans un cocon un peu en dessous de la surface du sol. Les adultes \u00e9mergent environ deux semaines plus tard, \u00e0 la fin de juillet ou au d\u00e9but d'ao\u00fbt. \u00c0 la mi-septembre les larves de la deuxi\u00e8me g\u00e9n\u00e9ration arrivent \u00e0 maturit\u00e9 et se laissent tomber au sol pour hiverner dans un cocon.</p></div></div>\n \n \n<h2>Dommages</h2>\n\n<div class=\"row\">\n <div class=\"col-md-4 pull-right\">\n <div class=\"mrgn-bttm-md\">\n<img alt=\"La description de cette image suit\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/pfra_redgall2.jpg\">\n<figcaption><i>Galles form\u00e9es sur une feuille de saule.<br>\nPhoto : J.K. Lindsey, commanster.eu</i></figcaption>\n </div>\n\n</div>\n\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n\n<p>Les femelles adultes causent l'apparition de galles en injectant un liquide dans les tissus des feuilles avec chaque oeuf qu'elles d\u00e9posent. Les galles mesurent environ 8\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr> de long, par 6\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr> de large et 6\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr> d'\u00e9paisseur. Elles sont rouges sur le dessus de la feuille et vert jaun\u00e2tre en dessous. Si la tenthr\u00e8de ne cause que des d\u00e9g\u00e2ts minimaux \u00e0 long terme, les galles, en revanche, d\u00e9figurent les plantations ornementales. Lors de pullulations, les galles causent une diminution de la croissance annuelle.</p></div></div>\n\n\n<h2>Lutte</h2>\n\n<p>Il est rarement n\u00e9cessaire, toutefois, de recourir \u00e0 la lutte chimique contre la tenthr\u00e8de responsable de la galle rouge du saule parce que cet insecte poss\u00e8de de nombreux ennemis naturels, parasites et pr\u00e9dateurs, et que les dommages permanents qu'il cause sont minimaux. En cas d'attaques importantes, l'application de carbaryl peut \u00eatre utile pour la r\u00e9pression des adultes au cours de leur p\u00e9riode de vol. L'installation de pi\u00e8ges jaunes collants, qui attirent les adultes, est un moyen efficace pour d\u00e9terminer la p\u00e9riode de vol.</p>\n \n\r\n    "
    }
}