{
    "dcr_id": "1537804431619",
    "title": {
        "en": "Screening for control agents for Trichoderma green mould in commercial mushrooms",
        "fr": "\u00c9valuations de pesticides contre la moisissure verte dans la culture de champignons commerciale"
    },
    "modified": "2018-10-12",
    "issued": "2018-10-12",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1535122427752",
    "dc_date_created": "2018-10-12",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/scr06-002_1537804431619_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/scr06-002_1537804431619_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Screening for control agents for Trichoderma green mould in commercial mushrooms",
        "fr": "\u00c9valuations de pesticides contre la moisissure verte dans la culture de champignons commerciale"
    },
    "breadcrumb": {
        "en": "Screening for control agents for Trichoderma green mould in commercial mushrooms",
        "fr": "\u00c9valuations de pesticides contre la moisissure verte dans la culture de champignons commerciale"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "modified": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Screening for control agents for Trichoderma green mould in commercial mushrooms",
            "fr": "\u00c9valuations de pesticides contre la moisissure verte dans la culture de champignons commerciale"
        },
        "subject": {
            "en": "technology transfer;architectural heritage;soil assessement",
            "fr": "gestion de cultures;mauvaises herbes;fraises"
        },
        "dcterms:subject": {
            "en": "statistics;architectural heritage;architecture",
            "fr": "inspection;irrigation;g\u00e9n\u00e9tique"
        },
        "description": {
            "en": "Alternative Formats. International Standard Serial Number: 2292-1524. Sub-Program 2.1.6: Federal-Provincial-Territorial Cost-shared Environment. Agriculture and Agri-Food Canada provides leadership in the growth and development of a competitive, innovative and sustainable Canadian agriculture and agri-food sector.Responsibilities.",
            "fr": "Navigation alphab\u00e9tique. A. B. C. D. E. F. G. H. I. J. K-L. M. N. O. P. Q-R. S. T. U-V. W-Z. Pachyloma setosum. Panicum gouinii. Fourn., Mex. Pl. Agric. Misc. Publ. 243: 263 f. 220."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Corlett;Buller;Bisby",
            "fr": "afsc.ca"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "sound",
            "fr": "consultation"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Screening for control agents for Trichoderma green mould in commercial mushrooms",
        "fr": "\u00c9valuations de pesticides contre la moisissure verte dans la culture de champignons commerciale"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <h2>Project Code: SCR06-002</h2>\n \n<h3>Project Lead</h3>\n\n<p><b>James Coupland</b> - FarmForest Research</p>\n\n<h3>Objective</h3>\n\n<p><b>Find alternative products to control Trichoderma Green Mould in commercial mushrooms by doing in vitro and in vivo screening of selected fungicides against <i>Trichoderma aggressivum</i> and by evaluating the efficacy and phytotoxicity of successful candidates</b></p>\n \n<h3>Summary of Results</h3>\n\n<p>Trichoderma green mould (<i>Trichoderma aggressivum f. aggressivum</i>; <i>Taa</i>) is one of the most destructive fungi in the commercial mushroom industry. The number of currently available chemical fungicides that inhibit the growth of Trichoderma but allow the growth of commercial mushrooms (<i>Agaricus bisporus</i>; <i>Ab</i>) is very limited. Senator 70WP, one of the few fungicides effective against Trichoderma, has been used in Canada since 2003 under an emergency registration. Alternative products to control Trichoderma would be greatly appreciated by producers.</p>\n\n<p>In a study at Pennsylvania State University that ended in December 2004, researchers screened approximately 180 products and/or formulations against Trichoderma by conducting in vitro tests on agar plates. The results of this research were used as a starting point by <abbr title=\"Doctor\">Dr.</abbr> Danny Lee Rinker (University of Guelph) and <abbr title=\"Doctor\">Dr.</abbr> James Coupland (FarmForest Research, Almonte) for selecting products that demonstrated some degree of efficacy for more in-depth evaluation.</p>\n\n<p>The first stage of the project consisted of in vitro screening of 26 fungicides from the Pennsylvania study. These fungicides were incorporated in the culture medium (agar) at various concentrations (5, 10, 20 and 40 <abbr title=\"parts per million\">ppm</abbr>). The fungi Trichoderma (the pathogen) and Agaricus (the commercial mushroom) were cultured on agar plates and mycelial growth was evaluated. Six fungicides from this first stage were chosen for the next stage.</p>\n\n<p>In the second stage, the six selected fungicides were evaluated using in vivo testing. Bags filled with compost, containing the commercial mushrooms and inoculated with the pathogenic fungus, were used. Various fungicide concentrations were tested: 500, 1000 and 1500 <abbr title=\"parts per million\">ppm</abbr>. In this stage of the test, four fungicides proved to be potentially effective, namely Fungazil, Mertect, Rovral and Senator.</p>\n\n<p>The last stage consisted of verifying the efficacy of the four potential fungicides under commercial growing conditions. The mycelium of the commercial mushrooms was inoculated with the pathogenic fungus and then sprayed with the fungicides. The concentrations used were 500 <abbr title=\"parts per million\">ppm</abbr> for Fungazil, 1500 <abbr title=\"parts per million\">ppm</abbr> for Mertect, 500 <abbr title=\"parts per million\">ppm</abbr> for Rovral and 875 <abbr title=\"parts per million\">ppm</abbr> for Senator.</p>\n\n<p>The fungicides that demonstrated the greatest efficacy in inhibiting the growth of the pathogenic fungus were Fungazil 100 SL (imazalil sulfate), Mertect SC (thiabendazole) and Senator 70WP (thiophanate-methyl). These fungicides provided excellent Trichoderma control and had no adverse effects on production yields.</p>\n\n<p>Fungazil 100 SL (500 <abbr title=\"parts per million\">ppm</abbr>) and Mertect SC (1500 <abbr title=\"parts per million\">ppm</abbr>) are not yet registered in Canada. These two products are promising, but additional studies on rates, mycotoxicity and residues left on commercial mushrooms are needed.</p> \n\r\n    ",
        "fr": "\r\n    <h2>Code de projet :  SCR06-002</h2>\n\n<h3>Chef de projet</h3>\n<p><b>James Coupland</b> - <i lang=\"en\">FarmForest Research</i></p>\n\n<h3>Objectif</h3>\n\n<p><b>Trouver des produits alternatifs pour le contr\u00f4le de la moisissure verte dans la culture commerciale de champignons en effectuant des essais in vitro et in vivo des fongicides pr\u00e9-s\u00e9lectionn\u00e9s et en \u00e9valuant l'efficacit\u00e9 et la phytotoxicit\u00e9 des fongicides prometteurs</b></p>\n\n<h3>Sommaire de R\u00e9sultats</h3>\n\n<p>La moisissure verte du champignon \u00ab Trichoderma <span lang=\"en\">green mould</span> \u00bb (<i>Trichoderma aggressivum f. aggressivum</i>; <i>Taa</i>) est un des champignons les plus destructeur dans les cultures de champignon commerciales. Actuellement, le nombre de fongicides chimiques qui inhibent la croissance du Trichoderma et qui permettent la croissance des champigons commerciaux (<i>Agaricus bisporus</i> ; <i>Ab</i>) est tr\u00e8s limit\u00e9. Le fongicide <span lang=\"en\">Senator</span> 70WP, qui est un des seuls produits efficaces et qui est utilis\u00e9 au Canada depuis 2003, est pr\u00e9sentement sous homologation d'urgence. Des produits alternatifs pour lutter contre le Trichoderma seraient grandement appr\u00e9ci\u00e9s par les producteurs.</p>\n\n<p>Lors d'une \u00e9tude termin\u00e9e en d\u00e9cembre 2004 \u00e0 l'universit\u00e9 de Pennsylvannie, des chercheurs ont \u00e9valu\u00e9 environ 180 produits et/ou formulations contre le Trichoderma en faisant des essais in vitro, sur des plaques d'agar. Les r\u00e9sultats de cette recherche ont \u00e9t\u00e9 utilis\u00e9s comme point de d\u00e9part par le <abbr title=\"Docteur\">Dr.</abbr> Danny Lee Rinker (Universit\u00e9 de Guelph) et le <abbr title=\"Docteur\">Dr.</abbr> James Coupland (<span lang=\"en\">FarmForest Research</span>, Almonte) pour choisir les produits ayant d\u00e9montr\u00e9 une certaine efficacit\u00e9 et pour les \u00e9valuer plus en profondeur.</p>\n\n<p>La premi\u00e8re \u00e9tape du projet a consist\u00e9 \u00e0 un criblage in vitro de 26 fongicides provenant de l'\u00e9tude de Pennsylvanie. Ces fongicides ont \u00e9t\u00e9 incorpor\u00e9 dans le milieu de culture (plaques d'agar) \u00e0 diff\u00e9rentes concentrations (5, 10, 20 et 40 <abbr title=\"parties par million\">ppm</abbr>). Par la suite, les champignons Trichoderma (le pathog\u00e8ne) et Agaricus (le champignon commercial) ont \u00e9t\u00e9 mis en culture sur les plaques d'agar et la croissance des leur myc\u00e9lium a \u00e9t\u00e9 \u00e9valu\u00e9e. De cette premi\u00e8re \u00e9tape, 6 fongicides ont \u00e9t\u00e9 retenus pour passer \u00e0 l'\u00e9tape suivante.</p>\n\n<p>Pour la deuxi\u00e8me \u00e9tape, on a \u00e9valu\u00e9 les 6 fongicides s\u00e9lectionn\u00e9s par des essais in vivo. On a utilis\u00e9 des sacs remplis de composte, contenant le champignon commercial, et ayant \u00e9t\u00e9 inocul\u00e9 avec le champignon pathog\u00e8ne. Diff\u00e9rentes concentrations de fongicides ont \u00e9t\u00e9 test\u00e9es, soient 500, 1000 et 1500 <abbr title=\"parties par million\">ppm</abbr>. De cet essai, 4 fongicides se sont r\u00e9v\u00e9l\u00e9s \u00eatre potentiellement efficaces, soient les fongicides <span lang=\"en\">Fungazil, Mertect, Rovral</span> et <span lang=\"en\">Senator</span>.</p>\n\n<p>Finalement, la derni\u00e8re \u00e9tape a consist\u00e9 \u00e0 v\u00e9rifier l'efficacit\u00e9 des 4 fongicides potentiels en conditions de culture commerciale. Le myc\u00e9lium du champignon commercial a \u00e9t\u00e9 inocul\u00e9 avec le champignon pathog\u00e8ne, puis a \u00e9t\u00e9 vaporis\u00e9 avec les fongicides. Les concentrations utilis\u00e9es furent 500 <abbr title=\"parties par million\">ppm</abbr> pour le <span lang=\"en\">Fungazil</span>, 1500 <abbr title=\"parties par million\">ppm</abbr> pour le <span lang=\"en\">Mertect</span>, 500 <abbr title=\"parties par million\">ppm</abbr> pour le <span lang=\"en\">Rovral</span> et 875 <abbr title=\"parties par million\">ppm</abbr> pour le <span lang=\"en\">Senator</span>.</p>\n\n<p>Les fongicides ayant d\u00e9montr\u00e9 la meilleure efficacit\u00e9 pour inhiber la croissance du champignon pathog\u00e8ne sont le <span lang=\"en\">Fungazil</span> 100 SL (Imazil sulfate), le <span lang=\"en\">Mertect</span> SC (thiabendiazole) et le <span lang=\"en\">Senator</span> 70WP (thiophanate methyl). Ces fongicides ont permis un excellent contr\u00f4le du Trichoderma et n'ont pas eu d'effets n\u00e9gatifs sur le rendement de la production.</p>\n \n\r\n    "
    }
}