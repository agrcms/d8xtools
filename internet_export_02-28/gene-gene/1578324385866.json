{
    "dcr_id": "1578324385866",
    "title": {
        "en": "Learning From Nature: Wild Wheat Responds to Climate Change",
        "fr": "Apprendre de la nature : un bl\u00e9 sauvage r\u00e9pond au changement climatique"
    },
    "modified": "2021-06-25",
    "issued": "2020-01-08",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1378999410478",
    "dc_date_created": "2020-01-08",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/CCB-DGCC/WORKAREA/CCB-DGCC/templatedata/comm-comm/gene-gene/data/feature_article_wild_wheat_1578324385866_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/CCB-DGCC/WORKAREA/CCB-DGCC/templatedata/comm-comm/gene-gene/data/feature_article_wild_wheat_1578324385866_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Learning From Nature: Wild Wheat Responds to Climate Change",
        "fr": "Apprendre de la nature : un bl\u00e9 sauvage r\u00e9pond au changement climatique"
    },
    "breadcrumb": {
        "en": "Learning From Nature: Wild Wheat Responds to Climate Change",
        "fr": "Apprendre de la nature : un bl\u00e9 sauvage r\u00e9pond au changement climatique"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2020-01-08",
            "fr": "2020-01-08"
        },
        "modified": {
            "en": "2021-06-25",
            "fr": "2021-06-25"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Learning From Nature: Wild Wheat Responds to Climate Change",
            "fr": "Apprendre de la nature : un bl\u00e9 sauvage r\u00e9pond au changement climatique"
        },
        "subject": {
            "en": "proactive disclosure;architectural heritage;soil assessement",
            "fr": "moutons;pommes de terre-producteurs;plantes-g\u00e9n\u00e9tique"
        },
        "dcterms:subject": {
            "en": "architectural heritage;architecture;government information",
            "fr": "g\u00e9n\u00e9tique;environnement;changement climatique"
        },
        "description": {
            "en": "Report. Office of Audit and Evaluation. The. The management of. AES. is shared between the Agri-Environment Services Branch (AESB) and Research Branch, while research projects, with very few exceptions, are carried out by scientists in the Research Branch. NPO. Non-pay operating. OAE. Office of Audit and Evaluation.",
            "fr": "30 mars 2017. Table des Mati\u00e8res. Tableau 12 \u2013 Types de syst\u00e8mes avanc\u00e9s ou achev\u00e9s. Tableau 13 \u2013 Modifications au Protocole d'entente sur l'expertise technique d'Agriculture et Agroalimentaire Canada et de l'Agence canadienne d'inspection des aliments. Programme de reconnaissance de la salubrit\u00e9 des aliments."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": null,
            "fr": "ACIB"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "memorandum",
            "fr": "description du programme ou du service"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Learning From Nature: Wild Wheat Responds to Climate Change",
        "fr": "Apprendre de la nature : un bl\u00e9 sauvage r\u00e9pond au changement climatique"
    },
    "body": {
        "en": "\r\n    <div class=\"col-md-4 pull-right\" style=\"z-index: 1000\">\n<section class=\"panel panel-default\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Set up an interview</h2>\n</header>\n<div class=\"panel-body\"><p><b>Media Relations</b><br>\nAgriculture and Agri-Food Canada<br>\n1-866-345-7972<br>\n<a href=\"mailto:aafc.mediarelations-relationsmedias.aac@canada.ca\">aafc.mediarelations-relationsmedias.aac@canada.ca</a></p></div>\n</section>\n</div> <p>In Canada, the Prairies are a major wheat growing area - with the crop generating farm gate receipts of over $6.7 billion in 2018. With wheat being one of Canada\u2019s most important crops, producers and industries are looking for ways to minimize climate change, as hotter temperatures and more severe droughts are expected to strain crop production.</p> \n\n<p>To understand how plant populations respond to climate stress, Agriculture and Agri-Food Canada researchers and collaborators are looking at modern wheat\u2019s ancestor, wild emmer, to see how it has genetically adapted to deal with the stress of changing climates.</p>\n\n<p>The project, led by researcher Dr. Yong-Bi Fu, looked at 10 populations of wild emmer wheat in Israel. The team analyzed population samples collected in 1980 and 2008. In that 28 year span, the average annual temperature in Israel increased while the average annual precipitation decreased.</p>\n\n<p>Through their analysis, the researchers found that plant populations can acquire beneficial mutations, such as temperature tolerance. One practical implication stemming from this research is the presence of adaptive genes responsible for climate adaptability. If identified, these climate-resistant genes could eventually be bred into modern wheat crops using traditional or cloning techniques. This could help to help make food crops more resilient.</p>\n\n<blockquote>\n <p>\"While the threat of global warming can seem overwhelming, these challenges can, in part, be addressed by learning from nature. This study is exciting because it shows an example of plants knowing how to survive global warming.\"</p>\n<p>- Dr. Yong-Bi Fu, Research Scientist - Crop Genetic Diversity</p>\n</blockquote>\n\n<p>Dr. Fu says much can be learned from this research in terms of predicting plants\u2019 adaptability and vulnerability in a hotter, drier climate.  Predictive models (using statistics to predict outcomes) can provide guidance when it comes to crop production vulnerability in a changing climate.</p> \n\n<p>Overall, the research findings underline the need to protect and conserve the diversity of plant genetic resources found throughout the world before they are lost to various threats, including climate change.</p> \n\n<h2>Key discoveries (benefits)</h2>\n\n<ul>\n <li>Due to global warming over the course of the research, the wild emmer populations faced increased selection pressure and accumulated more mutations, which suggests population vulnerability and the need to conserve genetic resources in plants.</li>\n <li>Emmer wheat\u2019s genetic reactions to climate change were extremely complex; however, some populations were still capable of acquiring climate-resistant mutations for future adaptation.</li>\n <li>Researching and identifying climate-resistant genes could lead to the development of more climate resistant crops.</li>\n <li>Understanding genetic responses to global warming could also inform the prediction of crop production vulnerability in a changing climate.</li>\n</ul>\n\n<h2>Photo gallery</h2>\n\n<div class=\"row\">\n <div class=\"col-md-4 mrgn-bttm-md\">\n <figure>\n  <img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/dr_yong_bi_fu.jpg\" class=\"float-left margin-left-medium img-responsive\" alt=\"Dr. Fu stands in his lab and inspects a dried head of emmer.\"><br>\n  <figcaption>Dr. Yong-Bi Fu inspects the head of an emmer plant in his lab in Saskatoon</figcaption>\n </figure>\n </div>\n <div class=\"col-md-4 mrgn-bttm-md\">\n <figure>\n  <img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/wild_emmer_v1.jpg\" class=\"float-left margin-left-medium img-responsive\" alt=\"A grassy, rocky area where the wild emmer was found.\"><br>\n  <figcaption>A wild emmer population from this research project</figcaption>\n </figure>\n </div>\n <div class=\"col-md-4 mrgn-bttm-md\">\n <figure>\n  <img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/emmer_seeds.jpg\" class=\"float-left margin-left-medium img-responsive\" alt=\"A close-up image of a head of emmer and a vial of seeds.\"><br>\n  <figcaption>Wild emmer is a relative of modern day wheat varieties</figcaption>\n </figure>\n </div> \n</div>\n\n<h2>Related information</h2>\n\n<ul>\n <li><a href=\"https://profils-profiles.science.gc.ca/en/profile/yong-bi-fu\">Dr. Yong-Bi Fu</a></li>\n <li><a href=\"https://profils-profiles.science.gc.ca/en/research-centre/saskatoon-research-and-development-centre\">Saskatoon Research and Development Centre</a></li>\n <li><a href=\"https://pgrc.agr.gc.ca/index_e.html\">Plant Gene Resources Canada</a></li> \n <li><a href=\"https://www.pnas.org/content/early/2019/09/10/1909564116\">PNAS: Elevated mutation and selection in wild emmer wheat in response to 28 years of global warming</a></li> \t\n</ul>\n\r\n    ",
        "fr": "\r\n    <div class=\"col-md-4 pull-right\" style=\"z-index: 1000\">\n<section class=\"panel panel-default\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Organiser une entrevue</h2>\n</header>\n<div class=\"panel-body\"><p><b>Relations avec les m\u00e9dias</b><br>\nAgriculture et Agroalimentaire Canadas<br>\n1-866-345-7972s<br>\n<a href=\"mailto:aafc.mediarelations-relationsmedias.aac@canada.ca\">aafc.mediarelations-relationsmedias.aac@canada.ca</a></p>\n</div>\n</section>\n</div><p>Au Canada, les provinces des Prairies sont de grandes r\u00e9gions productrices de bl\u00e9 et les recettes agricoles enregistr\u00e9es \u00e0 la ferme ont totalis\u00e9 plus de 6,7 milliards de dollars en 2018. Le bl\u00e9 \u00e9tant l'une des principales cultures canadiennes, les producteurs et les industries recherchent des fa\u00e7ons de r\u00e9duire les impacts du changement climatique, redoutant les effets de temp\u00e9ratures plus chaudes et de s\u00e9cheresses plus graves sur cette culture.</p> \n\n<p>Afin de comprendre la mani\u00e8re dont les populations de plantes r\u00e9pondent au stress du changement climatique, des chercheurs et des collaborateurs d'Agriculture et Agroalimentaire Canada ont \u00e9tudi\u00e9 l'amidonnier sauvage, un anc\u00eatre du bl\u00e9 moderne, pour voir de quelle mani\u00e8re il s'est adapt\u00e9 g\u00e9n\u00e9tiquement au stress d'une exposition \u00e0 un climat en changement.</p>\n\n<p>Le projet, men\u00e9 par le chercheur Yong-Bi Fu, consistait \u00e0 \u00e9tudier dix populations d'amidonnier sauvage en Isra\u00ebl. L'\u00e9quipe a analys\u00e9 des \u00e9chantillons de populations qui ont \u00e9t\u00e9 collect\u00e9s en 1980 et en 2008. Dans cette plage temporelle de 28 ans, la temp\u00e9rature moyenne annuelle en Isra\u00ebl a augment\u00e9 alors que les pr\u00e9cipitations annuelles moyennes ont diminu\u00e9.</p>\n\n<p>Dans leur analyse, les chercheurs ont constat\u00e9 que les populations de plantes peuvent acqu\u00e9rir des mutations avantageuses, comme la tol\u00e9rance \u00e0 la chaleur. Des applications pratiques sont entrevues par suite de cette d\u00e9couverte de l'existence de g\u00e8nes d'adaptation responsables de l'adaptabilit\u00e9 au climat. Si l'on r\u00e9ussit \u00e0 caract\u00e9riser ces g\u00e8nes de r\u00e9sistance, on pourrait les incorporer dans des cultivars de bl\u00e9 moderne par des techniques classiques d'am\u00e9lioration g\u00e9n\u00e9tique ou de clonage. Cela pourrait aider \u00e0 rendre les cultures vivri\u00e8res plus r\u00e9silientes au changement climatique.</p>\n\n<blockquote>\n <p>\u00ab\u00a0M\u00eame si les menaces du r\u00e9chauffement plan\u00e9taire peuvent sembler insurmontables, les d\u00e9fis pos\u00e9s peuvent \u00eatre abord\u00e9s, en partie, en se tournant vers la nature. Notre \u00e9tude est tr\u00e8s int\u00e9ressante, car elle fait \u00e9tat d'une plante qui est capable de s'adapter au r\u00e9chauffement plan\u00e9taire.\u00a0\u00bb</p>\n<p>- Yong-Bi Fu, chercheur scientifique \u2013 Diversit\u00e9 g\u00e9n\u00e9tique des cultures</p>\n</blockquote>\n\n<p>Selon M. Fu, cette recherche peut nous en apprendre beaucoup et nous aider \u00e0 pr\u00e9dire l'adaptabilit\u00e9 et la vuln\u00e9rabilit\u00e9 des plantes dans un climat qui devient plus chaud et plus sec. Les mod\u00e8les de pr\u00e9diction (qui utilisent des donn\u00e9es statistiques pour pr\u00e9dire des r\u00e9sultats) peuvent donner des indications sur la vuln\u00e9rabilit\u00e9 des cultures aux impacts du changement climatique.</p> \n\n<p>Globalement, la pr\u00e9sente recherche fait ressortir la n\u00e9cessit\u00e9 de prot\u00e9ger et de conserver la diversit\u00e9 des ressources phytog\u00e9n\u00e9tiques plan\u00e9taires avant qu'elles disparaissent sous l'effet de diverses menaces, dont celle du changement climatique.</p> \n\n<h2>Principales d\u00e9couvertes et avantages</h2>\n\n<ul>\n <li>En raison du r\u00e9chauffement climatique qui est survenu sur la dur\u00e9e de la recherche, les populations d'amidonnier sauvage ont \u00e9t\u00e9 expos\u00e9es \u00e0 une pression accrue de s\u00e9lection et ont accumul\u00e9 plus de mutations, ce qui t\u00e9moigne de la vuln\u00e9rabilit\u00e9 des populations et de la n\u00e9cessit\u00e9 de conserver les ressources phytog\u00e9n\u00e9tiques.</li>\n <li>Les r\u00e9ponses g\u00e9n\u00e9tiques de l'amidonnier au changement climatique ont \u00e9t\u00e9 extr\u00eamement complexes; cependant, certaines populations ont conserv\u00e9 la capacit\u00e9 d'acqu\u00e9rir d'autres mutations de r\u00e9sistance au changement climatique pour une adaptation future.</li>\n <li>La recherche et l'identification de g\u00e8nes de r\u00e9sistance aux impacts du changement climatique pourraient mener \u00e0 la mise au point de cultures plus r\u00e9sistantes.</li>\n <li>La compr\u00e9hension des r\u00e9ponses g\u00e9n\u00e9tiques au r\u00e9chauffement climatique pourrait aussi informer la pr\u00e9diction de la vuln\u00e9rabilit\u00e9 des cultures.</li>\n</ul>\n\n<h2>Galerie de photos</h2>\n\n<div class=\"row\">\n <div class=\"col-md-4 mrgn-bttm-md\">\n <figure>\n  <img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/dr_yong_bi_fu.jpg\" class=\"float-left margin-left-medium img-responsive\" alt=\"M. Fu stands se tient devant son laboratoire et inspecte un \u00e9pi d\u2019amidonnier s\u00e9ch\u00e9.\"><br>\n  <figcaption>Yong-Bi Fu inspecte un \u00e9pi d'amidonnier dans son laboratoire de Saskatoon</figcaption>\n </figure>\n </div>\n <div class=\"col-md-4 mrgn-bttm-md\">\n <figure>\n  <img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/wild_emmer_v1.jpg\" class=\"float-left margin-left-medium img-responsive\" alt=\"Une zone enherb\u00e9e et rocailleuse o\u00f9 pousse l\u2019amidonnier sauvage.\"><br>\n  <figcaption>Une des populations d'amidonnier \u00e9tudi\u00e9es dans le cadre du projet de recherche</figcaption>\n </figure>\n </div>\n <div class=\"col-md-4 mrgn-bttm-md\">\n <figure>\n  <img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/emmer_seeds.jpg\" class=\"float-left margin-left-medium img-responsive\" alt=\"Gros plan d\u2019un \u00e9pi d\u2019amidonnier et d\u2019une fiole de semences.\"><br>\n  <figcaption>L'amidonnier sauvage est une plante apparent\u00e9e aux cultivars de bl\u00e9 moderne</figcaption>\n </figure>\n </div> \n</div>\n\n<h2>Information connexe</h2>\n\n<ul>\n <li><a href=\"https://profils-profiles.science.gc.ca/fr/profil/yong-bi-fu\">Yong-Bi Fu, Ph. D.</a></li>\n <li><a href=\"https://profils-profiles.science.gc.ca/fr/centre-recherche/centre-de-recherche-et-de-developpement-de-saskatoon\">Centre de recherche et d\u00e9veloppement de Saskatoon</a></li>\n <li><a href=\"https://pgrc.agr.gc.ca/index_f.html\">Ressources phytog\u00e9n\u00e9tiques du Canada</a></li> \n <li><a href=\"https://www.pnas.org/content/early/2019/09/10/1909564116\">PNAS: Elevated mutation and selection in wild emmer wheat in response to 28 years of global warming (en anglais seulement)</a></li> \t\n</ul>\n\r\n    "
    },
    "js": {
        "en": "",
        "fr": ""
    }
}