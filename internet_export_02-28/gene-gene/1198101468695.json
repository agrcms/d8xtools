{
    "dcr_id": "1198101468695",
    "title": {
        "en": "Black Knot of Plum and Cherry",
        "fr": "Nodule noir du prunier et du cerisier"
    },
    "modified": "2014-06-10",
    "issued": "2009-02-24",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1354137773743",
    "dc_date_created": "2009-02-24",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/12/pfra_org_blackknot_1198101468695_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/12/pfra_org_blackknot_1198101468695_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Black Knot of Plum and Cherry",
        "fr": "Nodule noir du prunier et du cerisier"
    },
    "breadcrumb": {
        "en": "Black Knot of Plum and Cherry",
        "fr": "Nodule noir du prunier et du cerisier"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2009-02-24",
            "fr": "2009-02-24"
        },
        "modified": {
            "en": "2014-06-10",
            "fr": "2014-06-10"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Black Knot of Plum and Cherry",
            "fr": "Nodule noir du prunier et du cerisier"
        },
        "subject": {
            "en": "integrated pest management;forestry;agroforestry",
            "fr": "foresterie;lutte antiparasitaire int\u00e9gr\u00e9e;agroforesterie"
        },
        "dcterms:subject": {
            "en": "diseases;fungi",
            "fr": "maladie;champignon"
        },
        "description": {
            "en": "Dormant Spray Spray just before buds break in the spring with one of the following: 2 tablespoons/gallon (=2 lb/100 gal.) of thiram or lime-sulfur solution (1 part lime-sulfur to 8 parts of water) or 4:6:100 Bordeaux Mixture (see below for preparation).",
            "fr": "pulv\u00e9risation de dormance : juste avant l'\u00e9closion des bourgeons, au printemps, vaporiser avec l'une des solutions suivantes : thirame (2 c. \u00e0 table par gallon d'eau = 2 lb par 100 gal), polysulfure de calcium (1 partie pour 8 parties d'eau) et bouillie bordelaise 4-6-100 (voir mode de pr\u00e9paration ci-dessous)."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "lime-sulfur solution;Bordeaux Mixture;several inches;part lime-sulfur;parts of water",
            "fr": " mode d'emploi; parties d'eau; solutions suivantes; polysulfure de calcium;gallon d'eau"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "scientifiques;entreprises"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Black Knot of Plum and Cherry",
        "fr": "Nodule noir du prunier et du cerisier"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <p>The fungus, <i>Dibotryon morbosum</i>, causes this potentially serious disease which affects both wild and cultivated species of chokecherry to domestic varieties. Yield is reduced since severely infected trees are stunted and because control necessitates pruning to remove the knots.</p>\n<section>\n<h2>Symptoms</h2>\n<p>Rough, elongated, hard, black swellings commonly on twigs but also on branches or stems are characteristic of this disease. Usually knots occur on one side of the twig but occasionally branches become completely girdled killing the portion above the infection. New infections show up as green swellings which enlarge, develop cracks and turn black with age. Old black knots may be partially covered by a white to pinkish mold and be riddled with insect holes.</p>\n</section><section>\n<h2>Disease Cycle</h2>\n<p>Winter spores formed in mature black knots are spread by wind and rain to twigs where infection takes place through unwounded tissue. Infection continues to occur until terminal growth stops and it is most severe when conditions are mild and wet. Only several months after initiation of infection do green swellings become visible and usually not until spring. These newly formed knots produce summer spores. The fungus extends several inches beyond the knots and knots will expand with age.</p>\n</section><section>\n<h2>Control</h2>\n<ol>\n<li>Clean up trees annually. This involves pruning out swollen areas during winter or early spring. Prune at least 3 inches below the visible swelling as the fungus extends several inches beyond the knot. Wounds should be covered with a wound dressing, such as Braco, shellac, Bordeaux paint (Bordeaux powder plus linseed oil mixed into a paste) or other reliable wound dressing products available from seed and garden supply dealers. Destroy all prunings by burning before spring since pruned knots will still produce spores which can spread the disease. Severely infected trees should be removed and burned.</li>\n<li>In establishing new plantings and also in helping to control disease in established plantings, wild cherries and plums in the vicinity should be thoroughly cleaned up or removed.</li>\n<li>The above control measures will usually be sufficient to keep the disease under control, however, where the disease is a severe and continual problem additional control measures may be needed.</li>\n</ol>\n<p>In this regard, thorough applications of several fungicide sprays, along with diseased wood removal will help control the disease. Spraying alone will not control the disease. However, when using chemical control read the product label carefully since it shows the purpose for which the chemical is sold, directions for use, and handling precautions.</p>\n</section><section>\n<h2>Dormant Spray</h2>\n<ol>\n<li>Spray just before buds break in the spring with one of the following: 2\u00a0tablespoons/gallon (=2\u00a0<abbr title=\"pounds\">lb</abbr>/100\u00a0<abbr title=\"gallons\">gal.</abbr>) of thiram, or lime-sulfur solution (1 part lime-sulfur to 8\u00a0parts of water), or 4:6:100\u00a0Bordeaux Mixture (see below for preparation).</li>\n<li>At least two additional sprays at Full Bloom and at Shuck Fall. Use one of the following: 2 \u00a0tablespoons/gallon of captan, or 2\u00a0tablespoons/gallon of thiram, or lime-sulfur solution (1 part lime-sulfur to 50\u00a0parts of water).</li>\n<li>A small volume of 4:6:100\u00a0Bordeaux Mixture is made by dissolving 2\u00a0ounces copper sulfate in one gallon of water and 3\u00a0ounces of hydrated lime in two gallons of water. Pour the copper sulfate solution into the lime water and strain through fine cheesecloth. Use the solution immediately after mixing and also fresh lime is essential not some left over from the previous season.</li>\n</ol></section>\n\r\n    ",
        "fr": "\r\n    <p>Le nodule noir est une maladie caus\u00e9e par le champignon <i>Apiosporina morbosa</i> (syn. <i>Dibotryon morbosum</i>) qui affecte les cerisiers sauvages et ornementaux et qui peut se propager aux cultures fruiti\u00e8res. Elle provoque une diminution du rendement parce que les arbres gravement atteints perdent de leur vigueur et que pour lutter contre la maladie, il faut \u00e9laguer les branches infect\u00e9es.</p><section><h2>Sympt\u00f4mes</h2><p>La maladie se caract\u00e9rise par des renflements noirs, durs et allong\u00e9s sur les rameaux. Ces renflements peuvent \u00e9galement se produire sur les branches ou le tronc. En g\u00e9n\u00e9ral, un seul c\u00f4t\u00e9 des rameaux est affect\u00e9, mais il arrive parfois que les nodules encerclent compl\u00e8tement une branche, entra\u00eenant la mort de cette derni\u00e8re au-dessus du point d'infection. Les nouvelles infections se manifestent par un renflement vert, qui se gonfle, se fissure et devient noir au fil du temps. Les vieux nodules noirs peuvent \u00eatre partiellement recouverts d'une moisissure de coloration blanche ou ros\u00e9e et comporter de nombreux trous d'insectes.</p></section>      <section><h2>Cycle de vie</h2><p>Les spores blanches qui se forment sur les nodules noirs parvenus \u00e0 matur\u00e9 sont diss\u00e9min\u00e9es par le vent et la pluie. Elles germent et p\u00e9n\u00e8trent directement dans les tissus intacts. L'infection se poursuit jusqu'\u00e0 l'arr\u00eat de la croissance des nouvelles pousses. Les attaques sont plus fortes par temps pluvieux et doux. Les renflements verts ne sont visibles que plusieurs mois apr\u00e8s le d\u00e9but de l'infection, voire pas avant le printemps suivant. Les nouveaux nodules produisent des spores en \u00e9t\u00e9. La croissance du champignon s'\u00e9tend plusieurs centim\u00e8tres de part et d'autre des nodules, qui \u00e9largissent en vieillissant.</p></section>      <section><h2>Lutte</h2><ol><li>Nettoyer les arbres tous les ans en enlevant les renflements durant l'hiver ou au d\u00e9but du printemps. Tailler les rameaux au moins 8\u00a0\u00e0 10\u00a0<abbr title=\"centim\u00e8tres\">cm</abbr> en dessous des renflements, car le champignon colonise les tissus sur une distance de plusieurs centim\u00e8tres de part et d'autre des nodules. Enduire les plaies d'un produit cicatrisant, comme le Braco, le vernis \u00e0 la gomme laque ou tout autre produit efficace en vente chez les fournisseurs de semences et de produits horticoles, ou badigeonner avec une p\u00e2te faite d'un m\u00e9lange de bouillie bordelaise en poudre et d'huile de lin. D\u00e9truire tous les r\u00e9sidus d'\u00e9mondage avant le printemps car les nodules \u00e9mond\u00e9s produiront des spores qui peuvent propager la maladie. Les arbres gravement atteints doivent \u00eatre abattus et br\u00fbl\u00e9s.</li><li>Il est recommand\u00e9 de bien nettoyer ou de d\u00e9truire les cerisiers et les pruniers sauvages se trouvant \u00e0 proximit\u00e9 des vergers pour pr\u00e9venir les infections dans les nouvelles plantations ou pour lutter contre la maladie dans les plantations \u00e9tablies.</li><li>Ces mesures permettront g\u00e9n\u00e9ralement de ma\u00eetriser la maladie. Toutefois, des mesures suppl\u00e9mentaires pourraient \u00eatre n\u00e9cessaires en cas de fortes attaques ou de probl\u00e8mes r\u00e9p\u00e9t\u00e9s.</li></ol><p>Si tel est le cas, l'application de divers fongicides et l'\u00e9limination des parties atteintes permettront ensemble de lutter efficacement contre la maladie. \u00c0 elle seule, l'application de fongicides ne suffit pas \u00e0 ma\u00eetriser la maladie. Avant d'appliquer un quelconque produit chimique, s'assurer de bien lire l'\u00e9tiquette, qui indique les usages pour lesquels le produit a \u00e9t\u00e9 con\u00e7u ainsi que son mode d'emploi et les pr\u00e9cautions \u00e0 prendre lors des manipulations.</p></section>      <section><h2>Pulv\u00e9risation de dormance</h2><ol><li>Juste avant l'\u00e9closion des bourgeons, au printemps, vaporiser avec l'une des solutions suivantes\u00a0: thirame (2\u00a0<abbr title=\"cuiller\u00e9e\">c.</abbr> \u00e0 table par gallon d'eau = 2\u00a0<abbr title=\"livres\">lb</abbr> par 100\u00a0<abbr title=\"gallons\">gal</abbr>), polysulfure de calcium (1\u00a0partie pour 8\u00a0parties d'eau) et bouillie bordelaise 4-6-100 (voir mode de pr\u00e9paration ci-dessous).</li><li>Faire au moins deux autres pulv\u00e9risations, une lorsque les fleurs sont ouvertes et l'autre au moment de la chute des p\u00e9tales. Utiliser \u00e0 cette fin l'une des solutions suivantes\u00a0: captane (2\u00a0<abbr title=\"cuiller\u00e9e\">c.</abbr> \u00e0 table par gallon d'eau), thirame (2\u00a0<abbr title=\"livres\">lb</abbr> \u00e0 table par gallon d'eau) ou polysulfure de calcium (1 partie pour 50 parties d'eau).</li><li>Pour pr\u00e9parer une petite quantit\u00e9 de bouillie bordelaise (4-6-100), dissoudre 2\u00a0<abbr title=\"onces\">oz</abbr> de sulfate de cuivre dans un gallon d'eau et 3\u00a0<abbr title=\"onces\">oz</abbr> d'hydroxyde de calcium dans 2\u00a0gallons d'eau. Verser la solution de sulfate de cuivre dans la solution d'hydroxyde de calcium et passer dans une \u00e9tamine. Agiter le m\u00e9lange et l'utiliser imm\u00e9diatement. Pr\u00e9parer une nouvelle solution d'hydroxyde de calcium pour chaque utilisation; ne jamais utiliser des restes de la saison pr\u00e9c\u00e9dente.</li></ol></section>\n\r\n    "
    }
}