{
    "dcr_id": "1537373451014",
    "title": {
        "en": "Developing mating disruption for the integrated management of grape berry moth",
        "fr": "Mise au point de techniques de confusion sexuelle pour la lutte int\u00e9gr\u00e9e contre la tordeuse de la vigne"
    },
    "modified": "2018-10-12",
    "issued": "2018-10-12",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1535122427752",
    "dc_date_created": "2018-10-12",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr07-230_1537373451014_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr07-230_1537373451014_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Developing mating disruption for the integrated management of grape berry moth",
        "fr": "Mise au point de techniques de confusion sexuelle pour la lutte int\u00e9gr\u00e9e contre la tordeuse de la vigne"
    },
    "breadcrumb": {
        "en": "Developing mating disruption for the integrated management of grape berry moth",
        "fr": "Mise au point de techniques de confusion sexuelle pour la lutte int\u00e9gr\u00e9e contre la tordeuse de la vigne"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "modified": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Developing mating disruption for the integrated management of grape berry moth",
            "fr": "Mise au point de techniques de confusion sexuelle pour la lutte int\u00e9gr\u00e9e contre la tordeuse de la vigne"
        },
        "subject": {
            "en": "technology transfer;architectural heritage;soil assessement",
            "fr": "eau ressource;agroforesterie;mauvaises herbes"
        },
        "dcterms:subject": {
            "en": "statistics;architectural heritage;architecture",
            "fr": "b\u00e2timent;cultures;terre agricole"
        },
        "description": {
            "en": "Alternative Formats. International Standard Serial Number: 2292-1524. Sub-Program 2.1.6: Federal-Provincial-Territorial Cost-shared Environment. Agriculture and Agri-Food Canada provides leadership in the growth and development of a competitive, innovative and sustainable Canadian agriculture and agri-food sector.Responsibilities.",
            "fr": "Agriculture et Agroalimentaire Canada. 4200, route no 97 Sud. Laboratoire de recherche sur les aliments et laboratoire pilote d\u2019extraction et de fractionnement. D\u00e9terminer les porte-greffes de raisins de cuve et d\u2019arbres fruitiers qui r\u00e9sistent aux ravageurs des sols, utilisent l\u2019eau ou les nutriments d\u2019une mani\u00e8re plus efficace et produisent des fruits de meilleure qualit\u00e9. Renforcer la capacit\u00e9 du secteur \u00e0 r\u00e9agir face aux maladies, aux virus et aux autres menaces biologiques."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Centre.Facilities;Canada.Summerland",
            "fr": "Cumming;Brunke;Marla Schwarzfeld"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "sound",
            "fr": "avis"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Developing mating disruption for the integrated management of grape berry moth",
        "fr": "Mise au point de techniques de confusion sexuelle pour la lutte int\u00e9gr\u00e9e contre la tordeuse de la vigne"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n     \n<h2>Project Code: PRR07-230</h2>\n \n<h3>Project Lead</h3>\n<p><b>Robert Trimble</b> - Agriculture and Agri-Food Canada</p>\n\n<h3>Objective</h3>\n<p><strong>To determine whether reduced application rates of pheromone dispensers through peripheral treatments can be used to achieve effective and economic control of grape berry moth (GBM). Conduct a cost/benefit analysis of this reduced rate technology.</strong></p>\n\n<h3>Summary of Results</h3>\n\n<h4>Background</h4>\n<p>The grape berry moth, <i>Paralobesia viteana</i> (Clemens) is the most important insect pest of grapes in eastern North America with two-three generations appearing per year in Canada. Its management relies largely on the use of pesticides. In Niagara peninsula vineyards, one or two applications of organophosphate insecticide have been typically used to control this pest. However, registrations of these insecticides have been cancelled, or their use has been made more difficult because of the <abbr title=\"United States of America\">U.S.A.</abbr> Food Quality Protection Act and a pest control product re-evaluation program in Canada. The efficacy of lower risk insecticides that could be used in place of organophosphate insecticides has been examined in the <abbr title=\"United States of America\">U.S.A.</abbr> and in Canada. There was, however, a need for increased research and development effort on alternative approaches to minimize the use of insecticides for controlling <i>P. viteana</i>.</p>\n<p>The use of synthetic sex pheromone to disrupt sexual orientation, otherwise known as \"mating disruption\", is an alternative control method that may lead to the transition from insecticides-based management to integrated pest management (IPM). Mating disruption has been successfully used to control <i>P. viteana</i> in Ontario vineyards, but there has been limited adoption of this technique by growers. The primary reason for the low adoption rate is the higher cost of using pheromone compared to insecticides. Isomate GBM\u00ae Plus pheromone dispensers is the only commercial mating disruption product currently available to Canadian grape growers. One method of reducing the cost of mating disruption technique would be the use of reduced rates of pheromone dispensers.</p>\n\n<h4>Approaches</h4>\n<p>The effect of peripheral treatments with pheromone dispensers in controlling <i>P. viteana</i> was investigated in three commercial vineyards in the Niagara peninsula, Ontario in 2007. Four 1 hectare plots were established within each of the three vineyards. The activity and mate-locating ability of male <i>P. viteana</i> was monitored in each plot using 25 synthetic sex pheromone-baited delta traps (Cooper Mill Ltd., Madoc, Ontario) that were positioned 1 <abbr title=\"metre\">m</abbr> above the ground on a 20 <abbr title=\"metre\">m</abbr> x 20 <abbr title=\"metre\">m</abbr> grid prior to first moth flight in the spring. Each trap was baited with a 9 <abbr title=\"millimetre\">mm</abbr>-diameter, natural-rubber sleeve stopper (Chromatographic Specialties, Brockville, Ontario) loaded with 0.8 <abbr title=\"milligrams\">mg</abbr> of Z-9-dodecenyl acetate (Z9-12:OAc) and 0.2 <abbr title=\"milligrams\">mg</abbr> of (Z)-11-tetradecenyl acetate (Z11-14:OAc) (Pherobank, Plant Research International, Wageningen, the Netherlands). Moths were counted and removed from the traps on Mondays and Thursdays from May 22 until September 10 in 2007. Stoppers were changed at the end of the 1st and 2nd of the three flights of <i>P. viteana</i>.</p>\n\n<p>The Isomate\u00ae GBM Plus pheromone dispensers, each containing 221.5 <abbr title=\"miligrams\">mg</abbr> of <abbr title=\"Z-9-dodecenyl acetate\">Z9-12:OAc</abbr> (Pacific Biocontrol Corp., Vancouver, <abbr title=\"Washington\">WA</abbr>) were deployed after peak trap catch during the second flight (between July 6 and 11) of <i>P. viteana</i> moths. Each of the four plots in each vineyard received one of the following four treatments: 1) pheromone dispensers applied at equal spacing to the entire plot at the recommended rate of 500 units/hectare; 2) dispensers applied to the periphery of the plot at intervals of 5 <abbr title=\"metres\">m</abbr>; 3) dispensers applied to the periphery of the plot at intervals of 2.5 <abbr title=\"metres\">m</abbr>; and 4) no treatment with pheromone dispensers (<abbr title=\"that is\">i.e.</abbr> untreated control). Dispensers were attached to the top trellis wire 110-120 <abbr title=\"centimetres\">cm</abbr> above the ground within the grape vine canopy. Moth counts trapped in each plot before and after pheromone dispensers were applied and feeding injury to grape clusters by <i>P. viteana</i> larvae were assessed.</p>\n \n<h4>Results</h4>\n\n<p>The application of Isomate\u00ae GBM Plus dispensers at a rate of 500 units/hectare reduced the mean total number of moths trapped by 96% compared to the untreated control, indicating a high level of mating disruption. Trap catch was reduced by 87% when dispensers were applied at intervals of 2.5 or 5 <abbr title=\"metres\">m</abbr> along the periphery of 1 hectare plots of vines, but the reduction was not significantly different from the untreated control. The economic injury threshold of 2% infested grape clusters was not exceeded in any of the inspected plots. Results of this study demonstrate some potential for using peripheral treatments with pheromone dispensers for controlling <i>P. viteana</i> by mating disruption. Additional experiments using greater replication should be undertaken to confirm the current results and increase the likelihood of detecting significant treatment effects when using peripheral treatments. It is recommended that any future experiments should also include the use of tethered, virgin-female moths to confirm that the pheromone treatments are affecting the ability of male <i>P. viteana</i> to locate and mate with sexually receptive females. The successful outcomes from peripheral treatments with pheromone dispensers would make the use of pheromone for <i>P. viteana</i> management considerably less expensive for grape growers. For example, in a 200 <abbr title=\"metre\">m</abbr>-wide by 500 <abbr title=\"metre\">m</abbr>-long, 10 hectare vineyard, enclosing each of the 1 hectare \"plots\" in the vineyard with dispensers spaced at 2.5 or 5 <abbr title=\"metre\">m</abbr> intervals would result in a 78 and 89% reduction in the number of dispensers required, respectively, compared to the minimum recommended application rate of 500 dispensers/hectare.</p>\n \n\r\n    ",
        "fr": "\r\n     \n<h2>Code de projet : PRR07-230</h2>\n \n<h3>Chef de projet</h3>\n<p><b>Robert Trimble</b> - Agriculture et Agroalimentaire Canada</p>\n\n<h3>Objectif</h3>\n\n<p><b>D\u00e9terminer si la r\u00e9duction des taux d'application des diffuseurs de ph\u00e9romone au moyen de traitements p\u00e9riph\u00e9riques peut servir \u00e0 exercer un contr\u00f4le efficace et \u00e9conomique de la tordeuse de la vigne (TV). Mener une analyse co\u00fbt-avantage de cette technologie de r\u00e9duction des taux d'application</b></p>\n\n\n<h3>Sommaire des R\u00e9sultats</h3>\n\n<h4>Contexte</h4>\n\n<p>La tordeuse de la vigne (<i>Paralobesia viteana</i> [Clemens]) est le plus important insecte ravageur de la vigne dans l\u2019est de l\u2019Am\u00e9rique du Nord. Cet insecte conna\u00eet deux ou trois g\u00e9n\u00e9rations par ann\u00e9e au Canada. La lutte contre cet organisme nuisible repose en grande partie sur l\u2019utilisation des pesticides. Dans les vignobles de la p\u00e9ninsule de Niagara, une ou deux applications d\u2019insecticides organophosphor\u00e9s suffisaient habituellement pour combattre ce ravageur. Cependant, l\u2019homologation de ces insecticides a \u00e9t\u00e9 r\u00e9voqu\u00e9e, ou leur utilisation est devenue plus difficile en raison de la <span lang=\"en\">Food Quality Protection Act</span> des \u00c9tats-Unis et d\u2019un programme de r\u00e9\u00e9valuation des pesticides au Canada. Les autorit\u00e9s am\u00e9ricaines et canadiennes ont \u00e9tudi\u00e9 l\u2019efficacit\u00e9 des insecticides \u00e0 risque r\u00e9duit pour remplacer les insecticides organophosphor\u00e9s. Toutefois, de plus amples travaux de recherche et de d\u00e9veloppement s\u2019imposaient pour trouver des solutions de rechange contre le <i>P. viteana</i>.</p>\n\n<p>L\u2019usage de ph\u00e9romones sexuelles synth\u00e9tiques pour perturber l\u2019orientation sexuelle, processus autrement connu sous le nom de \u00ab confusion sexuelle \u00bb, est une m\u00e9thode de lutte de rechange qui peut permettre la transition de la lutte chimique \u00e0 la lutte int\u00e9gr\u00e9e. Cette technique a donn\u00e9 de bons r\u00e9sultats dans les vignobles de l\u2019Ontario, mais son adoption par les producteurs demeure limit\u00e9e. Le co\u00fbt plus \u00e9lev\u00e9 des ph\u00e9romones par rapport \u00e0 celui des insecticides est en grande partie responsable de ce faible taux d\u2019adoption. Les diffuseurs de ph\u00e9romones Isomate GBMMD Plus constituent le seul produit commercial de confusion sexuelle que les viticulteurs peuvent actuellement se procurer sur le march\u00e9 au Canada. L\u2019une des fa\u00e7ons de r\u00e9duire le co\u00fbt de la lutte reposant sur la technique de confusion sexuelle serait d\u2019employer des diffuseurs de ph\u00e9romones \u00e0 doses r\u00e9duites.</p>\n \n<h4>M\u00e9thodes</h4>\n\n<p>En 2007, on a \u00e9tudi\u00e9 l\u2019effet contre le <i>P. viteana</i> de traitements avec des diffuseurs de ph\u00e9romones install\u00e9s en p\u00e9riph\u00e9rie de trois vignobles commerciaux de la p\u00e9ninsule de Niagara, en Ontario. On a d\u00e9limit\u00e9 quatre parcelles de 1 hectare dans chacun des trois vignobles. On a install\u00e9 des 25 pi\u00e8ges delta (<span lang=\"en\">Cooper Mill Ltd.</span>, Madoc [Ontario]) contenant des ph\u00e9romones sexuelles synth\u00e9tiques \u00e0 1 <abbr title=\"m\u00e8tre\">m</abbr> du sol sur une grille de 20 <abbr title=\"m\u00e8tres\">m</abbr> x 20 <abbr title=\"m\u00e8tres\">m</abbr> avant le vol de la premi\u00e8re g\u00e9n\u00e9ration de la tordeuse de la vigne au printemps pour surveiller l\u2019activit\u00e9 et mesurer la capacit\u00e9 des <i>P. viteana</i> m\u00e2les \u00e0 chercher et \u00e0 trouver des partenaires. Chaque pi\u00e8ge a \u00e9t\u00e9 app\u00e2t\u00e9 au moyen d\u2019un bouchon-manchon en caoutchouc naturel de 9 <abbr title=\"millim\u00e8tres\">mm</abbr> de diam\u00e8tre (<span xml:lang=\"en\" lang=\"en\">Chromatographic Specialties</span>, Brockville, Ontario), rempli de 0,8 <abbr title=\"milligrammes\">mg</abbr> de Z-ac\u00e9tate de dod\u00e9c-9-\u00e9nyl (Z9-12:OAc) et 0,2 <abbr title=\"milligrammes\">mg</abbr> de (Z)-11-t\u00e9trad\u00e9cenal (Z11-14:OAc) (<span lang=\"en\">Pherobank, Plant Research International</span>, Wageningen, Pays-Bas). On a compt\u00e9 le nombre de ravageur et on les a retir\u00e9s des pi\u00e8ges le lundi et le jeudi, du 22 mai jusqu\u2019au 10 septembre 2007. Les bouchons ont \u00e9t\u00e9 chang\u00e9s \u00e0 la fin de la p\u00e9riode de vol de la premi\u00e8re et de la deuxi\u00e8me des trois g\u00e9n\u00e9rations de <i>P. viteana</i>.</p>\n\n<p>Les diffuseurs Isomate\u00ae GBM Plus contenant chacun 221,5 <abbr title=\"milligrammes\">mg</abbr> de <abbr title=\"dod\u00e9c-9-\u00e9nyl\">Z9-12:OAc</abbr> (<span xml:lang=\"en\" lang=\"en\">Pacific Biocontrol Corp.</span>, Vancouver, Washington) ont \u00e9t\u00e9 d\u00e9ploy\u00e9s apr\u00e8s le pic de prises dans les pi\u00e8ges pendant le vol de la deuxi\u00e8me g\u00e9n\u00e9ration (entre le 6 et le 11 juillet) de tordeuses de la vigne <i>P. viteana</i>. Dans chaque vignoble, chacune des quatre parcelles a fait l\u2019objet de l\u2019un ou l\u2019autre des traitements suivants : 1) diffuseurs de ph\u00e9romones r\u00e9partis \u00e0 distance \u00e9gale partout la parcelle a la densit\u00e9 recommand\u00e9e de 500 unit\u00e9s/hectare; 2) diffuseurs install\u00e9s en p\u00e9riph\u00e9rie des parcelles, \u00e0 intervalles de 5 <abbr title=\"m\u00e8tres\">m</abbr>; 3) diffuseurs install\u00e9s en p\u00e9riph\u00e9rie des parcelles, \u00e0 intervalles de 2,5 <abbr title=\"m\u00e8tres\">m</abbr>; 4) aucun traitement avec des diffuseurs de ph\u00e9romones (parcelle t\u00e9moin). Les diffuseurs ont \u00e9t\u00e9 install\u00e9s au sommet des treillis \u00e0 une hauteur de 110 \u00e0 120 <abbr title=\"centim\u00e8tres\">cm</abbr> au-dessus du sol dans le feuillage de la vigne. On a compt\u00e9 le nombre de <i>P. viteana</i> adultes captur\u00e9s dans chaque parcelle avant et apr\u00e8s l\u2019installation des diffuseurs, et l\u2019on a \u00e9valu\u00e9 les dommages inflig\u00e9s aux grappes de raisins par les chenilles.</p>\n \n<h4>R\u00e9sultats</h4>\n\n<p>L\u2019installation de diffuseurs Isomate\u00ae GBM Plus \u00e0 la densit\u00e9 de 500 unit\u00e9s/hectare a r\u00e9duit de <span class=\"nowrap\">96 p. 100</span> le nombre total moyen d\u2019adultes captur\u00e9s par rapport a les parcelles t\u00e9moin. Ce r\u00e9sultat t\u00e9moigne d\u2019un degr\u00e9 \u00e9lev\u00e9 de confusion sexuelle. On a constat\u00e9 une diminution de <span class=\"nowrap\">87 p. 100</span> du nombre d\u2019adultes pi\u00e9g\u00e9s lorsque les diffuseurs ont \u00e9t\u00e9 install\u00e9s \u00e0 intervalles de 2,5 ou 5 <abbr title=\"m\u00e8tres\">m</abbr> le long du p\u00e9rim\u00e8tre des parcelles de vigne de 1 hectare, mais la diff\u00e9rence par rapport \u00e0 la parcelle t\u00e9moin n\u2019\u00e9tait pas significative. Le seuil \u00e9conomique de dommages, soit <span class=\"nowrap\">2 p. 100</span> des grappes de raisin infest\u00e9es, n\u2019a \u00e9t\u00e9 d\u00e9pass\u00e9 dans aucune des parcelles \u00e9tudi\u00e9es. Les r\u00e9sultats de cette \u00e9tude d\u00e9montrent que l\u2019installation de diffuseurs de ph\u00e9romones en p\u00e9riph\u00e9rie des vignobles pour lutter contre le <i>P. viteana</i> \u00e0 l\u2019aide de la technique de confusion sexuelle peut \u00eatre une strat\u00e9gie efficace. D\u2019autres \u00e9tudes comportant un niveau de r\u00e9p\u00e9tition plus \u00e9lev\u00e9 s\u2019imposent pour corroborer les r\u00e9sultats obtenus \u00e0 ce jour et accro\u00eetre la probabilit\u00e9 de d\u00e9tecter des effets significatifs en cas de recours \u00e0 des traitements en p\u00e9riph\u00e9rie. Des essais devraient \u00e9galement \u00eatre r\u00e9alis\u00e9s avec des femelles vierges attach\u00e9es de mani\u00e8re \u00e0 ce qu\u2019on puisse confirmer que les traitements aux ph\u00e9romones affectent la capacit\u00e9 des <i>P. viteana</i> de trouver des femelles sexuellement r\u00e9ceptives et de s\u2019accoupler avec succ\u00e8s. Les r\u00e9sultats convaincants des traitements fond\u00e9s sur l\u2019installation de diffuseurs de ph\u00e9romones en p\u00e9riph\u00e9rie des vignobles permettraient d\u2019abaisser consid\u00e9rablement le co\u00fbt, pour les viticulteurs, de l\u2019utilisation de ph\u00e9romones contre le ravageur. Par exemple, pour un vignoble de 10 hectares mesurant 200 <abbr title=\"m\u00e8tres\">m</abbr> de largeur sur 500 <abbr title=\"m\u00e8tres\">m</abbr> de longueur, l\u2019installation de diffuseurs \u00e0 tous les 2,5 ou 5 <abbr title=\"m\u00e8tres\">m</abbr> autour de \u2018sou-parcelles\u2019 de 1 hectare dans le vignoble permettrait de r\u00e9duire de 78 et de 89 p. 100, respectivement, le nombre de diffuseurs requis par rapport \u00e0 la dose d\u2019application minimale recommand\u00e9e, soit 500 diffuseurs/hectare.</p>\n\n\n\r\n    "
    }
}