{
    "dcr_id": "1537375110541",
    "title": {
        "en": "Identification of efficient sprayer application tools to maximize onion thrips control",
        "fr": "D\u00e9termination d'outils de pulv\u00e9risation efficaces pour optimiser la lutte contre les thrips de l'oignon"
    },
    "modified": "2018-10-12",
    "issued": "2018-10-12",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1535122427752",
    "dc_date_created": "2018-10-12",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr07-350_1537375110541_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr07-350_1537375110541_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Identification of efficient sprayer application tools to maximize onion thrips control",
        "fr": "D\u00e9termination d'outils de pulv\u00e9risation efficaces pour optimiser la lutte contre les thrips de l'oignon"
    },
    "breadcrumb": {
        "en": "Identification of efficient sprayer application tools to maximize onion thrips control",
        "fr": "D\u00e9termination d'outils de pulv\u00e9risation efficaces pour optimiser la lutte contre les thrips de l'oignon"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "modified": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Identification of efficient sprayer application tools to maximize onion thrips control",
            "fr": "D\u00e9termination d'outils de pulv\u00e9risation efficaces pour optimiser la lutte contre les thrips de l'oignon"
        },
        "subject": {
            "en": "soil assessement;government information;awards and recognition",
            "fr": "volailles-transformation;cultures des c\u00e9r\u00e9ales;graines de l'alpiste des canaries"
        },
        "dcterms:subject": {
            "en": "businesses;architectural heritage;architecture",
            "fr": "m\u00e9dicament;engrais;p\u00e2turage"
        },
        "description": {
            "en": "By: Karen Croteau, Kevin Driscoll, Jim Melanson, Christine Vaillancourt, February 25, 2013. The model posits that a well-functioning regulatory system must be considered legitimate in the eyes of those regulated; otherwise they may seek avoidance rather than adherence to desired codes of behaviour. Canada Agricultural Products Standards Act. (which became the.",
            "fr": "Formats de rechange. ;. Donn\u00e9es historiques. 42\u00a0432. 41\u00a0217. Stocks de fin de campagne (kt). 12\u00a0591. p\u00a0: pr\u00e9vision d'Agriculture et Agroalimentaire Canada, sauf les donn\u00e9es sur la sperficie, le rendement et la production de 2016-2017 qui proviennent de Statistique Canada.Bl\u00e9. Bl\u00e9 dur."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": null,
            "fr": null
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "media release",
            "fr": "article"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Identification of efficient sprayer application tools to maximize onion thrips control",
        "fr": "D\u00e9termination d'outils de pulv\u00e9risation efficaces pour optimiser la lutte contre les thrips de l'oignon"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n     \n<h2>Project Code:  PRR07-350</h2>\n \n<h3>Project Lead</h3>\n<p><b>Jennifer Allen</b> - Ontario Ministry of Agriculture, Food and Rural Affairs</p>\n\n<h3>Objective</h3>\n<p><b>To develop and implement a plan for growers to reduce the number of sprays and amount of pesticide required to manage onion thrips in Ontario and Quebec onion production, and to make recommendations for reduced-risk control products to replace older chemistries</b></p>\n\n<h3>Summary of Results</h3>\n\n<h4>Background</h4>\n\n<p>Onion thrips, <i>Thrips tabaci Lindeman</i>, are economic pests of onion crops worldwide. During dry, hot months, crop loses could reach over 40%, forcing growers to make two to ten insecticide applications, depending on the season. Although individual thrips are easily killed with insecticides, problems are caused by heavily infested individual plants (local populations are able to grow exponentially in a very short period of time), rapid increases in field populations caused by pest immigration into fields, and the behaviour of the pests which tend to congregate deep within onion leaf axils. Studies have shown that only 1 to 5% of a given foliar insecticide reaches the intended target under field conditions.</p>\n\n<p>The purpose of this study is to improve the delivery of currently registered products to the inner onion leaf axils, as this is essential to maximizing insecticide efficacy. This is especially important with reduced-risk pesticides which possess narrower activity spectrums. This project aimed to establish spray parameters to maximize the delivery of insecticides to the target site by identifying the most effective (1) carrier volume, (2) surfactant, and (3) nozzle angle. The project also evaluated reduced-risk pesticides as potential candidates for future data generation and registration. Access to newer pest control products is not only important for economic and environmental reasons; it also helps to manage resistance, which has been documented in onion trips populations in North America.</p>\n \n<h4>Approaches</h4>\n\n<p>From 2007 to 2009, trials were conducted in organic soils of the Holland Marsh area of Ontario to evaluate different spray angles, surfactants, and water volumes for their ability to penetrate the crop canopy and cover the inner leaf axils. Two methods were used to assess spray coverage: water sensitive paper and an ultraviolet (UV) fluorescent dye. The water-sensitive paper was introduced to the plots before they were sprayed. Long paper rectangles were placed upright between the two youngest onion leaves of each plant and the fluorescent dye was dissolved in the spray tank. Plots were sprayed using a tractor-mounted sprayer with water volumes of 400, 500, and 600 litre (L) water per ha. Plots were sprayed with and without a surfactant (Sylgard 309) with nozzles at a 0\u00b0 or 22\u00b0 angle. Once dried, onions and water-sensitive paper were collected from each plot. The onions were photographed under a <abbr title=\"Ultraviolet\">UV</abbr> lamp and the papers were scanned. Images were then analysed using Assess (Image Analysis Software for Plant Disease Quantification) to determine the coverage percentage.</p>\n \n<h4>Results</h4>\n\n<p>Analysis of the water-sensitive paper revealed that in all years, regardless of nozzle angle, more spray reached the target site (inner leaf axils) when the solution was applied in greater amounts of water (500 and 600 L/ha). On these paper rectangles there were no differences in spray coverage between Sylgard 309 and water (that is, with or without surfactant). When nozzle angle, water volume, and surfactant were considered in combination, the top treatment for each year was applied at a 22\u00b0 angle. The most effective treatments were: Sylgard 309 at 600 L/ha (2007); water at 500 L/ha (2008); and water at 600 L/ha (2009).</p>\n\n<p>Analysis of actual onion leaf coverage revealed that, regardless of nozzle angle, the surfactant Sylgard 309 provided better coverage than water. Additionally, using 500 or 600 L/ha provided better coverage than using 400 L/ha. When combinations were compared, the top treatment for each year was applied with Sylgard 309 at the 22\u00b0 angle: 500 L/ha (2007); 600 L/ha (2008); and 600 L/ha (2009). These results demonstrate that all three factors (water volume, surfactant, and nozzle angle) affect spray coverage. The one factor that had the greatest impact was water volume.</p>\n\n<p>Depending on local conditions, a grower may not always be able to modify all three factors. In cases in which adding a surfactant is not feasible, increasing the amount of water as a carrier can help to improve delivery of the product to the target site. Conversely, when limitations exist regarding water volume output, adding a surfactant can increase coverage even if water volume is low.</p>\n\n<p>Spraying early in the morning or later in the evening (after sunset) is another helpful tip for maximizing thrips control. Dew in the morning will help the insecticide find its way into the leaf axils where thrips typically reside. Evening spraying, when temperatures have cooled down, helps to maximize efficacy of pyrethroid insecticides.</p>\n\n<p>The project also evaluated registered and non-registered reduced-risk insecticides for their efficacy in controlling onion thrips. Active ingredients that were found to be effective (spinetoram, cyantraniliprole) are currently being pursued for registration under Pesticide Minor Use Pesticides of <abbr title=\"Agriculture and Agri-Food Canada\">AAFC</abbr>.</p>\n\n<p>By maximizing the efficacy of insecticides, onion growers can reduce the number of applications per season and still maintain commercially acceptable levels of thrips control, benefiting growers (by reducing the cost of pest management activities), the environment, and human health - all of which are core goals of <abbr title=\"Agriculture and Agri-Food Canada\">AAFC</abbr>\u2019s Risk Reduction.</p>\n\n<p>The results of this project were communicated to growers and other target audiences through presentations in meetings during 2008 and 2009 (for example, Southwestern Agriculture Conference in Ridgetown, Ontario, in 2008; Scotia Horticulture Congress in Wolfville, Nova Scotia, in 2009; The Entomological Society of Canada and Manitoba Joint Annual Meeting in 2009), posters (for example, Ontario Fruit and Vegetable Convention, St. Catherines, Ontario, in 2008), reports, and publications. A factsheet to provide a more detailed account of results and recommendations is in preparation.</p>\n \n\r\n    ",
        "fr": "\r\n    <h2>Code de projet : PRR07-350</h2>\n\n<h3>Chef de projet</h3>\n<p><b>Jennifer Allen</b> - Minist\u00e8re de l'Agriculture, de l'Alimentation et des Affaires rurales de l'Ontario</p>\n\n<h2>Objectif</h2>\n\n<p><b>D\u00e9velopper et mettre en oeuvre un plan \u00e0 l'intention des producteurs visant \u00e0 r\u00e9duire le nombre de pulv\u00e9risations et la quantit\u00e9 de pesticides n\u00e9cessaire pour lutter contre les thrips de l'oignon dans les cultures d'oignons de l'Ontario et du Qu\u00e9bec et formuler des recommandations en vue de remplacer les anciens produits chimiques par des produits de lutte \u00e0 moindre risque</b></p>\n\n<h3>Sommaire de resultants</h3>\n\n<h4>Contexte</h4>\n\n<p>Le thrips de l\u2019oignon (<i>Thrips tabaci Lindeman</i>) est un parasite des cultures d\u2019oignon d\u2019importance \u00e9conomique dans toutes les r\u00e9gions du monde. Durant les mois secs et chauds, il peut d\u00e9truire plus de 40\u00a0% d\u2019une culture, ce qui oblige les producteurs \u00e0 faire deux \u00e0 dix applications d\u2019insecticide au cours de la saison. Bien que les insecticides homologu\u00e9s sont efficaces pour tuer l\u2019insecte, ils ne r\u00e8glent pas les probl\u00e8mes li\u00e9s \u00e0 une forte infestation localis\u00e9e (une population locale peut augmenter de fa\u00e7on exponentielle en tr\u00e8s peu de temps), \u00e0 l\u2019accroissement rapide des populations sous l\u2019effet de l\u2019immigration et au comportement de l\u2019insecte, qui cherche \u00e0 se loger au plus profond de l\u2019aisselle des feuilles. Des \u00e9tudes ont montr\u00e9 qu\u2019au champ seulement 1 \u00e0 5\u00a0% de la quantit\u00e9 d\u2019insecticide appliqu\u00e9e sur le feuillage atteint sa cible.</p>\n\n<p>La pr\u00e9sente \u00e9tude visait \u00e0 am\u00e9liorer les m\u00e9thodes d\u2019application des insecticides homologu\u00e9s de fa\u00e7on \u00e0 ce que le produit parvienne jusqu\u2019\u00e0 l\u2019aisselle des feuilles internes des oignons, condition indispensable pour obtenir une efficacit\u00e9 maximale. Il est particuli\u00e8rement important d\u2019optimiser l\u2019application dans le cas des pesticides \u00e0 risque r\u00e9duit, car leur spectre d\u2019activit\u00e9 est plus \u00e9troit. L\u2019\u00e9tude visait plus pr\u00e9cis\u00e9ment \u00e0 d\u00e9terminer les param\u00e8tres d\u2019application permettant de maximiser la quantit\u00e9 d\u2019insecticide atteignant la cible, notamment de d\u00e9terminer le volume de v\u00e9hiculant, le tensioactif et l\u2019angle de r\u00e9glage de la buse les plus efficaces. Des pesticides \u00e0 risque r\u00e9duit ont \u00e9galement \u00e9t\u00e9 \u00e9valu\u00e9s \u00e0 des fins de collecte de donn\u00e9es et d\u2019une \u00e9ventuelle homologation. Il est important d\u2019avoir acc\u00e8s \u00e0 de nouveaux pesticides non seulement pour des raisons \u00e9conomiques et \u00e9cologiques, mais aussi pour faire \u00e9chec \u00e0 la r\u00e9sistance aux pesticides acquise par le thrips de l\u2019oignon dans certaines localit\u00e9s d\u2019Am\u00e9rique du Nord.</p>\n \n<h4>M\u00e9thode</h4>\n\n<p>De 2007 \u00e0 2009, des essais ont \u00e9t\u00e9 r\u00e9alis\u00e9s sur des sols organiques de la r\u00e9gion de Holland Marsh, en Ontario, afin d\u2019\u00e9valuer diff\u00e9rents angles de pulv\u00e9risation, produits tensioactifs et volumes d\u2019eau quant \u00e0 la capacit\u00e9 de p\u00e9n\u00e9trer \u00e0 l\u2019int\u00e9rieur du feuillage et d\u2019atteindre l\u2019aisselle des feuilles int\u00e9rieures. Deux outils ont \u00e9t\u00e9 utilis\u00e9s pour \u00e9valuer la surface couverte : un papier hydrophile et un colorant \u00e0 fluorescence ultraviolette (UV). Le papier hydrophile a \u00e9t\u00e9 mis en place avant la pulv\u00e9risation des parcelles. De grands rectangles de papier ont \u00e9t\u00e9 plac\u00e9s en position verticale entre les deux plus jeunes feuilles de chaque plante. Le colorant fluorescent a \u00e9t\u00e9 dissous dans le r\u00e9servoir de solution \u00e0 pulv\u00e9riser. Les parcelles ont \u00e9t\u00e9 pulv\u00e9ris\u00e9es au moyen d\u2019un pulv\u00e9risateur port\u00e9 sur tracteur, \u00e0 raison de 400, 500 ou 600 litres (L) d\u2019eau par hectare, avec ou sans agent tensioactif (Sylgard 309) et avec la buse r\u00e9gl\u00e9e \u00e0 un angle de 0\u00b0 ou de 22\u00b0. Apr\u00e8s un temps de s\u00e9chage, les papiers et les oignons ont \u00e9t\u00e9 recueillis. Les oignons ont \u00e9t\u00e9 photographi\u00e9s sous \u00e9clairage <abbr title=\"Ultraviolet\">UV</abbr>, et les papiers ont \u00e9t\u00e9 scann\u00e9s. Les images ont \u00e9t\u00e9 analys\u00e9es \u00e0 l\u2019aide du logiciel ASSESS (<span lang=\"en\">Image Analysis Software for Plant Disease Quantification</span>) afin de d\u00e9terminer le pourcentage de surface couverte.</p>\n \n<h3>R\u00e9sultats</h3>\n\n<p>L\u2019examen des papiers hydrophiles a r\u00e9v\u00e9l\u00e9 que la quantit\u00e9 de solution atteignant la cible (aisselle des feuilles internes) augmentait avec l\u2019augmentation de la quantit\u00e9 d\u2019eau utilis\u00e9e (500 et 600 L/ha), ind\u00e9pendamment de l\u2019angle de pulv\u00e9risation, et cela chaque ann\u00e9e du projet. L\u2019ajout d\u2019un agent tensioactif (Sylgard 309) \u00e0 la solution ne modifiait pas la surface couverte. Chaque ann\u00e9e, les meilleurs r\u00e9sultats ont \u00e9t\u00e9 obtenus avec la pulv\u00e9risation sous un angle de 22\u00b0. Les traitements les plus efficaces ont \u00e9t\u00e9 les suivants\u00a0: Sylgard 309 avec eau \u00e0 raison de 600 L/ha (2007); eau seule \u00e0 raison de 500 L/ha (2008); eau seule \u00e0 raison de 600 L/ha (2009).</p>\n\n<p>L\u2019examen des feuilles d\u2019oignons a r\u00e9v\u00e9l\u00e9 que la solution contenant le tensioactif Sylgard 309 \u00e9tait plus efficace que la solution sans agent tensioactif, ind\u00e9pendamment de l\u2019angle de pulv\u00e9risation, et que l\u2019eau \u00e9tait plus efficace au volume de 500 ou de 600 L/ha qu\u2019au volume de 400 L/ha. Chaque ann\u00e9e, les meilleurs r\u00e9sultats ont \u00e9t\u00e9 obtenus avec les solutions contenant du Sylgard 309 pulv\u00e9ris\u00e9es sous un angle de 22\u00b0, avec un volume d\u2019eau de 500 L/ha (2007), de 600 L/ha (2008) et de 600 L/ha (2009).</p>\n\n<p>Ces r\u00e9sultats montrent que les trois param\u00e8tres (volume d\u2019eau, agent tensioactif et angle de pulv\u00e9risation) ont une incidence sur la surface couverte. Le volume d\u2019eau est le facteur qui a eu l\u2019effet le plus marqu\u00e9 sur l\u2019efficacit\u00e9 de l\u2019application.</p>\n\n<p>Les conditions locales ne permettent pas toujours d\u2019agir sur les trois param\u00e8tres. Dans les cas o\u00f9 il n\u2019est pas possible d\u2019utiliser un agent tensioactif, il peut s\u2019av\u00e9rer utile d\u2019augmenter le volume d\u2019eau. \u00c0 l\u2019inverse, dans les cas o\u00f9 la quantit\u00e9 d\u2019eau qui peut \u00eatre utilis\u00e9e est limit\u00e9e, l\u2019ajout d\u2019un agent tensioactif peut augmenter l\u2019efficacit\u00e9 de la pulv\u00e9risation, m\u00eame avec un faible volume d\u2019eau.</p>\n\n<p>Par ailleurs, il est avantageux de pulv\u00e9riser l\u2019insecticide t\u00f4t le matin ou tard le soir (apr\u00e8s le coucher du soleil). Le matin, la ros\u00e9e facilite la p\u00e9n\u00e9tration de la solution jusqu\u2019\u00e0 l\u2019aisselle des feuilles, o\u00f9 les thrips ont tendance \u00e0 se loger. Le soir, les temp\u00e9ratures fra\u00eeches permettent d\u2019obtenir le maximum d\u2019efficacit\u00e9 des insecticides \u00e0 base de pyr\u00e9thro\u00efdes.</p>\n\n<p>Des insecticides \u00e0 risque r\u00e9duit homologu\u00e9s et non homologu\u00e9s ont \u00e9galement \u00e9t\u00e9 \u00e9valu\u00e9s quant \u00e0 leur efficacit\u00e9 contre le thrips de l\u2019oignon. Le processus d\u2019homologation dans le cadre des Pesticides \u00e0 usage limit\u00e9 d\u2019<abbr title=\"Agriculture et Agroalimentaire Canada\">AAC</abbr> a \u00e9t\u00e9 mis en marche pour les mati\u00e8res actives qui se sont av\u00e9r\u00e9es efficaces (spin\u00e9torame, cyantraniliprole).</p>\n<p>S\u2019ils ont acc\u00e8s \u00e0 des insecticides plus efficaces, les producteurs d\u2019oignon pourront r\u00e9duire le nombre d\u2019applications par saison tout en maintenant les populations de thrips sous un seuil commercialement acceptable, dans leur propre int\u00e9r\u00eat (r\u00e9duction des co\u00fbts li\u00e9s \u00e0 la protection des cultures) comme dans l\u2019int\u00e9r\u00eat de l\u2019environnement et de la sant\u00e9 humaine, autant d\u2019objectifs vis\u00e9s par la R\u00e9duction des risques li\u00e9s aux pesticides.</p>\n\n<p>Les r\u00e9sultats du pr\u00e9sent projet ont \u00e9t\u00e9 communiqu\u00e9s aux producteurs et autres int\u00e9ress\u00e9s sous forme d\u2019expos\u00e9s lors de r\u00e9unions tenues en 2008 et 2009 (Southwestern Agriculture Conference, tenue \u00e0 Ridgetown, en Ontario, en 2008; Scotia Horticulture Congress, tenu \u00e0 Wolfville, en Nouvelle-\u00c9cosse, en 2009; r\u00e9union annuelle commune de la Soci\u00e9t\u00e9 d\u2019entomologie du Canada et de l\u2019Entomological Society of Manitoba, tenue en 2009), ainsi que sous forme d\u2019affiches (conf\u00e9rence des fruiticulteurs et des mara\u00eechers de l\u2019Ontario, tenue \u00e0 St. Catherines, en Ontario, en 2008), de rapports et de publications. Une fiche d\u2019information d\u00e9crivant les r\u00e9sultats de fa\u00e7on plus d\u00e9taill\u00e9e et pr\u00e9sentant des recommandations est en pr\u00e9paration.</p>\n \n\r\n    "
    }
}