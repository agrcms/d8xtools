{
    "dcr_id": "1537459349908",
    "title": {
        "en": "Refine and validate economic threshold for Lygus bugs in canola production in Alberta",
        "fr": "Pr\u00e9ciser et valider le seuil \u00e9conomique des punaises du genre Lygus pour la production de canola en Alberta"
    },
    "modified": "2018-10-12",
    "issued": "2018-10-12",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1535122427752",
    "dc_date_created": "2018-10-12",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr12-030_1537459349908_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr12-030_1537459349908_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Refine and validate economic threshold for Lygus bugs in canola production in Alberta",
        "fr": "Pr\u00e9ciser et valider le seuil \u00e9conomique des punaises du genre Lygus pour la production de canola en Alberta"
    },
    "breadcrumb": {
        "en": "Refine and validate economic threshold for Lygus bugs in canola production in Alberta",
        "fr": "Pr\u00e9ciser et valider le seuil \u00e9conomique des punaises du genre Lygus pour la production de canola en Alberta"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "modified": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Refine and validate economic threshold for Lygus bugs in canola production in Alberta",
            "fr": "Pr\u00e9ciser et valider le seuil \u00e9conomique des punaises du genre Lygus pour la production de canola en Alberta"
        },
        "subject": {
            "en": "technology transfer;architectural heritage;soil assessement",
            "fr": "betteraves \u00e0 sucre;l\u00e9gumes;l\u00e9gumineuses"
        },
        "dcterms:subject": {
            "en": "statistics;architectural heritage;architecture",
            "fr": "m\u00e9dicament;sol;changement climatique"
        },
        "description": {
            "en": "Alternative Formats. International Standard Serial Number: 2292-1524. Sub-Program 2.1.6: Federal-Provincial-Territorial Cost-shared Environment. Agriculture and Agri-Food Canada provides leadership in the growth and development of a competitive, innovative and sustainable Canadian agriculture and agri-food sector.Responsibilities.",
            "fr": "Le Centre de recherche et de d\u00e9veloppement de Lethbridge (CRD Lethbridge) appuie les activit\u00e9s innovatrices de recherche, de d\u00e9veloppement et de transfert des technologies et des connaissances recens\u00e9es dans sept des neuf. D\u00e9finition de l'empreinte \u00e9cologique de la production canadienne de b\u0153uf. Identification des nouveaux g\u00e8nes dans l'agropyre interm\u00e9diaire domestiqu\u00e9 pour am\u00e9liorer la r\u00e9sistance au stress biotique."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": null,
            "fr": "agroalimentaire.\u00e9tablissons"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "sound",
            "fr": "avis"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Refine and validate economic threshold for Lygus bugs in canola production in Alberta",
        "fr": "Pr\u00e9ciser et valider le seuil \u00e9conomique des punaises du genre Lygus pour la production de canola en Alberta"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <h2>Project Code: PRR12-030</h2>\n<h3>Project Lead</h3>\n<p><b>Hector Carcamo</b> - Agriculture and Agri-Food Canada</p>\n<h3>Objective</h3>\n<p><b>To make available to growers an improved economic threshold, as part of a spray decision support system for Lygus control in canola crops in Alberta</b></p>\n\n<h3>Summary of Results</h3>\n\n<h4>Background</h4>\n\n<p>Lygus bugs (<i>Lygus spp</i>.) are a serious threat to many oilseed and forage crops in the Canadian prairies, including canola. Once considered an intermittent pest of canola in Alberta, the frequency of Lygus bug outbreaks has increased over the last decade. Insecticide sprays are commonly used to control Lygus bugs in canola crops. </p>\n\n<p>In Northern Alberta, growers are particularly concerned about Lygus damage at the early bud stage and thus, may add insecticide to their last herbicide spray. In southern Alberta, some growers are spraying at the late pod stage. Economic thresholds for the early pod stage were developed in Manitoba in the mid-1990s and were based on conventional canola varieties. However, since then a number of new hybrids, including herbicide-tolerant cultivars with superior agronomic traits have entered the market and been adopted extensively. Managing Lygus across regions is complicated by the distinct climatic conditions in the northern Peace Region and southern Alberta, as well as by the differences in Lygus species present and their life cycles relative to crop growth. This project aimed to refine and validate the current Lygus economic thresholds in the context of currently used canola cultivars to reflect specific environmental, biological, and climatic conditions across Alberta. The goal was to enhance Lygus management strategies and reduce unnecessary pesticide use. </p>\n<h4>Approaches</h4>\n<p>On-farm trials conducted in southern Alberta during 2012 and 2013 assessed the need to spray for Lygus control at early, mid and late pod stages. A total of 10 canola fields was included over the two years of the study. To compare yield benefits, growers applied a pyrethroid insecticide during different pod stages and left large control plots unsprayed.</p>\n<p>Cage studies were used in both southern (Lethbridge) and northern (Beaverlodge) Alberta to validate economic thresholds for Lygus in canola. Individual cages were infested with varying numbers (4, 10, 20, 50, or 80) of Lygus alone, or as a mix of 10 Lygus and 10 cabbage seedpod weevil. Cage studies in Beaverlodge used two newer cultivars of canola (RR7345, L150) and the older cultivar Westar to determine how these varieties respond to feeding from two species of Lygus (<i>Lygus lineolaris</i> and <i>Lygus keltoni</i>). Cages were infested with Lygus at bolting stage and removed for analysis at harvest. </p>\n<h4>Results</h4>\n<p>The two growing seasons were very different in terms of moisture and temperature during the canola flowering periods. Dry and hot conditions in 2012 were conducive to large Lygus populations and poor seedset. In 2013, conditions were humid and cool, favouring seed production and reduced Lygus populations. </p>\n<p>In on-farm trials, three fields sprayed at early pod when Lygus abundance was higher than 5 per sweep showed a yield increase of about 222 kilograms per hectare (kg/ha). However, there also appeared to be a yield penalty associated with trampling the plants while spraying insecticide. At the other sites, Lygus were less abundant and yield responses to insecticide spraying were highly variable and did not differ significantly, especially when the crops were irrigated. In two dryland fields sprayed at early pod (soon after the end of flowering) there was a trend towards higher yield in sprayed strips than in those unsprayed when Lygus density was around 5 per sweep. Trampling by the sprayer appeared to reduce yields by 55-110 (<abbr title=\"Kilograms per hectare\">kg/ha</abbr>), so the net benefit of spraying is expected to be lower.</p>\n<p>In 2012 cage studies in southern Alberta, Lygus numbers were very high inside the cages, reaching over 1000 per cage in the 50 and 80 infestation level treatments. Yields in infested cages were lower than both the control cages with no Lygus and the uncaged plots, and showed very high variability between treatments. The control cages that received no insect inoculation had the highest yield of 1,162 <abbr title=\"Kilograms per hectare\">kg/ha</abbr>. These yields are about 13<abbr title=\"percent\">%</abbr> lower than the commercial farm average of 1,500 <abbr title=\"Kilograms per hectare\">kg/ha</abbr> for 2012, likely the result of late seeding. Only the two treatments with 50 and 80 Lygus per cage had significantly lower yield than the control cages. The yield in cages with the highest Lygus treatment of 80 Lygus/cage was 730 <abbr title=\"Kilograms per hectare\">kg/ha</abbr>, and 44<abbr title=\"percent\">%</abbr> lower than the control cages. In 2013, a wet and cool year, there were no treatment effects. These results suggest that growers should not reduce economic thresholds below 1 per sweep as suggested in the current table of economic threshold when canola prices are very high (over <abbr title=\"dollars\">$</abbr>600/tonne). </p>\n<p>The cage studies in northern Alberta also support the hypothesis that moderate Lygus bug densities (20 Lygus per meter squared or less than 1 per sweep) do not affect canola yield regardless of the cultivar. At harvest time, Lygus numbers averaged 50-100 per cage for the three canola cultivars, regardless of infestation densities at bud time. Abundances of 100 per cage would translate to about 5 Lygus per sweep. A regression analysis showed no relationship between Lygus density, canola cultivar and yield. The newer cultivars (RR7345 and L150) had higher yield than the older conventional cultivar (Westar), but there was no indication that they were affected differentially by Lygus feeding. </p>\n<p>Some preliminary recommendations can be made from these studies about the need to spray for Lygus at the pod stage. The results suggest that irrigated fields are unlikely to suffer from Lygus damage, despite high abundance of Lygus bugs, and growers can consider not spraying irrigated fields once the pods are ripening. Generally, growers are advised not to spray when the crop is at an advanced pod stage (8-10 days from swathing) unless Lygus bugs are clearly at high densities (greater than or equal to 5 per sweep). However, further research is needed to more accurately determine the need to spray for Lygus at this stage. </p>\n<p>Following completion of this project, the cage studies will continue for two more years (through March 2016) with support from the Alberta Crop Industry Development Fund, the Alberta Canola Producers Commission and the Alberta Pulse Growers Commission. </p>\n<p>For more details about this project please contact <a href=\"mailto:Hector.Carcamo@AGR.GC.CA\">Hector Carcamo</a>, Ph.D. </p>\n\r\n    ",
        "fr": "\r\n    <h2>Code de Projet: PRR12-030</h2>\n<h3>Chef de projet</h3>\n<p><b>Hector Carcamo</b>  - Agriculture et Agroalimentaire Canada</p>\n<h3>Objectif</h3>\n<p><b>Offrir aux producteurs de canola de l\u2019Alberta un meilleur seuil \u00e9conomique pour les punaises du genre Lygus dans le cadre du syst\u00e8me d\u2019aide \u00e0 la d\u00e9cision servant aux pulv\u00e9risations contre ces insectes</b></p>\n<h3>Sommaire de r\u00e9sultats</h3>\n<h4>Contexte</h4>\n<p>Dans les Prairies canadiennes, les punaises du genre Lygus (<i>Lygus spp</i>.) repr\u00e9sentent une grave menace pour de nombreuses cultures ol\u00e9agineuses et fourrag\u00e8res, dont le canola. Encore r\u00e9cemment, les infestations dans les cultures de canola survenaient de fa\u00e7on intermittente, mais leur fr\u00e9quence s\u2019est accrue au cours de la derni\u00e8re d\u00e9cennie. Les producteurs effectuent couramment des \u00e9pandages d\u2019insecticides dans leurs champs de canola pour lutter contre ces ravageurs.</p>\n<p>Dans le nord de l\u2019Alberta, les producteurs sont particuli\u00e8rement pr\u00e9occup\u00e9s par les dommages inflig\u00e9s par les punaises du genre Lygus au d\u00e9but du d\u00e9veloppement des boutons floraux et peuvent d\u00e8s lors choisir d\u2019ajouter un insecticide \u00e0 leur derni\u00e8re pulv\u00e9risation d\u2019herbicide. Dans le sud de l\u2019Alberta, certains producteurs effectuent une pulv\u00e9risation au stade avanc\u00e9 de d\u00e9veloppement des siliques. Des seuils \u00e9conomiques applicables aux stades pr\u00e9coces de d\u00e9veloppement des siliques ont \u00e9t\u00e9 \u00e9labor\u00e9s au Manitoba au milieu des ann\u00e9es 1990 pour les cultivars classiques de canola utilis\u00e9s \u00e0 l\u2019\u00e9poque. Depuis, un certain nombre de nouveaux cultivars hybrides, dont des cultivars pr\u00e9sentant une meilleure tol\u00e9rance aux herbicides et des caract\u00e9ristiques agronomiques am\u00e9lior\u00e9es, ont fait leur apparition sur le march\u00e9 et ont \u00e9t\u00e9 largement adopt\u00e9s par les producteurs. La lutte contre les punaises du genre Lygus au niveau r\u00e9gional est donc compliqu\u00e9e par les diff\u00e9rences climatique entre la r\u00e9gion de la rivi\u00e8re de la Paix au nord et le sud de l\u2019Alberta et entre les diff\u00e9rentes esp\u00e8ces de punaises pr\u00e9sentes et leur d\u00e9veloppement par rapport \u00e0 la ph\u00e9nologie du canola. Le pr\u00e9sent projet vise \u00e0 pr\u00e9ciser et \u00e0 valider les seuils \u00e9conomiques actuellement employ\u00e9s contre les punaises du genre Lygus de mani\u00e8re \u00e0 mieux refl\u00e9ter les conditions environnementales, biologiques et climatiques particuli\u00e8res des diff\u00e9rentes r\u00e9gions de l\u2019Alberta et les propri\u00e9t\u00e9s des cultivars de canola qui sont utilis\u00e9s actuellement. La validation de ces seuils a pour but d\u2019optimiser l\u2019efficacit\u00e9 des strat\u00e9gies de lutte contre les punaises du genre Lygus et de r\u00e9duire le recours non justifi\u00e9 aux pesticides.</p>\n<h4>Approches</h4>\n<p>Dans le cadre d\u2019essais \u00e0 la ferme men\u00e9s dans le sud de l\u2019Alberta en 2012 et en 2013, nous avons \u00e9valu\u00e9 la n\u00e9cessit\u00e9 de traiter les cultures de canola contre les punaises du genre Lygus aux stades pr\u00e9coce, interm\u00e9diaire et avanc\u00e9 du d\u00e9veloppement des siliques. Au total, 10 champs de canola ont \u00e9t\u00e9 utilis\u00e9s dans le cadre de cette \u00e9tude d\u2019une dur\u00e9e de deux ans. Pour comparer les gains de rendement, les producteurs ont appliqu\u00e9 un insecticide \u00e0 base de pyr\u00e9thro\u00efdes \u00e0 divers stades du d\u00e9veloppement des siliques et laiss\u00e9 de grandes parcelles de canola non trait\u00e9es.</p>\n<p>Nous avons \u00e9galement proc\u00e9d\u00e9 \u00e0 des essais en cage dans le sud (Lethbridge) et le nord (Beaverlodge) de l\u2019Alberta afin de valider les seuils \u00e9conomiques employ\u00e9s contre les punaises du genre Lygus dans les cultures de canola. Chacune des cages contenait un nombre pr\u00e9\u00e9tabli de punaises (4, 10, 20, 50 ou 80) ou un m\u00e9lange de 10 punaises et de 10 charan\u00e7ons de la silique. \u00c0 Beaverlodge, nous avons utilis\u00e9 deux nouveaux cultivars de canola (RR7345, L150) et un cultivar classique (Westar) afin de comparer leurs r\u00e9actions aux attaques de deux esp\u00e8ces de punaises du genre Lygus (<i>Lygus lineolaris</i> et <i>Lygus keltoni</i>). Les cultivars ont \u00e9t\u00e9 expos\u00e9s aux punaises au moment de la mont\u00e9e en graines et retir\u00e9s des cages aux fins des analyses au moment de la r\u00e9colte.</p>\n<h4>R\u00e9sultats</h4>\n<p>D\u2019importantes diff\u00e9rences li\u00e9es aux conditions d\u2019humidit\u00e9 et de temp\u00e9rature ont \u00e9t\u00e9 not\u00e9es entre les deux saisons de croissance durant la p\u00e9riode de floraison du canola. En 2012, le temps sec et chaud a favoris\u00e9 la croissance des populations des punaises et r\u00e9duit le taux de grenaison. En 2013, les conditions fra\u00eeches et humides ont eu l\u2019effet inverse.</p>\n<p>Lors des essais \u00e0 la ferme, un gain de rendement d\u2019environ 222 kilogrammes par hectare (kg/ha) a \u00e9t\u00e9 not\u00e9 dans les trois champs trait\u00e9s au stade pr\u00e9coce du d\u00e9veloppement des siliques alors que l\u2019abondance des punaises \u00e9tait sup\u00e9rieure \u00e0 5 individus par coup de filet fauchoir. Toutefois, le pi\u00e9tinement des plants occasionn\u00e9 par le pulv\u00e9risateur semble avoir caus\u00e9 une certaine perte de rendement. Dans les autres champs, les punaises \u00e9taient moins abondantes, et les impacts des traitements sur le rendement se sont r\u00e9v\u00e9l\u00e9s tr\u00e8s variables mais non significativement diff\u00e9rents, en particulier lorsque les champs \u00e9taient irrigu\u00e9s. Dans deux champs plus secs trait\u00e9s au stade pr\u00e9coce du d\u00e9veloppement des siliques (peu apr\u00e8s la fin de la floraison), le rendement \u00e9tait g\u00e9n\u00e9ralement plus \u00e9lev\u00e9 dans les bandes trait\u00e9es que dans les bandes non trait\u00e9es lorsque l\u2019abondance des punaises s\u2019\u00e9tablissait \u00e0 environ 5 individus par coup de filet fauchoir. Le pi\u00e9tinement caus\u00e9 par le pulv\u00e9risateur a toutefois sembl\u00e9 causer un fl\u00e9chissement des rendements d\u2019environ 55 \u00e0 110 <abbr title=\"kilogrammes par hectare\">kg/ha</abbr>. L\u2019avantage net du traitement est donc vraisemblablement plus faible.</p>\n<p>Lors des essais en cage r\u00e9alis\u00e9s en 2012 dans le sud de l\u2019Alberta, les punaises \u00e9taient tr\u00e8s abondantes dans les cages, leur nombre d\u00e9passant m\u00eame 1 000 punaises par cage dans le cas des cages infest\u00e9es initialement avec 50 ou 80 punaises. Les rendements \u00e9taient plus faibles dans les cages infest\u00e9es que dans les cages t\u00e9moins (ne contenant aucune punaise) ou les parcelles non encag\u00e9es et variaient consid\u00e9rablement d\u2019un traitement \u00e0 l\u2019autre. Le rendement le plus \u00e9lev\u00e9, 1 162 <abbr title=\"kilogrammes par hectare\">kg/ha</abbr>, a \u00e9t\u00e9 enregistr\u00e9 dans les cages t\u00e9moins. Cette valeur est inf\u00e9rieure d\u2019environ 13 <abbr title=\"pour cent \">%</abbr> au rendement moyen obtenu par les exploitations commerciales, estim\u00e9 \u00e0 1 500 <abbr title=\"kilogrammes par hectare\">kg/ha</abbr> en 2012. Le fait que l\u2019ensemencement ait \u00e9t\u00e9 effectu\u00e9 tardivement explique probablement cet \u00e9cart. Des rendements significativement inf\u00e9rieurs \u00e0 ceux obtenus dans les cages t\u00e9moins ont \u00e9t\u00e9 observ\u00e9s uniquement dans les cages infest\u00e9es initialement avec 50 ou 80 punaises. Le rendement enregistr\u00e9 dans les cages infest\u00e9es initialement avec 80 punaises s\u2019\u00e9tablissait \u00e0 730 <abbr title=\"kilogrammes par hectare\">kg/ha</abbr> et \u00e9tait inf\u00e9rieur de 44 <abbr title=\"pour cent \">%</abbr> au rendement atteint dans les cages t\u00e9moins. En 2013, ann\u00e9e fra\u00eeche et humide, aucun effet li\u00e9 aux traitements n\u2019a \u00e9t\u00e9 not\u00e9. Ces r\u00e9sultats portent \u00e0 croire que les producteurs ne devraient pas appliquer des seuils \u00e9conomiques inf\u00e9rieurs \u00e0 1 punaise par coup de filet fauchoir tel qu\u2019indiqu\u00e9 dans le tableau des seuils \u00e9conomiques en vigueur lorsque le prix du canola est tr\u00e8s \u00e9lev\u00e9 (plus de 600 dollars la tonne).</p>\n<p>Les essais en cage r\u00e9alis\u00e9s dans le nord de l\u2019Alberta confortent \u00e9galement l\u2019hypoth\u00e8se selon laquelle la pr\u00e9sence de densit\u00e9s mod\u00e9r\u00e9es de punaises du genre Lygus (20 punaises par m\u00e8tre carr\u00e9 ou moins de 1 punaise par coup de filet fauchoir) n\u2019a aucun effet sur le rendement du canola, quel que soit le cultivar utilis\u00e9. Au moment de la r\u00e9colte, le nombre moyen de punaises par cage oscillait entre 50 et 100 individus chez les trois cultivars de canola test\u00e9s, quelles qu\u2019aient \u00e9t\u00e9 les densit\u00e9s au moment du d\u00e9veloppement des boutons floraux. Une abondance de 100 punaises par cage \u00e9quivaut approximativement \u00e0 5 punaises par coup de filet fauchoir. Une analyse de r\u00e9gression n\u2019a r\u00e9v\u00e9l\u00e9 aucun lien entre la densit\u00e9 des punaises, le cultivar de canola utilis\u00e9 et le rendement obtenu. Les nouveaux cultivars (RR7345 et L150) ont affich\u00e9 un rendement sup\u00e9rieur \u00e0 celui du cultivar classique plus ancien (Westar), mais nous n\u2019avons relev\u00e9 aucun signe permettant de croire que ces trois cultivars r\u00e9agissaient diff\u00e9remment aux attaques des punaises.</p>\n<p>\u00c0 la lumi\u00e8re de ces r\u00e9sultats, nous pouvons \u00e9noncer un certain nombre de recommandations pr\u00e9liminaires relatives \u00e0 la lutte contre les punaises du genre Lygus au stade du d\u00e9veloppement des siliques. Nos r\u00e9sultats donnent \u00e0 croire que l\u2019irrigation des champs r\u00e9duit consid\u00e9rablement la menace pos\u00e9e par les punaises du genre Lygus m\u00eame si celles-ci sont pr\u00e9sentes en forte densit\u00e9 et que les producteurs peuvent envisager de ne pas traiter les champs irrigu\u00e9s une fois la maturation des siliques amorc\u00e9e. En g\u00e9n\u00e9ral, on conseille aux producteurs de ne pas effectuer de traitements au stade avanc\u00e9 du d\u00e9veloppement des siliques (8 \u00e0 10 jours avant l\u2019andainage), \u00e0 moins que la densit\u00e9 des populations de punaises du genre Lygus soit \u00e9lev\u00e9e (nombre sup\u00e9rieur ou \u00e9gal \u00e0 5 punaises par coup de filet fauchoir). De plus amples recherches s\u2019imposent toutefois pour pr\u00e9ciser les seuils \u00e0 partir desquels il convient d\u2019intervenir \u00e0 ce stade du d\u00e9veloppement du canola contre les punaises du genre Lygus.</p>\n<p>Une fois cette \u00e9tude termin\u00e9e, nous projetons de poursuivre les essais en cage au cours de deux autres ann\u00e9es (jusqu\u2019en mars 2016), avec le soutien financier de l\u2019Alberta Crop Industry Development Fund, de l\u2019Alberta Canola Producers Commission et de l\u2019Alberta Pulse Growers Commission.</p>\n<p>Pour de plus amples renseignements sur ce projet, veuillez contacter <a href=\"mailto:Hector.Carcamo@AGR.GC.CA\">Hector Carcamo</a>, Ph.D. </p>\n\r\n    "
    }
}