{
    "dcr_id": "1384971854425",
    "title": {
        "en": "Storage stocks background information",
        "fr": "Inventaire de produits avicoles et d\u2019\u0153ufs en entrep\u00f4t - contexte"
    },
    "modified": "2021-06-24",
    "issued": "2009-01-30",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1384971799968",
    "dc_date_created": "2009-01-30",
    "managing_branch": "MISB",
    "parent_node_id": "",
    "short_title": "Background information",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/MISB-DGSIM/SDAD-DDAS/PE-VO/WORKAREA/PE-VO/templatedata/comm-comm/gene-gene/data/storage_stocks_background_information_1384971854425_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/MISB-DGSIM/SDAD-DDAS/PE-VO/WORKAREA/PE-VO/templatedata/comm-comm/gene-gene/data/storage_stocks_background_information_1384971854425_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Background information",
        "fr": "Contexte"
    },
    "breadcrumb": {
        "en": "Background information",
        "fr": "Contexte"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2009-01-30",
            "fr": "2003-02-24"
        },
        "modified": {
            "en": "2021-06-24",
            "fr": "2021-06-24"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Storage stocks background information",
            "fr": "Inventaire de produits avicoles et d\u2019\u0153ufs en entrep\u00f4t - contexte"
        },
        "subject": {
            "en": "aafcsubject",
            "fr": "aafcsubject"
        },
        "dcterms:subject": {
            "en": "architectural heritage;architecture;statistics",
            "fr": "m\u00e9dicament;environnement;bact\u00e9rie"
        },
        "description": {
            "en": "All Public Cold Storage Warehouses in Canada are required by law to submit to Statistics Canada an inventory of their frozen poultry and egg product stored on their premises on the first full business day of each month. This is not a cumulative total for the entire month, but only what is residing in their storage facility on that day.",
            "fr": "Tous les entrep\u00f4ts frigorifiques publics au Canada sont tenus par la loi de soumettre \u00e0 Statistique Canada un relev\u00e9 des stocks de produits avicoles et d'oeufs congel\u00e9s qui se trouvent dans leurs entrep\u00f4ts au premier jour ouvrable complet de chaque mois. Il ne s'agit pas d'un total cumul\u00e9 du mois entier, mais du total des produits dans leurs entrep\u00f4ts ce jour l\u00e0."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Inventory Edible Dried Egg Products ;Inventory Statement of Frozen Eggs and Poultry Products;Public Cold Storage Warehouses",
            "fr": "D\u00e9claration des produits d'oeufs en poudre comestibles;Relev\u00e9 des stocks de produits avicoles et d'oeufs congel\u00e9s;entrep\u00f4ts frigorifiques publics"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "notice",
            "fr": "formulaire"
        },
        "creator": {
            "en": "Market and Industry Services Branch;Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Direction g\u00e9n\u00e9rale des services \u00e0 l'industrie et aux march\u00e9s;Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Storage stocks background information",
        "fr": "Inventaire de produits avicoles et d\u2019\u0153ufs en entrep\u00f4t - contexte"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n                                          \n<p>All cold storage warehouses in Canada are required to submit to Agriculture and Agri-food Canada (AAFC) an inventory of its frozen poultry and egg product stored on premises on the first full business day of each month. The inventory reflects what is residing in the warehouse on that day; it is not a cumulative total for the entire month.</p>\n\n<p>The collection of poultry and egg storage stocks information is a jointly operated program within the Federal Government of Canada. Poultry and Egg storage stocks data is shared between Agriculture and Agri-Food Canada and Statistics Canada. </p>\n\n<p>Storage stocks information is a central piece of information for the supply managed sectors such as chicken, turkey and egg, when setting poultry quota/allocation. This information is also an important industry indicator, and is used by governments, industry organizations, processors and retailers in evaluating programs and making policy and business decisions. The quality of the data is dependent on the completeness of the information received.</p>\n\n<p>AAFC in collaboration with Statistics Canada issue forms for collection.  AAFC is responsible to collect the data; final monthly reports are published by both AAFC and Statistics Canada.</p>\n\n<h2>Collection process in more detail</h2>\n<ul>\n<li>Around the middle of the month, Statistics Canada sends out storage stocks forms to each cold storage warehouse storing egg products and/or poultry products in Canada requiring a paper copy of the form; AAFC sends electronic versions. </li>\n<li>All forms are returned to AAFC\u2019s Animal Industry Division. AAFC undertakes follow up as required. </li>\n<li>Cold storage warehouses complete the form with the actual number of kilograms (not pounds) of poultry and egg product residing in its warehouses on the first full business day of the month. Instructions and definitions of content are included with the form.\n<ul>\n<li>The form used by cold storage warehouses is A6050_E <a href=\"/resources/prod/Internet-Internet/MISB-DGSIM/SDAD-DDAS/PE-VO/PDF/A6050_E-frozen_poultry_and_egg_form.pdf\">Inventory Statement of Frozen Eggs and Poultry Products</a>(PDF, 1592 KB)   </li>\n<li>Dried Eggs are collected on a separate form, A6051_E <a href=\"/resources/prod/Internet-Internet/MISB-DGSIM/SDAD-DDAS/PE-VO/PDF/A6051_E-Dried_egg_product_form.pdf\">Inventory Edible Dried Egg Products.</a>(PDF, 1584 KB)</li>\n</ul>\n</li>\n<li>Once completed the form is e-mailed, faxed or mailed to AAFC\u2019s Animal Industry Division, Market Information Section located in Ottawa, Ontario, Canada. </li>\n<li>Data is recorded and stored in a secure database.</li>\n<li>Internal validation and verification of storage stocks data is conducted by AAFC a week prior to web report release, allowing AAFC enough time to follow up with cold storage warehouses and make any changes if required. A storage stocks inventory report is produced, including estimated figures for cold storage warehouse failing to report on time. Web reports reflect aggregated, non-confidential data. </li>\n<li>On the release date agreed upon by both AAFC and Statistics Canada, a national summary report is produced. The number of estimated facilities in each province is listed in a footnote. This report is sent to Statistics Canada, Agriculture Division responsible for dissemination in its statistical system: CANSIM. </li>\n<li>Every effort is made to make the national storage monthly report available on <a href=\"http://www.agr.gc.ca/eng/industry-markets-and-trade/market-information-by-sector/poultry-and-eggs/poultry-and-egg-market-information/?id=1384971854388\" title=\"Link to the Poultry website\">AAFC\u2019s Poultry website</a> at 8:00am Eastern Standard Time on the release date. The national report shows storage data for the current month, with comparative monthly figures from the previous month, and from the same month the previous year. It is normally available in the middle of the month. <a href=\"http://www5.agr.gc.ca/eng/industry-markets-and-trade/statistics-and-market-information/by-product-sector/poultry-and-eggs/poultry-and-egg-market-information/storage-stocks/update-schedule/?id=1384971854428\" title=\"Link to the storage stock schedule\">A storage stocks update schedule is available.</a></li>\n</ul>\n\n\n<h2>Revisions to the storage data</h2>\n<ul>\n<li>\tFollowing the release date of the monthly storage stocks report, AAFC will correct/revise the storage stocks data for any revised facility reports submitted.</li>\n<li>Once a final report is received from a cold storage warehouse, AAFC retrieves estimated figures, edits/replaces and saves final figures in its database. </li>\n<li>Revisions are included in next month's data published by AAFC and Statistics Canada. </li>\n</ul>\n<br>\n<div class=\"well\">\n<p>If you have any questions or require additional information, please send an e-mail to <a href=\"mailto:aafc.poultry-volaille.aac@agr.gc.ca\" title=\"Email: Animal Industry Division - Market Information Section\"> aafc.poultry-volaille.aac@agr.gc.ca </a></p>\n\n<h2>Our service standards</h2>\n<p>Service standards play a vital role in our market information program\u2019s commitment to planning, reporting and performance management. \nThey clearly define the level of service that clients can expect under normal circumstances.</p>\n\n<p>View the <a href=\"http://www.agr.gc.ca/eng/industry-markets-and-trade/statistics-and-market-information/by-product-sector/poultry-and-eggs/service-standards-for-poultry-and-egg-market-information/?id=1481029938239\" title=\"Service standards for poultry reports\">Service standards for poultry reports.</a></p>\n</div>\n<div class=\"clearfix\"></div>\n\n\n             \n\r\n    ",
        "fr": "\r\n                                                         \n<h2>Information</h2>\n\n<p>Tous les entrep\u00f4ts frigorifiques au Canada sont tenus de soumettre \u00e0 Agriculture et Agroalimentaire Canada (AAC) un relev\u00e9 des inventaires de produits avicoles et d'\u0153ufs congel\u00e9s qui se trouvent dans leurs entrep\u00f4ts au premier jour ouvrable de chaque mois. L\u2019inventaire soumis doit refl\u00e9ter ce qui r\u00e9side dans l\u2019entrep\u00f4t ce jour-l\u00e0. Il ne s\u2019agit pas d\u2019un total cumulatif pour le mois entier. \n\n</p><p>La collecte de l'information sur les inventaires en entrep\u00f4t de viande de volaille et d'\u0153ufs congel\u00e9s est men\u00e9e conjointement au sein du gouvernement f\u00e9d\u00e9ral du Canada. Les donn\u00e9es d\u2019inventaire de produits avicoles et d\u2019\u0153ufs congel\u00e9s sont partag\u00e9es entre Agriculture et Agroalimentaire Canada et Statistique Canada.\n\n</p><p>Les donn\u00e9es d\u2019inventaires de produits avicoles et d\u2019\u0153ufs congel\u00e9s sont essentielles pour les secteurs sous la gestion de l\u2019offre tels que le poulet, le dindon et les \u0153ufs lors de la fixation des contingents. Cette information est un indicateur important de l\u2019industrie utilis\u00e9 par les gouvernements, les associations nationales, les transformateurs et les d\u00e9taillants dans l\u2019\u00e9valuation de programmes et la prise de d\u00e9cisions politiques et commerciales. La qualit\u00e9 des donn\u00e9es d\u00e9pend de l\u2019exhaustivit\u00e9 de l\u2019information re\u00e7ue.  \n \n</p><p>AAC en collaboration avec Statistique Canada envoient les formulaires \u00e0 compl\u00e9ter. AAC est responsable de la collecte des donn\u00e9es; les rapports finaux mensuels sont publi\u00e9s par AAC et Statistique Canada. \n\n\n</p><h2>Le processus de collecte des donn\u00e9es plus en d\u00e9tail</h2>\n<ul>\n<li>Vers le milieu du mois, Statistique Canada  envoie par la poste les formulaires \u00e0 compl\u00e9ter aux entrep\u00f4ts requ\u00e9rant une version papier; AAC s\u2019occupe d\u2019envoyer les versions \u00e9lectroniques. </li>\n<li>Tous les formulaires sont retourn\u00e9s \u00e0 AAC, Division de l\u2019industrie animale. AAC est responsable de faire les suivis n\u00e9cessaires. </li>\n<li>Les entrep\u00f4ts indiquent sur le formulaire le nombre exact de kilogrammes (et non de livres) de produits avicoles et d'\u0153ufs entrepos\u00e9s dans leurs installations au premier jour ouvrable de chaque mois. Les directives et d\u00e9finitions sont incluses avec le formulaire.\n<ul>\n<li>Le formulaire utilis\u00e9 par les entrep\u00f4ts est: A6050-F <a href=\"/resources/prod/Internet-Internet/MISB-DGSIM/SDAD-DDAS/PE-VO/PDF/A6050_F-stocks_produits_avicoles_et_oeufs_congeles.pdf\">Relev\u00e9 des stocks de produits avicoles et d'\u0153ufs congel\u00e9s. </a>\n</li><li>Les inventaires d'\u0153ufs en poudre sont inscrits sur un autre formulaire : A6051_F <a href=\"/resources/prod/Internet-Internet/MISB-DGSIM/SDAD-DDAS/PE-VO/PDF/A6051_F-stocks_produits_d_oeufs_en_poudre.pdf\">D\u00e9claration des produits d'\u0153ufs en poudre comestibles. </a>\n</li></ul>\n</li>\n<li>Lorsque compl\u00e9t\u00e9, le formulaire doit \u00eatre retourn\u00e9 par courriel ou t\u00e9l\u00e9copieur \u00e0 AAC, Division de l\u2019industrie animale, Section d\u2019information sur les march\u00e9s situ\u00e9 \u00e0 Ottawa, Ontario, Canada. </li>\n<li>Les donn\u00e9es sont enregistr\u00e9es et conserv\u00e9es dans une base de donn\u00e9es s\u00e9curis\u00e9e. </li>\n<li>Un processus de validation et de v\u00e9rification des donn\u00e9es d\u2019inventaire re\u00e7ues est entrepris par AAC une semaine avant la date de publication des donn\u00e9es sur le site internet afin de permettre \u00e0 AAC de contacter les entrep\u00f4ts au besoin et faire les corrections appropri\u00e9es si n\u00e9cessaire. Un relev\u00e9 estimatif des inventaires de produits avicoles et d\u2019\u0153ufs est pr\u00e9par\u00e9 pour chaque entrep\u00f4t n\u2019ayant pas fait parvenir son formulaire \u00e0 l\u2019int\u00e9rieur des d\u00e9lais requis. Les rapports sur le site internet refl\u00e8tent des donn\u00e9es agr\u00e9g\u00e9es et non-confidentielles.</li>\n<li>Lors de la date de publication des donn\u00e9es telle que convenue par AAC et Statistique Canada, un rapport sommaire national est pr\u00e9par\u00e9. Le nombre d'entrep\u00f4ts ayant fait l'objet d'un calcul estimatif est inscrit en note de bas de page sur le rapport. Le rapport est envoy\u00e9 \u00e0 Statistique Canada, Division de l'agriculture, pour publication dans son syst\u00e8me statistique CANSIM.</li>\n<li>Tous les efforts sont mis en place afin d\u2019assurer la disponibilit\u00e9 du rapport sur le site internet de AAC (<a href=\"http://www.agr.gc.ca/fra/industrie-marches-et-commerce/statistiques-et-information-sur-les-marches/par-produit-secteur/volaille-et-oeufs/information-sur-le-marche-de-la-volaille-et-des-oeufs-industrie-canadienne/?id=1384971854388\" title=\"Liens vers le site wet de la volaille\">www.agr.gc.ca/poultry-volaille</a>) \u00e0 8 h00, heure standard de l\u2019Est, le jour pr\u00e9vu de publication.  Le rapport national mensuel comprend les inventaires en entrep\u00f4t du mois courant en comparaison avec les inventaires en entrep\u00f4t du mois pr\u00e9c\u00e9dent ainsi que ceux du m\u00eame mois de l\u2019ann\u00e9e pr\u00e9c\u00e9dente. Le rapport est habituellement publi\u00e9 vers le milieu du mois. <a href=\"http://www.agr.gc.ca/fra/industrie-marches-et-commerce/statistiques-et-information-sur-les-marches/par-produit-secteur/volaille-et-oeufs/information-sur-le-marche-de-la-volaille-et-des-oeufs-industrie-canadienne/stocks-en-entrepot/horaire-de-mise-a-jour/?id=1384971854428\" title=\"Liens vers l'horaire de mise-\u00e0-jour des stocks en entrep\u00f4t\">Un horaire des dates de mise \u00e0 jour des inventaires en entrep\u00f4t est disponible.</a></li>\n</ul> \n\n\n<h2>R\u00e9vision /correction des donn\u00e9es d'entreposage</h2>\n<ul>\n<li>Suivant la date de publication du rapport mensuel d\u2019inventaire en entrep\u00f4t, AAC corrige/remplace les donn\u00e9es d'inventaire des entrep\u00f4ts ayant soumis un formulaire d\u2019inventaire r\u00e9vis\u00e9. </li>\n<li>Lorsque le relev\u00e9 officiel est soumis par l\u2019entrep\u00f4t frigorifique, AAC corrige les donn\u00e9es d\u2019inventaire estim\u00e9es et sauvegarde les donn\u00e9es finales dans  la base de donn\u00e9es d\u2019AAC. </li>\n<li>Les r\u00e9visions des donn\u00e9es sont incluses lors de la publication des inventaires en entrep\u00f4t du mois suivant par AAC et Statistique Canada. </li>\n</ul>\n\n\n<div class=\"well\">\n<p>Pour toute question ou information additionnelle, veuillez communiquer avec nous par courriel \u00e0 <a href=\"mailto:aafc.poultry-volaille.aac@agr.gc.ca\" title=\"Courriel : Division de l'industrie animale, Section de l'information sur les march\u00e9s\"> aafc.poultry-volaille.aac@agr.gc.ca</a></p>\n\n<h2>Nos normes de service</h2>\n<p>Les normes de service jouent un r\u00f4le cl\u00e9 dans les processus de gestion de la planification, des rapports et du rendement de notre programme d'information sur le march\u00e9. Elles d\u00e9finissent clairement le niveau de rendement auquel les clients peuvent s'attendre lorsqu'ils re\u00e7oivent un service dans des circonstances normales.</p>\n\n<p>Visionnez les  <a href=\"http://www.agr.gc.ca/fra/industrie-marches-et-commerce/statistiques-et-information-sur-les-marches/par-produit-secteur/volaille-et-oeufs/normes-de-service-pour-l-information-sur-le-marche-de-la-volaille-et-des-ufs/?id=1481029938239\" title=\"Normes de service relatives aux rapports sur la volaille\">Normes de service relatives aux rapports sur la volaille.</a></p>\n</div>\n<div class=\"clearfix\"></div>\n\n             "
    }
}