{
    "dcr_id": "1256652100794",
    "title": {
        "en": "Environment Programs",
        "fr": "Programmes environnementaux"
    },
    "modified": "2009-11-06",
    "issued": "2009-11-06",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1365605560469",
    "dc_date_created": "2009-11-06",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/sci_Ottawa/WORKAREA/sci_Ottawa/templatedata/comm-comm/gene-gene/data/10/proact_pia_apf_1256652100794_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/sci_Ottawa/WORKAREA/sci_Ottawa/templatedata/comm-comm/gene-gene/data/10/proact_pia_apf_1256652100794_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Environment Programs",
        "fr": "Programmes environnementaux"
    },
    "breadcrumb": {
        "en": "Environment Programs",
        "fr": "Programmes environnementaux"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2009-11-06",
            "fr": "2009-11-06"
        },
        "modified": {
            "en": "2009-11-06",
            "fr": "2009-11-06"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Environment Programs",
            "fr": "Programmes environnementaux"
        },
        "subject": {
            "en": "federal government;agricultural policy framework (apf);proactive disclosure;environmental health",
            "fr": "divulgation proactive;cadre strat\u00e9gique pour l'agriculture (csa);hygi\u00e8ne du milieu;gouvernement f\u00e9d\u00e9ral"
        },
        "dcterms:subject": {
            "en": "agricultural policy;environment;government information;assessment",
            "fr": "politique agricole;information gouvernementale;\u00e9valuation;environnement"
        },
        "description": {
            "en": null,
            "fr": null
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": null,
            "fr": null
        },
        "audience": {
            "en": "government;media;general public",
            "fr": "m\u00e9dias;gouvernement;grand public"
        },
        "type": {
            "en": "assessment",
            "fr": "\u00e9valuation"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Environment Programs",
        "fr": "Programmes environnementaux"
    },
    "body": {
        "en": "\r\n    \n<section>\n<h2>Introduction</h2>\n<p>During 2003-2009 the Government of Canada and the provincial and territorial governments worked with the agriculture and agri-food industry and interested Canadians to develop and implement the Agricultural Policy Framework (APF). A total of 14\u00a0<abbr title=\"Agricultural Policy Framework\">APF</abbr> Environment Programs were created with the intent of helping the agriculture and agri-food sector achieve environmental sustainability in the areas of soil, water, air and biodiversity.</p>\n<p>A Privacy Impact Assessment (PIA) was conducted on the following four <abbr title=\"Agricultural Policy Framework\">APF</abbr> programs which collect personal information:</p>\n<ul>\n<li>Environmental Farm Planning (EFP);</li>\n<li>National Farm Stewardship Program (NFSP);</li>\n<li>Greencover Canada; and,</li>\n<li>National Water Supply Expansion Program (NWSEP).</li>\n</ul>\n<p>Environmental farm planning (EFP) is a voluntary process used by individual land managers to systematically identify environmental risks and benefits from their own farming operation and to develop an action plan to mitigate the risks. The objective of the program is to encourage land managers to develop farm plans related to environmental risks and to continuously evaluate their environmental performance. The National Farm Stewardship Program (NFSP), Greencover Canada program, and the National Water Supply Expansion Program (NWSEP) provide technical and financial assistance to land managers and/or owners related to targeted areas of environmental sustainability.</p>\n<p>Environment Programs developed and implemented through the <abbr title=\"Agricultural Policy Framework\">APF</abbr> ended on March\u00a031, 2009. Under Growing Forward, provincial governments have been given responsibility for delivering all environmental grants and contribution initiatives across the country.</p>\n</section>\n\n<section>\n<h2>Objective</h2>\n<p>A <abbr title=\"Privacy Impact Assessment\">PIA</abbr> of the above four <abbr title=\"Agricultural Policy Framework\">APF</abbr> Environment Programs was conducted to determine if there were any privacy issues, and if so, to make recommendations for their resolution or mitigation. The <abbr title=\"Privacy Impact Assessment\">PIA</abbr> process was initiated while the Environment programs were in the early stages of implementation under <abbr title=\"Agricultural Policy Framework\">APF</abbr>.</p>\n</section>\n\n<section>\n<h2>Description</h2>\n<p>The scope of the <abbr title=\"Privacy Impact Assessment\">PIA</abbr> included the business process and the basic data flows of personal information associated with the four <abbr title=\"Agricultural Policy Framework\">APF</abbr> Environment Programs indicated above. The report focuses on <abbr title=\"Agriculture and Agri-Food Canada\">AAFC</abbr>'s role in the delivery and management of each of the programs. In addition, considering the integral role played by delivery agents in the four programs, the <abbr title=\"Privacy Impact Assessment\">PIA</abbr> includes a description of the delivery agents' responsibilities with respect to the programs. Site visits were conducted with selected delivery agents in the provinces of Ontario, Alberta, and British Columbia.</p>\n<p>The Personal Information Bank (PIB),<strong> Environment Programming (Agri-Environment Services Branch/Prairie Farm Rehabilitation Administration) <abbr title=\"Agriculture and Agri-Food Canada\">AAFC</abbr> PPU\u00a0624</strong>, outlines the details regarding the personal information collected under the Environment Programming, including the purpose, use, retention and disposal standards. This PIB will cover all past and future programming administered by <abbr title=\"Agri-Environment Services Branch\">AESB</abbr>/<abbr title=\"Prairie Farm Rehabilitation Administration\">PFRA</abbr>.</p>\n</section>\n\n<section>\n<h2>Conclusion</h2>\n<p>As a result of the <abbr title=\"Privacy Impact Assessment\">PIA</abbr>, measures have been determined to mitigate any risks associated with the collection of personal information. These include: access controls, compliance audits and limiting the information collected. In addition, the corrective actions identified in the <abbr title=\"Privacy Impact Assessment\">PIA</abbr> were implemented and lessons learned were transferred to delivery partners across Canada.</p>\n<p>For further information please contact:</p>\n<p>Access to Information and Privacy<br>\nTower 4, 5th Floor<br>\n1341 Baseline Road, Room 264<br>\nOttawa, Ontario, K1A 0C5<br>\nTelephone: 613-773-1390<br>\nFacsimile: 613-773-1380<br>\nEmail: <a title=\"Email: Access to Information and Privacy\" href=\"mailto:aafc.atip-aiprp.aac@canada.ca\">aafc.atip-aiprp.aac@canada.ca</a></p>\n</section>\n\n\r\n    ",
        "fr": "\r\n    <section>\n<h2>Introduction</h2>\n<p>De 2003 \u00e0 2009, le gouvernement du Canada et les gouvernements provinciaux et territoriaux ont collabor\u00e9 avec l'industrie agricole et agroalimentaire et les Canadiens int\u00e9ress\u00e9s \u00e0 l'\u00e9laboration et \u00e0 la mise en place d'un Cadre strat\u00e9gique pour l'agriculture (CSA). Au total, 14\u00a0programmes environnementaux du <abbr title=\"Cadre strat\u00e9gique pour l'agriculture\">CSA</abbr> ont \u00e9t\u00e9 cr\u00e9\u00e9s dans le but d'aider le secteur agricole et agroalimentaire \u00e0 atteindre une durabilit\u00e9 environnementale en ce qui a trait au sol, \u00e0 l'eau, \u00e0 l'air et \u00e0 la biodiversit\u00e9.</p>\n<p>Une \u00e9valuation des facteurs relatifs \u00e0 la vie priv\u00e9e (EFVP) a \u00e9t\u00e9 r\u00e9alis\u00e9e sur les quatre programmes du <abbr title=\"Cadre strat\u00e9gique pour l'agriculture\">CSA</abbr> ci-dessous. Ces programmes recueillent des renseignements personnels\u00a0:</p>\n<ul>\n<li>Planification environnementale \u00e0 la ferme (PEF)</li>\n<li>Programme national de g\u00e9rance agroenvironnementale (PNGA)</li>\n<li>Programme de couverture v\u00e9g\u00e9tale du Canada</li>\n<li>Programme national d\u2019approvisionnement en eau (PNAE).</li>\n</ul>\n<p>Le programme Planification environnementale \u00e0 la ferme (PEF) est un processus volontaire auquel les gestionnaires des terres ont recours pour cerner de fa\u00e7on syst\u00e9matique les risques et les avantages environnementaux qui d\u00e9coulent de leur exploitation agricole et \u00e9laborer un plan d'action pour r\u00e9duire les risques. L'objectif du programme est d'encourager les gestionnaires de terres \u00e0 \u00e9laborer des plans agricoles li\u00e9s aux risques environnementaux et \u00e0 continuellement \u00e9valuer leur rendement environnemental. Le Programme national de g\u00e9rance agroenvironnementale (PNGA), le Programme de couverture v\u00e9g\u00e9tale du Canada et le Programme national d\u2019approvisionnement en eau (PNAE) offrent une aide technique et financi\u00e8re aux gestionnaires de terres ou aux propri\u00e9taires fonciers en ce qui a trait aux domaines touch\u00e9s par la durabilit\u00e9 environnementale.</p>\n<p>Les programmes environnementaux \u00e9labor\u00e9s et mis en place dans le cadre du <abbr title=\"Cadre strat\u00e9gique pour l'agriculture\">CSA</abbr> ont pris fin le 31 mars 2009. Conform\u00e9ment \u00e0 Cultivons l'avenir, les gouvernements provinciaux ont \u00e9t\u00e9 charg\u00e9s d'accorder toutes les initiatives de subventions et de contributions environnementales au pays.</p>\n</section><section>\n<h2>Objectif</h2>\n<p>Une <abbr title=\"\u00e9valuation des facteurs relatifs \u00e0 la vie priv\u00e9e\">EFVP</abbr> des quatre programmes environnementaux du <abbr title=\"Cadre strat\u00e9gique pour l'agriculture\">CSA</abbr> nomm\u00e9s plus haut a \u00e9t\u00e9 r\u00e9alis\u00e9e afin de d\u00e9terminer s'il y a eu des probl\u00e8mes en mati\u00e8re de protection des renseignements personnels et, dans la positive, d'\u00e9mettre des recommandations en vue de la r\u00e9solution ou de la r\u00e9duction de ces probl\u00e8mes. Le processus d'<abbr title=\"\u00e9valuation des facteurs relatifs \u00e0 la vie priv\u00e9e\">EFVP</abbr> a \u00e9t\u00e9 lanc\u00e9 durant les premi\u00e8res \u00e9tapes de mise en place des programmes environnementaux du <abbr title=\"Cadre strat\u00e9gique pour l'agriculture\">CSA</abbr>.</p>\n</section><section>\n<h2>Description</h2>\n<p>La port\u00e9e de l'<abbr title=\"\u00e9valuation des facteurs relatifs \u00e0 la vie priv\u00e9e\">EFVP</abbr> englobait les processus op\u00e9rationnels et les flux de donn\u00e9es de base des renseignements personnels li\u00e9s aux quatre programmes environnementaux d\u00e9crits ci-dessus. Le rapport se concentre sur le r\u00f4le d'<abbr title=\"Agriculture et Agroalimentaire Canada\">AAC</abbr> dans la r\u00e9alisation et la gestion de chacun de ces programmes. De plus, compte tenu du r\u00f4le essentiel des agents de prestation pour les quatre programmes, l'<abbr title=\"\u00e9valuation des facteurs relatifs \u00e0 la vie priv\u00e9e\">EFVP</abbr> comprend une description des responsabilit\u00e9s des agents de prestation relativement aux programmes. Des visites des sites avec certains agents de prestation ont eu lieu en Ontario, en Alberta et en Colombie-Britannique.</p>\n<p>Le Fichier de renseignements personnels (FRP), Programmes environnementaux (Direction g\u00e9n\u00e9rale des services agroenvironnementaux/Administration du r\u00e9tablissement agricole des Prairies) <abbr title=\"Agriculture et Agroalimentaire Canada\">AAC</abbr> PPU\u00a0624, d\u00e9crit en d\u00e9tails les renseignements personnels recueillis dans le cadre de ces programmes, y compris le but, l'utilisation et les normes de conservation et de destruction. Le <abbr title=\"Fichier de renseignements personnels\">FRP</abbr> traitera de tous les programmes, anciens et nouveaux, g\u00e9r\u00e9s par la <abbr title=\"Direction g\u00e9n\u00e9rale des services agroenvironnementaux\">DGSA</abbr> et l'<abbr title=\"Administration du r\u00e9tablissement agricole des Prairies\">ARAP</abbr>.</p>\n</section><section>\n<h2>Conclusion</h2>\n<p>\u00c0 la suite de l'<abbr title=\"\u00e9valuation des facteurs relatifs \u00e0 la vie priv\u00e9e\">EFVP</abbr>, des mesures ont \u00e9t\u00e9 recommand\u00e9es afin de r\u00e9duire les risques associ\u00e9s \u00e0 la collecte de renseignements personnels, notamment les contr\u00f4les de l'acc\u00e8s, les v\u00e9rifications de conformit\u00e9 et la limitation des renseignements recueillis. De plus, les mesures correctives indiqu\u00e9es dans l'<abbr title=\"\u00e9valuation des facteurs relatifs \u00e0 la vie priv\u00e9e\">EFVP</abbr> ont \u00e9t\u00e9 mises en place et les le\u00e7ons apprises ont \u00e9t\u00e9 transmises aux partenaires de prestation dans tout le pays.</p>\n<p>Pour de plus amples renseignements, veuillez vous adresser \u00e0\u00a0:</p>\n<p>Acc\u00e8s \u00e0 l\u2019information et protection des renseignements personnels<br>Tour 4, 5e \u00e9tage<br>1341, chemin Baseline, pi\u00e8ce 264<br>Ottawa (Ontario) K1A 0C5<br>T\u00e9l\u00e9phone\u00a0: 613 773-1390<br>T\u00e9l\u00e9copieur\u00a0: 613 773-1380<br>Courriel\u00a0: <a href=\"mailto:aafc.atip-aiprp.aac@canada.ca\" title=\"Courriel : Acc\u00e8s \u00e0 l'information et protection des renseignements personnels\">aafc.atip-aiprp.aac@canada.ca</a></p>\n</section>\n\r\n    "
    },
    "js": {
        "en": "",
        "fr": ""
    }
}