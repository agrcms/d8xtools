{
    "dcr_id": "1535661108458",
    "title": {
        "en": "Management of onion thrips with biopesticides",
        "fr": "Contr\u00f4le de thrips de l'oignon avec biopesticides"
    },
    "modified": "2018-10-12",
    "issued": "2018-10-12",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1535122427752",
    "dc_date_created": "2018-10-12",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/08/bpi09-020_1535661108458_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/08/bpi09-020_1535661108458_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Management of onion thrips with biopesticides",
        "fr": "Contr\u00f4le de thrips de l'oignon avec biopesticides"
    },
    "breadcrumb": {
        "en": "Management of onion thrips with biopesticides",
        "fr": "Contr\u00f4le de thrips de l'oignon avec biopesticides"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "modified": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Management of onion thrips with biopesticides",
            "fr": "Contr\u00f4le de thrips de l'oignon avec biopesticides"
        },
        "subject": {
            "en": "vegetable crops;canola oil;soil assessement",
            "fr": "fraises;\u00e9valuation environnementale strat\u00e9gique;mauvaises herbes"
        },
        "dcterms:subject": {
            "en": "architecture;agriculture;technology",
            "fr": "pesticide;maladie;politique"
        },
        "description": {
            "en": "PRR-RRP Pesticide Risk Reduction Program s_root_insect_pests Pesticide Risk Reduction Strategy for Root Insect Pests of Carrot, Parsnip and Onion. \u2013 Prince Edward Island Department of Agriculture and Forestry;Mary Ruth McDonald. However, since the initiation of this Strategy, a number of other chemical insecticides re-evaluations have been initiated by PMRA: clothianidin and imidacloprid (carrot rust fly, onion maggot and seedcorn maggot), cypermethrin (carrot rust fly, onion maggot flies), lambda-cyhalothrin (carrot rust fly and carrot weevil), phosmet (carrot weevil), and chlorpyrifos (onion maggot larvae). Thus, there is an urgent need for the development of robust, reduced risk management options for these pests.Strategy development.",
            "fr": "MUP-PUL Programme des pesticides \u00e0 usage limit\u00e9 Pesticide \u00e0 usage limit\u00e9 - Soumissions. Laitue, feuilles. Scl\u00e9rotiniose mineure (Sclerotinia minor); Scl\u00e9rotinia, pourriture blanche (Sclerotinia sclerotiorum). Allegro 500F Agricultural Fungicide. Fraise. Mauvaises herbes \u00e0 feuilles larges."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Caneberry",
            "fr": "ampelina;Lyme.De;Blazer"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "presentation",
            "fr": "d\u00e9cision"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Management of onion thrips with biopesticides",
        "fr": "Contr\u00f4le de thrips de l'oignon avec biopesticides"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <h2>Project code BPI09-020</h2>\n<h3>Project lead</h3>\n\n<p><b>Jeff Tolman &amp; Heather Peill</b> - Agriculture and Agri-Food Canada</p>\n\n<h3>Objective</h3>\n\n<p><b>This project is part of the North American Free Trade Agreement Biopesticides Initiative for demonstration and efficacy testing of biopesticides against onion thrips (<i>Thrips tabaci</i>) in Canada. A comparable project was undertaken through the IR4 program under the lead of Silvia Rondon (Oregon State University)</b></p>\n\n<h3>Summary of Results</h3>\n\n<h4>Background</h4>\n\n<p>This project was part of the North American Free Trade Agreement Biopesticides Initiative for demonstration and efficacy testing of biopesticides against onion thrips (OT; <i>Thrips tabaci</i>) in Canada. A comparable project was undertaken through the United States IR4 program under the lead of Silvia Rondon (Oregon State University).</p>\n\n<p><abbr title=\"onion thrips\">OT</abbr> are serious pests in onions and are widely distributed in Canada and the United States. They can cause significant economic damage by piercing the leaves and feeding on plant chlorophyll. Infested onion plants are also more susceptible to secondary infections.</p>\n<p><abbr title=\"onion thrips\">OT</abbr> have many natural enemies, which include ladybird beetles, minute pirate bugs, lacewings, spiders, and predacious and parasitic wasps. The fungus <i>Entomopthora thripidum</i> also infects <abbr title=\"onion thrips\">OT</abbr>, providing some natural control. However, these measures are effective only when the <abbr title=\"onion thrips\">OT </abbr>population is low. Chemicals such as pyrethroids and organophosphate insecticides, while efficient in controlling the pest, have disadvantages that include concerns about environmental safety and human health (organophosphates), and incompatibility with natural enemies (pyrethroids). </p>\n\n<p>The development of alternative control measures is therefore crucial for managing <abbr title=\"onion thrips\">OT</abbr> populations in a safe and environmentally sustainable way. In this context, the present study evaluated the performance of several biopesticides for controlling <abbr title=\"onion thrips\">OT </abbr>attacks in dry bulb onions. Additional reduced-risk products were also evaluated.</p>\n\n<h4>Approaches</h4>\n\n<p>Eleven different treatment schedules were assessed, using various combinations of the following agents: Botanigard ES (<i>Beauveria bassiana conidia</i> 11.3%), Delegate WG (spinetoram 25%), Entrust Naturalyte Insecticide (spinosad A/D 80%), Surround WP (kaolin 95%), Trilogy (neem oil extract), Requiem 25 EC (Chenopodium ambrosioides extracts 25%), Movento 240 SC (spirotetramat 22.4%), Carzol SP (formetanate hydrochloride 92%), Ripcord (cypermethrin 400 EC), Cyazapyr (cyantraniliprole 100SE), and Malathion (malathion 500EC).</p>\n\n<p>The trial was conducted on a commercial dry bulb onion field in Berwick, Nova Scotia. Initial treatment applications were made in early July 2009, when the surrounding field reached an <abbr title=\"onion thrips\">OT</abbr> level of 1 per leaf. Subsequent applications were determined weekly by <abbr title=\"onion thrips\">OT</abbr> counts. When specific treatment plots reached the threshold of 1 <abbr title=\"onion thrips\">OT</abbr> per leaf, the next scheduled application was made, except for two of the treatments which received weekly insecticide applications. <abbr title=\"onion thrips\">OT</abbr> levels were monitored throughout the growing season in each treatment plot to identify decreases in populations. In late July and August, <abbr title=\"onion thrips\">OT</abbr> feeding damage from the inner 4 leaves of 5 randomly selected plants per treatment plot was rated on a 0-5 scale where 0 = no feeding damage, and 5 = total leaf surface showing signs of <abbr title=\"onion thrips\">OT</abbr> feeding.</p>\n\n<h4>Results</h4>\n\n<p>The largest reduction in <abbr title=\"onion thrips\">OT</abbr> numbers and feeding damage was achieved with a weekly schedule of Movento, Carzol, Cyazapyr and Delegate, closely followed by a weekly schedule of Ripcord, Carzol, Cyazapyr and Malathion. Four applications of Delegate over the 7 week spray period also significantly reduced <abbr title=\"onion thrips\">OT</abbr> numbers and leaf feeding damage. Entrust applications in combination with Requiem provided some significant control, but was inconsistent throughout the season. A schedule of Botanigard and Delegate only provided one significant reduction in <abbr title=\"onion thrips\">OT</abbr> population near the end of the growing season. Weekly applications of Botanigard, Surround, Trilogy or Requiem alone, as well as a combination of Botanigard and Surround, provided no reduction in <abbr title=\"onion thrips\">OT</abbr> population or leaf feeding throughout the course of the trial at the tested rates of these products.</p>\n<p>The results of this study suggest that Entrust, Delegate, Cyazapyr and Movento may have potential as reduced-risk pesticides for controlling <abbr title=\"onion thrips\">OT</abbr> in commercial dry onion fields. For more information about this study, please contact <a href=\"mailto:heather.peill@agr.gc.ca\">Heather Peill</a>.</p>\n\r\n    ",
        "fr": "\r\n    <h2>Code de projet BPI09-020</h2>\n<h3>Chef de projet</h3>\n<p><b>Jeff Tolman &amp; Heather Peill -</b> Agriculture et Agroalimentaire Canada</p>\n<h3>Objectif</h3>\n<p><b>Ce projet fait partie de l'initiative sur les biopesticides de l'Accord de libre-\u00e9change Nord-Am\u00e9ricain dont l\u2019objectif est de d\u00e9montrer et d\u2019\u00e9valuer l\u2019efficacit\u00e9 des pesticides contre les thrips de l'oignon (<i>Thrips tabaci</i>) au Canada. Un projet similaire a \u00e9t\u00e9 r\u00e9alis\u00e9 par Silvia Rondon (Oregon State University) dans le cadre du programme am\u00e9ricain IR4</b></p>\n\n<h3>Sommaire des r\u00e9sultats</h3>\n\n<h4>Contexte</h4>\n\n<p>Ce projet fait partie de l\u2019initiative sur les biopesticides de l'Accord de libre-\u00e9change Nord-Am\u00e9ricain visant \u00e0 d\u00e9montrer et \u00e0 \u00e9valuer l\u2019efficacit\u00e9 des pesticides contre les thrips de l\u2019oignon (<i>Thrips tabaci</i>) au Canada. Un projet similaire a \u00e9t\u00e9 r\u00e9alis\u00e9 par Silvia Rondon (Oregon State University) dans le cadre du programme am\u00e9ricain IR4.</p>\n\n<p>Les thrips de l\u2019oignon sont des ravageurs importants. Ils nuisent aux cultures dans un grand nombre de r\u00e9gions du Canada et des \u00c9tats-Unis, qui subissent de grandes pertes \u00e9conomiques. Ils s\u2019attaquent aux feuilles en se nourrissant de chlorophylle. Les plants d\u2019oignon ayant d\u00e9j\u00e0 \u00e9t\u00e9 infest\u00e9s sont plus susceptibles aux surinfections. Les thrips de l\u2019oignon ont de nombreux ennemis naturels, tels que les coccinelles, les punaises anthocorides, les chrysopes, les araign\u00e9es et les gu\u00eapes pr\u00e9datrices et parasites. Le champignon <i>Entomopthora thripidumi </i>infecte \u00e9galement les thrips, contribuant \u00e0 leur contr\u00f4le naturel. Toutefois, cette mesure est efficace uniquement quand le taux d\u2019infestation est faible. Les produits chimiques tels que les insecticides pyr\u00e9thro\u00efdes et organophosphates agissent efficacement contre les ravageurs, mais ils ont des d\u00e9savantages. En effet, ces insecticides posent certains risques pour l\u2019environnement et pour la sant\u00e9 publique (organophosphates), et peuvent \u00eatre incompatibles avec les ennemis naturels (pyr\u00e9thro\u00efdes).</p>\n<p>Il est absolument essentiel de se doter d\u2019outils de lutte de rechange afin de pouvoir g\u00e9rer les populations de thrips de l\u2019oignon de fa\u00e7on s\u00e9curitaire et durable. C\u2019est dans cette optique que le pr\u00e9sent projet d\u2019\u00e9tude a \u00e9valu\u00e9 le rendement de plusieurs biopesticides pour la lutte contre les thrips dans les bulbes d\u2019oignon secs. On a \u00e9galement \u00e9valu\u00e9 l\u2019action d\u2019autres produits \u00e0 risque r\u00e9duit.</p>\n<h4>M\u00e9thodes</h4>\n<p>L\u2019\u00e9valuation portait sur 11 calendriers de traitement diff\u00e9rents, avec de multiples combinaisons des agents suivants : Botanigard ES (<i>Beauveria bassiana conidiai</i> 11,3 %), Delegate WG (spinetoram 25 %), Entrust Naturalyte (spinosad A/D 80 %), Surround WP (kaolin 95 %), Trilogy (extrait d\u2019huile de margousier), Requiem 25 EC (extraits de Chenopodium ambrosioides 25 %), Movento 240 SC (spirotetramat 22,4 %), Carzol SP (chlorhydrate de form\u00e9tanate 92 %), Ripcord (cyperm\u00e9thrine 400 EC), Cyazapyr (cyantraniliprole 100SE), et Malathion (malathion 500EC).</p>\n<p>L\u2019essai a \u00e9t\u00e9 effectu\u00e9 dans une parcelle de culture commerciale d\u2019oignons situ\u00e9e \u00e0 Berwick en Nouvelle-\u00c9cosse. Les applications de traitement initiales ont \u00e9t\u00e9 faites au d\u00e9but du mois de juillet 2009, lorsque le champ environnant a atteint un niveau d\u2019infestation d\u2019un thrips de l\u2019oignon par feuille. Les applications subs\u00e9quentes ont \u00e9t\u00e9 d\u00e9termin\u00e9es \u00e0 chaque semaine, selon le nombre de thrips. Lorsque certaines aires d\u2019essai ont atteint le niveau d\u2019un thrips par feuille, on a effectu\u00e9 la prochaine application, sauf pour deux des traitements o\u00f9 les applications de pesticides ont eu lieu \u00e0 chaque semaine. On a surveill\u00e9 les niveaux de thrips dans chaque aire d\u2019essai durant toute la saison de croissance, afin d\u2019\u00e9valuer les diminutions de population. \u00c0 la fin des mois de juillet et d\u2019ao\u00fbt, on a \u00e9valu\u00e9 les dommages faits par les thrips aux quatre feuilles int\u00e9rieures de cinq plantes choisies au hasard dans chacune des aires d\u2019essai (\u00e9valu\u00e9 sur une \u00e9chelle de 0 \u00e0 5, o\u00f9 0 correspond \u00e0 aucune surface endommag\u00e9e et 5 correspond \u00e0 la totalit\u00e9 de la surface de la feuille endommag\u00e9e).</p>\n<h4>R\u00e9sultats</h4>\n<p>La plus grande r\u00e9duction du nombre de thrips de l\u2019oignon et de la superficie endommag\u00e9e s\u2019est faite avec un programme d\u2019applications comprenant Movento, Carzol, Cyazapyr et Delegate, suivi de pr\u00e8s par le programme comprenant Ripcord, Carzol, Cyazapyr et Malathion. Quatre applications de Delegate sur une p\u00e9riode d\u2019\u00e9pandage de sept semaines ont \u00e9galement r\u00e9duit consid\u00e9rablement le nombre de thrips et la superficie endommag\u00e9e des feuilles. Des applications de Entrust combin\u00e9es \u00e0 des applications de Requiem ont fourni un contr\u00f4le consid\u00e9rable, mais de fa\u00e7on non coh\u00e9rente durant la saison. Le programme d\u2019utilisation de Botanigard et de Delegate a donn\u00e9 une r\u00e9duction consid\u00e9rable de la population de thrips pr\u00e8s de la fin de la saison de croissance. Les applications hebdomadaires du seul insecticide Botanigard, Surround, Trilogy ou Requiem, ainsi que la combinaison de Botanigard et de Surround, n\u2019ont aucunement r\u00e9duit la population de thrips ou la superficie endommag\u00e9e aux taux d\u2019essai de ces produits durant la p\u00e9riode d\u2019essai.</p>\n<p>Les r\u00e9sultats de cette \u00e9tude sugg\u00e8rent que les insecticides Entrust, Delegate, Cyazapyr et Movento pourraient \u00eatre utilis\u00e9s comme pesticides \u00e0 risque r\u00e9duit pour la lutte contre les thrips de l\u2019onion dans les champs de culture commerciale. Pour plus d\u2019information au sujet de cette \u00e9tude, veuillez contacter <a href=\"mailto:heather.peill@agr.gc.ca\">Heather Peill</a>. </p>\n\r\n    "
    }
}