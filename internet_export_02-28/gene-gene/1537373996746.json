{
    "dcr_id": "1537373996746",
    "title": {
        "en": "Determination of the critical weed-free period in carrots grown on muck and mineral soils",
        "fr": "D\u00e9termination de la p\u00e9riode critique exempte de mauvaises herbes dans les plantations de carottes sur sols tourbeux et min\u00e9raux"
    },
    "modified": "2018-10-12",
    "issued": "2018-10-12",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1535122427752",
    "dc_date_created": "2018-10-12",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr07-260_1537373996746_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr07-260_1537373996746_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Determination of the critical weed-free period in carrots grown on muck and mineral soils",
        "fr": "D\u00e9termination de la p\u00e9riode critique exempte de mauvaises herbes dans les plantations de carottes sur sols tourbeux et min\u00e9raux"
    },
    "breadcrumb": {
        "en": "Determination of the critical weed-free period in carrots grown on muck and mineral soils",
        "fr": "D\u00e9termination de la p\u00e9riode critique exempte de mauvaises herbes dans les plantations de carottes sur sols tourbeux et min\u00e9raux"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "modified": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Determination of the critical weed-free period in carrots grown on muck and mineral soils",
            "fr": "D\u00e9termination de la p\u00e9riode critique exempte de mauvaises herbes dans les plantations de carottes sur sols tourbeux et min\u00e9raux"
        },
        "subject": {
            "en": "strawberries;air quality;soil assessement",
            "fr": "\u00e9coAgriculture;semences-production;industrie laiti\u00e8re"
        },
        "dcterms:subject": {
            "en": "soil;architectural heritage;architecture",
            "fr": "m\u00e9dicament;cultures;engrais"
        },
        "description": {
            "en": "Archived content. Archived content is provided for reference, research or recordkeeping purposes. Biology: symptoms. Apple scab is caused by the fungus. Better safe than sorry:Cultivar. For new orchards, choose tolerant or moderately susceptible cultivars (see Susceptibility of Apple Cultivars below).Pruning.",
            "fr": "Agriculture et Agroalimentaire Canada. 93, chemin Stone Ouest. De nombreux bureaux principaux canadiens et provinciaux de groupes de producteurs et de partenaires de l'industrie agroalimentaire, \u00e0 l'usage des scientifiques d'AAC. On travaille \u00e0 l\u2019utilisation de ce processus pour d\u00e9toxifier les moul\u00e9es animales contamin\u00e9es, ce qui permettrait d\u2019accro\u00eetre le taux de croissance et g\u00e9n\u00e9rerait de substantielles retomb\u00e9es \u00e9conomiques."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": null,
            "fr": null
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "media release",
            "fr": "\u00e9v\u00e9nement"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Determination of the critical weed-free period in carrots grown on muck and mineral soils",
        "fr": "D\u00e9termination de la p\u00e9riode critique exempte de mauvaises herbes dans les plantations de carottes sur sols tourbeux et min\u00e9raux"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n     \n<h2>Project Code: PRR07-260</h2>\n \n<h3>Project Lead</h3>\n<p><b>Clarence Swanton</b> - University of Guelph</p>\n \n<h3>Objective</h3>\n<p><b>To determine the critical period during carrot growth when the crop must be free of weeds and educate growers in reducing the use of herbicides based on recommendations resulting from this study</b></p>\n\n<h4>Summary of Results</h4>\n\n<p>Carrots are poor competitors with weeds and uncontrolled weeds may result in complete crop failure. Producers rely on a combination of mechanical methods and herbicides for weed control which includes the use of high doses of older residual chemicals. Carrot growers typically have a zero tolerance to weeds and in the absence of scientifically and economically based weed management guidelines, will attempt season-long weed eradication. This projects aimed at determining the critical weed-free period in carrots defined as the period after crop emergence when weeds must be controlled to prevent unacceptable yield loss. Knowledge of the critical weed-free period in carrots will enable growers to make informed weed management decisions based on economic costs and environmental benefits, that is controlling weeds only when these impact crop yield.</p>\n\n<p>The study was conducted by University of Guelph scientists, in collaboration with <abbr title=\"Agriculture and Agri-Food Canada\">AAFC</abbr> researchers, in 2007 and 2008 at three carrot growing regions of southern Ontario: on organic soil of Bradford Marsh area, and mineral soils at Ridgetown and Simcoe research stations. Common commercial carrot cultivars (Enterprise, Pursuit, Fontana) on hill and flat cropping systems were used to compare yield and weed biomass in the carrot crops under weed-free all season, weed-free for increasing durations of time (to the 2, 4, 6, 8, 10, and 12 leaf stage of carrot development), weedy all season, and standard commercial treatment regimens.</p>\n\n<p>Seeding date, the severity of weed infestation and the duration of weed emergence were found to influence the duration of the critical weed-free period in carrots. The critical weed-free period was relatively long and extended up to 1067 GDD (growing degree days), or approximately 89 days after planting, until carrots were at the 12 leaf stage, when carrots were seeded early and when weed infestation was high. In contrast, the critical weed-free period was short and lasted for a minimum of 414 <abbr title=\"growing degree days\">GDD</abbr>, or approximately 33 days after planting, until carrots were at the 4 leaf stage, when carrots were seeded later and weed infestation was moderate to low.</p>\n\n<p>The key outcome from this study is that the maximum duration that the carrots must be free of weeds, without compromising yield, is until the 12 leaf stage of crop growth. Based on this finding, growers are recommended to scout fields for weeds until carrots are at the 12 leaf stage to protect the yield potential of the carrot crop. This critical period may occur over a shorter duration when carrots are seeded later in the season and weed infestation is moderate to low. It is important to consider the information gathered through scouting in making field-specific weed management decisions. Recommendations on the window for essential weed control in carrot crops will be incorporated into provincial weed management guidelines to help carrot growers optimize yields and profitability while reducing the economic and environmental consequences of unnecessary herbicide use.</p>\n\n<p>For more information about this project or integrated weed management, please contact Dr. <a href=\"mailto:cswanton@uoguelph.ca\">Clarence J. Swanton</a>.</p>\n \n\r\n    ",
        "fr": "\r\n    <h2>Code de projet : PRR07-260</h2>\n\n<h3>Chef de projet</h3>\n<p><b>Clarence Swanton</b> - Universit\u00e9 de Guelph</p>\n\n<h2>Objectif</h2>\n<p><b>D\u00e9terminer la p\u00e9riode critique dans la vie des plants de carotte o\u00f9 le champ doit \u00eatre exempt de mauvaises herbes et conseiller les producteurs sur la r\u00e9duction de l'usage d'herbicides \u00e0 partir des recommandations issues de cette \u00e9tude</b></p>\n\n<h2>Sommaire des r\u00e9sultats</h2>\n\n<p>Les carottes r\u00e9sistent mal aux mauvaises herbes et des mauvaises herbes laiss\u00e9es sans contr\u00f4le peuvent d\u00e9truire compl\u00e8tement une culture. Les producteurs utilisent une combinaison de moyens de contr\u00f4le m\u00e9caniques et d'herbicides pour \u00e9liminer les mauvaises herbes, ce qui comprend le recours \u00e0 de fortes doses de produits chimiques r\u00e9siduaires. En g\u00e9n\u00e9ral, les producteurs de carottes ont une tol\u00e9rance z\u00e9ro pour les mauvaises herbes, et, en l'absence de l'information fond\u00e9es sur des principes scientifiques et \u00e9conomiques concernant la gestion des mauvaises herbes, ils vont tenter d'\u00e9liminer les mauvaises herbes pendant toute la dur\u00e9e de la saison. Ce projet avait pour but d'\u00e9tablir la p\u00e9riode critique exempte de mauvaises herbes dans les plantations de carottes, d\u00e9finie comme la p\u00e9riode suivant l'apparition des pousses pendant laquelle il faut contr\u00f4ler les mauvaises herbes afin de pr\u00e9venir une d\u00e9t\u00e9rioration inacceptable de la r\u00e9colte. La connaissance de la p\u00e9riode critique exempte de mauvaises herbes va permettre aux producteurs de prendre des d\u00e9cisions \u00e9clair\u00e9es en mati\u00e8re d'\u00e9limination des mauvaises herbes, fond\u00e9es sur les co\u00fbts \u00e9conomiques et les avantages pour l'environnement, ce qui signifie lutter contre les mauvaises herbes seulement lorsqu'elles nuisent au rendement de la culture.</p>\n\n<p>L'\u00e9tude a \u00e9t\u00e9 effectu\u00e9e en 2007 et 2008 par des scientifiques de l'Universit\u00e9 de Guelph, en collaboration avec des chercheurs d'<abbr title=\"Agriculture et Agroalimentaire Canada\">AAC</abbr>, dans trois r\u00e9gions de culture de carottes du sud de l'Ontario : l'une sur le sol organique (terre noires) de la r\u00e9gion mar\u00e9cageuse de Bradford, ainsi que sur des sols min\u00e9raux aux stations de recherche de Ridgetown et Simcoe. On a utilis\u00e9 des cultivars commerciaux courants (Enterprise, Pursuit, Fontana) dans des cultures en terrain vallonn\u00e9 et en terrain plat, afin de comparer le rendement et la biomasse des mauvaises herbes dans les cultures de carottes sous quatre situations diff\u00e9rentes : toute la saison sans mauvaises herbes, sans mauvaises herbes pendant des p\u00e9riodes de temps de dur\u00e9es croissantes (jusqu'aux stades de 2, 4, 6, 8, 10 et 12 feuilles de la croissance des carottes), avec des mauvaises herbes toute la saison et avec des r\u00e9gimes de traitement commerciaux courantes.</p>\n\n<p>On a constat\u00e9 que la date d'ensemencement, l'ampleur de l'infestation des mauvaises herbes et la dur\u00e9e de l'apparition des mauvaises herbes exercent une influence sur la p\u00e9riode critique exempte de mauvaises herbes dans les plantations de carottes. La p\u00e9riode critique exempte de mauvaises herbes \u00e9tait relativement longue, s'\u00e9tendant jusqu'\u00e0 1 067 jours-degr\u00e9s de croissance (ou environ 89 jours apr\u00e8s la plantation, quand les carottes atteignent le stade de 12 feuilles) quand les carottes \u00e9taient sem\u00e9es t\u00f4t et quand l'infestation des mauvaises herbes \u00e9tait forte. Par contre, la p\u00e9riode critique exempte de mauvaises herbes \u00e9tait courte et durait au moins de 414 jours-degr\u00e9s de croissance (ou environ 33 jours apr\u00e8s la plantation, jusqu'\u00e0 ce que les carottes atteignent le stade des quatre feuilles) quand les carottes \u00e9taient sem\u00e9es plus tard et l'infestation des mauvaises herbes \u00e9tait de mod\u00e9r\u00e9e \u00e0 faible.</p>\n\n<p>Le principal r\u00e9sultat de cette \u00e9tude est que la dur\u00e9e maximale pendant laquelle les carottes doivent \u00eatre exemptes de mauvaises herbes, sans compromettre le rendement, est jusqu'au stade de 12 feuilles de la croissance de la culture. \u00c0 partir de ces constatations, les producteurs sont recommand\u00e9es a examiner les champs afin de d\u00e9pister les mauvaises herbes jusqu'\u00e0 ce que les carottes aient atteintes le stade de 12 feuilles afin de prot\u00e9ger le rendement des cultures de carottes. Cette p\u00e9riode critique pourrait se pr\u00e9senter plus t\u00f4t si les carottes \u00e9taient ensemenc\u00e9es plus tard dans la saison et si l'infestation de mauvaises herbes \u00e9tait moyenne \u00e0 faible. Il est important de tenir compte de l'information recueillie en d\u00e9pistant le champ en prenant des d\u00e9cisions de contr\u00f4le des mauvaises herbes appropri\u00e9es pour chaque champ. Des recommandations concernant la p\u00e9riode de contr\u00f4le essentielle des mauvaises herbes dans les plantations de carottes seront incorpor\u00e9es dans les lignes directrices provinciales concernant le contr\u00f4le des mauvaises herbes afin d'aider les producteurs de carottes \u00e0 optimiser le rendement et la rentabilit\u00e9 tout en r\u00e9duisant les cons\u00e9quences \u00e9conomiques et environnementales de l'utilisation inutile des herbicides.</p>\n\n<p>Pour obtenir des informations additionnelles au sujet de ce projet ou de la gestion int\u00e9gr\u00e9e des mauvaises herbes, veuillez vous adresser \u00e0 <a href=\"mailto:cswanton@uoguelph.ca\">M. Clarence J. Swanton</a>, Ph. D.</p>\n\n \n\r\n    "
    }
}