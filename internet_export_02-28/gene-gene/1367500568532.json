{
    "dcr_id": "1367500568532",
    "title": {
        "en": "Verticillium wilt",
        "fr": "Verticilliose"
    },
    "modified": "2020-01-24",
    "issued": "2014-06-10",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1354137773743",
    "dc_date_created": "2014-06-10",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/05/vertici_1367500568532_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/05/vertici_1367500568532_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Verticillium wilt",
        "fr": "Verticilliose"
    },
    "breadcrumb": {
        "en": "Verticillium wilt",
        "fr": "Verticilliose"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2014-06-10",
            "fr": "2014-06-10"
        },
        "modified": {
            "en": "2020-01-24",
            "fr": "2020-01-24"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Verticillium wilt",
            "fr": "Verticilliose"
        },
        "subject": {
            "en": "plant pests;plant diseases",
            "fr": "plantes nuisibles;plantes-maladies et fl\u00e9aux"
        },
        "dcterms:subject": {
            "en": "trees;plant diseases",
            "fr": "maladie des plantes;arbre"
        },
        "description": {
            "en": "Verticillium wilt, caused by two similar fungal pathogens, Verticillium albo-artrum and Verticillium dalhiae, can infect over 300 kinds of annual, perennial and woody ornamental plants worldwide. Elm and maple trees are particularly susceptible to this pathogen. Both Verticillium spp. are soil-borne fungi that typically infect plants through wounds in the roots. Once infected, the fungus spreads throughout the plant by mycelium growth or via spores transported in the plant sap, eventually restricting water movement and causing branch dieback. Depending on the size of tree, they can be killed within a year or over time. Fungal spores can survive in the roots, trunk, or soil for long periods of time.",
            "fr": "La verticilliose est caus\u00e9e par deux champignons pathog\u00e8nes similaires, le Verticillium albo-atrum et le V. dahliae. Plus de 300 plantes annuelles, vivaces et ligneuses ornementales du monde entier peuvent \u00eatre touch\u00e9es par cette maladie, et l'orme et l'\u00e9rable y sont particuli\u00e8rement sensibles. Les deux esp\u00e8ces de Verticillium sont des champignons terricoles qui infectent g\u00e9n\u00e9ralement les plantes en y p\u00e9n\u00e9trant par des blessures des racines. Le champignon se propage alors au reste de la plante par la croissance de son myc\u00e9lium ou au moyen de spores transport\u00e9es par la s\u00e8ve et finit par bloquer l'approvisionnement en eau des branches, ce qui cause leur d\u00e9p\u00e9rissement. L'arbre peut \u00eatre tu\u00e9 en moins d'un an ou en une p\u00e9riode plus longue, selon sa grosseur. Les spores peuvent survivre dans les racines, le tronc et le sol pendant de longues p\u00e9riodes."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": " Ash; Maple; Elm; Verticillium dahlieae;Verticillium albo-atrum",
            "fr": " \u00e9rable et fr\u00eane; Orme;Verticillium albo-atrum et Verticillium dahlieae"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Verticillium wilt",
        "fr": "Verticilliose"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <p><i>Verticillium albo-atrum, Verticillium dahlieae</i></p>\n\n<h2>Hosts</h2>\n\n<p>Elm, Maple, Ash</p>\n\n<h2>Distribution and Disease Cycle</h2>\n\n<div class=\"row\">\n<div class=\"pull-right col-md-4\"><figure><img src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/vertici_1.jpg\" alt=\"Description of this image follows\"><br><figcaption><i>Tree crown infected with Verticillium wilt.<br>Photo credit: USDA Forest Service Archive, USDA Forest Service, Bugwood.org</i></figcaption></figure></div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>Verticillium wilt, caused by two similar fungal pathogens, <i>Verticillium albo-artrum</i> and <i>Verticillium dalhiae</i>, can infect over 300 kinds of annual, perennial and woody ornamental plants worldwide. Elm and maple trees are particularly susceptible to this pathogen. Both Verticillium spp. are soil-borne fungi that typically infect plants through wounds in the roots. Once infected, the fungus spreads throughout the plant by mycelium growth or via spores transported in the plant sap, eventually restricting water movement and causing branch dieback. Depending on the size of tree, they can be killed within a year or over time. Fungal spores can survive in the roots, trunk, or soil for long periods of time.</p>\n</div>\n</div>\n\n<h2>Symptoms and signs</h2>\n\n<div class=\"row\">\n<div class=\"pull-right col-md-4\"><figure><img src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/vertici_2.jpg\" alt=\"Description of this image follows\"><br><figcaption><i>Stem cross section showing wood staining caused by Verticillium wilt infection.<br>Photo credit: USDA Forest Service - Northeastern Area Archive, USDA Forest Service, Bugwood.org</i></figcaption></figure></div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>Wilt diseases typically have similar symptoms, with sudden wilting of individual branches. Trees infected with Verticillium wilt will often have chlorotic or partial defoliation of leaves prior to branches wilting. Growth of infected trees may also be reduced or stunted, small twigs may dieback and crowns may appear sparse. In branches with advanced infections, the sapwood will often have bands or streaks of light to dark brown (elms) or light to dark green (maple).</p>\n\n\n<h2>Control</h2>\n\n<p>Avoid planting trees in soil where plants were known to have died from Verticillium wilt, as the fungal pathogen can live for years in the soil. Remove trees with severe infections, burning or burying wood. There are no chemical control options for Verticillium wilt.</p>\n</div>\n</div>\n\r\n    ",
        "fr": "\r\n    <p><i>Verticillium albo-atrum et Verticillium dahlieae</i></p>\n\n<h2>H\u00f4tes</h2>\n\n<p>Orme, \u00e9rable et fr\u00eane</p>\n\n<h2>Dispersion et cycle de vie de la maladie</h2>\n\n<div class=\"row\">\n<div class=\"pull-right col-md-4\">\n\t<figure>\n\t\t<img alt=\"La description de cette image suit\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/vertici_1.jpg\"><br>\n\t\t<figcaption><i>Houppier d'un arbre infect\u00e9 par la verticilliose.<br>\nSource : <span lang=\"en-ca\" xml:lang=\"en-ca\">USDA Forest Service Archive, USDA Forest Service</span>, Bugwood.org.</i></figcaption>\n\t</figure>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>La verticilliose est caus\u00e9e par deux champignons pathog\u00e8nes similaires, le <i>Verticillium albo-atrum</i> et le <i>V.\u00a0dahliae</i>. Plus de 300 plantes annuelles, vivaces et ligneuses ornementales du monde entier peuvent \u00eatre touch\u00e9es par cette maladie, et l'orme et l'\u00e9rable y sont particuli\u00e8rement sensibles. Les deux esp\u00e8ces de <i>Verticillium</i> sont des champignons terricoles qui infectent g\u00e9n\u00e9ralement les plantes en y p\u00e9n\u00e9trant par des blessures des racines. Le champignon se propage alors au reste de la plante par la croissance de son myc\u00e9lium ou au moyen de spores transport\u00e9es par la s\u00e8ve et finit par bloquer l'approvisionnement en eau des branches, ce qui cause leur d\u00e9p\u00e9rissement. L'arbre peut \u00eatre tu\u00e9 en moins d'un an ou en une p\u00e9riode plus longue, selon sa grosseur. Les spores peuvent survivre dans les racines, le tronc et le sol pendant de longues p\u00e9riodes.</p>\n</div>\n</div>\n\n\n<h2>Signes et sympt\u00f4mes</h2>\n\n<div class=\"row\">\n<div class=\"pull-right col-md-4\">\n\t<figure>\n\t\t<img alt=\"La description de cette image suit\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/vertici_2.jpg\"><br>\n\t\t<figcaption><i>Section transversale de tige laissant voir l'alt\u00e9ration de la couleur du bois caus\u00e9e par la verticilliose.<br>\nSource : <span lang=\"en-ca\" xml:lang=\"en-ca\">USDA Forest Service - Northeastern Area Archive, USDA Forest Service</span>, Bugwood.org.</i></figcaption>\n\t</figure>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\">\n<p>La verticilliose et les autres types de fl\u00e9trissements causent des sympt\u00f4mes similaires, soit le fl\u00e9trissement soudain de branches isol\u00e9es. Chez les arbres infect\u00e9s par la verticilliose, on observe souvent des feuilles chloros\u00e9es ou une chute partielle des feuilles avant le fl\u00e9trissement de la branche. En outre, la croissance de l'h\u00f4te peut \u00eatre r\u00e9duite ou retard\u00e9e, les petites branches peuvent d\u00e9p\u00e9rir, et le houppier peut sembler clairsem\u00e9. L'aubier des branches gravement infect\u00e9es pr\u00e9sente souvent des bandes ou des rayures brun clair \u00e0 brun fonc\u00e9 (orme) ou vert clair \u00e0 vert fonc\u00e9 (\u00e9rable).</p>\n\n\n<h2>Lutte</h2>\n\n<p>\u00c9viter de replanter des arbres aux endroits o\u00f9 il y a d\u00e9j\u00e0 eu des cas de verticilliose, car le champignon responsable de la maladie peut survivre plusieurs ann\u00e9es dans le sol. \u00c9liminer les arbres gravement infect\u00e9s, et les br\u00fblant ou en les enfouissant. Il n'existe aucune m\u00e9thode chimique de lutte contre la verticilliose.</p>\n</div>\n</div>\n\r\n    "
    }
}