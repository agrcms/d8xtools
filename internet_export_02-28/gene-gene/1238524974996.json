{
    "dcr_id": "1238524974996",
    "title": {
        "en": "What is Hydroponics?",
        "fr": "Qu'est-ce que la culture hydroponique?"
    },
    "modified": "2021-06-25",
    "issued": "2009-04-08",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1378999410478",
    "dc_date_created": "2009-04-08",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/03/st_cr_science_explained_1238524974996_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/03/st_cr_science_explained_1238524974996_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "What is Hydroponics?",
        "fr": "Qu'est-ce que la culture hydroponique?"
    },
    "breadcrumb": {
        "en": "What is Hydroponics?",
        "fr": "Qu'est-ce que la culture hydroponique?"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2009-04-08",
            "fr": "2009-04-08"
        },
        "modified": {
            "en": "2021-06-25",
            "fr": "2021-06-25"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "What is Hydroponics?",
            "fr": "Qu'est-ce que la culture hydroponique?"
        },
        "subject": {
            "en": "aafcsubject",
            "fr": "aafcsubject"
        },
        "dcterms:subject": {
            "en": "plants;water",
            "fr": "plante;eau"
        },
        "description": {
            "en": "Hydroponics means simply growing plants in water. The plants are placed in growing media such as rockwool, clay pellets, foam, recycled foam, gravel, peat, sawdust, or coconut fibres, then fed a nutrient solution to make them grow. Since many hydroponic methods employ some type of growing medium these methods are often termed \"\"soilless culture\"\", while water culture alone is true hydroponics.",
            "fr": "La culture hydroponique consiste simplement a faire pousser des plants dans l'eau. Les plants sont plac\u00e9s dans un milieu de croissance, comme de la laine min\u00e9rale, des granules d'argile, de la mousse ou de la mousse recycl\u00e9e, du gravier, de la tourbe, de la sciure ou des fibres de coco, puis aliment\u00e9s avec une solution nutritive qui favorise la croissance. Puisque de nombreuses m\u00e9thodes hydroponiques impliquent l'utilisation de certains milieux de croissance, on parle souvent de \"\u00ab culture hors sol \u00bb\", la v\u00e9ritable culture hydroponique s'effectuant dans l'eau seulement."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": " Greenhouse plants; nutrients; soilless culture;hydroponics",
            "fr": "  l\u00e9gumes de serre; \u00e9l\u00e9ments nutritifs; culture hors sol;hydroponique"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "publication",
            "fr": "publication"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "What is Hydroponics?",
        "fr": "Qu'est-ce que la culture hydroponique?"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <p>Hydroponics means simply growing plants in water. The plants are placed in growing media such as rockwool, clay pellets, foam, recycled foam, gravel, peat, sawdust, or coconut fibres, then fed a nutrient solution to make them grow. Since many hydroponic methods employ some type of growing medium these methods are often termed \"soilless culture\", while water culture alone is true hydroponics.</p>\n\n<p>The process of hydroponic growing in our oceans goes back to about the time the earth was created. Hydroponic growing preceded soil growing. But as a farming tool, many believe it started in the ancient city of Babylon with its famous hanging gardens. They were listed as one of the Seven Wonders of the Ancient World, and were probably one of the first successful attempts to grow plants hydroponically.</p>\n\n<p>Most greenhouse vegetable production in developed countries is done using a hydroponic system. Research at the <a href=\"https://profils-profiles.science.gc.ca/en/research-centre/harrow-research-and-development-centre\">Harrow Research and Development Centre</a> in Harrow, Ontario has resulted in a patented computerized program called the \"Harrow Fertigation Manager\" which controls the flow of nutrients to the plants in the greenhouses.</p>\n\n<p>The centre is recognized as the largest greenhouse vegetable research facility in North America.</p>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/green_plans.jpg\" alt=\"Greenhouse plants\"></p>\n\n<p>Greenhouse plants start off in Rockwool blocks, then are moved to bigger slabs where they are fed nutrients via the Harrow Fertigation Manager.</p>\n\n<figure class=\"indent-none\">\n<table class=\"table table-bordered\">\n<thead>\n<tr>\n<td></td>\n<th scope=\"col\">Growing Season</th>\n<th scope=\"col\">Time to produce fruit (blossom to fruit)</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th scope=\"row\">Cucumber</th>\n<td>4\u00a0months</td>\n<td>1 week</td>\n</tr>\n<tr>\n<th scope=\"row\">Tomato</th>\n<td>8-12\u00a0months</td>\n<td>6-8 weeks</td>\n</tr>\n<tr>\n<th scope=\"row\">Pepper</th>\n<td>8-12\u00a0months</td>\n<td>6-8 weeks</td>\n</tr>\n</tbody>\n</table>\n</figure>\n\n<figure>\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/green_plans_a.jpg\" alt=\"Fertigation\">\u00a0 <img src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/green_plans_b.jpg\" alt=\"Greenhouse plants fed nutrients via the Harrow Fertigation Manager.\"></p>\n</figure>\n\n<p>Fertigation - The delivery of water and nutrents to individual plants</p>\n\n\r\n    ",
        "fr": "\r\n    <p>La culture hydroponique consiste simplement a faire pousser des plants dans l'eau. Les plants sont plac\u00e9s dans un milieu de croissance, comme de la laine min\u00e9rale, des granules d'argile, de la mousse ou de la mousse recycl\u00e9e, du gravier, de la tourbe, de la sciure ou des fibres de coco, puis aliment\u00e9s avec une solution nutritive qui favorise la croissance. Puisque de nombreuses m\u00e9thodes hydroponiques impliquent l'utilisation de certains milieux de croissance, on parle souvent de \u00ab\u00a0culture hors sol\u00a0\u00bb, la v\u00e9ritable culture hydroponique s'effectuant dans l'eau seulement.</p>\n\n<p>La culture hydroponique dans nos oc\u00e9ans remonte \u00e0 la naissance de la Terre. Elle a pr\u00e9c\u00e9d\u00e9 la culture en sol. Cependant, en tant qu'outil agricole, nombreux sont ceux qui croient que cette forme de culture a pris naissance dans l'ancienne cit\u00e9 de Babylone, bien connue pour ses jardins suspendus. Ceux-ci sont l'une des sept merveilles de l'Antiquit\u00e9 et sont probablement l'une des premi\u00e8res r\u00e9ussites en mati\u00e8re de culture hydroponique de v\u00e9g\u00e9taux.</p>\n\n<p>La majeure partie des l\u00e9gumes de serre des pays  d\u00e9velopp\u00e9s sont produits au moyen d'un syst\u00e8me hydroponique. Au <a href=\"https://profils-profiles.science.gc.ca/fr/centre-recherche/centre-de-recherche-et-de-developpement-de-harrow\">Centre de recherche et de d\u00e9veloppement de Harrow </a> \u00e0  Harrow (Ontario), les chercheurs ont cr\u00e9\u00e9 un  programme informatique brevet\u00e9 appel\u00e9 \u00ab\u00a0<i>Harrow Fertigation Manager</i>\u00a0\u00bb, qui r\u00e8gle les apports d'\u00e9l\u00e9ments nutritifs aux plantes cultiv\u00e9es en serre.</p>\n\n<p>Ce Centre de recherches sur les cultures abrit\u00e9es et industrielles est reconnu comme \u00e9tant le plus grand centre de recherches sur les l\u00e9gumes de serre en Am\u00e9rique du Nord.</p>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/green_plans.jpg\" alt=\"La culture des plants\"></p>\n\n<p>La culture des plants de serre commence dans des blocs de laine min\u00e9rale. Les plants sont ensuite transf\u00e9r\u00e9s dans de plus grandes plaques o\u00f9 ils sont aliment\u00e9s en \u00e9l\u00e9ments nutritifs par le programme informatique.</p>\n\n<figure class=\"indent-none\">\n<table class=\"table table-bordered\">\n<thead> \n<tr>            \n<td></td>            \n<th scope=\"col\">Saison de croissance</th>            \n<th scope=\"col\">Temps n\u00e9cessaire \u00e0 la mise \u00e0 fruit (de la fleur au fruit)</th>        \n</tr>\n</thead> \n<tbody>        \n<tr>            \n<th scope=\"row\">Concombre</th>            \n<td>4\u00a0mois</td>            \n<td>1 semaine</td>        \n</tr>        \n<tr>            \n<th scope=\"row\">Tomate</th>            \n<td>8 \u00e0 12\u00a0mois</td>            \n<td>6 \u00e0 8 semaines</td>        \n</tr>        \n<tr>            \n<th scope=\"row\">Poivron</th>            \n<td>8 \u00e0 12\u00a0mois</td>            \n<td>6 \u00e0 8 semaines</td>        \n</tr>    \n</tbody>\n</table>\n</figure>\n\n<figure>\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/green_plans_a.jpg\" alt=\"Irrigation fertilisante\">\u00a0 <img src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/green_plans_b.jpg\" alt=\"Plants de serre aliment\u00e9s en \u00e9l\u00e9ments nutritifs par le programme informatique.\"></p>\n</figure>\n\n<p>Irrigation fertilisante - Apport d'eau et d'\u00e9l\u00e9ments nutritifs aux v\u00e9g\u00e9taux.</p>\n\r\n    "
    }
}