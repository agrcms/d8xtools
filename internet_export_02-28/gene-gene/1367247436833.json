{
    "dcr_id": "1367247436833",
    "title": {
        "en": "Cytospora canker",
        "fr": "Chancre cytopor\u00e9en"
    },
    "modified": "2020-01-24",
    "issued": "2014-06-10",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1354137773743",
    "dc_date_created": "2014-06-10",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/04/cc_1367247436833_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/04/cc_1367247436833_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Cytospora canker",
        "fr": "Chancre cytopor\u00e9en"
    },
    "breadcrumb": {
        "en": "Cytospora canker",
        "fr": "Chancre cytopor\u00e9en"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2014-06-10",
            "fr": "2014-06-10"
        },
        "modified": {
            "en": "2020-01-24",
            "fr": "2020-01-24"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Cytospora canker",
            "fr": "Chancre cytopor\u00e9en"
        },
        "subject": {
            "en": "plant pests;plant diseases",
            "fr": "plantes nuisibles;plantes-maladies et fl\u00e9aux"
        },
        "dcterms:subject": {
            "en": "plant diseases;trees",
            "fr": "maladie des plantes;arbre"
        },
        "description": {
            "en": "Cytospora canker is caused by a fungal pathogen which attacks many species of conifer, including Colorado, white, Norway, and Engelmann spruce and Douglas-fir, with Colorado spruce more susceptible than any other species. Spores are released from cankered branches throughout the growing season and spread by rain, wind, insects, birds or man to other branches on the same or other trees. Infection typically occurs through wounds, first infecting and killing the bark and eventually spreading to kill the entire branch. Cytospora can also be opportunistic, growing in bark killed by other pathogens. Damage is usually seen on older, larger trees.",
            "fr": "Le chancre cytospor\u00e9en est caus\u00e9 par un champignon s'attaquant \u00e0 de nombreuses esp\u00e8ces de conif\u00e8res, dont l'\u00e9pinette du Colorado, l'\u00e9pinette blanche, l'\u00e9pinette de Norv\u00e8ge, l'\u00e9pinette d'Engelmann et le douglas, mais l'\u00e9pinette du Colorado est nettement l'esp\u00e8ce la plus sensible \u00e0 cette maladie. Pendant toute la saison de v\u00e9g\u00e9tation, les branches chancr\u00e9es lib\u00e8rent des spores, qui sont transport\u00e9es par la pluie, le vent, les insectes, les oiseaux ou les humains vers d'autres branches du m\u00eame arbre ou vers d'autres arbres. L'infection emprunte habituellement une blessure. Elle commence par atteindre et tuer l'\u00e9corce, puis elle s'\u00e9tend et finit par tuer toute la branche. Le Cytospora kunzei semble \u00eatre un champignon opportuniste, car il peut pousser dans l'\u00e9corce d\u00e9j\u00e0 tu\u00e9e par d'autres pathog\u00e8nes. Les dommages sont g\u00e9n\u00e9ralement observ\u00e9s sur les vieux arbres ayant atteint une grande taille."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": " Colorado and White spruce;Cytospora kunzei",
            "fr": " \u00c9pinette du Colorado et \u00e9pinette blanche;Cytospora kunzei"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Cytospora canker",
        "fr": "Chancre cytopor\u00e9en"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    \n<p><i>Cytospora kunzei</i></p>\n\n<h2>Hosts</h2>\n\n<p>Colorado and White spruce</p>\n\n\n<h2>Distribution and Disease Cycle</h2>\n  \n\n<div class=\"row\">\n  \n  \n    <div class=\"col-md-4 pull-right mrgn-lft-md\">\n<figure><img src=\"http://www.agr.gc.ca/resources/prod/img/pfra-arap/img/cc_1.jpg\" alt=\"Description of this image follows\"><br>\n<figcaption class=\"small\"><i>Cytospora canker in Colorado spruce.<br>Photo credit: Joseph O'Brien, USDA Forest Service, Bugwood.org</i></figcaption></figure>\n </div>\n \n <div class=\"mrgn-lft-md mrgn-rght-md\">  \n<p>Cytospora canker is caused by a fungal pathogen which attacks many species of conifer, including Colorado, white, Norway, and Engelmann spruce and Douglas-fir, with Colorado spruce more susceptible than any other species. Spores are released from cankered branches throughout the growing season and spread by rain, wind, insects, birds or man to other branches on the same or other trees. Infection typically occurs through wounds, first infecting and killing the bark and eventually spreading to kill the entire branch. Cytospora can also be opportunistic, growing in bark killed by other pathogens. Damage is usually seen on older, larger trees.</p>\n\n  \n</div>\n</div>\n\n\n<h2>Symptoms and Signs</h2>\n\n<div class=\"row\">\n     \n  \n <div class=\"col-md-4 pull-right mrgn-lft-md\">\n <figure>\n<img src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/cc_2.jpg\" alt=\"Description of this image follows\"><br>\n<figcaption class=\"small\"><i>Cytospora canker close-up with bark peeled.<br>Photo credit: Agroforestry Development Centre</i></figcaption>\n</figure>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\"><p>Cankers are a necrotic lesion on a localized area of stem/trunk tissue where tissue has died. Symptoms of Cytospora canker typically start on lower branches, with all needles on a branch being equally affected. Needles on infected branches turn brown and eventually drop, leaving entire branches bare, with dead (cankered) areas of the bark exuding a white or bluish resin. Infection is usually most severe on crowded or stressed trees.<br></p>  \n \n   <h2>Control</h2>\n<p>Because Cytospora typically infects trees weakened by environmental stresses, maintain tree vigour by watering during periods of drought, do not cultivate deeper than a few inches nearby trees and avoid inflicting wounds to bark which act as infection entry points. Remove and dispose of infected branches, these branches will not recover and only will serve as a source of infection. Prune during late winter or during dry periods whenever cankered branches are discovered. Prune to a lateral branch at least 10-15 centimeters (cm) below any visible cankers or to the main trunk as required, sterilizing pruning tools with 5% bleach solution or alcohol between cuts. When trees become large and crowded, thinning out some trees may help increase aeration and provide some control. There are no chemicals registered for canker control in spruce.</p>  \n  </div>\n\n</div>\n\r\n    ",
        "fr": "\r\n    \n\n<p><i>Cytospora kunzei</i></p>\n\n<h2>H\u00f4tes</h2>\n\n<p>\u00c9pinette du Colorado et \u00e9pinette blanche</p>\n\n\n<h2>Dispersion et cycle de vie de la maladie</h2>\n\n<div class=\"row\">\n  \n    \n    <div class=\"col-md-4 pull-right mrgn-lft-md\">\n    \n    <figure>\n<img alt=\"La description de cette image suit\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/cc_1.jpg\"><br>\n<figcaption class=\"small\"><i>Chancre cytospor\u00e9en sur une<br> \u00e9pinette du Colorado.<br>\nSource : Joseph O'Brien, <span lang=\"en\" xml:lang=\"en\">USDA Forest Service</span>, Bugwood.org</i></figcaption>\n</figure>\n</div>\n\n<div class=\"mrgn-lft-md mrgn-rght-md\"><p>Le chancre cytospor\u00e9en est caus\u00e9 par un champignon s'attaquant \u00e0 de nombreuses esp\u00e8ces de conif\u00e8res, dont l'\u00e9pinette du Colorado, l'\u00e9pinette blanche, l'\u00e9pinette de Norv\u00e8ge, l'\u00e9pinette d'Engelmann et le douglas, mais l'\u00e9pinette du Colorado est nettement l'esp\u00e8ce la plus sensible \u00e0 cette maladie. Pendant toute la saison de v\u00e9g\u00e9tation, les branches chancr\u00e9es lib\u00e8rent des spores, qui sont transport\u00e9es par la pluie, le vent, les insectes, les oiseaux ou les humains vers d'autres branches du m\u00eame arbre ou vers d'autres arbres. L'infection emprunte habituellement une blessure. Elle commence par atteindre et tuer l'\u00e9corce, puis elle s'\u00e9tend et finit par tuer toute la branche. Le Cytospora kunzei semble \u00eatre un champignon opportuniste, car il peut pousser dans l'\u00e9corce d\u00e9j\u00e0 tu\u00e9e par d'autres pathog\u00e8nes. Les dommages sont g\u00e9n\u00e9ralement observ\u00e9s sur les vieux arbres ayant atteint une grande taille.</p>\n\n  </div>\n</div>\n\n\n<h2>Signes et sympt\u00f4mes</h2>\n\n\n<div class=\"row\">\n  \n   <div class=\"col-md-4 pull-right mrgn-lft-md\">\n   \n    <figure>\n<img alt=\"La description de cette image suit\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/cc_2.jpg\"><br> \n<figcaption class=\"small\"><i>Gros plan d'un chancre cytospor\u00e9en,<br> avec \u00e9corce\u00a0enlev\u00e9e.<br>\nSource : Centre du d\u00e9veloppement<br> de l'agroforesterie.</i></figcaption>\n\t</figure>\n   </div>\n \n<div class=\"mrgn-lft-md mrgn-rght-md\"><p>Le chancre est une l\u00e9sion n\u00e9crotique (constitu\u00e9e de tissus morts) occupant une zone localis\u00e9e du tronc ou de la tige. Les sympt\u00f4mes du chancre cytospor\u00e9en apparaissent g\u00e9n\u00e9ralement sur les branches inf\u00e9rieures et touchent de mani\u00e8re \u00e9gale toutes les aiguilles de chaque branche atteinte. Ces aiguilles brunissent et finissent par tomber, ce qui laisse la branche enti\u00e8rement d\u00e9nud\u00e9e, avec des zones mortes (chancr\u00e9es) d'o\u00f9 suinte une r\u00e9sine blanche ou bleut\u00e9e. Les infections les plus graves surviennent g\u00e9n\u00e9ralement chez les arbres stress\u00e9s ou plant\u00e9s de mani\u00e8re trop serr\u00e9e.</p>\n\n  \n \n  <h2>Lutte</h2>\n<p>Comme le Cytospora kunzei s'attaque habituellement aux arbres d\u00e9j\u00e0 affaiblis par des stress d'origine environnementale, il faut maintenir la vigueur des arbres en les arrosant en p\u00e9riode de s\u00e9cheresse, ne pas travailler la terre \u00e0 plus de quelques pouces de profondeur \u00e0 proximit\u00e9 des arbres et \u00e9viter d'infliger \u00e0 l'\u00e9corce des blessures qui pourraient servir de voie d'entr\u00e9e au pathog\u00e8ne. Enlever et d\u00e9truire les branches infect\u00e9es, car elles ne peuvent pas se r\u00e9tablir et constituent une source d'infection. Si des chancres sont observ\u00e9s, \u00e9monder les arbres vers la fin de l'hiver ou en p\u00e9riode s\u00e8che; rabattre la branche atteinte jusqu'\u00e0 une ramification lat\u00e9rale situ\u00e9e \u00e0 au moins 10 \u00e0 15\u00a0centim\u00e8tres (cm) de tout chancre visible ou jusqu'au tronc, selon le cas, en st\u00e9rilisant les outils d'\u00e9mondage, apr\u00e8s chaque coupe, dans de l'alcool ou dans une solution renfermant 5\u00a0% d'eau de Javel. S'il s'agit d\u00e9j\u00e0 de grands arbres formant un peuplement serr\u00e9, l'abattage de quelques arbres peut am\u00e9liorer la circulation de l'air et ainsi aider \u00e0 pr\u00e9venir la maladie. Aucun produit chimique n'est homologu\u00e9 contre le chancre cytospor\u00e9en des \u00e9pinettes.</p>\n </div> \n</div>\n\n\r\n    "
    }
}