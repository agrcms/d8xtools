{
    "dcr_id": "1415860000002",
    "title": {
        "en": "Canada's red meat and livestock industry at a glance\u2026",
        "fr": "Aper\u00e7u de l'industrie de la viande rouge au Canada...."
    },
    "modified": "2021-06-15",
    "issued": "2013-02-07",
    "templatetype": "content page 1 column",
    "page_type": "1",
    "node_id": "1415863600003",
    "dc_date_created": "2013-02-07",
    "managing_branch": "MISB",
    "parent_node_id": "1354137773862",
    "short_title": "Industry profile",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/MISB-DGSIM/SDAD-DDAS/RML-VRB/WORKAREA/RML-VRB/templatedata/comm-comm/gene-gene/data/industry_profile_redmeat_1415860000002_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/MISB-DGSIM/SDAD-DDAS/RML-VRB/WORKAREA/RML-VRB/templatedata/comm-comm/gene-gene/data/industry_profile_redmeat_1415860000002_fr"
    },
    "type_name": "sitemapinit",
    "has_dcr_id": true,
    "label": {
        "en": "Industry profile",
        "fr": "Portrait de l'industrie"
    },
    "breadcrumb": {
        "en": "Industry profile",
        "fr": "Portrait de l'industrie"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2013-02-07",
            "fr": "2013-02-07"
        },
        "modified": {
            "en": "2021-06-15",
            "fr": "2021-06-15"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Canada's red meat and livestock industry at a glance\u2026",
            "fr": "Aper\u00e7u de l'industrie de la viande rouge au Canada...."
        },
        "subject": {
            "en": "goats;veal;sheep",
            "fr": "graines de tournesol;semences-production;statistiques du commerce"
        },
        "dcterms:subject": {
            "en": "food;dairy industry;statistics",
            "fr": "m\u00e9dicament;alimentation animale;\u00e9conomie agricole"
        },
        "description": {
            "en": "Documentation and example pages - AAFC Theme for WET3.0.6",
            "fr": "Documentation et pages de d\u00e9monstration - Th\u00e8me d'AAC pour BOEW3.0.6"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "assets.Low",
            "fr": null
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "sound",
            "fr": "description de l'organisation"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada;Agriculture and Agri-Food Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada;Agriculture et Agroalimentaire Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Canada's red meat and livestock industry at a glance\u2026",
        "fr": "Aper\u00e7u de l'industrie de la viande rouge au Canada...."
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    \n\n\n<p>Canada\u2019s red meat industry includes beef and veal, pork, lamb and mutton, goat, rabbit, horse, as well as venison and bison. The red meat industry had annual shipments worth $22.3 billion in 2019. </p>\n<p>Canada\u2019s meat processing companies manufacture a wide variety of meat products ranging from fresh and frozen meat to processed, smoked, canned and cooked meats, as well as sausage and deli meats. About 70% of processed meats in Canada, such as sausages or cold cuts, are made with pork.</p>\n<p>The wholesomeness and consistent safety of Canadian red meat is well established both in Canada and internationally. The Canadian Food Inspection Agency (CFIA) works with the industry to maintain and enhance this reputation. The CFIA inspects imported products and federally registered establishments that produce processed meats and ready-to-eat products to verify compliance with food safety regulations.</p>\n<section>\n<h2>Size of the industry:</h2>\n<p>By 2019, there were:</p>\n<ul>\n<li>12.24 million cattle and calves, down 1.4% from the previous year, on 72,860 Canadian farms and ranches. Alberta accounts for approximately 40% of the inventory. Farm cash receipts from the sale of non-dairy cattle and calves in 2019 totaled $8.3 billion, 12.4% of total farm receipts. </li>\n<li>13.93 million hogs, down 0.2% from the previous year, on 7,640 farms. Farm cash receipts from the sale of hogs in 2019 totaled $4.6 billion, 6.9% of total farm receipts.</li>\n<li>1,040,200 sheep and lambs down 2% from the previous year, on 9,390 farms. Canadian sheep production is primarily located in Ontario, Quebec and Alberta (72%). Farm cash receipts for sheep and lambs in 2019 totaled $189 million, 0.3% of total farm receipts.</li>\n<li>230,034 goats on 5,627 farms in Canada were reported in the 2016 Census of Agriculture, a growth of 2% from 2011. In Canada, the goat industry can be segmented into three distinct sectors: chevon (meat), dairy (milk) and fibre (mohair and cashmere).</li>\n<li>119,314 bison on 975 farms in Canada according to the 2016 Census of Agriculture. Bison production is primarily concentrated in the West, with 80% of the herd located in Saskatchewan and Alberta. Exports of bison meat are primarily to the US, but boneless product is also shipped to numerous countries in Europe, most notably France, Switzerland and Germany. </li>\n<li>37,343 head of Canadian farm raised cervids (deer and elk) on about 600 farms were reported in the 2016 Census of Agriculture, a decrease of 28% from 2011. Elk are primarily farmed in Western Canada and red deer in the Eastern provinces. Fallow deer, white-tailed deer and other cervid species are found throughout Canada.</li>\n</ul>\n</section>\n<section>\n<h2>Exports:</h2>\n<ul>\n<li>\tCanada\u2019s beef and veal exports increased by 10% from 2018 to 438,641 tonnes in 2019, valued at $3.2 billion. Exports to the United States were up by 7% in 2019, compared to 2018. Exports to Asia were mixed; Japan and China continued to import more beef and veal from Canada, increasing 54% and 17% respectively, from 2018, while exports to Hong Kong decreased by 18%. Exports to Mexico increased by 8% from 2018. The United States is the major export market for Canadian beef, accounting for approximately 72% of exports.</li>\n<li>In 2019, 722,872 head of cattle were exported to the United States for breeding, feeding and processing with slaughter cattle exports making up the largest proportion of trade (72%). In 2019, feeder cattle exports to the US represented 26% of the live exports.</li>\n<li>In 2019, 21.7 million hogs went to market in Canadian plants, an increase of 1.3% over 2018. In addition, 831 thousand head went to processing facilities in the United States and 4.1 million isoweans and feeder hogs went for feeding and finishing on United States farms. Live hog exports to all countries decreased by 3% over 2019 to 5.1 million head.</li>\n<li>Pork exports increased in 2019 to 1,262,326 tonnes; up by 0.02% from 2018. Exports of pork and pork products to all countries were $4.2 billion in 2019. Sales to the United States decreased 8% year-over-year to 319,845 tonnes, while shipments to Japan decreased 1% to 261,945 tonnes. Exports to China were down 10% to 255,507 tonnes, due to the Chinese suspension of Canadian pork and beef during the second half of 2019. </li>\n<li>Processed pork exports totaled 80,785 tonnes in 2019 with the United States being the dominant purchaser (55%).</li>\n</ul>\n<p>In addition to red meat exports, Canada can provide halal-certified, kosher and organic meat and meat products.</p>\n<br>\n<p><strong>...want more information?</strong> </p>\n<br>\n<p>For more detailed information, contact:</p>\n<p>Patti Negrave<br>Deputy Director<br>Red Meat Section - Animal Industry Division <br>Agriculture and Agri-Food Canada<br>1341 Baseline Road, Tower 5, 4<sup>th</sup> floor<br>Ottawa, Ontario K1A 0C5<br>\nTelephone: 613-773-0226<br>\nFax: 613-773-0300<br>\nEmail: <a href=\"mailto:patti.negrave@canada.ca\">patti.negrave@canada.ca</a><br>\nWeb site: <a href=\"/eng/canadas-agriculture-sectors/animal-industry/red-meat-and-livestock-market-information/?id=1415860000001\">http://www.agr.gc.ca/redmeat-vianderouge/index_eng.htm</a></p>\n<div class=\"well\">\n<p>If you have any questions or require additional information, please send an e-mail to <a title=\"Email: Animal Industry Division - Market Information Section\" href=\"mailto:aafc.redmeat-vianderouge.aac@agr.gc.ca\">aafc.redmeat-vianderouge.aac@agr.gc.ca</a></p>\n<h3>Our service standards</h3>\n<p>Service standards play a vital role in our market information program\u2019s commitment to planning, reporting and performance management. They clearly define the level of service that clients can expect under normal circumstances.</p>\n<p>View the <a title=\"Service standards for red meat and livestock reports\" href=\"http://www.agr.gc.ca/eng/industry-markets-and-trade/statistics-and-market-information/by-product-sector/red-meat-and-livestock/?id=1480968240072\">Service standards for red meat and livestock reports.</a></p>\n</div>\n</section>\n\r\n    ",
        "fr": "\r\n    \n\n\n\n<p>L\u2019industrie de la viande rouge comprend le b\u0153uf, le veau, le porc, l\u2019agneau et le mouton, la ch\u00e8vre, le lapin, le cheval ainsi que la venaison et le bison. L\u2019industrie de la viande rouge a enregistr\u00e9 des exp\u00e9ditions annuelles atteignant 22,3 milliards de dollars en 2019.</p>\n\n<p>Les entreprises canadiennes de transformation de la viande pr\u00e9parent une gamme vari\u00e9e de produits. Cette gamme s'\u00e9tend de la viande fra\u00eeche et congel\u00e9e aux viandes transform\u00e9es, fum\u00e9es et en conserve ainsi qu'\u00e0 la saucisse et \u00e0 la charcuterie. Environ 70% de la viande transform\u00e9e au Canada, telle que la saucisse et les viandes froides est fabriqu\u00e9e \u00e0 partir de porc.</p>\n\n<p>La fra\u00eecheur et la salubrit\u00e9 constante de la viande rouge et de ses produits provenant du Canada est reconnue dans de nombreux pays. L\u2019Agence canadienne d\u2019inspection des aliments (ACIA) travaille avec l\u2019industrie pour maintenir et consolider cette r\u00e9putation. L'ACIA inspecte les importations et les \u00e9tablissements enregistr\u00e9s aupr\u00e8s du gouvernement f\u00e9d\u00e9ral qui produisent des viandes transform\u00e9es et des plats pr\u00eats-\u00e0-manger afin de v\u00e9rifier la conformit\u00e9 aux r\u00e8glements sur la salubrit\u00e9 des aliments.</p>\n\n<section>\n<h2>Taille de l'industrie:</h2>\n<p>2019 il y avait : </p>\n<ul>\n\n<li>\t12,24 millions de bovins et de veaux, repr\u00e9sentant une hausse de 1,4% par rapport \u00e0 l'ann\u00e9e pr\u00e9c\u00e9dente, recens\u00e9s sur 72 860 exploitations agricoles canadiennes. Environ 41% de cet inventaire se trouvait en Alberta. Les recettes mon\u00e9taires agricoles provenant de la vente de bovins et de veaux ont atteint 8,3 milliards de dollars en 2019, soit 12,4% des recettes agricoles totales.</li>\n\n<li>13,93 millions de porcs, repr\u00e9sentant une baisse de 0,2 % par rapport \u00e0 l\u2019ann\u00e9e pr\u00e9c\u00e9dente, recens\u00e9s sur 7 640 exploitations agricoles canadiennes. Les recettes mon\u00e9taires agricoles provenant de la vente de porcs destin\u00e9s \u00e0 l\u2019abattage ont repr\u00e9sent\u00e9 4,6 milliards de dollars en 2019, soit 6,9% des recettes agricoles totales.</li>\n\n<li>1 040 300 moutons et agneaux, repr\u00e9sentant une baisse de 2% par rapport \u00e0 l\u2019ann\u00e9e pr\u00e9c\u00e9dente, recens\u00e9s sur 9 390 exploitations agricoles canadiennes. La production ovine canadienne se retrouve principalement en Ontario, au Qu\u00e9bec et en Alberta (72%). Les recettes mon\u00e9taires agricoles associ\u00e9es \u00e0 la production ovine ont atteint 189 millions de dollars en 2019, soit 0,3% des recettes agricoles totales.</li>\n\n<li>Selon le Recensement de l\u2019agriculture en 2016, il y avait 230 034 ch\u00e8vres r\u00e9parties sur environ 5 627 exploitations agricoles canadiennes, une croissance de 2% comparativement a 2011. Au Canada, l\u2019industrie de la ch\u00e8vre est segment\u00e9e en trois secteurs distincts : chevreau (viande), produits laitiers (lait) et fibre (mohair et cachemire).</li>\n\n<li>Selon le Recensement de l\u2019agriculture en 2016, il y avait 119 314 bisons r\u00e9partis sur 975 exploitations au Canada. L\u2019\u00e9levage du bison est principalement concentr\u00e9 dans l\u2019ouest du pays, avec 80% du troupeau situ\u00e9 en Saskatchewan et en Alberta. La majorit\u00e9 des exportations de viande de bison sont exp\u00e9di\u00e9es aux \u00c9tats-Unis. Les produits d\u00e9soss\u00e9s sont aussi exp\u00e9di\u00e9s dans de nombreux pays d\u2019Europe, notamment en France, en Suisse et en Allemagne.</li>\n\n<li>Selon le Recensement de l\u2019agriculture en 2016, il y avait 37 343 cervid\u00e9s (chevreuils et wapitis) \u00e9lev\u00e9s sur 600 fermes canadiennes. Cette statistique e repr\u00e9sente une diminution de 28% au cours des cinq derni\u00e8res ann\u00e9es. Le wapiti est principalement \u00e9lev\u00e9 dans l\u2019Ouest du Canada et le cerf rouge dans les provinces de l\u2019Est. On d\u00e9nombre \u00e9galement des exploitations d\u2019\u00e9levage de daims, de cerfs de Virginie et d\u2019autres esp\u00e8ces de grands gibiers dans les diverses r\u00e9gions du pays.</li>\n</ul>\n</section>\n<section>\n<h2>Exportations:</h2>\n<ul>\n<li>\tLes exportations canadiennes de b\u0153uf et de veau ont augment\u00e9 de 10% par rapport \u00e0 2018 pour atteindre 438 641 tonnes en 2019, \u00e9valu\u00e9es \u00e0 3,2 milliards de dollars. Les exportations vers les \u00c9tats-Unis ont augment\u00e9 de 7% en 2019 par rapport \u00e0 2018. Les exportations vers l'Asie ont \u00e9t\u00e9 mitig\u00e9es; Le Japon et la Chine ont continu\u00e9 d'importer davantage de b\u0153uf et de veau du Canada, augmentant respectivement de 54% et 17% par rapport \u00e0 2018, tandis que les exportations vers Hong Kong ont diminu\u00e9 de 18%. Les exportations vers le Mexique ont augment\u00e9 de 8% par rapport \u00e0 2018. Les \u00c9tats-Unis sont le principal march\u00e9 d'exportation du b\u0153uf canadien, repr\u00e9sentant environ 72% des exportations.</li>\n\n<li>En 2019, environs 722 872 bovins ont \u00e9t\u00e9 export\u00e9s aux \u00c9tats-Unis pour la reproduction, l\u2019engraissement et l\u2019abattage. Les bovins d\u2019engraissement (bouvillons et g\u00e9nisses) export\u00e9s \u00e0 l'abattoir repr\u00e9sentaient la plus grande part des exportations (72%). En 2019, les exportations de bovins d\u2019engraissement vers les \u00c9tats-Unis repr\u00e9sentaient 26 % des exportations de bovins vivants.</li>\n\n<li>En 2019, 21,7 millions de porcs ont \u00e9t\u00e9 envoy\u00e9s dans des usines de transformation canadiennes, une augmentation de 1,3% par rapport \u00e0 2018. De plus, 831 000 t\u00eates ont \u00e9t\u00e9 exp\u00e9di\u00e9es vers des installations de transformation aux \u00c9tats-Unis et 4,1 millions de porcelets sevr\u00e9s et de porcs d\u2019engraissement ont \u00e9t\u00e9 envoy\u00e9s dans des exploitations agricoles am\u00e9ricaines \u00e0 des fins d\u2019engraissement et de finition. Les exportations de porcs vivants ont diminu\u00e9 de 3% par rapport \u00e0 2018, atteignant 5,1 millions de t\u00eates.</li>\n\n<li>Les exportations de viande et produits de porc ont augment\u00e9 en 2019 de 0,02% par rapport \u00e0 2018, pour se chiffrer \u00e0 1 262 326 tonnes. La valeur totale des exportations de porc et de ses produits dans tous les pays en 2019 totalisait 4,2 milliards de dollars. Les ventes aux \u00c9tats-Unis ont baiss\u00e9 d\u2019ann\u00e9e en ann\u00e9e de 8% pour totaliser 319 845 tonnes. les exp\u00e9ditions vers le Japon ont diminu\u00e9 de 1% pour se chiffrer \u00e0 261 945 tonnes. Les exportations vers la Chine ont diminu\u00e9 de 10 % pour s\u2019\u00e9tablir \u00e0 255 507 tonnes, en raison de la suspension chinoise du porc et du b\u0153uf canadiens au cours du deuxi\u00e8me semestre de 2019.</li>\n\n<li>Le total des ventes de porc transform\u00e9 repr\u00e9sentait environ 80 785 tonnes en 2019, les \u00c9tats-Unis \u00e9tant l\u2019acheteur principal, repr\u00e9sentant 55% du march\u00e9.</li>\n</ul>\n\n<p>En plus de ses exportations de viandes rouges, le Canada peut fournir de la viande certifi\u00e9e halal et casher ainsi que toute une gamme de viande et de produits de viande biologiques.</p>\n<br>\n<p><strong>...vous d\u00e9sirez plus d'information?</strong></p>\n<br>\n<p>Communiquez avec\u00a0:</p>\n<p>Patti Negrave<br>Directrice adjointe<br>Section de la viande rouge<br>Division de l'industrie animale<br>Agriculture et Agroalimentaire Canada<br>1341, chemin Baseline, Tour 5, 4<sup>e</sup> \u00e9tage<br>Ottawa (Ontario) K1A 0C5<br>\nT\u00e9l.\u00a0: 613-773-0226<br>\nFax\u00a0: 613-773-0300<br>\nCourriel\u00a0: <a href=\"mailto:patti.negrave@canada.ca\">patti.negrave@canada.ca</a><br>\nSite Web\u00a0: <a href=\"/fra/secteurs-agricoles-du-canada/production-animale/information-sur-le-marche-des-viandes-rouges/?id=1415860000001\">http://www.agr.gc.ca/redmeat-vianderouge/index_fra.htm</a></p>\n\n<div class=\"well\">\n<p>Pour toute question ou information additionnelle, veuillez communiquer avec nous par courriel \u00e0 <a href=\"mailto:aafc.redmeat-vianderouge.aac@agr.gc.ca\" title=\"Courriel : Division de l'industrie animale, Section de l'information sur les march\u00e9s\">aafc.redmeat-vianderouge.aac@agr.gc.ca</a></p>\n<h3>Nos normes de service</h3>\n<p>Les normes de service jouent un r\u00f4le cl\u00e9 dans les processus de gestion de la planification, des rapports et du rendement de notre programme d'information sur le march\u00e9. Elles d\u00e9finissent clairement le niveau de rendement auquel les clients peuvent s'attendre lorsqu'ils re\u00e7oivent un service dans des circonstances normales.</p>\n<p>Visionnez les <a href=\"http://www.agr.gc.ca/fra/industrie-marches-et-commerce/statistiques-et-information-sur-les-marches/par-produit-secteur/viande-rouge-et-betail/normes-de-service-relatives-aux-rapports-sur-les-animaux-a-viande-rouge-et-le-betail/?id=1480968240072\" title=\"Normes de service relatives aux rapports sur les animaux \u00e0 viande rouge et le b\u00e9tail\">Normes de service relatives aux rapports sur les animaux \u00e0 viande rouge et le b\u00e9tail.</a></p>\n</div>\n</section>"
    }
}