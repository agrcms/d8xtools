{
    "dcr_id": "1185562486945",
    "title": {
        "en": "Farm surface water management",
        "fr": "Gestion de l'eau de surface en zone rurale"
    },
    "modified": "2020-01-24",
    "issued": "2009-03-31",
    "templatetype": "content page 1 column",
    "page_type": "1",
    "node_id": "1406207811914",
    "dc_date_created": "2009-03-31",
    "managing_branch": "PAB",
    "parent_node_id": "1354137773759",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/07/land_mang_robocow_multi_1185562486945_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/07/land_mang_robocow_multi_1185562486945_fr"
    },
    "type_name": "sitemapinit",
    "has_dcr_id": true,
    "label": {
        "en": "Farm surface water management",
        "fr": "Gestion de l'eau de surface en zone rurale"
    },
    "breadcrumb": {
        "en": "Farm surface water management",
        "fr": "Gestion de l'eau de surface en zone rurale"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2009-03-31",
            "fr": "2009-03-31"
        },
        "modified": {
            "en": "2020-01-24",
            "fr": "2020-01-24"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Farm surface water management",
            "fr": "Gestion de l'eau de surface en zone rurale"
        },
        "subject": {
            "en": "water supply;water quality;water conservation;groundwater;farm management;water resources;environmental health",
            "fr": "pratiques de gestion b\u00e9n\u00e9fiques;approvisionnement en eau;eau souterraine;eau-qualit\u00e9;eau-conservation;eau ressource;hygi\u00e8ne du milieu;gestion de l'exploitation agricole"
        },
        "dcterms:subject": {
            "en": "land management;water",
            "fr": "gestion des terres;eau"
        },
        "description": {
            "en": "Robocow: Operation H2O is an animated video in which on-farm environmental perils are discovered and remedied by Robocow, the charismatic and enigmatic hero of the series. In Operation H2O Robocow identifies and corrects agricultural practices affecting surface water.",
            "fr": "t\u00e9l\u00e9chargez flash player \u00e0 partir du site macromedia robovache : op\u00e9ration h2o robovache : op\u00e9ration h2o est un jeu d'animation dans lequel robovache, le charismatique et \u00e9nigmatique h\u00e9ro du jeu, d\u00e9tecte les dangers pour l'environnement \u00e0 la ferme et les \u00e9limine."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": " macromedia robocow; learn more about water using multimedia; affecting surface water; animated environmental advocate;read html",
            "fr": " effets n\u00e9fastes; deuxi\u00e8me aventure; d\u00e9fense de l'environnement; flash format html robovache;op\u00e9ration h2o"
        },
        "audience": {
            "en": "students",
            "fr": "\u00e9tudiants"
        },
        "type": {
            "en": "multimedia",
            "fr": "multim\u00e9dia"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Farm surface water management",
        "fr": "Gestion de l'eau de surface en zone rurale"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <p>Obtaining good quality water from farm surface water sources is challenging. The key is protecting and enhancing the water source, and using a series of treatment processes. These treatment processes are called barriers: each barrier reduces specific water quality problems from being passed on in the water.</p>\n\n<p>The following section shows options for multi-barrier protection.  Practices to protect the source and treatment options for obtaining water quality appropriate for different farm uses are demonstrated.</p>\n\n<section>\n<h2 id=\"fswm2\">Dugout aeration</h2>\n\n<p>Aeration is a management technique which enhances water quality in reservoirs. In Canada, farm ponds or dugouts are constructed in clay soils, and used to collect and store water from snow melt and rain. Roughly\u00a03.5 to 6.0\u00a0 metres (m) deep, dugouts typically store from 2\u00a0to 30\u00a0million Litres of water. In winter they are completely covered with ice (usually from November to March). In summer water in dugouts will stagnate. However, when dugouts are continuously aerated year round, water quality is improved. This animation shows an aerated reservoir beside an unaerated reservoir. The benefits of aeration are shown and described for each season of the year. As a water treatment barrier, aeration will keep the water fresher with less nutrient recycling from the bottom sediment.</p>\n\n<p><img src=\"http://multimedia.agr.gc.ca/22048/22048-10/22048-10-thumbnail-590x332-eng.png\" alt=\"Dugout Aeration.\"></p>\n</section>\n\n\n\n<section>\n<h2 id=\"fswm3\">Coagulation</h2>\n\n<p>Coagulation is a chemical process used to remove unwanted particles or matter in water. It is a very common barrier used in municipal water treatment. Coagulation can be used for farm water treatment. The coagulation animation describes the process and benefits of coagulation. Simple procedures are shown to determine the correct chemical dose. A simple method is depicted for treating dugouts and small storage cells. Coagulation treatment will improve water quality for many farm needs.</p>\n\n<ul>\n<li><a href=\"/eng/?id=1189690765212\">Coagulation\u00a0- text version</a></li>\n<li><a href=\"/eng/?id=1189690765212#animation\">Coagulation\u00a0- animated version</a></li>\n</ul>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/terr/images/coagulation-flash.jpg\" alt=\"Screenshot of the animation Coagulation.\"></p>\n</section>\n\n\n\n<section>\n<h2 id=\"fswm4\">Filtration systems</h2>\n\n<p>Improving water quality almost always involves some sort of filtration process. Filtration barriers remove larger particles from water, including those formed by coagulation. The animation shows small-scale filters used to treat surface water: rapid sand, rapid carbon, slow sand, and biological carbon filters. The process is shown for each type of filter, with simple explanations of the benefits, limitations, and basic operational requirements.</p>\n\n<ul>\n<li><a href=\"/eng/?id=1189695164631\">Filtration systems\u00a0- text version</a></li>\n<li><a href=\"/eng/?id=1189695164631#animation\">Filtration systems\u00a0- animated version</a></li>\n</ul>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/terr/images/filtration-flash.jpg\" alt=\"Screenshot of the animation Filtration Systems.\"></p>\n</section>\n\n\n\n<section>\n<h2 id=\"fswm5\">Disinfection solutions</h2>\n\n<p>When high quality water is required, such as drinking water, household water or livestock watering, disinfection is an essential barrier. Disinfection kills disease-causing organisms. The disinfection animation shows why bacteria, parasites and viruses pass through the initial treatment barriers, and describes how chlorine or ultraviolet light may be used to kill these organisms. Disinfection is usually the last barrier in municipal water treatment. Continual water testing is always necessary to ensure the disinfection process is working effectively.</p>\n\n<ul>\n<li><a href=\"/eng/?id=1189692479708\">Disinfection solutions\u00a0- text version</a></li>\n<li><a href=\"/eng/?id=1189692479708#animation\">Disinfection solutions\u00a0- animated version</a></li>\n</ul>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/terr/images/disinfection-flash.jpg\" alt=\"Screenshot of the animation Disinfection Solutions.\"></p>\n</section>\n\n<section>\n<h2 id=\"fswm6\">Polishing methods</h2>\n\n<p>Small-scale water treatment systems for private rural water treatment are generally not managed by chemists, biologists, engineers and technologists, and the treated water quality is not frequently tested, as is the case for municipal supplies. Therefore for rural households, another water treatment polishing barrier is recommended after the initial pre-treatment processes. A polishing barrier will increase the safety of water used for human consumption. Two ideal polishing barriers are shown in the animation: reverse osmosis filtration and distillation. These processes are very effective in removing impurities that the pre-treatment barriers did not remove. A final note: Remember to operate and maintain every barrier, and be sure to test the final drinking water on a regular basis to ensure its safety for consumption.</p>\n\n<ul>\n<li><a href=\"/eng/?id=1189697135106\">Polishing methods\u00a0- text version</a></li>\n<li><a href=\"/eng/?id=1189697135106#animation\">Polishing methods\u00a0- animated version</a></li>\n</ul>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/terr/images/polish-flash.jpg\" alt=\"Screenshot of the animation Polishing Methods.\"></p>\n</section>\n\n\r\n    ",
        "fr": "\r\n    <p>Pour les exploitations agricoles, obtenir de l'eau de qualit\u00e9 \u00e0 partir des eaux de surface pr\u00e9sente tout un d\u00e9fi. La cl\u00e9 est de prot\u00e9ger et d'am\u00e9liorer la source d'eau en recourant \u00e0 un ensemble de proc\u00e9d\u00e9s de traitement. Ces proc\u00e9d\u00e9s sont appel\u00e9s barri\u00e8res\u00a0: chaque barri\u00e8re \u00e9limine de l'eau un probl\u00e8me particulier touchant sa qualit\u00e9. </p>\n\n<p>La section suivante d\u00e9crit les options pour une protection \u00e0 barri\u00e8res multiples.  Les pratiques pour prot\u00e9ger la source et les traitements pour obtenir la qualit\u00e9 d'eau appropri\u00e9e pour les diff\u00e9rentes utilisations \u00e0  la ferme sont d\u00e9montr\u00e9es.</p>\n\n<section>\n<h2 id=\"gp2\">A\u00e9ration des fosses-r\u00e9servoirs</h2>\n\n<p>L'a\u00e9ration est une technique de gestion qui am\u00e9liore la qualit\u00e9 de l'eau dans les r\u00e9servoirs. Au Canada, les \u00e9tangs de ferme, ou \u00e9tangs-r\u00e9servoirs, sont creus\u00e9s dans des sols d'argile et servent \u00e0 recueillir et \u00e0 entreposer l'eau provenant de la neige et de la pluie. Mesurant environ 3,5\u00a0\u00e0 6,0\u00a0 m\u00e8tres (m) de profondeur, les \u00e9tangs-r\u00e9servoirs stockent g\u00e9n\u00e9ralement 2 \u00e0 30\u00a0millions de litres d'eau. En hiver (habituellement de novembre \u00e0 mars), ils sont compl\u00e8tement recouverts de glace. En \u00e9t\u00e9, l'eau des \u00e9tangs stagne. Toutefois, si les \u00e9tangs-r\u00e9servoirs sont continuellement a\u00e9r\u00e9s, la qualit\u00e9 de l'eau est accrue. Cette animation compare un r\u00e9servoir a\u00e9r\u00e9 \u00e0 un r\u00e9servoir non a\u00e9r\u00e9. Les avantages de l'a\u00e9ration sont illustr\u00e9s et d\u00e9crits. Gr\u00e2ce \u00e0 la barri\u00e8re que constitue l'a\u00e9ration, l'eau reste plus propre et le recyclage des nutriments est moindre.</p>\n\n<p><img src=\"http://multimedia.agr.gc.ca/22048/22048-10/22048-10-thumbnail-590x332-fra.png\" alt=\"Capture d'\u00e9cran de l'animation : A\u00e9ration des fosses-r\u00e9servoirs.\"></p>\n</section>\n\n<section>\n<h2 id=\"gp3\">Coagulation</h2>\n\n<p>La coagulation est un processus chimique qui \u00e9limine les particules ou les mati\u00e8res ind\u00e9sirables dans l'eau. C'est une barri\u00e8re tr\u00e8s couramment utilis\u00e9e dans les r\u00e9seaux municipaux de traitement de l'eau. L'animation d\u00e9crit le proc\u00e9d\u00e9 et les avantages de la coagulation. Elle montre des proc\u00e9d\u00e9s simples permettant de d\u00e9terminer la dose n\u00e9cessaire de produits chimiques. L'animation illustre une m\u00e9thode simple de traitement des \u00e9tangs-r\u00e9servoirs et des petites cellules de coagulation. Le traitement par coagulation am\u00e9liore la qualit\u00e9 de l'eau destin\u00e9e \u00e0 de nombreux usages agricoles.</p>\n\n<ul>\n<li><a href=\"/fra/?id=1189690765212\">Coagulation\u00a0- version texte</a></li>\n<li><a href=\"/fra/?id=1189690765212#anmimation\">Coagulation\u00a0- version anim\u00e9e</a></li>\n</ul>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/terr/images/coagulation-flash_fr.jpg\" alt=\"Capture d'\u00e9cran de l'animation : Coagulation.\"></p>\n</section>\n\n<section>\n<h2 id=\"gp4\">Filtration</h2>\n\n<p>L'am\u00e9lioration de la qualit\u00e9 de l'eau n\u00e9cessite presque toujours un proc\u00e9d\u00e9 de filtration. Les barri\u00e8res de filtration \u00e9liminent les plus grosses particules de l'eau, y compris celles form\u00e9es par la coagulation. L'animation montre de petits filtres utilis\u00e9s pour traiter les eaux de surface\u00a0: filtre \u00e0 sable rapide, filtre au charbon rapide, filtre \u00e0 sable lent et filtre au charbon biologique. Le proc\u00e9d\u00e9 est illustr\u00e9 pour chaque type de filtre et accompagn\u00e9 d'une explication des avantages, des limites et des exigences op\u00e9rationnelles de base.</p>\n\n<ul>\n<li><a href=\"/fra/?id=1189695164631\">Filtration\u00a0- version texte</a></li>\n<li><a href=\"/fra/?id=1189695164631#animation\">Filtration\u00a0- version anim\u00e9e</a></li>\n</ul>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/terr/images/filtration-flash_fr.jpg\" alt=\"Capture d'\u00e9cran de l'animation : Filtration.\"></p>\n</section>\n\n<section>\n<h2 id=\"gp5\">Solutions de d\u00e9sinfection</h2>\n\n<p>Quand une haute qualit\u00e9 de l'eau est n\u00e9cessaire, comme pour l'eau potable ou l'eau domestique, une barri\u00e8re de d\u00e9sinfection est essentielle. La d\u00e9sinfection tue les organismes pathog\u00e8nes. L'animation montre pourquoi les bact\u00e9ries, les parasites et les virus traversent les premi\u00e8res barri\u00e8res de traitement, puis d\u00e9crit comment le chlore ou la lumi\u00e8re ultraviolette peuvent tuer ces organismes. La d\u00e9sinfection constitue habituellement la derni\u00e8re barri\u00e8re des r\u00e9seaux municipaux de traitement de l'eau. Des analyses de l'eau sont faites en continu pour s'assurer que le proc\u00e9d\u00e9 de d\u00e9sinfection fonctionne efficacement.</p>\n\n<ul>\n<li><a href=\"/fra/?id=1189692479708\">Solutions de d\u00e9sinfection\u00a0- version texte</a></li>\n<li><a href=\"/fra/?id=1189692479708#animation\">Solutions de d\u00e9sinfection\u00a0- version anim\u00e9e</a></li>\n</ul>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/terr/images/disinfection-flash_fr.jpg\" alt=\"Capture d'\u00e9cran de l'animation : Solutions de d\u00e9sinfection.\"></p>\n</section>\n\n<section>\n<h2 id=\"gp6\">M\u00e9thodes de polissage</h2>\n\n<p>Les syst\u00e8mes priv\u00e9s de traitement de l'eau \u00e0 petite \u00e9chelle utilis\u00e9s dans les secteurs ruraux ne sont g\u00e9n\u00e9ralement pas g\u00e9r\u00e9s par des chimistes, des biologistes, des ing\u00e9nieurs ou des techniciens. De plus, contrairement \u00e0 ce qui se passe dans les villes, la qualit\u00e9 de l'eau trait\u00e9e n'est pas fr\u00e9quemment v\u00e9rifi\u00e9e. Par cons\u00e9quent, on recommande le recours \u00e0 une autre barri\u00e8re, la barri\u00e8re de polissage, apr\u00e8s les proc\u00e9d\u00e9s initiaux de pr\u00e9-traitement. Une barri\u00e8re de polissage augmente la salubrit\u00e9 de l'eau destin\u00e9e \u00e0 la consommation humaine. L'animation montre deux barri\u00e8res id\u00e9ales de polissage\u00a0: filtration par osmose inverse et distillation. Ces proc\u00e9d\u00e9s sont tr\u00e8s efficaces pour \u00e9liminer les impuret\u00e9s que les barri\u00e8res de pr\u00e9-traitement n'ont pas retenues. Derni\u00e8re note\u00a0: N'oubliez pas de bien exploiter et d'entretenir chacune des barri\u00e8res et assurez-vous d'analyser r\u00e9guli\u00e8rement l'eau potable finale afin de v\u00e9rifier qu'elle est propre \u00e0 la consommation.</p>\n\n<ul>\n<li><a href=\"/fra/?id=1189697135106\">M\u00e9thodes de polissage\u00a0- version texte</a></li>\n<li><a href=\"/fra/?id=1189697135106#animation\">M\u00e9thodes de polissage\u00a0- version anim\u00e9e</a></li>\n</ul>\n\n<p><img src=\"https://www.agr.gc.ca/resources/prod/img/terr/images/polish-flash_fr.jpg\" alt=\"Capture d'\u00e9cran de l'animation : M\u00e9thodes de polissage.\"></p>\n</section>\n\n\n\r\n    "
    }
}