<?php
include "updatejsons/UpdateExport.php";

//global $argv;
global $sitemap_export_root;
global $export_root;
//global $nid_old_to_new;
//global $nid_new_to_old;
global $site_dir;
//global $db;
global $d7_nids;

$site_dir = 'default';
//$nid_old_to_new = array();
//$nid_new_to_old = array();
$d7_nids = array();

$myargs = array_slice($argv, 1);

$updateTypes = array('teamsite, sitemap'); //Removed 'page' it is now d8 update instead

// Override when testing
// When testing and you just want to test one content type, use the option for the type and it will update only this..

if (empty($myargs)) {
  echo "\n\n";
  echo "Syntax:\n";
  echo "php update_json.php full-path-containing-exports teamsite\n";
  echo "OR\n";
  echo "php update_json.php full-path-containing-exports teamsite full_path_containing_sitemap_exports\n";
  echo "\n";
  echo "When testing and you just want to test one content type, use the option for the type and it will update only this..\n";
  echo "\n";
  exit;
}

$update_only_option_type = false;
$export_root = array_shift($myargs);
$option = array_shift($myargs);
echo "export_root: $export_root \n";

if (!empty($option)) {
  echo "update option: $option \n";
  if (strlen($option) > 0) {
    $update_only_option_type = true;
    $updateTypes = array($option);
  }
}
chdir($export_root);

$sitemap_export_root_test = array_shift($myargs);
if (!empty($sitemap_export_root_test)) {
  $sitemap_export_root = $sitemap_export_root_test;
}

/*if ($result = $db->query("SELECT d7_nid FROM {node}")) {
  while ($row = $result->fetchObject()) {
    $d7_nids[] = $row->d7_nid;
  }
}*/

if (isset($sitemap_export_root) && !empty($sitemap_export_root)) {
  echo "Fix references to node_id\n";
  fix_ctype();
  exit;
}

// Now update each content type
foreach ($updateTypes as $type) {
  if ($type == $option && $update_only_option_type) {
    update_ctype($type);
  }
  else if (!$update_only_option_type) {
    // Do all types.
    update_ctype($type);
  }
}

function fix_ctype($type = 'teamsite') {

  global $sitemap_export_root;
  echo "getcwd()= " . getcwd() . ";\n";
  $orig_dir = getcwd();

  $jsonfiles = array();
  $sitemapfiles = array();

  if ($dirh = opendir(".")) {
    while (($entry = readdir($dirh)) !== false) {
      if (!preg_match('/json$/', $entry)) continue;
      $jsonfiles[] = $entry;
      if (strtolower($type) == 'teamsite') {
        echo "json file $entry\n";
      }
    }
    closedir($dirh);
  }

  $cclass = ucfirst($type);
  include "updatejsons/$cclass.php";
  include "updatejsons/Sitemap.php";

  echo "Post Processing ".count($jsonfiles)." $type entities...\n";
  $cnt = 0;
  $cnt_success = 0;
  $cnt_could_not_load = 0;
  $needed_nodeid_fix = array();
  $sitemap_failure = array();
  foreach ($jsonfiles as $jfile) {
    $cnt++;
    //echo "$cnt\n";
    if (($cnt % 10) == 0) {
      echo "$cnt\n";
    }
    chdir($orig_dir); // Return to original dir, we need to load from here.
    $teamsite = new $cclass();
    $teamsite->loadData($jfile);
    $sitemap_jfile = $teamsite->data->node_id . '.json';
    chdir($sitemap_export_root); // We only need to read from here.
    $sitemapitem = new Sitemap();
    $sitemapitem->loadData($sitemap_jfile); // Reading /loading file.
    chdir($orig_dir); // Return to original dir, we want to write here.
    if (!isset($sitemapitem->data->has_dcr_id)) {
      $cnt_could_not_load++;
      $sitemap_failure[$sitemap_jfile] = $jfile;
    }
    else if (!$sitemapitem->data->has_dcr_id) {
      $needed_nodeid_fix[$jfile] = TRUE;
      $parent_node_id = $sitemapitem->data->parent_node_id;
      $teamsite->data->node_id = $parent_node_id;
      echo "save $orig_dir/$jfile\n";
      //die;
      $teamsite->save();
      $cnt_success++; //Successfully processed, fixing node_id.
    }
    else {
      $cnt_success++; //Successfully processed, nothing to do here.
    }
  }
  echo "------------------- RESULTS SUMMARY -------------------\n";
  echo "The following teamsite jsons we could fix and did need node_id fixes:\n";
  global $export_root;
  foreach ($needed_nodeid_fix as $key => $value) {
    echo $export_root . '/' . $key . "\n";
  }
  echo "The following sitemap jsons failed to load:\n";
  foreach ($sitemap_failure as $key => $value) {
    echo $sitemap_export_root . '/' . $key . " referenced by $value\n";
  }
  echo "----------------------- TOTALS ------------------------\n";
  echo "$cnt json files were processed out of a total of $cnt\n";
  echo ($cnt - $cnt_success) . " failed\n";
  echo "-------------------------------------------------------\n";
  chdir('..');
}

function refaire($jfile, $cclass, &$update) {
  echo "refaire($jfile, $cclass, ref)\n";
  $update = new $cclass();
  $refaire = TRUE;
  $update = $update->update($jfile, $refaire);
}

function update_ctype($type) {
//  global $nid_old_to_new;
//  global $nid_new_to_old;
//  global $db;
  global $d7_nids;

  echo "getcwd()= " . getcwd() . ";\n";
  //echo "chdir('" . $type . "');\n";
  //chdir($type);
  $jsonfiles = array();

  if ($dirh = opendir(".")) {
    while (($entry = readdir($dirh)) !== false) {
      if (!preg_match('/json$/', $entry)) continue;
      $jsonfiles[] = $entry;
      if (strtolower($type) == 'teamsite') {
        echo "json file $entry\n";
      }
    }
    closedir($dirh);
  }
  $cclass = ucfirst($type);
  include "updatejsons/$cclass.php";

  echo "Processing ".count($jsonfiles)." $type entities...\n";
  $cnt = 0;
  $cnt_success = 0;
  $cnt_could_not_load = 0;
  $failure_array = array();
  $cclass_failure_check = array();
  foreach ($jsonfiles as $jfile) {
    $d7_nid = null;
    if (preg_match('/(\d+)\.json$/', $jfile, $matches)) {
      $d7_nid = $matches[1];
      //echo $matches[1] . '.json' . "\n";
      $d7_nid = ltrim($d7_nid, '0');
    }
    /*if ($d7_nid && in_array($d7_nid, $d7_nids)) {
      echo "Found d7_nid: $d7_nid mapping in D8 \n";
      //continue;
    }*/
    $cnt++;
    //echo "$cnt\n";
    if (($cnt % 10) == 0) {
      echo "$cnt\n";
    }
    $update = new $cclass();
    $update->update($jfile);
    if ($update->success) {
      echo "update->data->dcrid  =" . $update->data->dcr_id . " \n";
      echo "update->data->node_id=" . $update->data->node_id . " \n";
      if (!isset($cclass_failure_check[$update->data->dcr_id])) {
        $cclass_failure_check[$update->data->dcr_id] = 1;
      }
      else {
        $cclass_failure_check[$update->data->dcr_id]++;
      }
      if ((is_array($update->data->body) && (empty($update->data->body['en']) || empty($update->data->body['fr']))) ||
          ((isset($update->data->body->en) && is_object($update->data->body->en)) && (empty($update->data->body->en) || empty($update->data->body->fr)))) {
        echo ("-------- 1- Fix for Start while body is empty------------------------ jfile = " . $jfile . " class= " . $cclass . "----------------------- \n");
          //sleep($cclass_failure_check[$update->data->dcr_id]);
          sleep(5);
          $cclass_failure_check[$update->data->dcr_id]++;
          refaire($jfile, $cclass, $update);
      }
      if ((is_array($update->data->body) && (empty($update->data->body['en']) || empty($update->data->body['fr']))) ||
          ((isset($update->data->body->en) && is_object($update->data->body->en)) && (empty($update->data->body->en) || empty($update->data->body->fr)))) {
          echo "sleep(3);\n";
          echo ("-------- 1- sleep (3) for Fix for Start while body is empty------------------------ jfile = " . $jfile . " class= " . $cclass . "----------------------- \n");
          sleep(10);
          $cclass_failure_check[$update->data->dcr_id]++;
          refaire($jfile, $cclass, $update);
      }
        //echo ("-------- 1- Start while body is empty------------------------ jfile = " . $jfile . " class= " . $cclass . "----------------------- \n");
/*	while (0 || ($cclass == 'Sitemap' && $cclass_failure_check[$update->data->dcr_id] < 30 && (empty($update->data->body['en']) || empty($update->data->body['fr']))) ||
	 ($cclass == 'Teamsite' && $cclass_failure_check[$update->data->dcr_id] < 30 && (empty($update->data->body->en) || empty($update->data->body->fr)))) {
		continue;
          if ($cclass == 'Teamsite') {
            if (is_object($update->data) && property_exists($update->data, 'pageNotFound') && $update->data->pageNotFound) {
              echo "Page not found error 404 on $cclass item jfile=$jfile\n";
              echo "usleep(100000);\n";
              $cclass_failure_check[$update->data->dcr_id]++;
              usleep(100000);
              refaire($jfile, $cclass, $update);
              continue;
	    } 
          }
          if ($cclass == 'Sitemap') {
            if (is_object($update->data) && property_exists($update->data, 'pageNotFound') && $update->data->pageNotFound) {
              echo "Page not found error 404 on $cclass item jfile=$jfile\n";
              echo "usleep(100000);\n";
              $cclass_failure_check[$update->data->dcr_id]++;
              usleep(100000);
              refaire($jfile, $cclass, $update);
              continue;
            } 
          }
	  $en_body_vide = FALSE;
	  $fr_body_vide = FALSE;
          if ($cclass == 'Teamsite') {
            $en_body_vide = empty($update->data->body->en) ? TRUE : FALSE;
            $fr_body_vide = empty($update->data->body->fr) ? TRUE : FALSE;
          }
          if ($cclass == 'Sitemap') {
            $en_body_vide = empty($update->data->body['en']) ? TRUE : FALSE;
            $fr_body_vide = empty($update->data->body['fr']) ? TRUE : FALSE;
          }
          echo "getcwd():" . getcwd() . "\n";
          if ($update->could_not_load) {
            echo "COULD_NOT_LOAD = TRUE\n";
            echo "sleep(2);\n";
            sleep(2);
          }
	  if ($en_body_vide && $fr_body_vide) {
            echo "EMPTY/VIDE / BODY/EN/BODY/FR --  sleep(" . $cclass_failure_check[$update->data->dcr_id] . ") secondes -------- " . $cclass_failure_check[$update->data->dcr_id] . "- While body is empty------------------------ jfile = " . $jfile . " class= " . $cclass . " --- and dcr_id=" . $update->data->dcr_id . "----------------------- \n";
	    echo "sleep(2);";
	          sleep(2);
	  }
	  elseif ($en_body_vide) {
            echo "EMPTY/VIDE / BODY/EN         --  sleep(" . $cclass_failure_check[$update->data->dcr_id] . ") secondes -------- " . $cclass_failure_check[$update->data->dcr_id] . "- While body is empty------------------------ jfile = " . $jfile . " class= " . $cclass . "--- and dcr_id=" . $update->data->dcr_id . "-------------------- \n";
	  }
	  elseif ($en_body_vide) {
            echo "EMPTY/VIDE / BODY/FR         --  sleep(" . $cclass_failure_check[$update->data->dcr_id] . ") secondes -------- " . $cclass_failure_check[$update->data->dcr_id] . "- While body is empty------------------------ jfile = " . $jfile . " class= " . $cclass . "--- and dcr_id=" . $update->data->dcr_id . "-------------------- \n";
	  }
          sleep($cclass_failure_check[$update->data->dcr_id]);
          refaire($jfile, $cclass, $update);
          $cclass_failure_check[$update->data->dcr_id]++;
      }*/
    //} // Disabled/changed.

      $cnt_success++;
    } else {
      $failure_array[$jfile] = TRUE;
    }
    if ($update->could_not_load) {
      $cnt_could_not_load++;
    }
  }
  echo "------------------- RESULTS SUMMARY -------------------\n";
  echo "The following jsons failed:\n";
  global $export_root;
  foreach ($failure_array as $key => $value) {
    echo $export_root . '/' . $key . "\n";
  }
  echo "----------------------- TOTALS ------------------------\n";
  echo "$cnt_success json files were successfully processed out of a total of $cnt\n";
  echo ($cnt - $cnt_success) . " failed\n";
  echo "-------------------------------------------------------\n";
  chdir('..');
}

