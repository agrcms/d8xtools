<?php

use Drupal\agri_admin\AgriAdminHelper;

class Teamsite extends ImportNode
{
  function getExportTitle(&$title, &$titleFr) {
    if ((is_null($title) || empty($title)) && !empty($this->data->title->en)) {
      if ($this->data->title->en != 'null') {
        $title = $this->data->title->en;
        $titleFr = $this->data->title->fr;
      }
    }
    if ((is_null($title) || empty($title)) && !empty($this->data->meta->title->en)) {
      if ($this->data->meta->title->en != 'null') {
        $title = $this->data->meta->title->en;
        $titleFr = $this->data->meta->title->fr;
      }
    }

  }

  public function isTooOld() {
    if (isset($this->data->meta->issued->en)) {
      $january2013 = 1356998400;
      $issued = strtotime($this->data->meta->issued->en);
      if ($issued > 1 && $issued < $january2013) {
        $this->too_old = TRUE;
        return TRUE;
      }
    }
    return FALSE;
  }

  private function preImport() {
    $this->type = 'page'; // Default.
    $ts_type = str_replace('intra-intra/', '', $this->data->type_name);
    switch ($ts_type) {
      case 'news-nouv':
        $this->type = 'news';
        break;
      case 'home-accu':
      case 'gene-gene':
        $this->type = 'page';
        return FALSE; // skip these for now.
        break;
      case 'empl-empl':
        $this->type = 'empl';
        break;
      case '':
      case NULL:
        $this->type = 'empty';
        return FALSE;
      case 'list-list':
      case 'widg-widg':
      case 'rank-clas':
      case 'nwinit-nwinit':
         $this->type = 'other';
         return FALSE;
      default:
        $this->type = $ts_type;
        break;
    }
    if (isset($this->data->pageNotFound) && $this->data->pageNotFound) {
      $this->type = 'deleted';
      //echo "pageNotFound for dcr_id: " . $this->data->dcr_id . "\n";
      return FALSE;
    }
    $ts_nid = $this->data->node_id;
    $dcr_id = $this->data->dcr_id;

    if (AgriAdminHelper::dcrIdPreviouslyImported($this->data->dcr_id)) {
      echo "Previously imported dcr_id: " . $this->data->dcr_id . "\n";
      $this->previously_imported = TRUE;
      return FALSE;
    }

    if (!AgriAdminHelper::tsNidPreviouslyImported($ts_nid)) {
      echo "Sitemap dependency was not yet imported, please run the import with \n" .
      "the sitemapinit option first and make sure you have the entire sitemap \n" .
      "content initialized first before running this teamsite import.\n";
      die;
    }
  }

  function import($jfile)
  {
    global $export_root;
    $this->loadData($jfile);

    if (strlen($this->data->dcr_id) > 7 && strlen($this->data->node_id) > 7) {
      $ts_nid = $this->data->node_id;
      $dcr_id = $this->data->dcr_id;
      $this->preImport();
      if ($this->isTooOld()) {
        //return FALSE;
      }
      echo "Creating a Drupal " . $this->type . " for "  . $export_root . "/" . $jfile . "\n";
      $this->createNode($this->type);


      $this->importField('field_meta_type');//meta_type

      //$this->importField('field_htype');//htype  //htype is sitemap only.

      $this->importField('body');
      $this->importField('field_issued');//issued
      $this->importField('field_modified');//modified
      $this->importField('field_dc_date_created'/*, 'dc_date_created'*/);
      $this->importField('field_meta_type');//meta_type
      $this->importField('field_description');//description
      $this->importField('field_subject');//subject
      $this->importField('field_keywords');//keywords
//    $this->importField('layout_builder_layout', 'layout_builder_layout');
//    $this->importField('layout_selection', 'layout_selection');

      if ($this->type == 'empl') {
        $this->importField('field_type');
        $this->importField('field_classification');
        $this->importField('field_branch');
        $this->importField('field_locations');
        $this->importField('field_date_closing');
        $this->importField('field_open_to');
        $this->importField('field_name');
        $this->importField('field_email');
      }

      $this->save();

      if (0) {
        // Disabled (for now).
        AgriAdminHelper::createInternalLegacyMenuLink($this->nid(), $ts_nid, $ts_nid, $dcr_id, 'sidebar', 'en');
      }

      //AgriAdminHelper::updateNodeLegacyIds($this->nid(), $ts_nid, $ts_pnid, $dcr_id); // this should be done by the import.php.
      //$this->generateAlias();
      return TRUE; // Success.
    } else {
      echo "Might not have a node_id or a dcr_id " . $this->data->dcr_id . ".json\n";
      $this->preImport();
      return FALSE;
    }
  }
}
