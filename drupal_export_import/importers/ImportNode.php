<?php

use Drush\Drush;
use Drupal\node\Entity\Node;

global $import_override;
$import_override = TRUE;

class ImportNode
{
  protected $node;
  protected $trnode;
  protected $data;
  protected $preserveNid = FALSE;
  public $type;
  public $too_old = FALSE;
  public $legacy_server_error = FALSE;
  public $previously_imported = FALSE;
  public $sitemap_external_only = FALSE;
  public $unable_to_import = FALSE;
  public $top_level_item = FALSE;

  function loadData($jsonfile)
  {
    $this->data = json_decode(file_get_contents($jsonfile));
    global $import_override; // See the agri_admin.module code.
    $import_override = TRUE; // See the agri_admin.module code.
  }

  function createNode($type)
  {
    if (isset($this->data->pageNotFound) && $this->data->pageNotFound) {
      if (isset($this->data->has_dcr_id) && !$this->data->has_dcr_id) {
        echo "pageNotFound and has no dcr_id for landingPageUrl " . $this->data->landingPageUrl->en . "\n";
        return;
      } else {
        echo "pageNotFound for dcr_id " . $this->data->dcr_id . "\n";
      }
    }
    if (isset($this->data->has_dcr_id) && !$this->data->has_dcr_id) {
      echo "has no dcr_id for " . $this->data->node_id . ".json\n";
      return;
    }
    if (!isset($this->data->has_dcr_id) && !is_numeric($this->data->dcr_id)) {
      echo "has no dcr_id for " . $this->data->dcr_id . ".json\n";
      return;
    }

    if (isset($this->data->body->en)) {
      $body_text = $this->data->body->en;
      //if (strpos($body_text, "We're sorry, but there's an error preventing you from continuing.") > 1) {
      if (strpos($body_text, "s an error preventing you from continuing.") > 1) {
        echo "Legacy server errors for dcr_id " . $this->data->dcr_id . ".json\n";
        $this->legacy_server_error = TRUE;
        $this->unable_to_import = TRUE;
        return;
      }
    }
    if (isset($this->data->body->fr)) {
      $body_text = $this->data->body->fr;
      //if (strpos($body_text, "We're sorry, but there's an error preventing you from continuing.") > 1) {
      if (strpos($body_text, "s an error preventing you from continuing") > 1) {
        echo "Legacy server errors for dcr_id " . $this->data->dcr_id . ".json\n";
        $this->legacy_server_error = TRUE;
        $this->unable_to_import = TRUE;
        return;
      }
    }

    $title = NULL;
    $titleFr = NULL;
    $this->getExportTitle($title, $titleFr);
    if (empty($title)) {
      $this->getTitle($title, $titleFr);
    }

    $this->type = $type;
    $this->node = Node::create([
      'type' => $type,
      'title' => $title,
    ]);

    if ($this->node->hasTranslation('fr')) { //Figure out if there is already a translation
      $this->trnode = $this->node->getTranslation('fr'); //Get the translation
      $this->trnode->set('title', $titleFr); //set the title
      //echo "Title set fr";
    }
    else { //If it does't have atranslation
      $this->trnode = $this->node->addTranslation('fr'); //Add the translation
      $this->trnode->set('title', $titleFr); //Set the title
      //echo "Title set fr";
    }

    // Logic to handle dcterms:title.
    $dcterms_title_en = '';
    $dcterms_title_fr = '';
    $this->getDctermsTitle($dcterms_title_en, $dcterms_title_fr);
    try {
      $metatags = unserialize($this->trnode->get('field_meta_tags')->value);
      $metatags['dcterms_title'] = $dcterms_title_fr;
      $this->trnode->set('field_meta_tags', serialize($metatags));
    } catch (Exception $e) {
      // suppress.
    }
    try {
      $metatags = unserialize($this->node->get('field_meta_tags')->value);
      $metatags['dcterms_title'] = $dcterms_title_en;
      $this->node->set('field_meta_tags', serialize($metatags));
    } catch (Exception $e) {
      // suppress.
    }
    // End of logic to handle dcterms:title.

    if (!isset($titleFr)) {
      echo "\nIncomplete translation in french for " . $title . "\n";
      echo "Using english title instead \n";
      echo "ABORT! skipping \n";
      $this->unable_to_import = TRUE;
      return FALSE;
      $this->trnode->set('title', $title);
    }

    if ($this->preserveNid) {
      $this->node->set('nid', $this->data->nid);
      $this->trnode->set('nid', $this->data->nid);
    }

    $this->node->set('uid', 1);
    $this->node->set('status', isset($this->data->status) ? $this->data->status : '1');
    $this->trnode->set('status', isset($this->data->status) ? $this->data->status : '1');
    $created = isset($this->data->meta->issued->en) ? strtotime($this->data->meta->issued->en) : time();
    $this->node->set('created', $created);
    $this->trnode->set('created', $created);
    $changed = isset($this->data->meta->modified->en) ? strtotime($this->data->meta->modified->en) : time();
    $this->node->set('changed', $changed);
    $this->trnode->set('changed', $changed);
    $this->node->set('promote', isset($this->data->promote) ? $this->data->promote : '0');
    $this->trnode->set('promote', isset($this->data->promote) ? $this->data->promote : '0');
    $this->node->set('sticky', isset($this->data->sticky) ? $this->data->sticky : '0');
    $this->trnode->set('sticky', isset($this->data->sticky) ? $this->data->sticky : '0');
  }


  function importField($field, $d8_field=null)
  {
    global $id_old_to_new;
    global $id_new_to_old;

    if (!$d8_field) {
      $d8_field = $field;
    }

   /* if ($this->data->language == "en") {
      $origLang = "en";
      $oppLang = "fr";
    }
    else {
      $origLang = "fr";
      $oppLang = "en";
    }*/
    $origLang = 'en';
    $oppLang = 'fr';

    switch ($field) {
      case 'field_date_closing':
      case 'field_name':
        $emplfield = str_replace('field_', '', $field);
        $this->node->set($d8_field, $this->data->empl->{$emplfield}->{$origLang});
        break;
      case 'field_dc_date_created':
        if ($this->type == 'page' || $this->type == 'news' || $this->type == 'empl') {
          $value = $this->data->dc_date_created;
          $valueFr = $this->data->dc_date_created;
        }
        else {
          $value = $this->data->meta->issued->{$origLang};
          $valueFr = $this->data->meta->issued->{$oppLang};
        }
        $this->node->set($field, $value);
        $this->trnode->set($field, $valueFr);
        break;
      case 'field_issued':
        $value = $this->data->meta->issued->{$origLang};
        $valueFr = $this->data->meta->issued->{$oppLang};
        //echo $field . ' ' . $value . "\n";
        $this->node->set($field, $value);
        $this->trnode->set($field, $valueFr);
        break;
      case 'field_posted':
        $value = $this->data->news->{'date_posted'}->{$origLang};
        //$date_posted = isset($value) ? strtotime($value) : time();
        //echo $field . ' ' . $value . "\n";
        $this->node->set($field, $value);
        //$this->trnode->set($field, $date_posted);
        break;
      case 'field_subject'://aafc:subject
        //$metafield = 'subject';
        //corrige ça , brisé pour l'instant
        $subjectArray = explode(";", $this->data->meta->subject->{$origLang});
        $subjectFrArray = explode(";", $this->data->meta->subject->{$oppLang});
        //print_r($subjectArray);
        $countEn = count($subjectArray);
        $countFr = count($subjectFrArray);
        $tids = [];
        foreach ($subjectArray as $key => $term_label_en) {
          $term_label_en = trim($term_label_en);
          if ($term_label_en == 'aafcsubject') {
            continue;
          }
          if (($countEn == $countFr) && isset($subjectFrArray[$key]) && !empty($subjectFrArray[$key])) {
            $type_fr = trim($subjectFrArray[$key]);
            $type_fr = strtolower($type_fr);
            $tids[] = $this->term_label_to_id($term_label_en, $type_fr, 'category');
          }
          else {
            $tids[] = $this->term_label_to_id($term_label_en, '', 'category');
          }
        }
        if (!empty($tids)) {
          $this->node->set($d8_field, $tids);
        }
        // $subjectArrayFr = explode(" ", $this->data->meta->subject->{$oppLang});
        // $this->trnode->set($d8_field, $subjectArrayFr);
        break;
      case 'field_managing_branch':
        $metafield = str_replace('field_', '', $field);
        $this->node->set($d8_field, $this->data->{$metafield});
        break;  
      case 'field_teamsite_location':
        $metafield = str_replace('field_', '', $field);
        $this->node->set($d8_field, $this->data->{$metafield}->{$origLang});
        $this->trnode->set($d8_field, $this->data->{$metafield}->{$oppLang});
        break;
      case 'field_issued':
      case 'field_modified':
      case 'field_description':
      case 'field_keywords':
        $metafield = str_replace('field_', '', $field);
        $this->node->set($d8_field, $this->data->meta->{$metafield}->{$origLang});
        $this->trnode->set($d8_field, $this->data->meta->{$metafield}->{$oppLang});
        break;
      case 'field_meta_type':// dcterms:type
        $metafield = 'type';
        $metaTypeArray = explode(";", $this->data->meta->{$metafield}->{$origLang});
        $metaTypeFrArray = explode(";", $this->data->meta->{$metafield}->{$oppLang});
        $countEn = count($metaTypeArray);
        $countFr = count($metaTypeFrArray);
        $tids = [];
        foreach ($metaTypeArray as $key => $type_en) {
          $type_en = trim($type_en);
          if (($countEn == $countFr) && isset($metaTypeFrArray[$key]) && !empty($metaTypeFrArray[$key])) {
            $type_fr = trim($metaTypeFrArray[$key]);
            $type_fr = strtolower($type_fr);
            $tids[] = $this->term_label_to_id($type_en, $type_fr, 'aafc_type');
          }
          else {
            $tids[] = $this->term_label_to_id($type_en, '', 'aafc_type');
          }
        }
        if (!empty($tids)) {
          $this->node->set($d8_field, $tids);
        }
        break;
      case 'field_dcterms_subject':
        // FONCTIONNE BIEN;
        // if (!isset($type) || $type == 'home-accu') {
        //   Drush::output()->writeln('debug: type='. $type . ' :print_r ' . print_r($this->data->$field, TRUE));
        //   return;
        // }
        //$metafield = str_replace('field_meta_', '', $field);
        $metafield = 'dcterms:subject';
        $metaTypeArray = explode(";", $this->data->meta->{$metafield}->{$origLang});
        $metaTypeFrArray = explode(";", $this->data->meta->{$metafield}->{$oppLang});
        $countEn = count($metaTypeArray);
        $countFr = count($metaTypeFrArray);
        $tids = [];
        foreach ($metaTypeArray as $key => $type_en) {
          $type_en = trim($type_en);
          if (($countEn == $countFr) && isset($metaTypeFrArray[$key]) && !empty($metaTypeFrArray[$key])) {
            $type_fr = trim($metaTypeFrArray[$key]);
            $type_fr = strtolower($type_fr);
            $tids[] = $this->term_label_to_id($type_en, $type_fr, 'dcterms_subject');
          }
          else {
            $tids[] = $this->term_label_to_id($type_en, $type_fr, 'dcterms_subject');
          }
        }
        //print_r($metaTypeArray);
        if (!empty($tids)) {
          $this->node->set($d8_field, $tids);
        }
        // $metafield = str_replace('field_meta_', '', $field);
        // $this->node->set($d8_field, $this->data->meta->{$metafield}->{$origLang});
        // $this->trnode->set($d8_field, $this->data->meta->{$metafield}->{$oppLang});
        break;
      case 'field_date_released':
        if (strlen($this->data->date_released) > 10) {
          $this->data->date_released[10] = 'T';
          $this->node->set($d8_field, $this->data->date_released);
          $this->trnode->set($d8_field, $this->data->date_released);
        }
        break;
      case 'body':
        $this->node->set($d8_field, array(
          'summary' => isset($this->data->meta->description->{$origLang}) ? $this->data->meta->description->{$origLang} : '',
          'value' => $this->data->body->{$origLang},
          'format' => isset($this->data->body_format->{$origLang}) ? $this->data->body_format->{$origLang} : 'full_html',
        ));
        $this->trnode->set($d8_field, array(
          'summary' => $this->data->meta->description->{$oppLang},
          'value' => $this->data->body->{$oppLang},
          'format' => isset($this->data->body_format->{$oppLang}) ? $this->data->body_format->{$oppLang} : 'full_html',
        ));
        break;
      case 'field_photos_by':
        if (isset($this->data->photos_by)) {
          $this->node->set($d8_field, $this->data->photos_by->$origLang);
          $this->trnode->set($d8_field, $this->data->photos_by->$oppLang);
        }
        break;
      case 'field_subtitle':
         $this->node->set($d8_field, array(
            'subtitle' => $this->data->subtitle->$origLang,
            'format' => 'basic_html'
          ));
         $this->trnode->set($d8_field, array(
            'subtitle' => $this->data->subtitle->$oppLang,
            'format' => 'basic_html'
          ));
          break;
        //File needs to be created before this can be done?
        case 'field_image_caption':
          $this->node->set($d8_field, array(
            'image_caption' => $this->data->image_caption->$origLang,
            'format' => 'basic_html'
          ));
          $this->trnode->set($d8_field, array(
            'image_caption' => $this->data->image_caption->$oppLang,
            'format' => 'basic_html'
          ));
          break;
        case 'field_override_image_sharing':
        case 'override_image_sharing':
        case 'field_news_image':
        case 'news_image':
        case 'field_news_thumbnail':
        case 'news_thumbnail':
         if($imageValues = $this->data->$field) {
          $file_uri = $imageValues->$origLang->file_uri;
          $pm_file_path = $imageValues->$origLang->pm_file_path;
          $folder_path = str_replace('public://', '', $file_uri);
          $folder_path = substr($folder_path, 0, strrpos( $folder_path, '/'));
          $this->importRemoteFile($this->data->type, $this->data->date_released, $imageValues->$origLang->fid, $d8fid, $folder_path, $pm_file_path);
    //Drush::output()->writeln($d8_field);
    //Drush::output()->writeln($d8fid);
    //Drush::output()->writeln($imageValues->$origLang->alt);
    //Drush::output()->writeln($imageValues->$origLang->width);
    //Drush::output()->writeln($imageValues->$origLang->height);
          $this->node->set($d8_field, array(
            'target_id' => $d8fid,//$imageValues->$origLang->fid,
            'alt' => isset($imageValues->$origLang->alt) ? $imageValues->$origLang->alt : 'Image',
            'title' => isset($imageValues->$origLang->title) ? $imageValues->$origLang->title : 'Image without a title',
            'width' => $imageValues->$origLang->width,
            'height' => $imageValues->$origLang->height,
          ));
          $file_uri_fr = $imageValues->$oppLang->file_uri;
          $pm_file_path_fr = $imageValues->$oppLang->pm_file_path;
          $folder_path_fr = str_replace('public://', '', $file_uri);
          $folder_path_fr = substr($folder_path, 0, strrpos( $folder_path_fr, '/'));

    $this->importRemoteFile($this->data->type,
                                  $this->data->date_released,
          $imageValues->$oppLang->fid,
          $d8fid,
                                  $folder_path_fr,
                                  $pm_file_path_fr);
          $this->trnode->set($d8_field, array(
            'target_id' => $d8fid,//$imageValues->$oppLang->fid,
            'alt' => isset($imageValues->$oppLang->alt) ? $imageValues->$oppLang->alt : 'Image',
            'title' => isset($imageValues->$oppLang->title) ? $imageValues->$oppLang->title : 'Image sans titre',
            'width' => $imageValues->$oppLang->width,
            'height' => $imageValues->$oppLang->height,
          ));
         }
          break;
        case 'field_assignment':
         if (isset($this->data->field_assignment)) {
          $this->node->set($d8_field, $this->data->field_assignment->$origLang);
          $this->trnode->set($d8_field, $this->data->field_assignment->$oppLang);
          /**$this->node->set($d8_field, array(
            'title' => $this->data->field_assignment->$origLang,
            //'format' => 'basic_html'
          ));
          $this->trnode->set($d8_field, array(
            'title' => $this->data->field_assignment->$oppLang,
            //'format' => 'basic_html'
          ));*/
         }
          break;
        case 'field_date_range':
            $this->node->set($d8_field, array(
                'date_range' => $this->data->date_range->$origLang,
                'date_range2' => $this->data->date_range2->$origLang,
            ));
            $this->trnode->set($d8_field, array(
                'date_range' => $this->data->date_range->$oppLang,
                'date_range2' => $this->data->date_range2->$oppLang,
            ));
          break;
        case 'field_featured_title':
          $this->node->set($d8_field, array(
            'fid' => $this->data->fid->$origLang,
            'alt' => $this->data->alt->$origLang,
            'photo_title' => $this->data->photo_title->$origLang,
            'width' => $this->data->width->$origLang,
            'height' => $this->data->height->$origLang,
          ));
          $this->trnode->set($d8_field, array(
            'fid' => $this->data->fid->$oppLang,
            'alt' => $this->data->alt->$oppLang,
            'photo_title' => $this->data->photo_title->$oppLang,
            'width' => $this->data->width->$oppLang,
            'height' => $this->data->height->$oppLang,
          ));
          break;
        case 'field_is_archived':
         if (isset($this->data->is_archived)) {
          $this->node->set($d8_field, $this->data->is_archived);
         }
          //$this->node->set($d8_field, isset($this->data->is_archived) ? true : false);
          break;
         case 'field_title':
           //kind of confusing but this field was re-purposed from the old minister title field and now contains the last name for sorting purposes
           if ($this->data->type == "minister" || $this->data->type == "parliamentary_secretaries"){
               $nodeTitle = $this->data->node_title;
               //get last name
               $lastName = array_pop(explode(' ', $nodeTitle));
               //echo "\n last name".$lastName;
               //lastname is not translatable
               $this->node->set($d8_field, $lastName);
           }
           break;
         case 'date_range':
             if ($this->data->type == "mandate_letter"){
                 //this is the date range field for Mandate Letters
                 //"date_range": "2015-11-12 00:00:00",
                 //gotta take the title field from D7 ML and parse it into start date and end date in drupal 8.
                 //note that the field_date_range field in d8 is repurposed as the start_date, end date captured as a new field for archiving purposes.
                 //account for range inthe title, if there is a dash 2018-2019)

                 //field_letter_end_date
                 //field_date_range
                 $nodeTitle = $this->data->node_title;
                 //parse date from brackets () in d7
                 $dateFromTitle = preg_match('#\((.*?)\)#', $nodeTitle, $match);
                 if (!empty($match)){
                     //echo "\nML Date:".$match[1]." = ";
                     $dateString = $match[1];
                     //$dateObj = date_create($dateString);
                     //$dateFormatted = date_format($dateObj, "Y-m-d")."T01:00:00";//date('Y-m-d', strtotime($dateString));//
                     //$dateFormatted = //date('Y-m-d\T00:00:00', time());
                     //*****add a day to the date because there is a TimeZone bug causing it to save a day earlier****
         // REMOVED add a day: was: strtotime($dateString." +1 day") , now strtotime($dateString)
         $dateFormatted = \Drupal::service('date.formatter')->format(strtotime($dateString), 'custom', 'Y-m-d\T06:00:00','UTC');//'Y-m-d\T00:00:00 P');
                     //echo "\n".$dateFormatted;
                     $this->node->set($d8_field, $dateFormatted);
                     /*$this->node->set($d8_field, array(
                         'value' => format_date(strtotime('now'), 'custom', 'Y-m-d H:i:s', 'UTC'),
                         'timezone' => 'UTC',
                         'timezone_db' => 'UTC',
                     ));*/
                 }

                 /*
                  $this->node->set($d8_field, $this->data->field_order);
                  //account for if order field is translatable
                  if ($this->node->hasTranslation('fr')) { //Figure out if there is already a translation
                  $this->trnode = $this->node->getTranslation('fr'); //Get the translation
                  $this->trnode->set($d8_field, $this->data->field_order); //set the order
                  //echo "Title set fr";
                  }
                  */
             }
             break;
        case 'featured_description':
          $this->node->set($d8_field, $this->data->location->$origLang);
          $this->trnode->set($d8_field, $this->data->location->$oppLang);
          break;
        case 'field_youtube_id':
          $this->node->set($d8_field, $this->data->youtube_id->$origLang);
          $this->trnode->set($d8_field, $this->data->youtube_id->$oppLang);
          break;
      default:
      {
        Drush::output()->writeln('debug: field='. $field . ' :print_r ' . print_r($this->data->$field, TRUE));
  if (isset($this->data->$field)) {
          Drush::output()->writeln('isset: field=' .$field);
          $this->node->set($d8_field, $this->data->$field);
  }
      }
    } //End switch
  } //End import field function

  function getExportTitle(&$title, &$titleFr) {
    if ((is_null($title) || empty($title)) && (!empty($this->data->h1->en))) {
      if ($this->data->h1->en != 'null') {
        $title = $this->data->h1->en;
        $titleFr = $this->data->h1->fr;
      }
    }
    if ((is_null($title) || empty($title)) && !empty($this->data->title->en)) {
      if ($this->data->title->en != 'null') {
        $title = $this->data->title->en;
        $titleFr = $this->data->title->fr;
      }
    }
    if ((is_null($title) || empty($title)) && !empty($this->data->meta->title->en)) {
      if ($this->data->meta->title->en != 'null') {
        $title = $this->data->meta->title->en;
        $titleFr = $this->data->meta->title->fr;
      }
    }

  }


  function getDctermsTitle(&$title, &$titleFr) {
    if ((is_null($title) || empty($title)) && !empty($this->data->meta->title->en)) {
      if ($this->data->meta->title->en != 'null') {
        $title = $this->data->meta->title->en;
        $titleFr = $this->data->meta->title->fr;
      }
    }
    if ((is_null($title) || empty($title)) && !empty($this->data->title->en)) {
      if ($this->data->title->en != 'null') {
        $title = $this->data->title->en;
        $titleFr = $this->data->title->fr;
      }
    }
    if ((is_null($title) || empty($title)) && (!empty($this->data->h1->en))) {
      if ($this->data->h1->en != 'null') {
        $title = $this->data->h1->en;
        $titleFr = $this->data->h1->fr;
      }
    }
  }


  function getTitle(&$title, &$titleFr) {
    if ((is_null($title) || empty($title)) && isset($this->data->meta->title->en) && !empty($this->data->meta->title->en)) {
      if ($this->data->meta->title->en != 'null') {
        $title = $this->data->meta->title->en;
        $titleFr = $this->data->meta->title->fr;
      }
    }
    if ((is_null($title) || empty($title)) && !empty($this->data->title->en)) {
      if ($this->data->title->en != 'null') {
        $title = $this->data->title->en;
        $titleFr = $this->data->title->fr;
      }
    }
    if (isset($this->data->node_title->en) && $this->data->node_title->en != 'null' && !empty($this->data->node_title->en)) {
      $title = $this->data->node_title->en;
      $titleFr = $this->data->node_title->fr;
    }
    if ((is_null($title) || empty($title)) && isset($this->data->breadcrumb->en) && !empty($this->data->breadcrumb->en)) {
      if ($this->data->breadcrumb->en != 'null') {
        $title = $this->data->breadcrumb->en;
        $titleFr = $this->data->breadcrumb->fr;
      }
    }
  }

  function nid()
  {
    if (!isset($this->node)) {
      return 0;
    }
    return $this->node->id();
  }

  function old_id()
  {
    if (isset($this->data->dcr_id) && is_numeric($this->data->dcr_id)) {
      return $this->data->dcr_id;
    }
    else {
      return 0;
    }
  }

  function sitemap_id()
  {
    if (isset($this->data->node_id) && is_numeric($this->data->node_id)) {
      return $this->data->node_id;
    }
    else {
      return 0;
    }
  }

  function sitemap_pid()
  {
    if (isset($this->data->parent_node_id) && is_numeric($this->data->parent_node_id)) {
      return $this->data->parent_node_id;
    }
    else {
      return 0;
    }
  }

  /**
   * Import files (images) from the Drupal 7 site to respected folder
   * @param int $type
   * @param int $creationdate
   * @param int $fidOrig
   */
  function importRemoteFile($type, $creationdate, $fidOrig, &$d8fid=null, $folderpath, $urlToFile){
    //echo "type=$type\n";
    //echo "creationdate=$creationdate\n";
    //echo "fidOrig=$fidOrig\n";
    //echo "d8fid=$d8fid\n";
    //echo "folderpath=$folderpath\n";
    //echo "urlToFile=$urlToFile\n";
    global $export_root;
    global $site_dir;

    $src_root = "$export_root/../files";

    $fpos = null;
    if ($fx = fopen("$export_root/files.idx", 'r')) {
      while (!feof($fx)) {
        list ($fid, $pos) = explode(',', trim(fgets($fx)));
        if ($fid == $fidOrig) {
          $fpos = $pos;
          break;
        }
      }
      fclose($fx);
    }

    if (!$fpos) return;

    // Get the image data and copy the file from the src to the dst site
    if ($fd = fopen("$export_root/files.csv", 'r')) {
      fseek($fd, $fpos);
      $data = fgetcsv($fd);
      fclose($fd);
      $fpath = str_replace('public://', '', $data[1]);
      $srcfile = "$src_root/$fpath";

      //Dstfile creation
      if($creationdate == null) {
        $creationdate = "no-date";
      }


      // No need to use the date, we have the path from the source.
      //$folderpath = "media/$type/".substr($creationdate, 0, 7);

      if (!is_dir(DRUPAL_ROOT."/sites/$site_dir/files/$folderpath")) {
        mkdir(DRUPAL_ROOT."/sites/$site_dir/files/$folderpath", 0777, true);
      }
      $file = strrchr($data[1], "/");

      $dstfile = DRUPAL_ROOT."/sites/$site_dir/files/$folderpath".$file;
      $folder_file = $folderpath.$file;

      Drush::output()->writeln("SELECT fid FROM {file_managed} where uri='public://$folder_file'");

      if (file_exists($dstfile)) {
        echo "\n file exists! $dstfile \n";
        $connection = \Drupal::database();
        if ($result = $connection->query("SELECT fid FROM {file_managed} where uri='public://$folder_file'")) {
          if ($row = $result->fetchObject()) {
            $d8fid = $row->fid;
          }
        }
      } else {
        echo "\n copy $srcfile to $dstfile \n";
        //copy($srcfile, $dstfile);
        copy($urlToFile, $dstfile);
        $size = filesize($dstfile);
        echo "\n folder_file= $folder_file \n";
        $file = \Drupal::entityTypeManager()->getStorage('file')->create(array('uri' => 'public://'.$folder_file, 'uid' => 1, 'filesize' => $size, 'status' => 1));
        $file->save();
        $d8fid = $file->id();
      }
      if(!file_exists($dstfile)) { //Check if src file already exists in the destination
        //copy($srcfile, $dstfile);
        echo "\n second try copy $srcfile to $dstfile \n";
        copy($urlToFile, $dstfile);
        $size = filesize($dstfile);
          echo "\n folder_file= $folder_file \n";
        $file = \Drupal::entityTypeManager()->getStorage('file')->create(array('uri' => 'public://'.$folder_file, 'uid' => 1, 'filesize' => $size, 'status' => 1));
        $file->save();
        $d8fid = $file->id();
      }


      //      $data = file_get_contents($srcfile);
      // This thing is not ready for public:// uri, period.
      //      $file = file_save_data($data, 'public://'.$fpath, FILE_EXISTS_REPLACE);

      //$d8fid = $file->id();
      Drush::output()->writeln('d8fid=' . $d8fid);
    }
  }


  function news_category_d7_to_d8($d7_tid)
  {
    $d8_tid = 0;
    $d8_tid = $d7_tid; // If the ids are different can do a mapping like this or better would be to use the term label instead.
    switch ($d7_tid) {
      case 6970: // News Releases
        $d8_tid = 1;
        break;
      case 6971: // Speeches
        $d8_tid = 4;
        break;
      case 6972: // Statements
        $d8_tid = 3;
        break;
      case 6974: // Backgrounders
        $d8_tid = 8;
        break;
      case 6976: // The Briefing Room (now Readouts)
        $d8_tid = 7;
        break;
      case 9207: // Itineraries
        $d8_tid = 6;
        break;
    }
    return $d8_tid;
  }


  function empl_classification_is_equivalent($field) {
    $is_equivalent = FALSE;
    $emplfield = str_replace('field_', '', $field);
    $classification_en = strtoupper($this->data->empl->{$emplfield}->en);
    $classification_fr = strtoupper($this->data->empl->{$emplfield}->fr);
    // Do english.
    $pos_and_equivalent = stripos($classification_en, ' and');
    if ($pos_and_equivalent > 0) {
      $is_equivalent = TRUE;
    }
    $pos_et_equivalent = stripos($classification_fr, ' et');
    if ($pos_et_equivalent > 0) {
      $is_equivalent = TRUE;
    }
    return $is_equivalent;
  }


  function empl_classification_to_paragraphs($field) {
    $emplfield = str_replace('field_', '', $field);
    $classification_en = strtoupper($this->data->empl->{$emplfield}->en);
    $classification_fr = strtoupper($this->data->empl->{$emplfield}->fr);
    // Handle ' and equivalent' elsewhere.
    $classification_en = str_replace(strtoupper(' and equivalent'), '', $classification_en); // Remove ' and equivalent'.
    $classification_fr = str_replace(strtoupper(' et équivalent'), '', $classification_fr); // Remove ' et équivalent'.
    // Do english.
    $pos_comma = strpos($classification_en, ',');
    if ($pos_comma > 0) {
      $classif_array_en = explode(',', $classification_en);
      if (!is_array($classif_array_en)) {
        // Failsafe.
        $classif_array_en = array();
      }
    }
    else {
      $classif_array_en = array($classification_en);
    }
    // Do français.
    $pos_comma_fr = strpos($classification_fr, ',');
    if ($pos_comma_fr > 0) {
      $classif_array_fr = explode(',', $classification_fr);
      if (!is_array($classif_array_fr)) {
        // Failsafe.
        $classif_array_fr = array();
      }
    }
    else {
      $classif_array_fr = array($classification_fr);
    }
    $tid = 0;
    $pgs = array();
    $classif_array = array();
    if (count($classif_array_en) == count($classif_array_fr)) {
      $classif_array = $classif_array_en;
    }
    else if (count($classif_array_en) > 0 && count($classif_array_fr) > 0) {
      if (count($classif_array_en) > count($classif_array_fr)) {
        $classif_array = $classif_array_en;
      }
      if (count($classif_array_fr) > count($classif_array_en)) {
        $classif_array = $classif_array_fr;
      }
    }
    else if (count($classif_array_en) > 0) {
      $classif_array = $classif_array_en;
    }
    else if (count($classif_array_fr) > 0) {
      $classif_array = $classif_array_fr;
    }
    $array_idx = 0;
    foreach ($classif_array as $classif) {
      $pos_hyphen = strpos($classif, '-');
      if ($pos_hyphen > 0) {
        $paragraph = $this->classification_to_paragraph($classif);
        if (!is_null($paragraph) && !empty($paragraph)) {
          $pgs[$array_idx]['target_id'] = $paragraph->id();
          $pgs[$array_idx]['target_revision_id'] = $paragraph->getRevisionId();
          $array_idx++;
        }
      }
    }
    return $pgs; // Return array of classification paragraphs.
  }


  /**
   * parameter is a string ex: "as-1" , "as-2" , "as-3" , "as-4"
   * parameter is a string ex: "as-aio-1" , "as-bus-2" , "as-cgc-3" , "as-coi-4"
   * Have to parse this into two values, classification and level.
   * "as"=group "1"=level.
   */
  function classification_to_paragraph($classif) {
    $subclass = '';
    $classif = trim($classif);
    // Do english.
    $pos_hyphen = strpos($classif, '-');
    if ($pos_hyphen > 0) {
      $group = substr($classif, 0, $pos_hyphen);
      $pos_last_hyphen = strpos($classif, '-');
      // Begin sub class.
      if ($pos_last_hyphen > $pos_hyphen) {
        $subclass_dirty = substr($classif, $pos_hyphen, $pos_last_hyphen($classif));
        $subclass = str_replace('-', '', $subclass_dirty);
      }
      // End sub class.
      $level_dirty = substr($classif, $pos_last_hyphen, strlen($classif));
      $level = str_replace('-', '', $level_dirty);
      $paragraph = $this->create_classification_paragraph($group, $level, $subclass);
      return $paragraph;
    }
  }


  function create_classification_paragraph($group, $level, $subclass) {
    if (strtolower($level) == 'Minus 1' || strtolower($level) == 'moins 1') {
      $level_key = '-1';
    } else if (strtolower($level) == 'Minus 2' || strtolower($level) == 'moins 2') {
      $level_key = '-2';
    } else {
      $level_key = $level;
    }
    $paragraph = Drupal\paragraphs\Entity\Paragraph::create([
      'type' => 'classification',
      'field_class_id' => $group,
      'field_class_sub' => $subclass,
      'field_class_level' => $level_key
    ]);
    $paragraph->save();
    return $paragraph;
  }

  /**
   * To get Termid available.
   */
  public function get_term_id($termname, $vid)
  {
    $termname = trim($termname);
    $termname = strtolower($termname);
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vid, '='); //select the collection
    $query->condition('name', $termname, '='); //searching the title for a search term
    $term_ids= $query->execute();
    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($term_ids);  //process the nodes however is desired

    $result = [];
    $term_id = NULL;
    foreach($terms as $term){
      //$result[$term->id()] = $term->getName();
      // Grab the first one.
      $term_id = $term->id();
      return $term->id();
    }
    return $term_id;
  }


   /**
    * To Create Terms if it is not available.
    */
   public function create_term($voc, $term, $vid, $term_fr)
   {
     $termArr = [
      'name' => $term,
      'vid' => $vid,
      'langcode' => 'en',
      'parent' => [$voc],
     ];
      
     $new_term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->create($termArr);

    //  $new_term = Drupal\taxonomy\Entity\Term::create(
    //    [
    //      'parent' => [$voc],
    //      'name' => $term,
    //      'vid' => $vid,
    //    ]
    //  )->save();
     //$termId = $this->get_term_id($term, $vid);
     $txt_file = file_get_contents('../../drupal_export_import/importers/taxonomy/term_en_fr_2.txt');
     $rows = explode("\n", $txt_file);
     $clevaleur = [];
     //echo print_r(serialize($new_term));
     //echo getcwd();
     foreach($rows as $row => $data)
     {
       $row_data = explode('=', $data);
       if (trim($row_data[0]) == trim($term)) {
         $clevaleur[trim($row_data[0])] = trim($row_data[1]);
         break;
         // Break after the first one.
       }
     }
   
      // Inconsistent data between french/english, so disabling auto translation.
      if (isset($clevaleur[$term]) && strlen($clevaleur[$term]) > 2) {
        $term_fr = $clevaleur[$term];
        //$termObj = \Drupal\taxonomy\Entity\Term::load($termId);  //process the nodes however is desired
        //$termObj->addTranslation('fr', ['name' => $term_fr]);
        $new_term->addTranslation('fr', ['name' => $term_fr]);
        echo $vid . 'Ajouter traduction :' . $term_fr;
      }
      $new_term->save();
      $termId=$new_term->id();
      //if (!empty($term_fr) && strlen($term_fr) > 1) {
      //  $new_term->addTranslation('fr', ['name' => $term_fr]]);
      //}
      return $termId;
    }

  function term_label_to_id($type_en, $type_fr = null, $vid = 'aafc_type'){
    $tid = 0;
    $type_en = strtolower($type_en);
    $term_id = $this->get_term_id($type_en, $vid);
    if (empty($term_id) && !empty($type_en) && strlen($type_en) > 1) {
      $voc = null; //there is no parent (pour le moment).
      $type_fr = null; // disabled (pour le moment).
      $term_id = $this->create_term($voc, $type_en, $vid, $type_fr);
      echo $vid . ' Add new term for :' . $type_en;
      echo "\n";
      if (is_string($type_fr)) {
        echo $vid . ' Add new term for :' . $type_fr . ' fr';
        echo "\n";
      }
//      \Drupal::messenger()
//        ->addMessage('Add new term for :' . $type_en);
    }

    return $term_id;
  }


  function newstype_to_term_id($type_en)
  {
    $tid = 0;
    $deputy_flag = FALSE;
    $strpos_deputy = strpos($type_en, 'eputy Minist');
    if ($strpos_deputy > 0) {
      $tid = 26;
      return $tid;
    }
    switch ($type_en) {
      case 'General':
        $tid = 21;
        break;
      case 'Pay and Benefits':
        $tid = 22;
        break;
      case 'Professional Development':
        $tid = 23;
        break;
      case 'Events':
        $tid = 24;
        break;
      case 'ISB Service Notices':
        $tid = 25;
        break;
      case "Deputy Ministers' Messages":
        $tid = 26;
        break;
      case 'Accross the Public Service':
        $tid = 27;
        break;
      case 'GCWCC':
        $tid = 28;
        break;
      default:
        echo $type_en . " = type_en ????? newstype_to_term_id";
	$tid = 21;
        break;
    }
    return $tid;
  }

  function empl_type_to_term_id($type_en)
  {
    $tid = 0;
    switch ($type_en) {
      case 'Assignment':
        $tid = 6;
        break;
      case 'Deployment':
        $tid = 7;
        break;
      case 'Acting':
        $tid = 8;
        break;
      default:
        echo $type_en . " = type_en ????? empl_type_to_term_id";
        break;
    }
    return $tid;
  }

  function save()
  {
    if ($this->top_level_item) {
      $this->node->set("path", ["pathauto" => FALSE]);
      $this->trnode->set("path", ["pathauto" => FALSE]);
    }
    else {
      $this->node->set("path", ["pathauto" => TRUE]);
      $this->trnode->set("path", ["pathauto" => TRUE]);
    }
    $this->node->set('moderation_state', 'published');
    $this->trnode->set('moderation_state', 'published');
    $this->node->save();
    $this->trnode->save();
  }

  function generateAlias($title = NULL, $titleFr = NULL) {
    $origLang = '';
    $oppLang = '';
    if($this->data->language == "en") {
      $origLang = "en";
      $oppLang = "fr";
    }
    else {
      $origLang = "fr";
      $oppLang = "en";
    }
    if ($this->top_level_item && !empty($title) && !empty($titleFr)) {
      $alias = str_replace(' ', '-', $title);
      $alias = strtolower($alias);
      $aliasFr = str_replace(' ', '-', $titleFr);
      $aliasFr = strtolower($aliasFr);
      // Instantiate the transliteration class.
      $trans = \Drupal::transliteration();

      // Use this to transliterate some text.
      $alias = $trans->transliterate($alias, 'en');
      $aliasFr = $trans->transliterate($aliasFr, 'fr');

      \Drupal::service('path.alias_storage')->save("/node/" . $this->node->id(), '/' . $alias, 'en');
      \Drupal::service('path.alias_storage')->save("/node/" . $this->trnode->id(), '/' . $aliasFr,'fr');
      return; // Do not run pathauto generator for top level items.
    }
    \Drupal::service('pathauto.generator')->updateEntityAlias($this->node, 'insert', array('language' => $origLang));
    \Drupal::service('pathauto.generator')->updateEntityAlias($this->trnode, 'insert', array('language' => $oppLang));
  }
}
