<?php

use Drupal\agri_admin\AgriAdminHelper;

function _shutdown_call_test($action, $nid, $ts_nid, $ts_pnid, $dcr_id, $menu_name, $lang, $title = NULL, $title_fr = NULL) {
    if ($dcr_id == '1395690825741') {
      echo "home page processing.\n";
    }
//  AgriAdminHelper::postUpdateProcess('update', $nid, $page, $disable_menu_link, $title, $title_fr);
    AgriAdminHelper::createInternalLegacyMenuLink($nid, $ts_nid, $ts_pnid, $dcr_id, $menu_name, $lang, $title, $title_fr);
}

function _shutdown_call_test_two($title, $external_link, $menu_name, $ts_nid, $ts_pnid, $title_fr, $external_link_fr, $lang = 'en') {
  $result_sb = AgriAdminHelper::createExternalLegacyMenuLink($title, $external_link, 'sidebar', $ts_nid, $ts_pnid, $title_fr, $external_link_fr);
  AgriAdminHelper::createExternalLegacyMenuLink($title, $external_link, 'sidebar', $ts_nid, $ts_pnid, $title_fr, $external_link_fr, 'fr');
}

class Sitemapinit extends ImportNode
{

  function getBreadcrumbTitle(&$title, &$titleFr) {
    if (!empty($this->data->breadcrumb->en) && $this->data->breadcrumb->en != 'null') {
      $title = $this->data->breadcrumb->en;
      $titleFr = $this->data->breadcrumb->fr;
    }
    else {
      $this->getSitemapTitle($title, $titleFr);
    }
  }

  function getSitemapTitle(&$title, &$titleFr) {
    if (is_null($title) && isset($this->data->node_title) && $this->data->node_title->en != 'null' && !empty($this->data->node_title->en)) {
      $title = $this->data->node_title->en;
      $titleFr = $this->data->node_title->fr;
    }
    if (is_null($titleFr) && isset($this->data->node_title) && $this->data->node_title->en != 'null' && !empty($this->data->node_title->en)) {
      $titleFr = $this->data->node_title->fr;
    }
    if ((is_null($title) || empty($title)) && !empty($this->data->title->en)) {
      if ($this->data->title->en != 'null') {
        $title = $this->data->title->en;
        $titleFr = $this->data->title->fr;
      }
    }
    if ((is_null($title) || empty($title)) && !empty($this->data->meta->title->en)) {
      if ($this->data->meta->title->en != 'null') {
        $title = $this->data->meta->title->en;
        $titleFr = $this->data->meta->title->fr;
      }
    }

  }

  function fix_sitemap_dependency($ts_nid, $ts_pnid, $menuid, $dcr_id = NULL) {
    AgriAdminHelper::updateTsNid($menuid, $ts_nid, 'en');
    AgriAdminHelper::updateTsPnid($menuid, $ts_pnid, 'en');
    if (!empty($dcr_id)) {
      AgriAdminHelper::updateDcrId($menuid, $dcr_id, 'en');
    }
    $nid = AgriAdminHelper::getNidFromMenuLinkContentId($menuid, 'sidebar', 'en');
    if (is_numeric($nid)) {
      AgriAdminHelper::updateNodeLegacyIds($nid, $ts_nid, $ts_pnid, $dcr_id);
    }
    //AgriAdminHelper::updateTsNid($menuid, $node_id, 'fr');
    //AgriAdminHelper::updateTsPnid($menuid, $parent_node_id, 'fr');
  }

  private function preImport() {
/*	  {
    "has_dcr_id": true,
    "node_id": "1279029750897",
    "parent_node_id": "1279029750897",
    "dcr_id": "1287261736402",
    "landingPageUrl": {
        "en": "1287261736402",
        "fr": "1287261736402"
    },
    "parent_dcr_id": "1287261736402",
    "node_title": {
        "en": "null",
        "fr": "null"
    },
   menu link id = 110
*/
    if ($this->data->parent_node_id == '1354137773675' /*main*/) {
      //$parent_node_id = '1354137773675'; // Home.
      //$this->data->parent_node_id = $parent_node_id;
      $parent_dcr_id = '1395690825741'; // Home.
      $this->data->parent_dcr_id = $parent_dcr_id;
    }
    return TRUE;
  /*  $menuid = 4; // Find People and Collaborate.
    if (!AgriAdminHelper::hasTsNid($menuid, 'en')) {
      $node_id = '1522243010679'; // Find People and Collaborate.
      $parent_node_id = '1279565444868';
      $dcr_id = '1522094988320';
      $this->fix_sitemap_dependency($node_id, $parent_node_id, $menuid, $dcr_id);
      unset($node_id);
      unset($parent_node_id);
    }*/

  }

  function import($jfile)
  {
    global $export_root;
    $this->loadData($jfile);
    $this->preImport();

    if ($this->data->has_dcr_id && strlen($this->data->parent_node_id) > 7) {
      $ts_pnid = $this->data->parent_node_id;
      $ts_nid = $this->data->node_id;
      $dcr_id = $this->data->dcr_id;
      // this is a top level item, process it during sitemap init.
      //echo "Skipping dcr_id for sitemap init, it will be done in the sitemap import.\n";
      if (AgriAdminHelper::dcrIdPreviouslyImported($this->data->dcr_id)) {
        echo "Previously imported dcr_id: " . $this->data->dcr_id . "\n";
        $this->previously_imported = TRUE;
        return FALSE;
      }
      if (isset($this->data->pageNotFound) && $this->data->pageNotFound) {
        //echo "pageNotFound for dcr_id " . $this->data->dcr_id . "\n";
        //echo "skipped " .  $this->data->node_id . ".json\n";
        return FALSE;
      }
      //1354137773675 /*Home*/
      //echo "Has dcr_id " . print_r($this->data->dcr_id, TRUE) . "\n";
      $top_level_array = array(
        '1354137773675',/*Home*/
      );
      if (in_array($ts_nid, $top_level_array)) {
        $this->top_level_item = TRUE;
      }
      $main_menu_ts_nid = '1354137773675';
      $dependency_bypass = FALSE;
      foreach ($top_level_array as $top_level) {
      if (($top_level == $ts_nid && $top_level == '1354137773675')/*Home*/) {
          $dependency_bypass = TRUE;
        }
      }
      $all_top_level_ready = TRUE;
      // foreach ($top_level_array as $top_level) {
      //   if (!AgriAdminHelper::tsNidPreviouslyImported($top_level)) {
      //     $all_top_level_ready = FALSE;
      //   }
      // }


      if (!$dependency_bypass && !$all_top_level_ready && !AgriAdminHelper::tsNidPreviouslyImported($ts_pnid)) {
        echo "This import depends on (ts_nid aka ts node_id): " . $this->data->parent_node_id . " which must be imported first, skipping this import.\n";
        return FALSE;
      }
      else if (AgriAdminHelper::tsNidPreviouslyImported($ts_pnid) || $ts_nid == $main_menu_ts_nid) {
        echo "Dependency check passed, continue import of " . $export_root . "/" . $jfile . "\n";
      }
      else {
        echo "This import depends on (ts_nid aka ts node_id): " . $this->data->parent_node_id . " which must be imported first, skipping this import.\n";
        return FALSE;
      }

      $type = 'landing_page';
      if (in_array($this->data->parent_node_id, $top_level_array)) {
        $type = 'landing_page';
      }
      else {
        // Import as a landing page not a 'page'.
        $type = 'landing_page';
      }
      echo "Creating a Drupal $type for "  . $export_root . "/sitemapinit/" . $jfile . "\n";
      $this->createNode($type);
      if ($this->unable_to_import) {
        echo "unable to import\n";
        return FALSE;
      }

      if ($this->legacy_server_error) {
        echo "unable to import\n";
        return FALSE;
      }

      if (empty($this->data->body->en)) {
        $this->data->body->en = 'Fix me.';
      }
      if (empty($this->data->body->fr)) {
        $this->data->body->fr = 'Corriger ceci.';
      }
      $external_link = $this->data->landingPageUrl->en;
      $this->importField('body');
      $this->importField('field_issued'/*, 'issued'*/);
      $this->importField('field_modified'/*, 'modified'*/);
      $this->importField('field_dc_date_created'/*, 'dc_date_created'*/);
      $this->importField('field_meta_type');//dcterms:type  (étiquette champ contenu) meta_type (type dcterms:type) aafc_type (vocab id)
      $this->importField('field_description'/*, 'description'*/);
      $this->importField('field_subject'/*, 'aafc:subject'*/); // subject (dans fichier.json) category (vocab id)
      $this->importField('field_dcterms_subject'/*, 'dcterms:subject'*/); // ? (dans fichier.json)
      $this->importField('field_keywords'/*, 'keywords'*/);
      $this->importField('field_teamsite_location');
      $this->importField('field_managing_branch');
//    $this->importField('layout_builder_layout', 'layout_builder_layout');
//    $this->importField('layout_selection', 'layout_selection');

      global $import_override;
      $import_override = TRUE;

      $this->save(); // Saves the node.
      //echo "TEST123;\n";
      //die;


      $title = NULL;
      $titleFr = NULL;
      $this->getBreadcrumbTitle($title, $titleFr);

      if ($dependency_bypass && $ts_pnid == $main_menu_ts_nid  /*main menu*/ || $ts_nid == '1354137773675') {
      //  AgriAdminHelper::createInternalLegacyMenuLink($this->nid(), $ts_nid, $ts_pnid, $dcr_id, 'sidebar', 'en');
        if ($this->data->dcr_id == '1395690825741') {
          echo "home page processing options here\n";
        }
        AgriAdminHelper::createInternalLegacyMenuLink($this->nid(), $ts_nid, $ts_pnid, $dcr_id, 'sidebar', 'en', $title, $titleFr);
        //AgriAdminHelper::createTopLevelInternalMenuItem($this->nid(), $menu_name = 'sidebar', $ts_nid, $ts_pnid, $dcr_id); // This function is broken.
        AgriAdminHelper::createTopLevelInternalMenuItem($this->nid(), 'sidebar', $ts_nid, $ts_pnid, $dcr_id, $title, $titleFr);
//        AgriAdminHelper::createTopLevelInternalMenuItem($this->nid(), 'sidebar', $ts_nid, $ts_pnid, $dcr_id, $title, $titleFr);
       // AgriAdminHelper::createTopLevelInternalMenuItem(14 /*news top level*/, $menu_name = 'sidebar', '1522242982779', $ts_pnid, '1522092668036');
      } else {

        AgriAdminHelper::createInternalLegacyMenuLink($this->nid(), $ts_nid, $ts_pnid, $dcr_id, 'sidebar', 'en', $title, $titleFr);
        //AgriAdminHelper::createInternalLegacyMenuLink($this->nid(), $ts_nid, $ts_pnid, $dcr_id, 'sidebar', 'en');
        //AgriAdminHelper::postUpdateProcess('update', $this->nid(), 'page');
        drupal_register_shutdown_function('_shutdown_call_test', 'update', $this->nid(), $ts_nid, $ts_pnid, $dcr_id, 'sidebar', 'en', $title, $titleFr);

      }
      //AgriAdminHelper::updateNodeLegacyIds($this->nid(), $ts_nid, $ts_pnid, $dcr_id); // this should be done by the import.php.
      if ($this->top_level_item) {
        $this->generateAlias($title /*breadcrumb title*/, $titleFr /*breadcrumb title*/);
      }
      else {
        $this->generateAlias();
      }

      return TRUE;
    } else if ($this->data->has_dcr_id) {
      //@TODO , refactor this, do something.
      echo "Has a dcr_id " . $this->data->dcr_id . "\n";
      echo "@TODO , refactor this, do something other than nothing.. \n";
      return FALSE;
      // Create a menu link instead, inspire from agri_admin/src/AgriAdminMenuHelper.php
    } else {
      if (AgriAdminHelper::tsNidPreviouslyImported($this->data->node_id)) {
        $this->previously_imported = TRUE;
        return FALSE;
      }
      echo "Has no dcr_id " . $this->data->dcr_id . "\n";
      echo " Create a menu link instead. \n";
      $title = NULL;
      $titleFr = NULL;
      $this->getExportTitle($title, $titleFr);
      $this->getBreadcrumbTitle($title, $titleFr);
      echo "\n";
      echo $title;
      echo "\n";
      $external_link = $this->data->landingPageUrl->en;
      $external_link_fr = $this->data->landingPageUrl->fr;
      $ts_nid = $this->data->node_id;
      $ts_pnid = $this->data->parent_node_id;
      $menu_name = 'sidebar';
      if (is_numeric($external_link)) {
        return FALSE;
      }
      //AgriAdminHelper::addToLog('title=' . $title);
      //AgriAdminHelper::addToLog('titleFr=' . $titleFr);
      $result = AgriAdminHelper::createExternalLegacyMenuLink($title, $external_link, $menu_name, $ts_nid, $ts_pnid, $titleFr, $external_link_fr);
      AgriAdminHelper::createExternalLegacyMenuLink($title, $external_link, $menu_name, $ts_nid, $ts_pnid, $titleFr, $external_link_fr, 'fr');
      // Create a menu link instead, inspire from agri_admin/src/AgriAdminMenuHelper.php
      $this->sitemap_external_only = TRUE;
      if (!$result) {
        $this->unable_to_import = TRUE;
      }
     // $result_sb = AgriAdminHelper::createExternalLegacyMenuLink($title, $external_link, 'sidebar', $ts_nid, $ts_pnid, $titleFr, $external_link_fr);
     // AgriAdminHelper::createExternalLegacyMenuLink($title, $external_link, 'sidebar', $ts_nid, $ts_pnid, $titleFr, $external_link_fr, 'fr');
//      AgriAdminHelper::createExternalLegacyMenuLink($title, $external_link, 'sidebar', $ts_nid, $ts_pnid, $titleFr, $external_link_fr, 'en');
//      AgriAdminHelper::createExternalLegacyMenuLink($title, $external_link, 'sidebar', $ts_nid, $ts_pnid, $titleFr, $external_link_fr, 'fr');
      drupal_register_shutdown_function('_shutdown_call_test_two', $title, $external_link, 'sidebar', $ts_nid, $ts_pnid, $titleFr, $external_link_fr);
      return $result;
    }
  }
}
