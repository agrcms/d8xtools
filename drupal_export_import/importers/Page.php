<?php

use Drush\Drush;
use Drupal\agri_admin\AgriAdminHelper;

function _shutdown_call_page($action, $nid, $ts_nid, $ts_pnid, $dcr_id, $menu_name, $lang, $title = NULL, $title_fr = NULL) {
  //Drush::output()->writeln('debug: title='. $type . ' :print_r ' . print_r($title, TRUE));
  //Drush::output()->writeln('debug: title_fr='. $type . ' :print_r ' . print_r($title_fr, TRUE));
  global $import_override;
  $import_override = TRUE;
  AgriAdminHelper::createInternalLegacyMenuLink($nid, $ts_nid, $ts_pnid, $dcr_id, $menu_name, $lang, $title, $title_fr);
  AgriAdminHelper::disableMenuLink(NULL, $nid, $menu_name, TRUE);
//  AgriAdminHelper::disableMenuLink(NULL, $nid, 'sidebar', TRUE);
  //drupal_register_shutdown_function('_shutdown_call_page_again', 'update', $nid(), 'page');
}

function _shutdown_call_page_again($action, $nid, $page, $disable_menu_link = TRUE) {
}

class Page extends ImportNode
{

  public function isTooOld() {
    return FALSE; // Import ALL page /gene-gene.
    if (isset($this->data->meta->issued->en)) {
      $january2013 = 1356998400;
      $issued = strtotime($this->data->meta->issued->en);
      if ($issued > 1 && $issued < $january2013) {
        $this->too_old = TRUE;
        return TRUE;
      }
    }
    return FALSE;
  }

  private function preImport() {
    $this->type = 'page'; // Default.
    //$ts_type = str_replace('intra-intra/', '', $this->data->type_name);
    $ts_type = str_replace('comm-comm/', '', $this->data->type_name);
    switch ($ts_type) {
      case 'news-nouv':
        $this->type = 'news';
        break;
      case 'gene-gene':
        $this->type = 'page';
        break;
      case 'empl-empl':
        $this->type = 'empl';
        break;
      case '':
      case NULL:
        $this->type = 'empty';
        echo "type_name is NULL for dcr_id=" . $this->data->dcr_id . "\n";
        return FALSE;
      case 'list-list':
      case 'widg-widg':
      case 'rank-clas':
      case 'nwinit-nwinit':
         $this->type = 'other';
         return FALSE;
      default:
        $this->type = 'page';//$ts_type;
        break;
    }
    if (isset($this->data->pageNotFound) && $this->data->pageNotFound) {
      //$this->type = 'deleted';
      echo "pageNotFound for dcr_id: " . $this->data->dcr_id . "\n";
      return FALSE;
    }
    $ts_nid = $this->data->node_id;
    $dcr_id = $this->data->dcr_id;

    if (AgriAdminHelper::dcrIdPreviouslyImported($this->data->dcr_id)) {
      echo "Previously imported dcr_id: " . $this->data->dcr_id . "\n";
      $this->previously_imported = TRUE;
      return FALSE;
    }

    if (!AgriAdminHelper::tsNidPreviouslyImported($ts_nid)) {
      echo "Sitemap dependency was not yet imported, please run the import with \n" .
      "the sitemapinit option first and make sure you have the entire sitemap \n" .
      "content initialized first before running this teamsite import.\n";
      $this->unable_to_import = FALSE;
      return FALSE;
      die; //@TODO , Lakshman is reviewing content issues, need to re-export as the sitemap appears to be incomplete.
    }
    return TRUE;
  }

  function import($jfile)
  {
    global $export_root;
    $this->loadData($jfile);

    if (strlen($this->data->dcr_id) > 7 && strlen($this->data->node_id) > 7) {
      $ts_nid = $this->data->node_id;
      $dcr_id = $this->data->dcr_id;
      $pre_import_code = $this->preImport();
      if ($this->isTooOld() || !$pre_import_code) {
        echo "Something went wrong in " . $this->type . " import\n";
        return FALSE;
      }
      echo "Creating a Drupal " . $this->type . " for "  . $export_root . "/" . $this->type . "/" . $jfile . "\n";
      $this->createNode($this->type);
      if ($this->unable_to_import) {
        echo "unable to import\n";
        return FALSE;
      }
      if ($this->legacy_server_error) {
        echo "unable to import\n";
        return FALSE;
      }
      if (empty($this->data->body->en)) {
        $this->data->body->en = 'Fix me.';
      }
      if (empty($this->data->body->fr)) {
        $this->data->body->fr = 'Corriger ceci.';
      }
      $this->importField('field_meta_type');//meta_type

      $this->importField('body');
      $this->importField('field_issued');//issued
      $this->importField('field_modified');//modified
      $this->importField('field_meta_type');//dcterms:type  (étiquette champ contenu) meta_type (type dcterms:type) aafc_type (vocab id)
      $this->importField('field_description');//description
      $this->importField('field_subject'/*, 'aafc:subject'*/); // subject (dans fichier.json) category (vocab id)
      $this->importField('field_dcterms_subject'/*, 'dcterms:subject'*/); // ? (dans fichier.json)
      $this->importField('field_dc_date_created'/*, 'dc_date_created'*/);
      $this->importField('field_keywords');//keywords
      $this->importField('field_aafc_type'/*, 'field_aafc_type'*/);
      $this->importField('field_teamsite_location');
      $this->importField('field_managing_branch');
//    $this->importField('layout_builder_layout', 'layout_builder_layout');
//    $this->importField('layout_selection', 'layout_selection');

      if ($this->type == 'page') {
        $this->importField('field_dc_date_created'/*, 'dc_date_created'*/);
      }


      global $import_override;
      $import_override = TRUE;
      $this->save();

      if (1) {
        // Enabled (for now).
        $disable_menu_link = TRUE;
        $title_eng = '';
        $title_fra = '';
        $this->getExportTitle($title_eng, $title_fra);
        $breadcrumb = (empty($this->data->breadcrumb->{'en'})) ? $title_eng: $this->data->breadcrumb->{'en'};
        $breadcrumb_fr = (empty($this->data->breadcrumb->{'fr'})) ? $title_fra: $this->data->breadcrumb->{'fr'};
  //Drush::output()->writeln('debug: title='. $type . ' :print_r ' . print_r($breadcrumb, TRUE));
  //Drush::output()->writeln('debug: title_fr='. $type . ' :print_r ' . print_r($breadcrumb_fr, TRUE));
        AgriAdminHelper::createInternalLegacyMenuLink($this->nid(), $ts_nid, $ts_nid, $dcr_id, 'sidebar', 'en', $breadcrumb, $breadcrumb_fr, $disable_menu_link);
        //AgriAdminHelper::createInternalLegacyMenuLink($this->nid(), $ts_nid, $ts_nid, $dcr_id, 'sidebar', 'en');
        drupal_register_shutdown_function('_shutdown_call_page', 'update', $this->nid(), $ts_nid, $ts_nid, $dcr_id, 'sidebar', 'en', $breadcrumb, $breadcrumb_fr);
      }

      //AgriAdminHelper::updateNodeLegacyIds($this->nid(), $ts_nid, $ts_pnid, $dcr_id); // this should be done by the import.php.
      $this->generateAlias();
      return TRUE; // Success.
    } else {
      echo "Might not have a node_id or a dcr_id " . $this->data->dcr_id . ".json\n";
      $this->preImport();
      return FALSE;
    }
  }
}
