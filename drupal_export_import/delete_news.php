<?php

use Drupal\node\Entity\Node;

$nids = \Drupal::entityQuery('node')->condition('type','news')->execute();
foreach ($nids as $nid) {
  Drush::output()->writeln('processing delete news for nid ' . $nid . "\n");
  $node = Node::load($nid);
  $node->delete();
}

