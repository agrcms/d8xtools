<?php
$tableau_category = [];

foreach (Drupal::entityTypeManager()->getStorage("taxonomy_term")->loadByProperties(["vid" => ["dcterms_subject","category", "aafc_type" ]]) as $term) {
  $fr = '';
  if ($term->hasTranslation('fr')) {
    $fr = $term->getTranslation('fr')->label();  
    echo ($term->label() . " = " . $fr . "\n" );
  }
  // else {
  //   echo ('Aucune traduction pour ' . $term->label() . "\n"); 
  // }
  $tableau_category[$term->label()] = $fr;
}

echo "***************************************************************\n";
ksort($tableau_category);
foreach ($tableau_category as $cle => $traduction_fr) {
  echo "$cle = $traduction_fr\n";
}
// foreach (Drupal::entityTypeManager()->getStorage("taxonomy_term")->loadByProperties(["vid" => ["dcterms_subject" /*,"category", "aafc_type"*/ ]]) as $term) {
//   echo ($term->label() . "\n" );
// }