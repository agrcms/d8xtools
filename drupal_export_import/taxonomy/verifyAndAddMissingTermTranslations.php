<?php
$tableau_category = [];
// Clé+valeurs / Key+values.
// Store english as keys, french as values.
$clevaleurs = prend_charge_en_fr();

$already_translated = TRUE;
foreach (Drupal::entityTypeManager()->getStorage("taxonomy_term")->loadByProperties(["vid" => ["dcterms_subject","category", "aafc_type" ]]) as $term) {
  $fr = '';
  if ($term->hasTranslation('fr')) {
    $fr = $term->getTranslation('fr')->label();  
    //echo ($term->label() . " = " . $fr . "\n" );
  }
  else if ($term->label() != 'gctype') {
    echo $term->label() . " needs translating...\n";
    $already_translated = FALSE;
    ajouter_traduction_manquant($term, $clevaleurs);
  }
  // else {
  //   echo ('Aucune traduction pour ' . $term->label() . "\n"); 
  // }
  $tableau_category[$term->label()] = $fr;
}

if ($already_translated) {
  echo "Congratulations, terms were already translated.\n";
}
else if (!$already_translated) {
  echo "***************************************************************\n";
  ksort($tableau_category);
  foreach ($tableau_category as $cle => $traduction_fr) {
    if (empty($traduction_fr)) {
      echo $cle . " is not yet translated.\n";
      continue;
    }
    echo "$cle = $traduction_fr\n";
  }
}

function prend_charge_en_fr() {
  // Clé+valeurs / Key+values.
  // Store english as keys, french as values.
  $clevaleurs = [];
  //echo DRUPAL_ROOT . '/../../d8xtools/drupal_export_import/importers/taxonomy/term_en_fr_2.txt';
  $txt_file = file_get_contents(DRUPAL_ROOT . '/../../d8xtools/drupal_export_import/importers/taxonomy/term_en_fr_2.txt');
  $rows = explode("\n", $txt_file);
  //echo getcwd();
  foreach($rows as $row => $data)
  {
    $row_data = explode('=', $data);
    $clevaleurs[trim($row_data[0])] = trim($row_data[1]);
  }
  return $clevaleurs;
}

function ajouter_traduction_manquant($term /* = \Drupal\Entity\Term*/, &$clevaleurs = []) {
  $french = '';
   
  // Inconsistent data between french/english, so disabling auto translation.
  foreach($clevaleurs as $cle => $valeur) {
    if (trim($cle) == trim($term->label())) {
      $french = $valeur;
      //$termObj = \Drupal\taxonomy\Entity\Term::load($termId);  //process the nodes however is desired
      //$termObj->addTranslation('fr', ['name' => $term_fr]);
      $term->addTranslation('fr', ['name' => $french]);
      echo " **************************** new value translation *******************************;\n";
      echo $vid . ':' . $term->label() . ' Ajouter traduction :' . $french . "\n";
      echo "term->save();\n";
      $term->save();
      break;
    }
  }
  return $french;
}

