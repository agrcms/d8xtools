<?php

use Drush\Drush;

class Page extends UpdateNode
{
  public $success = FALSE;
  //public $could_not_load = FALSE; // inherited.
  function update($jfile)
  {
    $this->loadData($jfile);
    //Drush::output()->writeln(getcwd());
    //Drush::output()->writeln($jfile);

    if ($metatag = $this->data->meta) {
      $this->loadNodeByOldId($this->old_id());
      $title = '';
      $title_fr = '';
      $this->getTitle($title, $title_fr);
      if ($this->node) {
        //$this->updateField('field_override_image_sharing');
        $this->success = TRUE;
        Drush::output()->writeln("verified $title");
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln("could not load: $title");
        Drush::output()->writeln('d8nid could not be found using dcr_id=' . $this->old_id());
      }
    } else {
      $this->could_not_load = TRUE;
      Drush::output()->writeln('no metatag information using dcr_id=' . $this->old_id());
    }
//    $this->save();
  }

  function getTitle(&$title, &$titleFr) {
    if ((is_null($title) || empty($title)) && isset($this->data->meta->title->en) && !empty($this->data->meta->title->en)) {
      if ($this->data->meta->title->en != 'null') {
        $title = $this->data->meta->title->en;
        $titleFr = $this->data->meta->title->fr;
      }
    }
    if ((is_null($title) || empty($title)) && !empty($this->data->title->en)) {
      if ($this->data->title->en != 'null') {
        $title = $this->data->title->en;
        $titleFr = $this->data->title->fr;
      }
    }
    if (isset($this->data->node_title->en) && $this->data->node_title->en != 'null' && !empty($this->data->node_title->en)) {
      $title = $this->data->node_title->en;
      $titleFr = $this->data->node_title->fr;
    }
    if ((is_null($title) || empty($title)) && isset($this->data->breadcrumb->en) && !empty($this->data->breadcrumb->en)) {
      if ($this->data->breadcrumb->en != 'null') {
        $title = $this->data->breadcrumb->en;
        $titleFr = $this->data->breadcrumb->fr;
      }
    }
  }

}

