<?php


if (!isset($argv[1])) {
  echo "usage: " . $argv[0] . '  path/to/d8tools/Internet_Database_dumpEN_FR_tab_adjusted.csv \t' . "\n";
  //echo "assumes header as follows: DCR_ID   LANG   TITLE   MODIFIED   ISSUED   TYPE_NAME   NODE_ID   LAYOUT_NAME   CREATED   UPDATED\n";
  //echo "assumes header as follows: dcr_id   lang   title   modified   issued   type_name   node_id   layout_name   created   updated\n";
//                                 "dcr_id"	"langcode"	"title"  	"modified"	"issued"	"dcr_type"	"templatetype"  	"page_type"	"node_id"	"dc_date_created"	"managing_branch"	"teamsite_location"
//                                 "DCR_ID"	 "LANGCODE"	 "TITLE"	"MODIFIED"	"ISSUED"	"DCR_TYPE"	"TEMPLATETYPE"   	"PAGE_TYPE"	"NODE_ID"	"dc_date_created"	 "MANAGING_BRANCH"	"parent_node_id"	"label"	   "extURL"	"TEAMSITE_LOCATION"
  echo "assumes header as follows: \"dcr_id\"	\"langcode\"	\"title\"	\"modified\"	\"issued\"	\"dcr_type\"	\"templatetype\"	\"page_type\"	\"node_id\"	\"dc_date_created\"	\"managing_branch\"	\"teamsite_location\"\n";
  die;
}

$file = file_get_contents($argv[1]);
$csv = csv_to_array($argv[1]);
function csv_to_array($filename='', $delimiter="\t")
{
  if (!file_exists($filename) || !is_readable($filename)) {
    return FALSE;
  }

    $header = NULL;
    $data = array();
    $home_row_en = [];
    $home_row_fr = [];
    if (($handle = fopen($filename, 'r')) !== FALSE) {
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
      {
        if (!$header) {
          $new_row = [];
          foreach ($row as $cle => $colonne) {
            if (is_string($colonne)) {
              $colonne = strtolower($colonne);
            }
            $new_row[$cle] = $colonne;
          }
          $row = $new_row;
          $header = $row;
	}
        else {
	  $temp_row = $row;
          $temp_row[11] = 'temp';
	  $temp_row[12] = $temp_row[2];
	  $temp_row = array_combine($header, $temp_row);
          if ($temp_row['node_id'] == 1354137773675 && $temp_row['langcode'] == 'eng' && $temp_row['dcr_id'] == 1395690825741) {
            $temp_row['parent_node_id'] = $temp_row['node_id'];
	    $temp_row['dcr_type'] = 'comm-comm/gene-gene';
	    $temp_row['type_name'] = 'sitemapinit';
            $home_row_en = $temp_row;
          }
          if ($temp_row['node_id'] == 1354137773675 && $temp_row['langcode'] == 'fra' && $temp_row['dcr_id'] == 1395690825741) {
            $temp_row['parent_node_id'] = $temp_row['node_id'];
	    $temp_row['dcr_type'] = 'comm-comm/gene-gene';
	    $temp_row['type_name'] = 'sitemapinit';
            $home_row_fr = $temp_row;
          }
          $data[] = array_combine($header, $row);
        }
      }
      fclose($handle);
    }
    if (!empty($home_row_en) && !empty($home_row_fr)) {
      $data[] = $home_row_en;
      $data[] = $home_row_fr;
    }
    return $data;
}

$array_of_content = [];

$has_dcr_id = TRUE;
$eng_exturl = '';
$no_dcr_id_array = [];
foreach ($csv as $row) {
  $row['type_name'] = $row['dcr_type'];
  // source data changed from first run for intranet, but we are lazy, just copy over.
  unset($row['dcr_type']); // Remove this noise. du bruit.
  if (isset($row['parent_node_id']) && !empty($row['parent_node_id'])) {
    $row['type_name'] = 'sitemapinit';
  }
  if (0 && !empty($row['exturl']) && !is_numeric($row['exturl']) && strlen($row['exturl']) > 7) {
    if ($row['type_name'] == 'sitemapinit' && $row['langcode'] == 'eng') {
      $row['dcr_id'] = $row['exturl'];
      $no_dcr_id_array[] = '/' . $row['node_id'] . '.json';
      $eng_exturl = $row['exturl'];
      $has_dcr_id = FALSE;
      //$row['node_id'] = $row['node_id'] . '_TEST'; // DEBUGGING.
    }
    else if ($row['type_name'] == 'sitemapinit' && $row['langcode'] == 'fra') {
      $has_dcr_id = FALSE;
    }
  }
  else {
    $has_dcr_id = TRUE;
    $eng_exturl = '';
  }
  $row['has_dcr_id'] = $has_dcr_id;
  if(isset($row['short_title'])){
      $row['label'] = $row['short_title'];
  }
  $row['breadcrumb'] = $row['label'];
  if (empty($row['label'])) {
    $row['breadcrumb'] = $row['title'];
    $row['label'] = $row['title'];
  }
  $row['templatetype'] = $row['templatetype'];
  $row['layout_name'] = $row['templatetype'];
  if ($row['layout_name'] == '<NULL>') {
    $row['layout_name'] = NULL;
  }
  if (!isset($row['lang']) && isset($row['langcode'])) {
    $row['lang'] = $row['langcode'];
    unset($row['langcode']);
  }
  if ($row['lang'] == 'eng') {
    $row['lang'] = 'en';
  }
  if ($row['lang'] == 'fra') {
    $row['lang'] = 'fr';
  }
  if ((!isset($array_of_content[$row['dcr_id']]) && $row['lang'] == 'en' && $row['type_name'] != 'sitemapinit') ||
       (!isset($array_of_content[$row['node_id']]) && $row['lang'] == 'en' && $row['type_name'] == 'sitemapinit')
    ) {
      if (!empty($row['parent_node_id']) && $row['type_name'] == 'sitemapinit') {
        $array_of_content[$row['node_id']] = $row;
      }
      else {
        $array_of_content[$row['dcr_id']] = $row;
      }
  } else {
    if (!$has_dcr_id && !empty($eng_exturl)) {
      $origRow = $array_of_content[$eng_exturl];
    }
    else {
      if (!empty($row['parent_node_id']) && $row['type_name'] == 'sitemapinit') {
        $origRow = $array_of_content[$row['node_id']];
      }
      else {
        $origRow = $array_of_content[$row['dcr_id']];
      }
    }
    if ($origRow['lang'] == 'en' && !isset($row['title']['en']) && $row['lang'] == 'fr') {
	    //echo print_r($row, TRUE);
	    //die;
      $titleEn = $origRow['title'];
      if (isset($origRow['title']['en']) && !empty($titleEn['en'])) {
        $titleEn = $origRow['title']['en'];
      }
      $breadcrumbEn = $origRow['breadcrumb'];
      if (isset($breadcrumbEn['en']) && !empty($breadcrumbEn['en'])) {
        $breadcrumbEn = $breadcrumbEn['en'];
      }
      $labelEn = $origRow['label'];
      if (isset($labelEn['en']) && !empty($labelEn['en'])) {
        $labelEn = $labelEn['en'];
      }
      $origRow['breadcrumb'] = [];
      $origRow['breadcrumb']['en'] = $breadcrumbEn;
      $origRow['breadcrumb']['fr'] = $row['breadcrumb'];
      $origRow['label'] = [];
      $origRow['label']['en'] = $labelEn;
      $origRow['label']['fr'] = $row['label'];
      $origRow['title'] = [];
      $origRow['title']['en'] = $titleEn;
      $origRow['title']['fr'] = $row['title'];
      if ($origRow['lang'] == 'en' && !isset($row['teamsite_location']['en']) && $row['lang'] == 'fr') {
        $tslocationEn = $origRow['teamsite_location'];
        if (isset($tslocationEn['en']) && !empty($tslocationEn['en'])) {
          $tslocationEn = $origRow['teamsite_location']['en'];
        }
        $origRow['teamsite_location'] = array();
        $origRow['teamsite_location']['en'] = $tslocationEn;
        $origRow['teamsite_location']['fr'] = $row['teamsite_location'];
      //  $array_of_content[$origRow['dcr_id']] = $origRow;
      }
      if (!empty($row['parent_node_id']) && $row['type_name'] == 'sitemapinit') {
        $array_of_content[$row['node_id']] = $origRow;
      }
      else {
        $array_of_content[$row['dcr_id']] = $origRow;
      }
    }
  }
}

$objs = arrayToObject($array_of_content);
function arrayToObject($array) {
  if (!is_array($array)) {
    return $array;
  }

  $object = new stdClass();
  if (is_array($array) && count($array) > 0) {
    foreach ($array as $name=>$value) {
      $name = strtolower(trim($name));
      if (!empty($name)) {
        $object->$name = arrayToObject($value);
      }
    }
    return $object;
  }
  else {
    return FALSE;
  }
}

//echo print_r($objs, TRUE);
$month_day = '02-28'; //date('m-d', time());

$dir = getcwd() . '/internet_export_' . $month_day;
if (!is_dir($dir)) {
  mkdir($dir);
}
chdir($dir);
symlink('gene-gene', 'page');
$keep_existing = true;
$count_json = 0;
$count_skipped = 0;
$dec31_2017 = strtotime('2017-12-31');
$oct11_2020 = strtotime('2020-10-10');
$skip = FALSE;
$written_by_type = [];
foreach ($array_of_content as $key => $entity) {
  if ($entity['type_name'] == 'sitemapinit') {
    $key = $entity['node_id'];
    echo "Sitemapinit json key=$key";
    echo "\n";
  }
  $skip = FALSE;// This needs to be at the top of the forloop inside.
  if ($keep_existing) {
    if (file_exists($key.'.json')) {
      continue;
    }
  }
  $entity['layout_name'] = $entity['templatetype'];
  //unset($entity['templatetype']);
  $type = str_replace('intra-intra/', '', $entity['type_name']);
  $type = str_replace('comm-comm/', '', $entity['type_name']);
  if ($entity['type_name'] == 'exec-exec/summ-somm')
    $type = 'gene-gene';

  //unset($entity['dcr_type']);
  chdir($dir);
  if (!is_dir($type)) {
    mkdir($type);
  }
  chdir($type);
  if ($fp = fopen($key.'.json', 'w')) {
    if (!isset($written_by_type[$type])) {
      $written_by_type[$type] = 1;
    }
    else {
      $written_by_type[$type]++;
    }
    $count_json++;
    $entity_obj = arrayToObject($entity);
    if ($type == 'empl-empl' || $type == 'news-nouv') {
//    if (strtotime($entity_obj->modified) < $dec31_2017) {
      if (strtotime($entity_obj->modified) < $oct11_2020) {
        $count_skipped++;
        $skip = TRUE;
      }
    }
    echo "Write $key.json\n";
    fwrite($fp, json_encode($entity, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));
    fclose($fp);
    if ($skip) {
      echo "Delete $key.json $type\n";
      unlink($key.'.json');// or die("Couldn't delete $key.json");
      $skip = FALSE;
    }
  }
  chdir($dir);
}

echo "$count_json entities processed, skipped $count_skipped entities.\n";
echo "Summary by type:\n";
foreach ($written_by_type as $key => $value) {
  echo "Wrote $value $key items\n";
}
echo "------------------------------------------------------------------------------\n";
echo count($no_dcr_id_array) . " has no dcr_id (menu link) items:\n";
foreach ($no_dcr_id_array as $value) {
  echo "has no dcr_id (menu link) internet_export_$month_day/sitemapinit$value\n";
}
echo "------------------------------------------------------------------------------\n";
echo "Wrote " . ($count_json - $count_skipped) . " json files to $dir\n";
echo "      " . ($count_json - $count_skipped - count($no_dcr_id_array))*2 . " TOTAL pages (English AND French).\n";
