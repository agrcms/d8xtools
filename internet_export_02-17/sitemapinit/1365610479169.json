{
    "dcr_id": "1169564397864",
    "title": {
        "en": "About the Central Experimental Farm",
        "fr": "\u00c0 propos de la Ferme exp\u00e9rimentale centrale"
    },
    "modified": "2019-09-17",
    "issued": "2009-02-24",
    "templatetype": "content page 1 column",
    "page_type": "1",
    "node_id": "1365610479169",
    "dc_date_created": "2009-02-24",
    "managing_branch": "PAB",
    "parent_node_id": "1354137774046",
    "label": {
        "en": "About the Farm",
        "fr": "\u00c0 propos de la Ferme"
    },
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/01/cef_about-apropos_1169564397864_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/01/cef_about-apropos_1169564397864_fr"
    },
    "type_name": "sitemapinit",
    "has_dcr_id": true,
    "breadcrumb": {
        "en": "About the Farm",
        "fr": "\u00c0 propos de la Ferme"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "meta": {
        "issued": {
            "en": "2009-02-24",
            "fr": "2009-02-24"
        },
        "modified": {
            "en": "2019-09-17",
            "fr": "2019-09-17"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "About the Central Experimental Farm",
            "fr": "\u00c0 propos de la Ferme exp\u00e9rimentale centrale"
        },
        "subject": {
            "en": "architectural heritage;architecture;statistics",
            "fr": "m\u00e9dicament;agriculture;politique"
        },
        "description": {
            "en": "to balance the farm's dual role as a national historic site and an active research landscape, the central experimental farm national historic site management plan provides the farm's long-term management framework.",
            "fr": "afin d'\u00e9quilibrer le double r\u00f4le de la ferme, celui de lieu historique national et celui de milieu de recherches dynamique, le plan directeur du lieu historique national de la ferme exp\u00e9rimentale centrale constitue le cadre de gestion \u00e0 long terme de la ferme."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": " agricultural science; active research landscape; farm's long-term management framework; scientific collections;central experimental farm advisory council",
            "fr": " propos de la fermeles; long terme de la ferme; histoire des sciences; double r\u00f4le de la ferme;lieu historique national"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "memorandum",
            "fr": "avis"
        },
        "creator": {
            "en": "Central Experimental Farm Integrated Services;Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Services int\u00e9gr\u00e9s de la Ferme exp\u00e9rimentale centrale;Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "body": {
        "en": "<!-- D&eacute;but du contenu -->\r\n    <div class=\"col-md-4 pull-right\" style=\"z-index: 1000\">\n<section class=\"panel panel-default\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Related links</h2>\n</header>\n<div class=\"panel-body\"><ul class=\"wet-boew-zebra\">\n<li><a href=\"/eng/?id=1232732638116\">Photo gallery</a></li>\n<li><a href=\"/eng/?id=1180546650582\">Ottawa Research and Development Centre</a></li>\n<li><a href=\"/eng/?id=1169814977713\">Central Experimental Farm Advisory Council</a></li>\n<li><a href=\"/eng/?id=1170695386778\">Central Experimental Farm National Historic Site Management Plan</a></li>\n<li><a href=\"/eng/?id=1176399349495\">Maps of the Central Experimental Farm</a></li>\n</ul>\n</div>\n</section>\n</div><p>The Central Experimental Farm is 427\u00a0hectares (1,055\u00a0acres) of open space in downtown Ottawa, the capital of Canada, and borders the historic Rideau Canal.</p>\n\n<h2>History of the Farm</h2>\n\n<p>The Central Experimental Farm dates from 1886. Its original mandate was to be the central or focal point for a Canada-wide system of experimental farms to help resolve farm production questions. From an initial 5\u00a0sites, this national network has grown into a partnership of\u00a019 and now includes multiple locations, sub stations and field sites in every Canadian province.</p>\n\n<p>The Central Experimental Farm cropping fields and plot areas have played an essential part in many research and development successes. These include new cereal grains such as wheat varieties which have proven to be highly adapted to Canadian soils and weather. These varieties have demonstrated their value as they helped open up the Canadian prairies to agricultural production during the 20th century.</p>\n\n<p>The <a href=\"/eng/?id=1169814977713\">Central Experimental Farm Advisory Council</a> was established in 1999 to ensure public participation in the Farm\u2019s management and to help ensure the continuing historic site designation. The <a href=\"/eng/?id=1170695386778\">Central Experimental Farm National Historic Site Management Plan</a> provides the Farm's long-term management framework to balance the Farm's dual role as National Historic Site and an active research centre though the <a href=\"/eng/?id=1180546650582\">Ottawa Research and Development Centre</a>.</p>\n\n<p>The Central Experimental Farm has housed many diverse research programs including:</p>\n\n<ul>\n<li>soils and Canadian land inventory;</li> \n<li>food and dairy products processing technology;</li> \n<li>horticulture and ornamental plant breeding;</li> \n<li>agriculture engineering and farm mechanical systems;</li> \n<li>animal and poultry breeding and production;</li> \n<li>agricultural and forest insect identification and control methods;</li> \n<li>agricultural chemistry analysis methodology;</li> \n<li>plant and animal pathology;</li> \n<li>bacteriology and plant health;</li> \n<li>cereal and forage crop production utilization;</li> \n<li>tobacco;</li> \n<li>bee research.</li>\n</ul>\n \n<p>Frequently the research subject ranged from the whole organism down to the cellular or more recently the molecular level.</p>\n\n<h2>On the Farm</h2>\n\n<p>As an active research centre, the Central Experimental Farm is home to the Canadian National Collection of Insects, Arachnids and Nematodes and field plots devoted to plant breeding and agronomy studies.</p>\n\n<p><a href=\"/eng/?id=1169817907211#major\">Major attractions</a> on the Farm include the Arboretum and the adjacent Ornamental Gardens, the Tropical Greenhouse.</p>  \n\n<p>The <a href=\"https://ingeniumcanada.org/agriculture/index.php\">Canada Agriculture and Food Museum</a> is a tourist destination on the Central Experimental Farm. Visit the museum to see exhibitions, artifacts and collections, learn about Canada\u2019s scientific and technological heritage and experience the petting zoo and Farm.</p>\n\n<p>The <a href=\"https://ofnc.ca/programs/fletcher-wildlife-garden\">Fletcher Wildlife Garden - The Ottawa Field-Naturalists' Club</a> and <a href=\"http://www.friendsofthefarm.ca/\">Friends of the Farm</a> are two organizations who use the grounds and facilities of the Central Experimental Farm for public outreach programs.</p> \n\n<p>Into its second century, the Central Experimental Farm continues its scientific focus as the location for laboratories and research plots.  More than an active research centre and beautiful surroundings, the Central Experimental Farm is a National Historic Site and a taste of rural Canada.</p>\n\r\n    <!-- Fin du contenu -->",
        "fr": "\r\n    \r\n    <!-- MainContentStart -->\r\n    <!-- D&eacute;but du contenu -->\r\n    <div class=\"col-md-4 pull-right\" style=\"z-index: 1000\">\n<section class=\"panel panel-default\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Liens connexes</h2>\n</header>\n<div class=\"panel-body\"><ul class=\"wet-boew-zebra\">\n<li><a href=\"/fra/?id=1232732638116\">Galerie de photos</a></li>\n<li><a href=\"/fra/?id=1180546650582\">Centre de recherche et de d\u00e9veloppement d\u2019Ottawa</a></li>\n<li><a href=\"/fra/?id=1169814977713\">Conseil consultatif de la Ferme exp\u00e9rimentale centrale</a></li>\n<li><a href=\"/fra/?id=1170695386778\">Plan directeur du lieu historique national de la Ferme exp\u00e9rimentale centrale</a></li>\n<li><a href=\"/fra/?id=1176399349495\">Cartes de la Ferme exp\u00e9rimentale centrale</a></li>\n</ul></div>\n</section>\n</div><p>La Ferme exp\u00e9rimentale centrale s'\u00e9tend sur un espace vert de 427\u00a0hectares (1\u00a0055\u00a0acres) au c\u0153ur d'Ottawa, la capitale du Canada et jouxte le canal Rideau, lieu historique.</p>\n\n<h2>Histoire de la Ferme</h2>\n\n<p>La Ferme exp\u00e9rimentale centrale a \u00e9t\u00e9 \u00e9tablie en 1886. Au d\u00e9part, sa mission consistait \u00e0 servir de centre de liaison pour un vaste r\u00e9seau de fermes exp\u00e9rimentales diss\u00e9min\u00e9es partout au pays afin d'aider \u00e0 r\u00e9gler des questions touchant la production agricole. Alors qu'\u00e0 l\u2019origine ce r\u00e9seau national ne comptait que cinq fermes, il s'est \u00e9largi pour r\u00e9unir 19\u00a0partenaires. Il comprend aujourd'hui de nombreux centres, sous-stations et champs dans chacune des provinces canadiennes.</p>\n\n<p>Les champs de culture et les parcelles d'essai de la Ferme exp\u00e9rimentale centrale ont jou\u00e9 un r\u00f4le de premier plan dans bon nombre de recherches couronn\u00e9es de succ\u00e8s. Parmi celles-ci, mentionnons la conception de nouvelles c\u00e9r\u00e9ales, notamment des vari\u00e9t\u00e9s de bl\u00e9, qui ont d\u00e9montr\u00e9 une excellente capacit\u00e9 d'adaptation aux sols et au climat du Canada. Ces vari\u00e9t\u00e9s ont prouv\u00e9 leur valeur en contribuant \u00e0 ouvrir les prairies canadiennes \u00e0 la production agricole au cours du XXe si\u00e8cle.</p>\n\n<p>Le <a href=\"/fra/?id=1169814977713\">Conseil consultatif de la Ferme exp\u00e9rimentale centrale</a> a \u00e9t\u00e9 mis sur pied en 1999 pour aider \u00e0 conserver sa d\u00e9signation comme lieu historique et pour assurer la participation du public \u00e0 sa gestion. Le <a href=\"/fra/?id=1170695386778\">Plan directeur du lieu historique national de la Ferme exp\u00e9rimentale centrale</a> , qui comprend un cadre de gestion \u00e0 long terme, a \u00e9t\u00e9 con\u00e7u pour \u00e9quilibrer les deux r\u00f4les conf\u00e9r\u00e9s \u00e0 la Ferme, celui de lieu historique national et celui de milieu de recherche dynamique par le biais du <a href=\"/fra/?id=1180546650582\">Centre de recherche et de d\u00e9veloppement d\u2019Ottawa</a>.</p>\n\n<p>La Ferme exp\u00e9rimentale centrale a permis la mise en \u0153uvre de programmes de recherche diversifi\u00e9s, notamment dans les domaines suivants\u00a0:</p>\n\n<ul>\n<li>inventaire des sols et des terres canadiens;</li> \n<li>technologies de transformation d'aliments et de produits laitiers;</li> \n<li>s\u00e9lection de plantes horticoles et ornementales;</li> \n<li>g\u00e9nie agricole et instruments de m\u00e9canisation agricoles;</li> \n<li>s\u00e9lection et production d\u2019animaux, notamment la volaille;</li> \n<li>m\u00e9thodes d'identification et de contr\u00f4le des insectes nuisibles en agriculture et en foresterie;</li> \n<li>m\u00e9thodes d'analyse bas\u00e9es sur la chimie agricole;</li> \n<li>pathologie des v\u00e9g\u00e9taux et des animaux;</li> \n<li>bact\u00e9riologie et sant\u00e9 des plantes;</li> \n<li>production et exploitation de c\u00e9r\u00e9ales et de plantes fourrag\u00e8res;</li> \n<li>tabac;</li> \n<li>recherche sur les abeilles.</li>\n</ul>\n \n<p>Les th\u00e8mes de la recherche ont constamment vari\u00e9, passant de l'\u00e9chelle de l'organisme \u00e0 celle de la cellule, voire, plus r\u00e9cemment, \u00e0 celle de la mol\u00e9cule.</p>\n\n<h2>Sur la Ferme</h2>\n\n<p>\u00c0 titre de centre de recherche dynamique, la Ferme exp\u00e9rimentale centrale h\u00e9berge la Collection nationale canadienne d\u2019insectes, d\u2019arachnides et de n\u00e9matodes ainsi que des parcelles de terrain d\u00e9di\u00e9es \u00e0 des \u00e9tudes sur la s\u00e9lection v\u00e9g\u00e9tale et l\u2019agronomie. </p>\n\n<p>Les <a href=\"/fra/?id=1169817907211#principaux\">principaux points d\u2019int\u00e9r\u00eat</a> sur la Ferme comprennent l\u2019Arboretum et les Jardins ornementaux adjacents, ainsi que la Serre tropicale.</p>  \n\n<p>Le <a href=\"http://museeaac.techno-science.ca/fr/accueil.php\">Mus\u00e9e de l\u2019agriculture et de l\u2019alimentation du Canada</a> est une destination touristique de la Ferme exp\u00e9rimentale centrale. Visitez le Mus\u00e9e et ses expositions, d\u00e9couvrez ses artefacts et ses collections, informez-vous sur le patrimoine scientifique et technologique du Canada et vivez l\u2019exp\u00e9rience de la ferme et du zoo pour les enfants.</p>\n\n<p>Le <a href=\"https://ofnc.ca/programs/fletcher-wildlife-garden\">Jardin \u00e9cologique Fletcher - The Ottawa Field-Naturalists' Club (en anglais seulement)</a> et les <a href=\"http://www.friendsofthefarm.ca/\">Amis de la ferme (en anglais seulement)</a> sont deux organisations qui pr\u00e9sentent des programmes de sensibilisation du public en utilisant les terrains et les installations de la Ferme.</p> \n\n<p>Au d\u00e9but de son deuxi\u00e8me si\u00e8cle d\u2019existence, la Ferme exp\u00e9rimentale centrale poursuit son mandat scientifique comme place privil\u00e9gi\u00e9e h\u00e9bergeant des laboratoires et des parcelles de recherche. Bien plus qu\u2019un centre de recherche dynamique et un environnement magnifique, la Ferme exp\u00e9rimental centrale constitue un lieu historique national et offre une exp\u00e9rience du milieu rural du Canada.</p>\n\r\n    <!-- Fin du contenu -->\r\n\t\r\n\t\r\n\t\r\n  "
    },
    "css": {
        "fr": null
    },
    "js": {
        "en": null,
        "fr": null
    }
}